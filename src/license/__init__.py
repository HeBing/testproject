#! /usr/bin/env python3
# -*- coding:utf-8 -*-


import os
import copy
from enum import Enum
import json
import time
import pickle
import base64
import hashlib
from datetime import datetime
import pytz
from collections import (
    Callable,
    Iterable,
    OrderedDict,
)
from functools import (
    partial,
    reduce,
)
from itertools import (
    compress,
    takewhile,
)
from operator import (
    add,
    and_,
    attrgetter,
    concat,
    eq,
    itemgetter,
    methodcaller,
    ne,
    is_,
    is_not,
    setitem,
    sub,
    truth,
)
from darwinutils.crypt import (
    loads_public_key,
    sign_with_private_key,
    verify_with_public_key,
)
from darwinutils.fn import (
    f_call,
    f_compose_l,
    f_compose_r,
    f_echo,
    f_flip_call,
    f_group,
    f_list_rize,
    f_si_call,
    f_star_call,
    f_unstar_call,
    f_zip,
    i_batch,
)


__all__ = [
    "DarwinLicense",
    "DarwinLicenseGroup",
]


DARWIN_DEFAULT_OUTPUT_TZ = "Asia/Shanghai"
DARWIN_DEFAULT_SIGNATURE_HASH_ALGORITHM = "sha1"
DARWIN_DEFAULT_PUB_KEY_S_test = \
    "-----BEGIN PUBLIC KEY-----\n" \
    "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDChiLI8oMEhtcx7ama/WXbdPI0\n" \
    "NIJbOGU+/AOJP9asD9CgMGP28+cXR0Zi8q2Ty9c3uhV+OkvrsBN2AaWOZQmQf+NT\n" \
    "K+CKtkB8pGGyQZwoOQJ4fpliufCyEliBStHlbTp8zAniW30+LhoiPNLFRCdX8okG\n" \
    "3G1RQPS36/GgT9RA6wIDAQAB\n" \
    "-----END PUBLIC KEY-----"
# TODO: replaced with a formal
DARWIN_DEFAULT_PUB_KEY_S = \
    "-----BEGIN PUBLIC KEY-----\n" \
    "MIICIjANBgkqhkiG9w0BAQEFAAOCAg8AMIICCgKCAgEAyTvJKTljm8zjHAnIXlV2\n" \
    "ACiv/7Yqz6Uap9sIgbvvyiqbChvao/TAmja4RaWn3xJmTR+dd3bv3O+zvh8JoypT\n" \
    "POQDCs5nTBs0Q1CCNHd0jfKNMa1iW64pnRKvkSmJpRegedfAXsxNmZOFmVFhE9uW\n" \
    "PfzJQXwEP+et1uLn9WUhtlkECVoZRdn/RaqaNvu0AN7OXNzcKRpeVdoUOH8akhmr\n" \
    "8i/+41elZejcA6u0M8hILFnmoaocwUFctnHMv7raBX/t9/YMOpSl/LXhNao+8DZW\n" \
    "xFFcTXOFJPz48W4Cl3WnF4gbXb3NLowBchgTuXpd1+6q/efag9rZdSNDTXQlQIID\n" \
    "DnNBR03npowhhmHTWJ1RDsC9QmvouJvlp2es/tDOI0WxBaMVyxF/OENxwIY/dhFA\n" \
    "MeZcZ+fdRTnTVmWeg9BUyo5wF1DYOC3wmfB/JsPDcEpVYk1Dw++ifFjA9vEkwzLH\n" \
    "svTpXSU6jgSfaxgFerJ/H+Rr7tg2fB6FJW/ivRyvQUNnoVuiWtYN/Xw3cIKonorW\n" \
    "CxKp5bbOOuAGv9fB/1oXP5H4rBDMg7SRVx+/7cQrY7LL9kBA5r70KrnN705gEbqE\n" \
    "cxd19hZRsHfQQOBhKJdMgXpEsj5tFhPU8JsO1G+oaFaU1KN6BBoQf1KMVhedgGgp\n" \
    "vp47pb08ML1PaUG5V9pjWQMCAwEAAQ==\n" \
    "-----END PUBLIC KEY-----"
DARWIN_DEFAULT_PUB_KEY = loads_public_key(DARWIN_DEFAULT_PUB_KEY_S)

"""
Convert signature string to a list string so that it's convenient to read and record
:param sigature: raw signature string
:return: a list of string which splitted from raw signature with 80 char width
"""
f_signature_str2lst = f_compose_r(
    partial(i_batch, batch=80),
    partial(map, partial(str.join, "")),
    # use list instead of tuple to make sure de-serialized value is consistent
    list,
)
"""
Convert readable signature string list back to its raw string
:param sig_list: 80 char splited signature list
:return: a single signature string
"""
f_signature_lst2str = partial(str.join, "")
"""
Get <attribute_name, attribute_value> tuple from object
:param obj: object to extract attribute
:param name: attribute name which needs to be extracted
:return a tuple of attribute name and value
:rtype: tuple([str, obj])
"""
f_getattr_pair = partial(f_unstar_call, f_compose_r(
    f_zip(
        f_compose_r(
            partial(partial, getattr),
            partial(f_group, f_echo),
            partial(f_compose_l, list),
        ),
        f_echo,
    ),
    list,
    partial(f_star_call, f_call),
))


def get_utc_timestamp():
    #return datetime.utcnow().timestamp()
    # TODO: https://bugs.python.org/issue12758, time.time() IS timezone-independent!?
    return time.time()


def convert_utc_timestamp_to_str(timestamp, tz=None):
    if tz is None:
        tz = pytz.timezone(DARWIN_DEFAULT_OUTPUT_TZ)
    return f_compose_r(
        datetime.fromtimestamp,
        methodcaller("astimezone", tz),
        methodcaller("strftime", "%Y-%m-%dT%H:%M:%S%z"),
    )(timestamp)


def convert_timestr_to_utc_stamp(timestr):
    return f_compose_r(
        partial(f_flip_call, datetime.strptime, "%Y-%m-%dT%H:%M:%S%z"),
        methodcaller("timestamp"),
    )(timestr)


class DarwinLicense:
    TYPE = Enum(
        "TYPE",
        (
            "server",
            "inference",
            "cpu_worker",
            "gpu_worker",
            "client",
            "web",
        ),
        qualname="DarwinLicense.TYPE",
    )

    def __init__(self):
        self._version = None
        self._type = None
        self._quantity = None
        self._expire = None
        self._eol = None
        self._recipient = None
        self._project = None
        self._signature = None

        # initialize cache attributes
        self._c_is_verified = None
        self._c_is_not_expired = None
        self._c_is_not_eol = None
        self._c_seed = None

    def __getstate__(self):
        state = self.__dict__.copy()

        # cache attributes should not be persistent
        del state["_c_is_verified"]
        del state["_c_is_not_expired"]
        del state["_c_is_not_eol"]
        del state["_c_seed"]

        # convert state dict to ordered state list
        return f_compose_r(
            methodcaller("items"),
            partial(f_flip_call, sorted, key=itemgetter(0)),
        )(state)

    def __setstate__(self, state):
        # convert from ordered state list to dict
        state = dict(state)

        # apply incoming state
        self.__dict__.update(state)

        # reset cache attributes
        self._c_is_verified = None
        self._c_is_not_expired = None
        self._c_is_not_eol = None
        self._c_seed = None

    def to_dict(self):
        f_split_sig = f_compose_r(
            attrgetter("signature"),
            lambda x: f_signature_str2lst(x) if x else None,
        )
        return f_compose_r(
            # remove "empty" fields
            partial(filter, f_compose_r(
                itemgetter(1),
                partial(f_flip_call, is_not, None),
            )),
            list,
            # dictorize
            OrderedDict,
        )([
            *f_compose_r(
                partial(map, partial(f_getattr_pair, self)),
                list,
            )([
                "project",
                "version",
                "type_",
                "quantity",
                "recipient",
            ]),
            [
                "expire",
                # NOTE: this conversion use default tz of the function
                convert_utc_timestamp_to_str(self.expire),
            ],
            [
                "eol",
                # NOTE: this conversion use default tz of the function
                convert_utc_timestamp_to_str(self.eol),
            ],
            [
                "signature",
                f_split_sig(self),
            ],
        ])

    @classmethod
    def from_dict(cls, value):
        # create an empty license
        lic_s = DarwinLicenseShadow()

        # dup input value
        value = copy.deepcopy(value)

        # update specific field
        if "signature" in value:
            value["signature"] = f_signature_lst2str(value["signature"])
        if "eol" in value and isinstance(value["eol"], str):
            value["eol"] = convert_timestr_to_utc_stamp(value["eol"])
        if "expire" in value and isinstance(value["expire"], str):
            value["expire"] = convert_timestr_to_utc_stamp(value["expire"])

        # fill in the license with input attributes
        _ = f_compose_r(
            methodcaller("items"),
            partial(map, partial(f_star_call, partial(setattr, lic_s))),
            list,
        )(value)
        return lic_s.export()

    def __repr__(self):
        return f_compose_r(
            methodcaller("to_dict"),
            partial(json.dumps, indent=4, ensure_ascii=False),
        )(self)

    """
    def __str__(self):
        pass
    """

    @property
    def version(self):
        return self._version

    @property
    def type_(self):
        return self._type

    @property
    def quantity(self):
        return self._quantity

    @property
    def expire(self):
        return self._expire

    @property
    def eol(self):
        return self._eol

    @property
    def project(self):
        return self._project

    @property
    def signature(self):
        return self._signature

    @property
    def recipient(self):
        return self._recipient

    @property
    def _seed(self):
        if self._c_seed is None:
            self._c_seed = f_compose_r(
                methodcaller("__getstate__"),
                partial(filter, f_compose_r(
                    itemgetter(0),
                    partial(ne, "_signature")
                )),
                tuple,
                pickle.dumps,
                hashlib.sha1,
                methodcaller("digest"),
            )(self)
        return self._c_seed

    @property
    def default_public_key(self):
        return DARWIN_DEFAULT_PUB_KEY

    @property
    def default_recipient_verify_func(self):
        # default to True always
        return partial(f_si_call, partial(f_echo, True))

    @property
    def is_verified(self):
        if self._c_is_verified is None:
            self._c_is_verified = self.verify()
        return self._c_is_verified

    @property
    def is_not_expired(self):
        if self._c_is_not_expired is None or self._c_is_not_expired == True:
            self._c_is_not_expired = get_utc_timestamp() < self.expire
        return self._c_is_not_expired

    @property
    def is_not_eol(self):
        if self._c_is_not_eol is None or self._c_is_not_eol == True:
            self._c_is_not_eol = get_utc_timestamp() < self.eol
        return self._c_is_not_eol

    def clone(self):
        return pickle.loads(pickle.dumps(self))

    def verify_signature(self, pub_key=None, *args, **kwargs):
        if pub_key is None:
            pub_key = self.default_public_key

        try:
            r = f_compose_r(
                attrgetter(
                    "_seed",
                    "signature",
                ),
                # NOTE: keep msg->sig order to cooperate verify(...) function
                f_zip(
                    # msg/seed
                    f_echo,
                    # signature
                    f_compose_r(
                        methodcaller("encode", "ascii"),
                        base64.urlsafe_b64decode,
                    ),
                ),
                list,
                # verify
                partial(f_star_call, partial(
                    f_flip_call, partial(verify_with_public_key, pub_key), DARWIN_DEFAULT_SIGNATURE_HASH_ALGORITHM,
                )),
            )(self)
            r = r is None
        except Exception as _e:
            r = False
        return r

    def verify_recipient(self, v_func=None, *args, **kwargs):
        if v_func is None:
            v_func = self.default_recipient_verify_func

        if isinstance(v_func, Callable):
            r = v_func(self.recipient)
        else:
            r = False
        return r

    def verify(self, *args, **kwargs):
        verify_functions = [
            methodcaller("verify_signature", *args, **kwargs),
            methodcaller("verify_recipient", *args, **kwargs),
        ]
        return f_compose_r(
            f_group(*verify_functions),
            partial(takewhile, truth),
            list,
            len,
            partial(eq, len(verify_functions)),
        )(self)


class DarwinLicenseShadow(DarwinLicense):
    def export(self):
        lic = DarwinLicense()
        lic.__setstate__(self.__getstate__())
        return lic

    def __init__(self, lic=None):
        super().__init__()
        if lic is not None:
            self.__setstate__(lic.__getstate__())

    @DarwinLicense.version.setter
    def version(self, value):
        self._version = value

    @DarwinLicense.type_.setter
    def type_(self, value):
        if isinstance(value, str):
            value = DarwinLicense.TYPE[value].name
        elif isinstance(value, DarwinLicense.TYPE):
            # TODO: cython had problem to pickle class enum, use its name for now.
            value = value.name
        else:
            raise TypeError("\"type\" of DarwinLicense should be {} instead of {}".format(
                DarwinLicense.TYPE.__name__, type(value).__name__,
            ))
        self._type = value

    @DarwinLicense.quantity.setter
    def quantity(self, value):
        if isinstance(value, str):
            value = int(value)
        self._quantity = value

    @DarwinLicense.expire.setter
    def expire(self, value):
        """
        Set license's "expire" time
        NOTE: we assume local time if str input which imply a local2utc timestamp convertion.
              we assume utc time if float input. TODO: is that reasonable?
        :param value:
        :return:
        """
        if isinstance(value, str):
            value = f_compose_r(
                convert_timestr_to_utc_stamp,
            )(value)

        elif not isinstance(value, float):
            raise TypeError("\"expire\" of DarwinLicense should be {} instead of {}".format(
                float.__name__, type(value).__name__,
            ))
        self._expire = value
        self._c_is_not_expired = None

    @DarwinLicense.eol.setter
    def eol(self, value):
        """
        Set license's "eol" time
        NOTE: we assume local time if str input which imply a local2utc timestamp convertion.
              we assume utc time if float input. TODO: is that reasonable?
        :param value:
        :return:
        """
        if isinstance(value, str):
            value = f_compose_r(
                convert_timestr_to_utc_stamp,
            )(value)

        elif not isinstance(value, float):
            raise TypeError("\"eol\" of DarwinLicense should be {} instead of {}".format(
                float.__name__, type(value).__name__,
            ))
        self._eol = value
        self._c_is_not_eol = None

    @DarwinLicense.project.setter
    def project(self, value):
        self._project = value

    @DarwinLicense.signature.setter
    def signature(self, value):
        if isinstance(value, Iterable):
            value = f_signature_lst2str(value)
        elif not isinstance(value, str):
            raise TypeError("\"signature\" of DarwinLicense should be {} instead of {}".format(
                str.__name__, type(value).__name__,
            ))
        self._signature = value
        self._c_is_verified = None

    @DarwinLicense.recipient.setter
    def recipient(self, value):
        self._recipient = value

    def sign(self, priv_key):
        return f_compose_r(
            attrgetter("_seed"),
            # sign
            partial(f_flip_call, partial(sign_with_private_key, priv_key), DARWIN_DEFAULT_SIGNATURE_HASH_ALGORITHM),
            # encode to readable string
            base64.urlsafe_b64encode,
            methodcaller("decode", "ascii"),
            # tee to "signature" attribute
            f_group(
                partial(setattr, self, "signature"),
                f_echo,
            ),
            list,
            itemgetter(1),
        )(self)


class DarwinLicenseGroup:
    def __init__(self, project=None):
        self._licenses = []
        self._project = project

    def __getstate__(self):
        state = self.__dict__.copy()
        return state

    def __setstate__(self, state):
        self.__dict__.update(state)

    def __len__(self):
        return len(self._licenses)

    def to_dict(self):
        return OrderedDict([
            *f_compose_r(
                partial(map, partial(f_getattr_pair, self)),
                list,
            )([
                "project",
            ]),
            [
                "licenses",
                f_compose_r(
                    attrgetter("licenses"),
                    partial(map, methodcaller("to_dict")),
                    list,
                )(self),
            ],
        ])

    @classmethod
    def from_dict(self, value):
        # create an empty license group
        lic_g = DarwinLicenseGroup()

        # dup input value
        value = copy.deepcopy(value)

        # handle specific encoded field
        value["licenses"] = f_compose_r(
            partial(map, DarwinLicense.from_dict),
            list,
        )(value["licenses"])

        # fill in the license group with input attributes
        _ = f_compose_r(
            methodcaller("items"),
            partial(map, partial(f_star_call, partial(setattr, lic_g))),
            list,
        )(value)
        return lic_g

    def __repr__(self):
        return "{}.from_dict({})".format(type(self).__name__, self.dumps())

    @classmethod
    def loads(cls, data, *args, **kwargs):
        state = json.loads(data, *args, **kwargs)
        return cls.from_dict(state)

    @classmethod
    def load(cls, fd, *args, **kwargs):
        if isinstance(fd, str):
            fd = open(fd, "r")
        with fd:
            state = fd.read()
        return cls.loads(state, *args, **kwargs)

    def dumps(self, *args, indent=4, ensure_ascii=False, **kwargs):
        state = self.to_dict()
        return json.dumps(state, *args, indent=indent, ensure_ascii=ensure_ascii, **kwargs)

    def dump(self, fd, *args, **kwargs):
        if isinstance(fd, str):
            fd = open(fd, "w")
        with fd:
            r = fd.write(self.dumps(*args, **kwargs))
        return r

    @property
    def project(self):
        return self._project

    @project.setter
    def project(self, value):
        self._project = value

    @property
    def licenses(self):
        return self._licenses

    @licenses.setter
    def licenses(self, value):
        self._licenses = value

    @property
    def is_verified(self):
        return tuple(map(attrgetter("is_verified"), self.licenses))

    @property
    def is_not_expired(self):
        return tuple(map(attrgetter("is_not_expired"), self.licenses))

    @property
    def is_not_eol(self):
        return tuple(map(attrgetter("is_not_eol"), self.licenses))

    @property
    def active_licenses(self):
        return f_compose_r(
            attrgetter("is_verified", "is_not_eol", "is_not_expired"),
            partial(f_star_call, zip),
            partial(map, all),
            partial(compress, self.licenses),
            tuple,
        )(self)

    def has_active_license(self, type_):
        if isinstance(type_, str):
            type_ = DarwinLicense.TYPE[type_].name
        elif isinstance(type_, DarwinLicense.TYPE):
            type_ = type_.name
        else:
            raise TypeError("Can only detect {} of license instead of {}".format(
                DarwinLicense.TYPE.__name__, type(type_).__name__,
            ))
        return f_compose_r(
            attrgetter("active_licenses"),
            partial(map, f_compose_r(
                attrgetter("type_"),
                partial(eq, type_),
            )),
            any,
        )(self)

    def clone(self):
        return type(self).from_dict(self.to_dict())

    def add_license(self, lic):
        self._licenses.append(lic)

    def del_license(self, lic):
        self._licenses.remove(lic)

    def verify(self, *args, **kwargs):
        return f_compose_r(
            attrgetter("licenses"),
            partial(map, methodcaller("verify", *args, **kwargs)),
            list,
        )(self)

    def sign(self, *args, **kwargs):
        _ = f_compose_r(
            attrgetter("licenses"),
            partial(map, f_compose_r(
                DarwinLicenseShadow,
                f_group(
                    methodcaller("sign", *args, **kwargs),
                    f_echo,
                ),
                list,
                itemgetter(1),
            )),
            list,
            partial(setattr, self, "licenses"),
        )(self)
