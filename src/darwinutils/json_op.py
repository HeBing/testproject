#! /usr/bin/env python3
# -*- coding: utf-8 -*-

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import os
import json
import copy
import numpy as np


class Json:
    """
    It is a common Json to file or file to Json class which be used by different components
    """
    @staticmethod
    def file_to_json(file_path):
        """
        Read a file and transfer to json format
        :param file_path: file full path
        :type file_path: string
        :return: json data from file
        :rtype: dict
        """
        if file_path is None:
            return None
        if os.path.exists(file_path) is False or os.path.isfile(file_path) is False:
            return None
        with open(file_path, 'rt') as f:
            data = f.read()
            try:
                json_data = json.loads(data)
            except json.decoder.JSONDecodeError as e:
                # Cannot use logger here since maybe recursive import
                #print("Decode {} failed. Reason: {}".format(file_path, str(e)))
                raise json.decoder.JSONDecodeError
        if json_data is not None:
            return json_data

    @classmethod
    def _remove_dict_comment(cls, d):
        if isinstance(d, dict):
            all_keys = list(d.keys())
            for key in all_keys:
                if isinstance(key, str) and key.startswith("#"):
                    # print("Remove {}".format(key))
                    del d[key]
                    continue

                # remove recursively
                val = d[key]
                if isinstance(val, dict):
                    d[key] = cls._remove_dict_comment(val)
            return d
        elif isinstance(d, list):
            d_lst = d
            for d in d_lst:
                all_keys = list(d.keys())
                for key in all_keys:
                    if isinstance(key, str) and key.startswith("#"):
                        # print("Remove {}".format(key))
                        del d[key]
                        continue

                    # remove recursively
                    val = d[key]
                    if isinstance(val, dict):
                        d[key] = cls._remove_dict_comment(val)
            return d_lst
        else:
            return d

    @classmethod
    def file_to_json_without_comments(cls, file_path):
        if file_path is None or not os.path.exists(file_path):
            return None
        _json = cls.file_to_json(file_path)
        if _json is None:
            return None
        return cls._remove_dict_comment(_json)

    @staticmethod
    def _check_json_validity(item_obj):
        if isinstance(item_obj, dict):
            for each_item in item_obj.items():
                item_obj[each_item[0]] = Json._check_json_validity(item_obj[each_item[0]])
        elif isinstance(item_obj, list):
            for idx, each_item in enumerate(item_obj):
                item_obj[idx] = Json._check_json_validity(each_item)
        elif isinstance(item_obj, np.ndarray):
            item_obj = list(item_obj)
        return item_obj

    @staticmethod
    def dict_to_file(file_path, content):
        """
        Save a json content to a file
        :param file_path: target file full path
        :type file_path: string
        :param content: json format content
        :type content: json
        :return: None
        :rtype:
        """
        if isinstance(content, dict):
            copy_content = copy.deepcopy(content)
            for each_item in copy_content.items():
                copy_content[each_item[0]] = Json._check_json_validity(copy_content[each_item[0]])
            with open(file_path, 'w') as f:
                f.write(json.dumps(copy_content, indent=2))
