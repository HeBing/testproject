# -*- coding: utf-8 -*-
"""
The file has been obsoleted. Change to ibis_utils.py
"""

from impala.dbapi import connect
from impala.util import as_pandas
from time import time
import traceback
from darwinutils.log import get_task_logger
logger = get_task_logger(__name__)

DEFAULT_IMPALA_CONFIG = {
    "host": "10.1.30.218",
    "port": 21050,
    "user": "impala",
    "passwd": "96cEj260C8Vs",
    "database": "dwsrc"
}


class ImpalaConnector(object):
    def __init__(self, host, port, user, passwd, database):
        self._host_name = host
        self._port = port
        self._user = user
        self._password = passwd
        self._database= database
        self._conn = None

    def connect(self):
        try:
            self._conn = connect(host=self._host_name, port=self._port, database=self._database, user=self._user,
                           password=self._password, auth_mechanism='PLAIN')
        except Exception as e:
            logger.error("Connect {}:{} for table {} by user {} failed with reason {}".format(self._host_name,
                                                                                               self._port,
                                                                                               self._user,
                                                                                               self._database,
                                                                                               str(e)))
            traceback.print_exc()
            return False

        return True

    def fetch(self, sql_str):
        try:
            cur = self._conn.cursor()
            cur.execute(sql_str)
        except Exception as e:
            logger.error("Execute {} failed with reason {}".format(sql_str, str(e)))
            return None
        return cur.fetchall()

    def fetch_as_df(self, sql_str):
        try:
            cur = self._conn.cursor()
            cur.execute(sql_str)
        except Exception as e:
            logger.error("Execute {} failed with reason {}".format(sql_str, str(e)))
            return None
        return as_pandas(cur)

