#! /usr/bin/env python3
# -*- coding:utf-8 -*-

from enum import Enum
import os
import json
import traceback
import numpy as np
import shutil
from threading import Event
import gevent
import collections
import inspect
from typing import (
    Callable,
    List,
    Iterable,
    Mapping,
    Sequence,
    Tuple,
    Union,
)
from functools import (
    lru_cache,
    partial,
    reduce,
)
from itertools import (
    chain,
    filterfalse,
    groupby,
    islice,
    tee,
    zip_longest,
)
from operator import (
    add,
    attrgetter,
    concat,
    contains,
    eq,
    getitem,
    is_not,
    itemgetter,
    methodcaller,
    setitem,
    sub,
)

from darwinutils.common import *
from darwinutils.fn import *

class EAConfigTemplateType(Enum):
    ml = 1
    dl = 2
    dlobj = 3


def iepoch(iterable, epoch=1):
    return chain.from_iterable(tee(iter(iterable), epoch))


def ibatch(iterable, batch=1, ignore_rest=False):
    it = iter(iterable)
    while True:
        try:
            seq = []
            for i in range(batch):
                seq.append(next(it))
            yield seq

        except StopIteration as e:
            if not ignore_rest and len(seq) > 0:
                yield seq
            break

def slice_data(data, data_range):
    """
    Return the slice of input data
    :param data:
    :type data: ndarray
    :param data_range: (begin, end), float means percentage, int means absolute position
    :type data_range: tuple
    :return:
    """
    import numpy as np
    assert isinstance(data_range, (tuple, list, np.ndarray)), \
        "data_range should be a list instead of {}".format(type(data_range))
    cnt = len(data)
    beg, end = data_range
    #logger.debug("bef: beg={}, end={}, cnt={}".format(beg, end, cnt))
    if isinstance(beg, (np.float, np.float32, np.float64)):
        beg = max(np.floor(cnt * beg).astype(np.int32), 0)
        end = min(np.ceil(cnt * end).astype(np.int32), cnt)
    elif isinstance(beg, (np.int, np.int32, np.int64)):
        beg = max(beg, 0)
        end = min(end, cnt)
    else:
        raise TypeError("data range could only be int or float instead of {}".format(type(beg)))

    assert 0 <= beg < end <= cnt, \
        "invalid data range {} in data which has {} records".format(data_range, cnt)
    #logger.debug("aft: beg={}, end={}".format(beg, end))

    return data[beg:end]


def percentage_range(num_partitions):
    from operator import itemgetter
    import numpy as np
    return zip(*itemgetter(slice(0, -1), slice(1, None))(np.linspace(0.0, 1.0, num_partitions+1).tolist()))


def is_iterable(obj):
    """
    Detect if an object is iterable or not
    :param obj:
    :return:
    """
    from collections import Iterable
    return isinstance(obj, Iterable)


def generate_mongo_condition(filter, key_map):
    import json
    return dict(map(lambda item: (key_map.get(item[0], item[0]), item[1]), json.loads(filter).items()))


def reduce_uuid(*uuids):
    import uuid
    f_gen_uuid = f_compose_r(
        partial(zip, [uuid.UUID, str]),
        partial(map, partial(f_star_call, f_call)),
        partial(f_star_call, uuid.uuid3),
        str,
    )
    f_reduce_uuid = partial(reduce, lambda x, y: f_gen_uuid([x, y]))
    return f_reduce_uuid(uuids)


def get_celery_task_state(task_id, service_name="darwin_service"):
    # NOTE: load celery env implicitly thouth it was not used directly.
    #from darwin_service import celery
    from darwinutils.config import DARWIN_CONFIG
    _ = load_func_from_spec(DARWIN_CONFIG.service_name, "celery_app")

    from celery.result import AsyncResult
    from operator import attrgetter
    return f_compose_r(
        AsyncResult,
        attrgetter("state"),
    )(task_id)


def get_free_celery_task_id(base_uuid, max_search_range=1000):
    from celery.states import PENDING, STARTED
    from darwinutils.log import get_task_logger
    logger = get_task_logger(__name__)

    for i in range(max_search_range):
        if i == 0:
            curr_uuid = base_uuid
        else:
            curr_uuid = reduce_uuid(base_uuid, str(i))
        state = get_celery_task_state(curr_uuid)
        if state == PENDING:
            logger.debug("Found free celery task id {} for uuid space {} in slot {}".format(
                curr_uuid, base_uuid, i,
            ))
            break
        elif state == STARTED:
            logger.debug("Found running celery task id {} for uuid space {} in slot {}".format(
                curr_uuid, base_uuid, i,
            ))
            break
    return curr_uuid, state


def load_func_from_spec(from_, import_):
    """
    Load function definition from module
    :param func_module_spec: a function definition
    :type func_module_spec: Union(PLFuncModuleSpec, PLFuncModuleSpec compatible iterable)
    :return: a function object
    :rtype: Union(Callable, Exception)
    """
    import importlib
    from darwinutils.log import get_task_logger
    logger = get_task_logger(__name__)

    from operator import attrgetter
    try:
        module = importlib.import_module(from_)
        func = attrgetter(import_)(module)

    except Exception as e:
        logger.exception("Fail to load function from module")
        func = e

    return func


def inspect_func(func, *args, **kwargs):
    """
    Inspect the function's argument spec
    :param func: a function or its definition
    :type func: Union(PLFuncModuleSpec, PLFuncModuleSpec compatible iterable, Callable)
    :return: Signature object, or an exception object
    """
    from darwinutils.log import get_task_logger
    logger = get_task_logger(__name__)

    if callable(func):
        try:
            spec = inspect.signature(partial(func, *args, **kwargs))
        except Exception as e:
            logger.exception("Fail to inspect function of {}".format(func))
            spec = e

    elif not isinstance(func, Exception):
        spec = ValueError("Fail to load function with incorrect type of return {}".format(
            type(func).__name__
        ))
    return spec


def posix_touch(f_path):
    # touch the lock file
    from pathlib import Path
    Path(f_path).touch()


def is_process_alive(pid):
    import os
    from darwinutils.log import get_task_logger
    logger = get_task_logger(__name__)

    try:
        os.kill(pid, 0)
        result = True
    except PermissionError:
        result = True
    except ProcessLookupError:
        logger.warning('Process {} does not exist'.format(pid))
        result = False
    return result


def __run_cmds(cmds, log_func=print):
    import subprocess

    lines = ''
    try:
        lines = subprocess.check_output(cmds, shell=False)
    except OSError as e:
        log_func("\"{}\" failed with following error:\n{}".format(
            ' '.join(cmds),
            '\n'.join(map(lambda line: ">> "+line, str(e).split('\n')))))
    except subprocess.CalledProcessError as e:
        log_func("\"{}\" failed with rc equals to %d and following error message:\n{}".format(
            ' '.join(cmds),
            e.returncode, map(lambda line: ">> "+line, e.output)))
    # NOTE: convert byte-string to regular string by enforcing utf-8 code.
    return tuple(map(lambda _: str(_[4:], 'utf-8'), lines.splitlines()))


@lru_cache(maxsize=1)
def query_num_nvidia_gpus(log_func=print):
    import glob
    try:
        r = glob.glob("/dev/nvidia[0-9]*")
    except Exception as _e:
        log_func("\"{}\" failed with following error:\n{}".format(
            "query_num_nvidia_gpus",
            '\n'.join(map(lambda line: ">> "+line, str(_e).split('\n')))))
        r = []
    num_gpus = len(r)
    return num_gpus


@lru_cache(maxsize=1)
def query_nvidia_gpus(log_func=print):
    return __run_cmds(['nvidia-smi', '--format=csv,noheader', '--query-gpu=gpu_uuid'], log_func=log_func)


def query_nvidia_compute_apps(log_func=print):
    return __run_cmds(['nvidia-smi', '--format=csv,noheader', '--query-compute-apps=gpu_uuid,pid'], log_func=log_func)


def gpu_balance_no_lock(num_gpus=1, app_pid=0):
    import os
    from darwinutils.log import get_task_logger
    logger = get_task_logger(__name__)

    if app_pid == 0:
        app_pid = os.getpid()

    from functools import (
        partial,
    )

    def read_text_file(fpath):
        with open(fpath, 'r') as f:
            return f.readlines()

    f_reg_path = os.path.join('/tmp', '.darwin_gpu_balance_registration.{}_{}.txt'.format(
        os.environ.get('USER'),
        os.getuid(),
    ))
    posix_touch(f_reg_path)
    cmds = [
        partial(query_nvidia_gpus, logger.error),
        partial(query_nvidia_compute_apps, logger.error),
        partial(read_text_file, f_reg_path),
    ]
    from darwinutils.fn import (
        f_call,
    )
    gpu_lines, app_lines, reg_lines = tuple(map(f_call, cmds))

    gpu_uuids = gpu_lines
    logger.debug({'gpu_uuids': gpu_uuids})

    from itertools import (groupby, chain)
    from operator import (itemgetter, contains)
    """
    [['gpu1-uuid', 'pid1'],
     ['gpu1-uuid', 'pid2'],
     ['gpu2-uuid', 'pid3'],
     ...
    ]
    """
    gpu_apps = tuple(map(
        lambda line: tuple(map(lambda item: item.strip(), line.split(','))),
        chain(app_lines, reg_lines)
    ))
    logger.debug({'raw_gpu_apps': gpu_apps})
    from functools import partial
    from darwinutils.fn import f_compose_r
    gpu_apps = f_compose_r(
        partial(
            filter,
            f_compose_r(
                itemgetter(1),
                int,
                lambda pid: is_process_alive(pid) and app_pid != pid,
            )
        ),
        tuple
    )(gpu_apps)
    logger.debug({'live_gpu_apps_other_than_self': gpu_apps})
    """
    group by pid,
    {'gpu1-uuid': (pid1, pid2),
     'gpu2-uuid': (pid3),
     ...
    }
    """
    key_f = itemgetter(0)
    gpu_apps = dict(map(
        lambda item: (item[0], set(map(int, tuple(zip(*item[1]))[1]))),
        groupby(sorted(gpu_apps, key=key_f), key=key_f)
    ))
    logger.debug({'grouped_gpu_apps': gpu_apps})

    """ append total idle gpu entries"""
    gpu_apps = dict(chain(
        gpu_apps.items(),
        map(lambda uuid: (uuid, set([])), filter(lambda _: _ not in gpu_apps, gpu_uuids))
    ))
    logger.debug({'complete_gpu_apps': gpu_apps})

    """
    app count
    [('gpu1-uuid', n_small1),
     ('gpu2-uuid', n_medium1),
     ('gpu3-uuid', n_medium2),
     ...
    ]
    """
    gpu_app_cnts = tuple(sorted(map(lambda item: (item[0], len(item[1])), gpu_apps.items()), key=itemgetter(1)))
    logger.debug({'gpu_app_cnts': gpu_app_cnts})

    """ 'gpu1-uuid' """
    import random
    if num_gpus < 0:
        num_gpus = len(gpu_uuids)
    elif num_gpus > 0:
        num_gpus = min(num_gpus, len(gpu_uuids))
    top_idle_gpu_uuids = tuple(zip(*gpu_app_cnts))[0][:num_gpus]
    logger.debug({'top_idle_gpu_uuids': top_idle_gpu_uuids})

    """ register app and refreshing updated old apps """
    _ = list(map(lambda uuid: gpu_apps[uuid].add(app_pid), top_idle_gpu_uuids))
    logger.debug({'registered_gpu_apps': gpu_apps})

    from functools import reduce
    from operator import add
    lines = tuple(reduce(add, map(lambda item: tuple(map(lambda pid: '{},{}\n'.format(item[0], pid), item[1])), gpu_apps.items())))

    with open(f_reg_path, 'w') as fd:
        fd.writelines(lines)

    # manipulate CUDA_VISIBLE_DEVICES list
    CUDA_VISIBLE_DEVICES=','.join(map(lambda uuid: 'GPU-{}'.format(uuid), top_idle_gpu_uuids))
    os.environ["CUDA_VISIBLE_DEVICES"] = CUDA_VISIBLE_DEVICES
    logger.debug({'CUDA_VISIBLE_DEVICES': CUDA_VISIBLE_DEVICES})
    return top_idle_gpu_uuids, tuple(zip(*gpu_app_cnts))[0][num_gpus:]


@wraps(gpu_balance_no_lock)
def gpu_balance(*args, lock=True, **kwargs):
    """
    balance GPU to CUDA_VISIBLE_DEVICES based on workload
    :param num_gpus: number of visible GPUs to be exported, 0 means all
    :param app_pid: application process id which need to be charged to the specific GPUs
    :param logger:
    :return:
    """
    from darwinutils.log import get_task_logger
    logger = get_task_logger(__name__)

    if lock:
        import os
        f = f_lock_mutex(
            "darwin_f_lock_mutex.{}_{}.gpu_balance.ufahra3ke7aachuyohdeiy3oowei4yoo".format(
                os.environ["USER"],
                os.getuid(),
            ),
            timeout=-1, interval=[0.5, 2.0],
            printFunctor=logger.debug,
        )(gpu_balance_no_lock)
    else:
        f = gpu_balance_no_lock
    return f(*args, **kwargs)


def apply_cpu_affinity(cpus: Iterable[int], pid: int = None):
    """
    Apply cpu affinity request to target process
    # TODO: API only support Linux for now
    :param cpus:
    :param pid:
    :return:
    """
    if pid is None:
        pid = os.getpid()

    if hasattr(os, "sched_setaffinity"):
        _ = os.sched_setaffinity(pid, cpus)
        r = os.sched_getaffinity(pid)
    else:
        # TODO: do not raise error in unsupported OS, such as osx, for now
        # TODO: we assume it was "succ" because osx is only for testing
        r = set(cpus)

    return r


def cpu_balance_no_lock(num_cpus=1, app_pid=0, need_core=False, apply=True):
    from darwinutils.log import get_task_logger
    logger = get_task_logger(__name__)

    if app_pid == 0:
        app_pid = os.getpid()

    def read_text_file(fpath):
        with open(fpath, 'r') as f:
            return f.readlines()

    f_reg_path = os.path.join('/tmp', '.darwin_cpu_balance_registration.{}_{}.txt'.format(
        os.environ.get('USER'),
        os.getuid(),
    ))
    posix_touch(f_reg_path)

    # STEP_1: get resource pool
    resources = os.sched_getaffinity(1)
    resource_map = dict(zip(map(str, resources), resources))
    # log for debug
    _ = list(map(lambda _: logger.debug("resource_map[{}] = {}".format(_[0], _[1])), enumerate(resource_map.items())))

    # STEP_2: get current allocation
    resource_alloc_empty = dict(zip(resource_map.keys(), map(lambda _: list(), range(len(resource_map)))))
    lines = read_text_file(f_reg_path)
    # log for debug
    _ = list(map(lambda _: logger.debug("lines[{}] = {}".format(_[0], _[1])), enumerate(lines)))

    resource_alloc_items = f_compose_r(
        partial(map, f_compose_r(
            # read_text output still has LF, remove it.
            methodcaller("replace", "\n", ""),
            # entry is a comma separated list with the 1st field is the resource id, the other fields are consumer.
            methodcaller("split", ","),
            f_group(
                # resource id
                itemgetter(0),
                # valid resource alloc
                f_compose_r(
                    itemgetter(slice(1, None)),
                    # remove empty items
                    partial(filter, f_echo),
                    # convert from str to int
                    partial(map, int),
                    # remove in-active consumer
                    partial(filter, is_process_alive),
                    list,
                ),
            ),
            list,
        )),
        dict,
        # in case the registry does not have all resources, we append empty list for them.
        partial(f_flip_call, collections.ChainMap, resource_alloc_empty),
        dict,
        # re-convert to k-v pairs so that they could be sorted before selecting/allocating.
        methodcaller("items"),
        # it's safe to pick the candidate from order list because allocations are protected by lock and sync.
        partial(sorted, key=f_compose_r(itemgetter(1), len)),
    )(lines)
    # log for debug
    _ = list(map(lambda _: logger.debug("resource_alloc.bef[{}] = {}".format(_[0], _[1])), enumerate(resource_alloc_items)))

    # initial resource allocation w/o app registered.
    resource_alloc = dict(resource_alloc_items)
    # all resources which could be reserved
    resource_pool = f_compose_r(
        partial(map, itemgetter(0)),
        list,
    )(resource_alloc_items)
    # log for debug
    logger.debug("resource_pool={}".format(repr(resource_pool)))

    # function to exclude a set of resources while still keep the order of resource
    f_resource_pool_exclude = f_compose_r(
        partial(partial, contains),
        partial(f_flip_call, filterfalse, resource_pool),
        list,
    )

    # resources which were already allocated to target app
    resource_allocated = f_compose_r(
        partial(filter, f_compose_r(
            itemgetter(1),
            partial(f_flip_call, contains, app_pid),
        )),
        partial(map, itemgetter(0)),
        list,
    )(resource_alloc_items)
    # other resources which could be still allocated for extra request.
    resource_others = f_resource_pool_exclude(resource_allocated)

    # log for debug
    logger.debug("resource_allocated={}/{}".format(resource_allocated, resource_others))

    # STEP_3: allocate resource
    num_cpus_remains = max(0, num_cpus - len(resource_allocated))
    if num_cpus_remains == 0:
        if num_cpus > 0:
            logger.warning("app {} already has cpu {} reserved, cannot change to {}, skip allocating".format(
                app_pid, resource_allocated, num_cpus,
            ))
        resource_alloc_proposal = resource_allocated

    else:
        resource_alloc_proposal = f_compose_r(
            itemgetter(slice(0, num_cpus_remains)),
            list,
        )(resource_others)

        # register app_pid into the allocation
        _ = f_compose_r(
            partial(map, f_compose_r(
                partial(getitem, resource_alloc),
                methodcaller("append", app_pid),
            )),
            list,
        )(resource_alloc_proposal)
        _ = list(map(lambda _: logger.debug("resource_alloc.aft[{}] = {}".format(_[0], _[1])), enumerate(resource_alloc.items())))

        # export the allocation
        lines = f_compose_r(
            methodcaller("items"),
            # format the pids to comma separated str list
            partial(map, f_compose_r(
                f_zip(
                    f_echo,
                    f_compose_r(
                        partial(map, str),
                        ",".join,
                    ),
                ),
                list,
                partial(f_star_call, "{},{}".format),
            )),
            "\n".join,
        )(resource_alloc)
        with open(f_reg_path, 'w') as fd:
            fd.writelines(lines)

        resource_alloc_proposal += resource_allocated
        resource_others = f_resource_pool_exclude(resource_alloc_proposal)

    # apply the affinity
    if apply and num_cpus > 0:
        r = apply_cpu_affinity(
            map(partial(getitem, resource_map), resource_alloc_proposal),
            pid=app_pid,
        )
        assert len(r.difference(map(int, resource_alloc_proposal))) == 0, \
            "affinity apply failed {} vs. {}".format(resource_alloc_proposal, r)

    return resource_alloc_proposal, resource_others


@wraps(cpu_balance_no_lock)
def cpu_balance(*args, lock=True, **kwargs):
    """
    balance CPU to workload
    :param num_cpus: number of visible CPUs to be balanced, 0 means all
    :param app_pid: application process id which need to be charged to the specific GPUs
    :param lock:
    :return:
    """
    from darwinutils.log import get_task_logger
    logger = get_task_logger(__name__)

    if lock:
        import os
        f = f_lock_mutex(
            "darwin_f_lock_mutex.{}_{}.cpu_balance.4zdvseq3dpiztiam3yzprruj9s4ynkbu".format(
                os.environ["USER"],
                os.getuid(),
            ),
            timeout=-1, interval=[0.1, 0.5],
            printFunctor=logger.debug,
        )(cpu_balance_no_lock)
    else:
        f = cpu_balance_no_lock
    return f(*args, **kwargs)


def convert_time_to_utc(time):
    from datetime import datetime
    if time is None:
        return
    return datetime.utcfromtimestamp(time).strftime("%Y-%m-%dT%H:%M:%SZ")


def shape_squeeze_tail(shape):
    from itertools import dropwhile
    from operator import eq
    from functools import partial
    from darwinutils.fn import f_compose_r
    shape = f_compose_r(
        reversed,
        partial(dropwhile, partial(eq, 1)),
        list,
        reversed,
        tuple,
    )(shape)
    if len(shape) == 0:
        shape = (1, )
    return shape


def enforce_gc():
    import gc
    from darwinutils.log import get_task_logger
    logger = get_task_logger(__name__)

    func = gc.collect

    from darwinutils.config import DARWIN_CONFIG
    if DARWIN_CONFIG.debug_performance:
        func = partial(f_timeit_call, func)

    r = func()

    if DARWIN_CONFIG.debug_performance:
        r, duration = r
        logger.critical("perf: enforce python gc in {:.6f}s with rc equals to {}".format(duration, r))


def loop_run(target, condition_func, sleep_interval=None, timeout=None, sleep_func=None):
    import time
    from collections import Callable

    if timeout is None:
        timeout = -1
    if sleep_func is None:
        sleep_func = time.sleep

    if isinstance(sleep_interval, Callable):
        sleep_func = partial(f_unstar_call, f_compose_r(
            partial(f_star_call, sleep_interval),
            sleep_func,
        ))
    elif sleep_interval is not None:
        sleep_func = partial(f_si_call, partial(sleep_func, sleep_interval))
    else:
        sleep_func = None

    i_cnt = 0
    time_beg = time.time()
    eta = timeout
    cond_r = False
    while timeout < 0 or eta > 0.0:
        r = target()
        cond_r = condition_func(r, i_cnt, eta)
        if cond_r:
            break
        if sleep_func:
            sleep_func(i_cnt, eta/timeout)
        eta = timeout - time.time() + time_beg
        i_cnt += 1

    if not cond_r:
        raise TimeoutError("Loop run {} with condition {} failed in {:.6f}(timeout) plus {:.6f}(eta)".format(
            target, condition_func, timeout, eta,
        ))
    return r


def loop_run_cnt(target, cnt_cond_func, *args, **kwargs):
    from operator import (itemgetter, le)

    if not callable(cnt_cond_func):
        cnt_cond_func = partial(le, cnt_cond_func-1)

    cond_func = partial(
        f_unstar_call,
        f_compose_r(
            itemgetter(1),
            cnt_cond_func,
        ),
    )
    return loop_run(target, cond_func, *args, **kwargs)


def redis_lock(lock_key, ttl, *args, redis_cli=None, **kwargs):
    from redlock import Redlock
    if redis_cli is None:
        from darwinutils.config import DARWIN_CONFIG
        redis_cli = load_func_from_spec(DARWIN_CONFIG.service_name, "redis_client")

    dlm = Redlock([redis_cli.connection_pool.connection_kwargs], *args, **kwargs)
    return dlm.lock(lock_key, int(ttl*1000))


def redis_unlock(lock, redis_cli=None):
    from redlock import Redlock
    if redis_cli is None:
        from darwinutils.config import DARWIN_CONFIG
        redis_cli = load_func_from_spec(DARWIN_CONFIG.service_name, "redis_client")

    dlm = Redlock([redis_cli.connection_pool.connection_kwargs])
    return dlm.unlock(lock)


def redis_extlock(lock, ttl, redis_cli=None):
    import math
    from darwinutils.log import get_task_logger
    logger = get_task_logger(__name__)

    from redlock import Lock
    if redis_cli is None:
        from darwinutils.config import DARWIN_CONFIG
        redis_cli = load_func_from_spec(DARWIN_CONFIG.service_name, "redis_client")

    # backdoor to extend by upscale percentage
    if isinstance(ttl, float):
        ttl = math.ceil(lock.validity*ttl/1000)

    # TODO: needs to check the rc of expire op
    r = redis_cli.expire(lock.resource, ttl)
    if r:
        r = Lock(ttl, lock.resource, lock.key)
        logger.debug("extend lock ttl from {} to {}".format(lock.validity, r.validity))
    return r


# Note, numpy can not be direclty json.dumps, if you met the issue, you can call
# the function by dumped = json.dumps(data, cls=NumpyEncoder)
class NumpyEncoder(json.JSONEncoder):
    """ Special json encoder for numpy types """
    def default(self, obj):
        if isinstance(obj, (np.int_, np.intc, np.intp, np.int8,
                            np.int16, np.int32, np.int64, np.uint8,
                            np.uint16, np.uint32, np.uint64)):
            return int(obj)
        elif isinstance(obj, (np.float_, np.float16, np.float32,
            np.float64)):
            return float(obj)
        elif isinstance(obj,(np.ndarray,)): #### This is the fix
            return obj.tolist()
        return json.JSONEncoder.default(self, obj)


def read_config_file(config_path):
    from darwinutils.log import get_task_logger
    logger = get_task_logger(__name__)

    if os.path.exists(config_path):
        with open(config_path) as f:
            try:
                config_info = json.load(f)
                logger.debug("config info: {}".format(config_info))
                return config_info
            except json.decoder.JSONDecodeError as e:
                logger.error(e)
                logger.error("Config file is not json-compatible.")
                return None
    else:
        logger.error("Config path {} does not exist.".format(config_path))
        return None


def get_basic_per_job_config_dict():
    from darwinutils.json_op import Json
    from darwinutils.config import get_per_job_config_path
    from darwinutils.log import get_task_logger

    logger = get_task_logger(__name__)
    try:
        file_path = get_per_job_config_path()
        logger.debug("Read per_job_config.template path is {}".format(file_path))
        basic_per_job_config_dict = Json.file_to_json_without_comments(file_path)
        logger.debug("basic_per_job_config_dict is {}".format(basic_per_job_config_dict))
    except:
        logger.error("Something error in read per_job_config.template")
        basic_per_job_config_dict = None
    return basic_per_job_config_dict


def _fill_dict_value(new_dict, refer_dict):
    if isinstance(refer_dict, dict):
        for key in refer_dict.keys():
            if new_dict.get(key) is None:
                if isinstance(refer_dict[key], dict):
                    new_dict[key] = {}
                    _fill_dict_value(new_dict[key], refer_dict[key])
                else:
                    new_dict[key] = refer_dict[key]


def get_all_celery_job_done(job_lst, celery_app, timer=None):
    from time import sleep
    from darwinutils.config import DARWIN_CONFIG
    from celery.result import AsyncResult
    from darwinutils.log import get_task_logger

    logger = get_task_logger(__name__)
    good_job_lst = []
    error_msg = ""
    if timer is None:
        timer = 5
    count = 0
    while len(job_lst) > 0:
        sleep(timer)
        wait_job_lst = []
        for idx, job_id in enumerate(job_lst):
            asyc_rst = AsyncResult(id=job_id, app=celery_app)
            if asyc_rst is not None:
                try:
                    if not asyc_rst.ready():
                        logger.debug("Waiting for job {} done. Current Status: {}".format(job_id, asyc_rst.status))
                        wait_job_lst.append(job_lst[idx])
                    else:
                        if asyc_rst.failed():
                            logger.error("Job {} failed.".format(job_id))
                            error_msg += "Job {} failed.".format(job_id)
                        else:
                            rst = asyc_rst.result
                            good_job_lst.append(rst)
                except Exception:
                    logger.exception("Job {} failed.".format(job_id))
                    error_msg += "Job {} failed with exception.".format(job_id)
            else:
                logger.error("Cannot get context for job {} failed.".format(job_id))
            del asyc_rst
        del job_lst
        job_lst = wait_job_lst
        count += 1
        if timer < DARWIN_CONFIG.get("timeout"):
            timer = int(min(1.2 * timer, DARWIN_CONFIG.get("timeout")))
    return good_job_lst, error_msg


def find_free_port(host, range=None):
    """
    Find a free tcp port in target host
    :param host: target ip address in localhost to find the free port
    :param range: port range <==TODO: not implemented for now
    :return:
    """
    import socket
    from contextlib import closing
    with closing(socket.socket(socket.AF_INET, socket.SOCK_STREAM)) as s:
        s.bind((host, 0))
        return s.getsockname()[1]


def get_local_source_ip_address(target_host):
    """
    Get local ip address which will be used when connects to the target host
    :param target_host:
    :return:
    """
    import socket
    from contextlib import closing
    ip_address = None
    with closing(socket.socket(socket.AF_INET, socket.SOCK_DGRAM)) as s:
        try:
            s.connect((target_host, 80))
            ip_address = s.getsockname()[0]
        except Exception as _e:
            # drop all error and return None to let caller handle
            pass
    return ip_address


def convert_uuid_to_bson_object_id(_uuid):
    """
    Convert an UUID to bson object id
    TODO: this convertion is not 100% safe since it reduce the hash size
    TODO: from uuid(34?) to bson_id(12)

    the initial usage of this function was to associate an uuid object to mongo db record
    more user friendly which can be easily find out its relationship.

    :param _uuid:
    :return: a bson object id
    :rtype: ObjectId
    """
    from operator import (attrgetter, itemgetter)
    import uuid
    from bson.objectid import ObjectId
    from darwinutils.fn import f_compose_r
    return f_compose_r(
        uuid.UUID,
        attrgetter("bytes"),
        itemgetter(slice(None, 12)),
        ObjectId,
    )(_uuid)


@lru_cache(maxsize=10)
def get_spec_by_uuid_from_db(pl_uuid):
    """
    Load pipeline from mongo db by its uuid w/ cache enabled.

    NOTE: the cache was to speed up the query to reduce response time.
    it does not consider if the pipeline_info record had been updated which means
    the same pl_uuid may have different content!

    :param pl_uuid:
    :return:
    """
    import db.mongodb_api as api
    _id = convert_uuid_to_bson_object_id(pl_uuid)
    record = api.get_one_by_id(_id, table="pipeline_info")
    if record is not None:
        print("load from db")
        pl_spec = record["graph"]
    else:
        pl_spec = None
    return pl_spec


def check_augment_pipeline_format(pipeline):
    if not isinstance(pipeline, list):
        return False
    for op_params in pipeline:
        if not isinstance(op_params, dict):
            return False
        if len(op_params.keys()) != 1:
            return False
        if len(op_params.values()) != 1:
            return False
        if not isinstance(list(op_params.keys())[0], str):
            return False
        if not isinstance(list(op_params.values())[0], dict):
            return False
    return True


def get_dict_filter_function_per_keys(keys, _getter=itemgetter):
    """
    Form a sub dictionary extracting function based on a list of given keys
    NOTE: this is a function to generate a function. so, it's more like a decorator.
    :param keys: dictionary keys to extract from target dictionary
    :type keys: Union(list, tuple)
    :param _getter: function to get value per key from target object
    :type _getter: callable
    :return: a function which can extract a sub dictionary from target
    :rtype Callable

    NOTE: use different _getter can filter any type of object to a dictionary
    """
    f_subs = [
        partial(f_star_call, _getter),
        lambda _: f_list_rize,
        partial(partial, zip),
        lambda _: dict,
    ]

    n_keys = len(keys)
    if n_keys > 1:
        f = f_compose_r(
            f_star_call(f_group, itemgetter(0, 2, 3)(f_subs)),
            partial(f_star_call, f_compose_r),
        )
    elif n_keys == 1:
        f = f_compose_r(
            f_star_call(f_group, f_subs),
            partial(f_star_call, f_compose_r),
        )
    else:
        f = lambda _: lambda _: {}
    return f(keys)


def f_exception_catch_run(f, args=None, kwargs=None, catch_exceptions=None, log_func=None):
    """
    Catch required exceptions from wrapped function
    :param f: target function to be run
    :type f: Callable
    :param args: args to run target function
    :type args: Union(tuple, list)
    :param kwargs: kwargs to run target function
    :type kwargs: dict
    :param catch_exceptions: a list of exception types to be catched
    :type catch_exceptions: list
    :param log_func: valid logging function or none to disable log
    :type log_func: callable
    :return: a named tuple of ExceptCatchRunResult.
            succ=true and result is the real result.
            succ=false and result is the an ExceptCatchRunResultErrorMsg tuple.
    :rtype ExceptCatchRunResult
    """
    def _show_ExceptCatchRunResultErrorMsg(msg, log_func):
        for item, lines in zip(["exc", "tb"], [str(msg.exception), msg.traceback]):
            for i, line in enumerate(lines.splitlines()):
                log_func(">> {}[{}]: {}".format(item, i, line))

    if args is None:
        args = []
    if kwargs is None:
        kwargs = {}
    if catch_exceptions is None:
        catch_exceptions = [Exception]

    try:
        result = f(*args, **kwargs)
        success = True

    except Exception as e:
        result = ExceptCatchRunResultErrorMsg(exception=e, traceback=traceback.format_exc())
        success = False

        if not any(map(partial(isinstance, e), catch_exceptions)):
            if log_func is not None:
                log_func("--------------------------------------------------------------")
                log_func("First eyewitness of unknown invoking error being catched:")
                _show_ExceptCatchRunResultErrorMsg(result, log_func)
                log_func("--------------------------------------------------------------")
            raise

        # log for early debug
        if not success and log_func is not None:
            log_func("--------------------------------------------------------------")
            log_func("First eyewitness of intended invoking error being catched:")
            for item, msg in zip(["wrapped_function"], [repr(f)]):
                log_func(">> {}: {}".format(item, msg))
            _show_ExceptCatchRunResultErrorMsg(result, log_func)
            log_func("--------------------------------------------------------------")

    finally:
        # handle graceful exceptional error
        if isinstance(result, ExceptCatchRunResultErrorMsg) and success == True:
            success = False

            # raise un-catched exception
            if not any(map(partial(isinstance, result.exception), catch_exceptions)):
                if log_func is not None:
                    log_func("--------------------------------------------------------------")
                    log_func("Second eyewitness of unknown invoking error being catched:")
                    _show_ExceptCatchRunResultErrorMsg(result, log_func)
                    log_func("--------------------------------------------------------------")
                raise result.exception from None

            # log for early debug
            if not success and log_func is not None:
                log_func("--------------------------------------------------------------")
                log_func("Second eyewitness of invoking graceful error being catched:")
                for item, msg in zip(["wrapped_function"], [repr(f)]):
                    log_func(">> {}: {}".format(item, msg))
                _show_ExceptCatchRunResultErrorMsg(result, log_func)
                log_func("--------------------------------------------------------------")
        wrapped_result = ExceptCatchRunResult(success=success, result=result)
    return wrapped_result


def dump_function_args(dump_filename = None):
    """
    This function can dump the caller functions' args for offline debugging purpose
    !!! Notice, be careful, the inspect module should not be used in production env due to the limitation of Cython
    See also, https://cython.readthedocs.io/en/latest/src/userguide/limitations.html,

    :param dump_filename: if None, will dump the output to /tmp/uuid_function_name.bin
    :return:
    """
    import  inspect
    import uuid
    from darwinutils.log import get_task_logger
    import pickle
    logger = get_task_logger(__name__)

    frame = inspect.currentframe()
    outer_frames = inspect.getouterframes(frame)
    if len(outer_frames) < 2:
        logger.info("Fail to get the caller frame to dump its args!")
        return
    caller_frame_info = outer_frames[1]
    caller_frame = caller_frame_info.frame
    caller_function = caller_frame_info.function
    val = None
    if dump_filename is None:
        dump_filename = "/tmp/{}_{}.bin".format(uuid.uuid4(), caller_function)
    try:
        with open(dump_filename, "wb") as f:
            s = inspect.getargvalues(caller_frame)
            val = {'args': s.args,
                   'varargs': s.varargs,
                   'keywords': s.keywords,
                   'locals': s.locals}
            pickle.dump(val, f)
    except Exception as e:
        logger.info("Fail to dump args!  Error Msg: {}".format(e))
    finally:
        return val


def map_ndarray_to_lst(param):
    # Becareful to use the function, it maybe very slow
    if isinstance(param, dict):
        for key, value in param.items():
            param[key] = map_ndarray_to_lst(value)
    elif isinstance(param, list):
        if isinstance(param[0], dict) or isinstance(param[0], tuple) or isinstance(param[0], list):
            for idx, item in enumerate(param):
                param[idx] = map_ndarray_to_lst(item)
        elif isinstance(param[0], np.ndarray):
            param = list(map(lambda s: s.tolist(), param))
    elif isinstance(param, tuple):
        pass
    elif isinstance(param, np.ndarray):
        param = param.tolist()
    return param


def map_npfloat_to_float(param):
    # Becareful to use the function, it maybe very slow
    if isinstance(param, dict):
        for key, value in param.items():
            param[key] = map_npfloat_to_float(value)
    elif isinstance(param, list):
        for idx, item in enumerate(param):
            param[idx] = map_npfloat_to_float(item)
    elif isinstance(param, (np.float_, np.float16, np.float32, np.float64)):
        param = float(param)
    else:
        pass
    return param


def f_filter_matrix_by_kv(matrix, keys, vals, _getter=getattr):
    """
    Filter a matrix, list of objects with attributes which could be searched, by attribute keys-values pairs

    :param matrix: iterable object sequence which support extracting attributes
    :type matrix: iterable
    :param keys:  list of attribute names
    :type keys: list(str)
    :param vals: list of HASHABLE(!!!) attribute values which we expected to find/search
    :type vals: list(obj)
    :param _getter: function to extract value by key from matrix entry
    :type _getter: callable
    :return: list of object in the matrix which matches the exprected attributes kv pairs
    :rtype: list

    NOTE:
    * all matrix entries must have all attributes which you wants to search, or, AttributeError will raise
    * use different _getter can handle different type of matrix entry.
      default "getattr" handles namedtuple likely obj.
      use "getitem" to handle dict likely obj.
    """
    f_filter = lambda entry: list(map(partial(_getter, entry), keys)) == list(vals)
    return filter(f_filter, matrix)


def f_merge_kv_pair_sets(
        mappings: Iterable[Mapping],
        f_merge: Callable=None,
        f_key: Callable=None,
        f_encoder=None,
        f_decoder=None,
) -> Mapping:
    """
    Merge a set of <k, v> sets
    :param mappings:
    :param f_merge:
    :param f_key:
    :param f_encoder:
    :param f_decoder:
    :return:
    """
    if f_merge is None:
        f_merge = f_echo
    if f_key is None:
        f_key = hash
    if f_encoder is None:
        f_encoder = dict
    if f_decoder is None:
        f_decoder = methodcaller("items")
    f_key = f_compose_r(itemgetter(0), f_key)

    return f_compose_r(
        # iter(({'a1': 1, 'a2': 2}, {'a1': 3, ra3': 4}))
        partial(map, f_compose_r(f_decoder, list)),
        partial(reduce, concat),
        # [('a1', 1), ('a2', 2), ('a1', 3), ('a3', 4)]
        partial(sorted, key=f_key),
        # iter([('a1', 1), ('a1', 3), ('a2', 2), ('a3', 4)])
        partial(groupby, key=f_key),
        # iter([[f_key('a1'), <itertools._grouper object>], [f_key('a2'), <itertools._group object>], ...])
        partial(map, f_compose_r(
            # [f_key('a1'), <itertools._grouper object>]
            itemgetter(1),
            # <itertools._grouper object>([['a1', 1], ['a1', 3]])
            partial(f_star_call, zip),
            list,
            # <itertools._zip object>([['a1', 'a1'], [1, 3]])
            f_zip(
                # -> ['a1', 'a1']
                f_compose_r(iter, next),
                # -> [1,3]
                f_merge,
            ),
            tuple,
            # ('a1', f_merge([1,3]))
        )),
        list,
        # iter([('a1', f_merge([1,3])), ('a2', f_merge([2])), ('a3', f_merge([4]))])
        f_encoder,
        # {'a1': f_merge([1,3]), 'a2': f_merge([2]), 'a3': f_merge([4])}
    )(mappings)


def register_var_kwargs_to_obj(obj, var_kwargs: Mapping[str, object]):
    """
    Register the elements in a variant kwargs dict to an object
    :param obj:
    :param var_kwargs:
    :return: the updated object
    """
    arg_names = set(var_kwargs.keys())
    # find out obj's all property type of attributes and those writable ones
    cls_property_names, cls_writable_property_names = f_compose_r(
        type,
        f_group(
            # lambda o: f_compose_r(
            #     f_group(
            #         f_echo,
            #         partial(getattr, o),
            #     ),
            #     list,
            # ),
            f_compose_r(
                partial(partial, getattr),
                partial(f_group, f_echo),
                partial(f_compose_l, list),
            ),
            dir,
        ),
        partial(f_star_call, map),
        partial(filter, f_compose_r(
            itemgetter(1),
            partial(f_flip_call, isinstance, property),
        )),
        list,
        f_group(
            f_echo,
            f_compose_r(
                partial(filter, f_compose_r(
                    itemgetter(1),
                    f_group(
                        partial(f_flip_call, getattr, "fset"),
                        partial(is_not, None),
                    ),
                    all,
                )),
            ),
        ),
        partial(map, f_compose_r(
            partial(map, itemgetter(0)),
            set,
        )),
    )(obj)
    cls_readonly_property_names = cls_property_names - cls_writable_property_names

    # validate to make sure you are not attempting to assign to read-only properties.
    readonly_property_conflict_arg_names = cls_readonly_property_names & arg_names
    if len(readonly_property_conflict_arg_names) > 0:
        raise ValueError("input varkwargs conflicts with following read-only properties: {}".format(
            readonly_property_conflict_arg_names,
        ))

    writable_property_arg_names = cls_writable_property_names & arg_names
    attribute_arg_names = arg_names - writable_property_arg_names

    # validate to make sure you are not attempting to overwrite existing object attributes.
    dup_attrs = set(obj.__dict__.keys()) & attribute_arg_names
    if len(dup_attrs) > 0:
        raise ValueError("input varkwargs conflicts with following object attributes: {}".format(dup_attrs))

    # firstly, assign injected args to writable properties which are same name.
    property_kwargs = get_dict_filter_function_per_keys(writable_property_arg_names)(var_kwargs)
    _ = f_compose_r(
        methodcaller("items"),
        partial(map, f_compose_r(
            partial(f_star_call, partial(setattr, obj)),
        )),
        list,
    )(property_kwargs)

    # then, register injected args as object's new attributes.
    attribute_kwargs = get_dict_filter_function_per_keys(attribute_arg_names)(var_kwargs)
    obj.__dict__.update(attribute_kwargs)

    return obj


class PayloadEvent(Event):
    """
    threading.Event with payload
    TODO: payload set/get was just a "payload" of event set/get.
    TODO: take care by yourself if you use this payload independently.
    """
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._payload = None

    @property
    def payload(self):
        return self._payload

    def set(self, payload=None):
        self._payload = payload
        return super().set()

    def clear(self):
        self._payload = None
        return super().clear()

    def async_wait(
            self,
            timeout=None,
            sleep_interval=0.0,
            sleep_func: Callable[[Union[float, int]], None] = None,
    ) -> bool:
        """
        Wait for Event's is_set flag asynchronously
        # it's an enhancement to Event's own wait(timeout=None) in order to support working with gevent/greenlet.
        :param timeout:
        :param sleep_interval:
        :param sleep_func:
        :return:
        """
        if sleep_func is None:
            sleep_func = gevent.sleep
        try:
            _ = loop_run(
                self.is_set,
                partial(f_unstar_call, itemgetter(0)),
                timeout=timeout,
                sleep_interval=sleep_interval,
                sleep_func=sleep_func,
            )
            wait_succ = True
        except TimeoutError:
            wait_succ = False
        return wait_succ


def slice_without_axis(axis: int, n_dim: int) -> List[int]:
    """
    calculate slice to extract data from non-axis dimentions
    :param axis:
    :param n_dim:
    :return:
    """
    if axis < 0:
        axis = n_dim - 1
    elif axis >= n_dim:
        raise IndexError("axis {} should not larger than {}={}-1".format(axis, n_dim-1, n_dim))
    return tuple(chain(range(0, axis), range(axis+1, n_dim)))


def f_f_shape_without_axis(axis: int) -> Callable:
    """
    Generate shape extraction function which exclude desired axis
    :param axis: axis to be excluded
    :return:
    """
    # lambda shape: np.array(shape)[slice_without_axis(axis, len(shape))]
    return f_compose_r(
        tuple,
        f_group(
            f_compose_r(
                np.array,
                partial(partial, getitem),
            ),
            f_compose_r(
                len,
                partial(slice_without_axis, axis),
                list,
            ),
        ),
        partial(f_star_call, f_call),
        methodcaller("tolist"),
        tuple,
    )


def f_f_calc_concat_shape_in_axis(axis: int) -> Callable:
    """
    Generate the function to calculate the output shape of concat op in specific axis
    :param axis:
    :return:
    """
    return f_compose_r(
        np.array,
        f_group(
            lambda _: setitem(_, [slice(1, None), slice_without_axis(axis, _.shape[1])], 0),
            f_echo,
        ),
        list,
        itemgetter(1),
        partial(np.sum, axis=0),
        methodcaller("tolist"),
        tuple,
    )


def f_i_extract_sub_dict_by_key_prefix(prefix: str) -> Callable[[Mapping[str, object]], Mapping[str, object]]:
    """
    Generate a function to extract a sub-directory with entries who has the key with a specific prefix
    Moreover!!! the input dictionary will be updated to excluded those extracted entries!!!

    NOTE: this is a
    :param prefix: the prefix of keys who needs to be extracted from the source dictionary.
    :return: a callable function
    """
    return f_compose_r(
        # extract prefixed kwargs with <map_func, iterable> pair
        f_group(
            attrgetter("pop"),
            f_compose_r(
                methodcaller("keys"),
                partial(filter, methodcaller("startswith", prefix)),
                list,
            ),
        ),
        list,
        # compose prefixed kwargs dict and pop from original dict
        f_group(
            # keys w/o prefix
            f_compose_r(
                itemgetter(1),
                partial(map, itemgetter(slice(len(prefix), None))),
                list,
            ),
            # values and pops from original dict
            f_compose_r(
                partial(f_star_call, map),
                list,
            ),
        ),
        list,
        # generate target dict.
        partial(f_star_call, zip),
        dict,
    )


def merge_args(args, same_shape=True, fillvalue=None):
    """
    Merge a list of args to args with list type
    :param args:
    :param same_shape: if each args needs to be same shape.
    :param fillvalue: the value to be filled if missing one args when same_shape is false.
    :return:
    """
    n_args = list(map(len, args))
    if same_shape and \
            len(set(n_args)) > 1:
        raise ValueError("Args have different shape while same_shape was requested. {}".format(n_args))

    return f_compose_r(
        partial(f_star_call, partial(zip_longest, fillvalue=fillvalue)),
        list,
    )(args)


def merge_kwargs(kwargs, same_shape=True, fillvalue=None):
    """
    Merge a list of kwargs to kwargs with list type
    :param kwargs:
    :param same_shape: if each args needs to be same shape.
    :param fillvalue: the value to be filled if missing one args when same_shape is false.
    :return:
    """
    arg_names = list(map(methodcaller("keys"), kwargs))
    full_set_arg_names = set.union(*map(set, arg_names))
    if same_shape and \
            (
                    len(set(map(len, arg_names))) > 1 or \
                    full_set_arg_names != set(arg_names[0])
            ):
        raise ValueError("Kwargs have different shape while same_shape was requested. {}".format(arg_names))

    if not same_shape:
        default_kwargs = dict(zip(full_set_arg_names, [fillvalue]*len(full_set_arg_names)))
        kwargs = f_compose_r(
            partial(map, f_compose_r(
                partial(f_flip_call, collections.ChainMap, default_kwargs),
                dict,
            )),
        )(kwargs)

    return f_compose_r(
        partial(
            f_merge_kv_pair_sets,
            f_merge=f_echo,
            f_key=hash,
            f_encoder=dict,
            f_decoder=methodcaller("items"),
        ),
    )(kwargs)


def merge_bound_args(arg1: Tuple[tuple, dict], arg2: Tuple[tuple, dict]) -> Tuple[tuple, dict]:
    """
    Merge two BoundArguments together with the 1st one take precedence.
    :param arg1:
    :param arg2:
    :return:
    """
    _arg1, _kwarg1 = arg1
    _arg2, _kwarg2 = arg2
    return (
        concat(tuple(_arg1), tuple(_arg2)),
        dict(collections.ChainMap(_kwarg1, _kwarg2)),
    )


def filter_func_parameter_names(
        func: Union[Callable, inspect.Signature],
        f_filter: Callable[[inspect.Parameter], bool],
) -> Tuple[str]:
    """
    Query/filter the function's parameters which satisfy a specific condition
    :param func: function to be queried/filterred
    :param f_filter: condition function
    :return: a tuple of parameter names
    """
    if callable(func):
        sig = inspect_func(func)
    else:
        sig = func
    return f_compose_r(
        attrgetter("parameters"),
        methodcaller("values"),
        partial(filter, f_filter),
        partial(map, attrgetter("name")),
        tuple,
    )(sig)


def start_pipeline(pipeline_str=None, pipeline_path=None, pipeline_param_dict=None, **kwargs):
    from pipeline.pipeline import PipeLine
    from pipeline.pipetask import PipeTask
    import json
    from darwinutils.json_op import Json
    from darwinutils.log import get_task_logger
    logger = get_task_logger(__name__)

    if pipeline_path is not None and os.path.exists(os.path.abspath(pipeline_path)):
        try:
            my_pipe = Json.file_to_json_without_comments(pipeline_path)
        except Exception as e:
            logger.exception("Failed get json dict from {} with reason {}".format(pipeline_path, str(e)))
            return None
    elif pipeline_str is not None:
        try:
            my_pipe = Json._remove_dict_comment(json.loads(pipeline_str))
        except Exception as e:
            logger.exception("Failed get json dict from {} with reason {}".format(pipeline_str, str(e)))
            return None
    else:
        logger.error("There is no valide pipeline_str or pipeline_path")
        return None
    p = PipeLine(my_pipe)
    task = PipeTask(p)
    param_dict = kwargs
    if pipeline_param_dict is not None:
        param_dict.update(pipeline_param_dict)
    return task(**param_dict)
