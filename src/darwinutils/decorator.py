#! /usr/bin/env python3
# -*- coding: utf-8 -*-

from functools import (wraps)
from darwinutils.common import (
    ExceptCatchRunResult,
    ExceptCatchRunResultErrorMsg,
)


def logFuncHeaderFunctor(printFunctor):
    import time
    def _logFuncHeaderFunctor(f):
        if printFunctor is None:
            return f

        @wraps(f)
        def wrapper(*kw, **kwargs):
            printFunctor('+--- enter func "{}"'.format(f.__qualname__))
            beg = time.time()

            result = f(*kw, **kwargs)

            end = time.time()
            printFunctor('+--- exit  func "{}" in {:.3f}s'.format(
                f.__qualname__, end-beg))

            return result
        return wrapper
    return _logFuncHeaderFunctor


def except_catch_run(catch_exceptions, log_func=print):
    """
    Catch required exceptions from wrapped function
    :param catch_exceptions: a list of exception types to be catched
    :param log_func: valid logging function or none to disable log
    :return:
    """
    from darwinutils.helper import f_exception_catch_run

    def _except_catch_run(f):
        @wraps(f)
        def wrapper(*args, **kwargs):
            return f_exception_catch_run(f, args, kwargs, catch_exceptions=catch_exceptions, log_func=log_func)

        return wrapper
    return _except_catch_run


class threadsafe_iter:
    """
    A generic iterator and generator that takes any iterator and wrap it to make it thread safe.
    This method was introducted by Anand Chitipothu in http://anandology.com/blog/using-iterators-and-generators/
    but was not compatible with python 3. This modified version is now compatible and works both in python 2.8 and 3.0

    Takes an iterator/generator and makes it thread-safe by
    serializing call to the `next` method of given iterator/generator.
    """
    def __init__(self, it):
        from collections import Iterator
        if not isinstance(it, Iterator):
            it = iter(it)

        self.it = it

        import threading
        self.lock = threading.Lock()

    def __iter__(self):
        return self

    def __next__(self):
        with self.lock:
            return next(self.it)


def threadsafe_generator(f):
    """A decorator that takes a generator function and makes it thread-safe.
    """
    @wraps(f)
    def _wrap(*a, **kw):
        return threadsafe_iter(f(*a, **kw))
    return _wrap


def timeit(f):
    import time
    from darwinutils.log import get_task_logger
    logger = get_task_logger(__name__)

    def timed(*args, **kw):
        ts = time.time()
        result = f(*args, **kw)
        te = time.time()
        logger.info('[Performance Stats]: {}  {} s'.format(f.__name__, te - ts))
        return result

    return timed
