#! /usr/bin/env python3
# -*- coding:utf-8 -*-


from functools import (wraps)
from collections import namedtuple


# for darwinutils.graph
TraverseHookSpec = namedtuple("TraverseHookSpec", ["workload", "prep", "post"])
TraverseEngineSpec = namedtuple("TraverseEngineSpec", ["workload", "prep", "post"])
TraverseHookInputNodeSpec = namedtuple("TraverseHookInputNodeSpec", ["id", "depth", "obj"])


# for except_catch_run_decorator
ExceptCatchRunResult = namedtuple("ExceptCatchRunResult", ["success", "result"])
ExceptCatchRunResultErrorMsg = namedtuple("ExceptCatchRunResultErrorMsg", ["exception", "traceback"])


# for darwin net
DarwinCkptSpec = namedtuple("DarwinCkptSpec", ["create_time", "genepool_version", "type", "payload", "net"])

# for darwin graph
DarwinGraphEstimationSpec = namedtuple("DarwinGraphEstimationSpec", ["feature_shape", "n_cpu_vars", "n_gpu_vars"])

# for Target enum
DarwinDatasetTarget2FrameworkMapEntrySpec = namedtuple(
    "DarwinTarget2FrameworkMapEntrySpec",
    [
        "prog_type",
        "evaluation_target",
        "framework_type",
        "framework",
    ],
)

def get_current_function_name():
    """
    Get the function name at the top of the stack
    NOTE: Cython in-compatible!!!
    :return:
    :rtype: str
    """
    import inspect
    stack = inspect.stack()
    # to workaround a cython compiled module problem
    if stack[0][3] == "get_current_function_name":
        frame = stack[1]
    else:
        raise EnvironmentError("Cython limitations about using inspect and stack frames: http://cython.readthedocs.io/en/latest/src/userguide/limitations.html")
    return frame[3]


def f_lock_mutex(name, timeout=-1.0, interval=[0.5, 1.5], printFunctor=print):
    import inspect
    from functools import partial
    frame = inspect.currentframe()
    args, _, _, values = inspect.getargvalues(frame)
    f_name = inspect.getframeinfo(frame)[2]
    f_arg_name = partial('{}_{}'.format, f_name)
    default_kwargs = dict(zip(
        map(f_arg_name, args),
        map(values.get, args)
    ))
    #_ = list(map(printFunctor, default_kwargs.items()))

    def _mutex_functor(f):
        from darwinutils.fn import (f_compose_r)

        @wraps(f)
        def wrapper(*kw, **kwargs):
            from collections import ChainMap
            live_kwargs = ChainMap(kwargs, default_kwargs)
            f_get_arg = f_compose_r(f_arg_name, live_kwargs.get)
            #_ = list(map(printFunctor, dict(live_kwargs).items()))

            arg_names = ['name', 'timeout', 'interval', 'printFunctor']
            # extract arg from live first and then default
            name_, timeout_, interval_, printFunctor_ = f_compose_r(
                partial(map, f_get_arg),
                list,
            )(arg_names)
            # remove arg from live kwargs to prevent confusing wrapped function
            from operator import (contains)
            _ = f_compose_r(
                partial(map, f_arg_name),
                partial(filter, partial(contains, kwargs)),
                partial(map, kwargs.pop),
                list,
            )(arg_names)

            # touch the lock file
            import os
            f_lock_path = os.path.join('/tmp', '.{}'.format(name_))
            from pathlib import Path
            Path(f_lock_path).touch()

            # lock files in <timeout> seconds with a random sleep interval
            from fcntl import (flock, LOCK_EX, LOCK_NB, LOCK_UN)
            import math
            import random
            import time
            from operator import sub

            time_beg = time.time()
            time_left = timeout_
            with open(f_lock_path, 'w') as fd:
                locked = False
                while not math.isclose(time_left, 0.0):
                    try:
                        printFunctor_("Acquiring lock for {} with timeout {:.2f}/{:.2f}".format(
                            f_lock_path, time_left, timeout_))
                        flock(fd, LOCK_EX | LOCK_NB)
                        time_end = time.time()
                        printFunctor_("Acquired lock for {} in {:.2f}/{:.2f} seconds.".format(
                            f_lock_path, timeout_ - time_left, time_end - time_beg))
                        locked = True
                        break

                    except OSError as e:
                        # wait random time before timeout
                        delay_seconds = min(
                            random.random()*sub(*interval_[::-1])+interval_[0],
                            time_left if timeout_ > 0.0 else math.inf
                        )
                        time.sleep(delay_seconds)
                        time_left -= delay_seconds

                if locked:
                    # call the workload/wrapped function
                    result = f(*kw, **kwargs)

                    flock(fd, LOCK_UN)
                    time_beg = time_end
                    time_end = time.time()
                    printFunctor_("Released lock for {} after {:.2f} seconds".format(
                        f_lock_path, time_end - time_beg))
                else:
                    raise TimeoutError('Cannot acquire lock for {} in timeout of {:.2f}'.format(f_lock_path, timeout_))
            return result
        return wrapper
    return _mutex_functor
