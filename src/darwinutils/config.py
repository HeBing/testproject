#! /usr/bin/env python3
# -*- coding: utf-8 -*-


import os
import sys
import inspect
import collections
from typing import (
    Union,
)
from functools import (
    partial,
    lru_cache,
)
from operator import (
    setitem,
)
from singleton_decorator import singleton
from darwinutils.json_op import Json
from darwinutils.fn import (
    f_compose_r,
    f_group,
    f_star_call,
)
from darwinutils.helper import (
    query_num_nvidia_gpus,
)
from license import DarwinLicenseGroup


def remove_dict_comment(d):
    all_keys = list(d.keys())
    for key in all_keys:
        if isinstance(key, str) and key.startswith("#"):
            # print("Remove {}".format(key))
            del d[key]
            continue

        # remove recursively
        val = d[key]
        if isinstance(val, dict):
            d[key] = remove_dict_comment(val)
    return d


def get_darwin_conf_home(defaults=None):
    import conf
    # the configure path will be picked from following candidates with priority high to low.
    path_candidates = [
        os.path.join("$DARWIN_HOME", "conf"),
        os.path.join("$HOME", ".config", "darwin"),
        os.path.join("~", ".config", "darwin"),
        '/etc/darwin',
        '/usr/local/etc/darwin',
        defaults,
        os.path.dirname(inspect.getsourcefile(conf))
    ]
    # expand first
    path_exists = list(map(lambda _: os.path.expanduser(os.path.expandvars(_)) if _ is not None else None,
                           path_candidates))
    # validate
    path_exists = list(filter(lambda _: _ is not None and os.path.exists(_), path_exists))
    if len(path_exists) < 1:
        raise EnvironmentError("Darwin configure directory does not exists at {}".format(path_candidates))
    return path_exists[0]


@lru_cache(maxsize=100)
def f_validate_dir_path(description, value, create_dir=True):
    if value is None:
        raise EnvironmentError("{} was not configured".format(description))

    value = f_compose_r(
        os.path.expanduser,
        os.path.expandvars,
    )(value)

    if value and not os.path.exists(value):
        if create_dir:
            try:
                os.makedirs(value, mode=0o711, exist_ok=True)
            except OSError as e:
                print(e)
                raise EnvironmentError("{} was configured to {} which does not exist and cannot be created!".format(
                    description, value
                ))
        else:
            raise EnvironmentError("{} does not exist!".format(description))
    return value


def f_validate_file_path(description, value):
    if value is None:
        raise EnvironmentError("{} was not configured".format(description))

    value = f_compose_r(
        f_group(
            f_compose_r(
                os.path.dirname,
                partial(f_validate_dir_path, description, create_dir=False),
            ),
            os.path.basename,
        ),
        partial(f_star_call, os.path.join),
        os.path.expanduser,
        os.path.expandvars,
    )(value)

    if not os.path.exists(value):
        raise FileNotFoundError("{} was configured to {} which does not exist and cannot be created!".format(
            description, value
        ))
    return value


def f_default_dir_if_not_exist_in_pwd(home: str, path: str) -> str:
    """
    Append a default home directory for non-exist name only file path
    :param cfg:
    :param path:
    :param home:
    :return:
    """
    if os.path.exists(path) or os.path.dirname(path):
        return path
    else:
        return os.path.join(home, path)


@singleton
class DarwinConfig(object):
    def __init__(self, json_path):
        self._json_path = json_path
        self._json = None
        self._service_name = None
        # cache attributes to improve read performance
        self._c_licenses = None
        self._c_verbose_level = None
        self._c_debug_performance = None
        self._c_route = None

        self.load_from_json()

    def __getstate__(self):
        state = self.__dict__.copy()
        del state["_c_licenses"]
        del state["_c_verbose_level"]
        del state["_c_debug_performance"]
        del state["_c_route"]
        return state

    def __setstate__(self, state):
        self.__dict__.update(state)
        self._c_licenses = None
        self._c_verbose_level = None
        self._c_debug_performance = None
        self._c_route = None

    def load_from_json(self, json_path=None):
        '''
        Load configure from json
        :param json_path: optional, in case you wants to load cfg from a different json source
        :return:
        '''
        # delete old config first
        # TODO: should protect with lock
        if self._json is not None:
            del self._json
            self._json = None
        if json_path is not None:
            self._json_path = json_path

        # trigger an immediate load to validate
        _ = self.json # noqa: disable=F841

    @property
    def json_path(self):
        return self._json_path

    @property
    def json(self):
        # load on demand
        # TODO: should protect with lock
        if self._json is None:
            _json = Json.file_to_json(self.json_path)
            self._json = remove_dict_comment(_json)
        return self._json

    @property
    def route(self):
        if self._c_route is None:
            self._c_route = {
                # NOTE: please order the entries
                "ckpt_location":
                    partial(f_validate_dir_path, "Checkpoint directory(\"ckpt_location\")"),
                "weight_pool_path":
                    partial(f_validate_dir_path, "Weight Pool directory(\"weight_pool_path\")"),
                "data_location":
                    partial(f_validate_dir_path, "Data directory(\"data_location\")"),
                "export_model_location":
                    partial(f_validate_dir_path, "Data directory(\"export_model_location\")"),
                # TODO: was data_review location migrated to data_review_location_for_gui?
                "data_review_location_for_gui":
                    partial(f_validate_dir_path, "Data directory(\"data_review_location_for_gui\")"),
                "JOBLIB_TEMP_FOLDER":
                    partial(f_validate_dir_path, "Sklearn temp directory(\"JOBLIB_TEMP_FOLDER\")"),
                "json_path":
                    partial(f_validate_dir_path, "Base config file(\"json_path\")"),
                "license_file":
                    f_compose_r(
                        lambda _: f_default_dir_if_not_exist_in_pwd(self["home"], _),
                        partial(f_validate_file_path, "License file(\"license_file\")"),
                    ),
                "model_location":
                    partial(f_validate_dir_path, "Model directory(\"model_location\")"),
                "model_predict_location":
                    partial(f_validate_dir_path, "Model predict directory(\"model_predict_location\")"),
                "model_evaluate_location":
                    partial(f_validate_dir_path, "Model evaluate directory(\"model_evaluate_location\")"),
                "per_job_config_location":
                    partial(f_validate_dir_path, "User per job config directory(\"per_job_config_location\")"),
                "tmp_data_aug_location":
                    partial(f_validate_dir_path, "Tmp data aug directory(\"tmp_data_aug_location\")"),
                "upload_data_location":
                    partial(f_validate_dir_path, "Upload Data directory(\"upload_data_location\")"),
                "default_dl_checkpoint_dir":
                    partial(f_validate_dir_path, "Default directory to host DL checkpoint for weight pool(\"default_dl_checkpoint_dir\")"),
            }
        return self._c_route

    def validate(self, key, value):
        # attribute specific validation for complex configure item.
        f = getattr(self, "validate_{}".format(key), None)
        if callable(f):
            return f(value)

        # categorical attribute validation
        f = self.route.get(key)
        if callable(f):
            return f(value)

        # no attribute validation
        return value

    def __getitem__(self, key):
        if not hasattr(self, key) and key not in self.json:
            raise KeyError("Configure item \"{}\" does not exist.".format(key))
        return self.get(key)

    def get(self, key, default=None):
        value = getattr(self, key, None)
        if value is None:
            value = self.json.get(key, default)
        return self.validate(key, value)

    # TODO: only for testing, should not be called in production.
    def __setitem__(self, key, value):
        if hasattr(self, key):
            setattr(self, key, value)
        elif key in self.json:
            setitem(self.json, key, value)
        else:
            raise KeyError("Configure item \"{}\" does not exist.".format(key))

    @property
    def debug_performance(self):
        if self._c_debug_performance is None:
            key = "debug_performance"
            self._c_debug_performance = self.validate(key, self.json.get(key, "False"))
        return self._c_debug_performance

    @property
    def service_name(self):
        if self._service_name is None:
            if os.environ.get('DARWIN_SERVICE_NAME') is not None:
                self._service_name = os.environ.get('DARWIN_SERVICE_NAME')
            else:
                self._service_name = 'darwin_service'
        return self._service_name

    @property
    def verbose_level(self):
        if self._c_verbose_level is None:
            key = "verbose_level"
            self._c_verbose_level = self.validate(key, self.json.get(key, 0))
        return self._c_verbose_level

    @property
    def home(self):
        return os.path.dirname(self.json_path)

    @property
    def licenses(self):
        if self._c_licenses is None:
            lic_f = self.get("license_file")
            if not os.path.isabs(lic_f):
                lic_f = os.path.join(self.home, lic_f)
            try:
                lic_g = DarwinLicenseGroup.load(lic_f)
            except Exception as _e:
                print("Fail to load license from \"{}\"".format(lic_f))
                print("{}".format(repr(_e)))
                lic_g = DarwinLicenseGroup()
            self._c_licenses = lic_g
        return self._c_licenses

    @property
    def model_dump_suffix_ext(self):
        return "{}{}".format(self["model_dump_suffix"], self["model_dump_ext"])

    def validate_apply_gevent_monkey_patch(self, value):
        if value is False and "zrpc" in self.get("pipeline_warm_node_call_accepted_channels"):
            pass
            # TODO: do we needs to change it to "True" enforcely?
            # TODO: no! instead, we have to keep monkey-patch free works because customer code, either model or
            # TODO: pipeline might not greenlet or monkey-patch compatible!!!

        # env cfg takes precedence
        env_val = os.environ.get("DARWIN_APPLY_GEVENT_MONKEY_PATCH")
        if env_val is not None:
            env_val = env_val.lower()
            if env_val == "true":
                env_val = True
            elif env_val == "false":
                env_val = False
            else:
                env_val = value

            if env_val != value:
                print("WARNING: cfg[{}]({}) had been replaced by env[{}]({})".format(
                    "apply_gevent_monkey_patch", value,
                    "DARWIN_APPLY_GEVENT_MONKEY_PATCH", env_val,
                ), file=sys.stderr)
                value = env_val
        return value


def get_darwin_conf_path():
    return DARWIN_CONFIG["json_path"]


def get_darwin_data_path():
    return DARWIN_CONFIG['data_location']


def get_darwin_weight_pool_path():
    return DARWIN_CONFIG["weight_pool_path"]


def get_darwin_data_review_gui_path():
    return DARWIN_CONFIG['data_review_location_for_gui']


def get_darwin_model_path():
    return DARWIN_CONFIG['model_location']


def get_ea_image_path_for_gui():
    return DARWIN_CONFIG["ea_images_location_for_gui"]


def get_darwin_model_path_for_gui():
    return DARWIN_CONFIG["model_location_for_gui"]


def get_darwin_model_predict_path():
    return DARWIN_CONFIG['model_predict_location']

def get_darwin_model_evaluate_path():
    return DARWIN_CONFIG['model_evaluate_location']

def get_darwin_per_job_config_path():
    return DARWIN_CONFIG['per_job_config_location']


def get_darwin_ckpt_path():
    return DARWIN_CONFIG['ckpt_location']


def get_darwin_job_web_path():
    return DARWIN_CONFIG['train_job_web']


def get_darwin_upload_data_path():
    return DARWIN_CONFIG['upload_data_location']


def get_darwin_export_model_path():
    return DARWIN_CONFIG['export_model_location']


def get_darwin_template_path():
    return os.path.join(get_darwin_conf_home(), "template")


def get_per_job_config_path():
    return os.path.join(get_darwin_conf_home(), "per_job_config.template")


def apply_monkey_patch(*args, **kwargs):
    from gevent import monkey
    return monkey.patch_all(*args, **kwargs)


if not os.environ.get("num_cpus"):
    if hasattr(os, "sched_getaffinity"):
        # get number of usable cpus.
        f = partial(f_compose_r(os.sched_getaffinity, len), 0)
    else:
        # static cpu_count
        f = os.cpu_count
    os.environ["num_cpus"] = str(f())
if not os.environ.get("num_gpus"):
    # get number of usable cpus.
    os.environ["num_gpus"] = str(query_num_nvidia_gpus())

DARWIN_CONFIG = DarwinConfig(os.path.join(get_darwin_conf_home(), 'darwin_config.json'))
# apply monkey patch at the very begin of darwin
if DARWIN_CONFIG["apply_gevent_monkey_patch"] is True:
    r = apply_monkey_patch()
    if DARWIN_CONFIG.verbose_level > 0:
        print("Apply monkey patch with return of \"{}\"".format(r), file=sys.stderr)
if len(DARWIN_CONFIG.licenses.active_licenses) < 1:
    print(
        "\n\n\nNo valid license in \"{}\". Abort!\n\n\n".format(DARWIN_CONFIG["license_file"]),
        file=sys.stderr,
    )
    sys.exit(1)
