#! /usr/bin/env python3
# -*- coding:utf-8 -*-


from itertools import (
    starmap,
    chain,
    compress,
)
from functools import (
    partial,
    reduce,
)
from operator import (
    attrgetter,
    getitem,
    itemgetter,
    methodcaller,
    mul,
    not_,
    setitem,
)
import traceback
import pickle
import uuid
import numpy as np
from darwinutils.decorator import (
    ExceptCatchRunResult,
    ExceptCatchRunResultErrorMsg,
    except_catch_run,
)
from darwinutils.common import (
    get_current_function_name,
)
from darwinutils.fn import (
    f_compose_r,
    f_star_call,
)
from darwinutils.helper import (
    is_iterable,
    reduce_uuid,
)
from darwinutils.log import get_task_logger
from darwinutils.common import (
    TraverseHookSpec,
    TraverseEngineSpec,
    TraverseHookInputNodeSpec,
)


logger = get_task_logger(__name__)


def _wrap_with_catch_exception(node, hook, catch_exceptions, log_func):
    return except_catch_run(catch_exceptions=catch_exceptions, log_func=log_func)(hook)(node)


def pack_adj_matrix(adj_matrix):
    shape = adj_matrix.shape
    matrix = adj_matrix.flatten()
    is_not_nan = np.logical_not(np.isnan(matrix))
    edge_idx = np.compress(is_not_nan, np.arange(len(matrix), dtype=np.int32), axis=0).tolist()
    edge_ids = np.compress(is_not_nan, matrix, axis=0).tolist()
    return edge_idx, edge_ids, shape


def unpack_adj_matrix(data):
    edge_idx, edge_ids, shape = data
    adj_matrix = np.empty((reduce(mul, shape),), dtype=np.float32)
    adj_matrix.fill(np.nan)

    adj_matrix[edge_idx] = edge_ids
    adj_matrix = adj_matrix.reshape(shape)
    return adj_matrix


class Graph:
    def __init__(self, adj_matrix, nodes):
        self._adj_matrix = None
        self._nodes = None

        self.adj_matrix = adj_matrix
        self.nodes = nodes

    @property
    def adj_matrix(self):
        return self._adj_matrix

    @adj_matrix.setter
    def adj_matrix(self, value):
        # TODO: validate and convert to nd.array
        self._adj_matrix = value

    @property
    def nodes(self):
        return self._nodes

    @nodes.setter
    def nodes(self, value):
        if len(value) != len(self.adj_matrix):
            raise ValueError("inconsistent #nodes {} vs {}".format(len(value), len(self.adj_matrix)))

        if len(value) < 1 or isinstance(value[0], GraphNode):
            self._nodes = value
        else:
            self._nodes = list(starmap(GraphNode, zip(
                [self]*len(value),
                range(len(value)),
                value,
            )))
            # fill in depth
            results = self.traverse(lambda _: True, executor=map)
            succ = map(attrgetter('success'), results)
            if not all(succ):
                succ = list(succ)
                raise ValueError('Fail to calculate depth for nodes of {}'.format(
                    list(compress(range(len(succ)), map(not_, succ)))))

    @property
    def size(self):
        return len(self)

    def __getstate__(self):
        logger.debug("Pickle dump {}({})".format(type(self).__name__, hex(id(self))))
        state = self.__dict__.copy()
        return state

    def __setstate__(self, state):
        self.__dict__.update(state)
        logger.debug("Pickle load {}({})".format(type(self).__name__, hex(id(self))))

    def __len__(self):
        """
        Support len() operator
        Calculate the number of nodes in the graph
        :return: number of nodes in the graph
        :rtype int
        """
        return len(self._nodes)

    def __getitem__(self, key):
        """
        Support [<key>] operator
        :param key: node index in the adjacent matrix
        :return: one or a list of graph node object(s) which associated to the key
        """
        if isinstance(key, (int, np.int32)):
            return itemgetter(key)(self.nodes)
        elif is_iterable(key):
            #return itemgetter(*key)(self.nodes)
            return list(map(lambda _: itemgetter(_)(self.nodes), key))
        else:
            raise TypeError("key in node[<key>] needs to be int or [int] instead of {}".format(type(key)))

    def dumps(self, dumper=None):
        """
        Serialize the object
        :param dumper: dump function, default to pickle.dumps
        :return:
        """
        if not dumper:
            dumper = pickle.dumps

        state = self.__getstate__()
        state["__cls__"] = type(self).__name__
        # pack adj_matrix for better serialize efficient and compatibility
        state["_adj_matrix"] = pack_adj_matrix(self._adj_matrix)
        # extract nodes's payload while others could be restored by rule.
        state["_nodes"] = list(map(attrgetter("obj"), self.nodes))
        return dumper(state)

    @classmethod
    def loads(cls, data, loader=None):
        """
        De-serialize the object
        :param data:
        :param loader: load function, default to pickle.loads
        :return:
        """
        if not loader:
            loader = pickle.loads

        state = loader(data)
        assert isinstance(state, dict) and "__cls__" in state, state
        assert state["__cls__"] == cls.__name__
        del state["__cls__"]

        # unpack adj_matrix from a more efficient and compatible format
        adj_matrix = unpack_adj_matrix(state.pop("_adj_matrix"))
        # nodes' payload
        nodes = state.pop("_nodes")

        obj = cls(np.array([]), [])

        # restore adj_matrix and nodes first smartly
        obj.adj_matrix = adj_matrix
        obj.nodes = nodes

        # restore other states
        obj.__setstate__(state)

        return obj

    @classmethod
    def hash_namespace(cls):
        # TODO: hard code a namespace UUID, put it into DARWIN_CONFIG?
        return "f8946b6b-8bf8-48d1-94b6-bb99d1c4d9ce"

    def uuid(self):
        # TODO: sort the adj_matrix and nodes with topo order first!!!
        seed = "{}_{}".format(repr(self.adj_matrix.tolist()), list(map(attrgetter("obj"), self.nodes)))
        return reduce_uuid(type(self).hash_namespace(), seed)

    def __hash__(self):
        # TODO: this is not safe map from UUID(128b) to integer(64b)
        return int.from_bytes(uuid.UUID(self.uuid()).bytes[:8], byteorder="little")

    # TODO: how to keep the order of parent nodes stable? sort by its edge id?
    def _get_parents(self, adjIdx):
        """
        Get the parent nodes' indexes in the adjacent matrix
        :param adjIdx: node id in the graph's adjacent matrix
        :return: a list of node index
        :rtype: numpy.ndarray([int])
        """
        return np.extract(np.sum(np.logical_not(np.isnan(self.adj_matrix[:, adjIdx].reshape(len(self), -1))), axis=1),
                          np.arange(0, len(self), dtype=np.int32))

    # TODO: how to keep the order of children node stable? sort by its edge id?
    def _get_children(self, adjIdx):
        """
        Get the children nodes' indexes in the adjacent matrix
        :param adjIdx: node id in the graph's adjacent matrix
        :return: a list of node index
        :rtype: numpy.ndarray([int])
        """
        return np.extract(np.sum(np.logical_not(np.isnan(self.adj_matrix[adjIdx, :].reshape(-1, len(self)))), axis=0),
                          np.arange(0, len(self), dtype=np.int32))

    def _get_roots(self):
        """
        Get graph's root nodes who has no parent.
        :return: a list of node index
        :rtype: numpy.ndarray([int])
        """
        depMatrix = np.logical_not(np.isnan(self.adj_matrix))
        v_num_inputs = np.sum(depMatrix, axis=0)
        v_candidate_node = (v_num_inputs == 0)
        return np.extract(v_candidate_node, np.arange(0, len(self), dtype=np.int32))

    @property
    def roots(self):
        return self[self._get_roots()]

    def _get_leaves(self):
        """
        Get graph's leaf nodes which has no children
        :return: a list of node index
        :rtype: numpy.ndarray([int])
        """
        depMatrix = np.logical_not(np.isnan(self.adj_matrix))
        v_num_outputs = np.sum(depMatrix, axis=1)
        v_candidate_node = (v_num_outputs == 0)
        return np.extract(v_candidate_node, np.arange(0, len(self), dtype=np.int32))

    @property
    def leaves(self):
        return self[self._get_leaves()]

    def traverse(self, hook, executor=map, earlyStop=True, catch_exceptions=[]):
        """
        Traverse the graph from input(root) to output(leaf) with hook(adjIdx, deepth, graph, ...)
        :param hook: Hook function when visiting a node
        :type hook: Callable
        :param executor: function to execute the hook in bulk either serial, parallel or distribute, such as builtin map
        :type executor: Callable
        :param earlyStop: stop traverse if any error hit or finish remains, default to True to prevent error
        :type earlyStop: bool
        :param catch_exceptions: a list of exception types which needs to be catched during hook run instead of abort
        :type catch_exceptions: list
        :return:
        """
        f_get_non_callable_obj_mask = f_compose_r(
            partial(map, lambda obj: obj is not None and not callable(obj)),
            list,
        )
        f_get_non_callable_items = lambda spec_obj, mask: f_compose_r(
            partial(
                compress,
                zip(
                    type(spec_obj)._fields,
                    map(lambda _: type(_).__name__, spec_obj),
                ),
            ),
            list,
        )(mask)

        # flat the hooks
        if isinstance(hook, TraverseHookSpec):
            non_callable_mask = f_get_non_callable_obj_mask(hook)
            if any(non_callable_mask):
                raise TypeError("graph traverse hooks needs to be all Callable while followings are not: {}".format(
                    f_get_non_callable_items(hook, non_callable_mask)))
            workload_hook, prep_hook, post_hook = hook
        else:
            workload_hook = hook
            prep_hook = None
            post_hook = None

        # flat the executors
        if isinstance(executor, TraverseEngineSpec):
            non_callable_mask = f_get_non_callable_obj_mask(executor)
            if any(non_callable_mask):
                raise TypeError("graph traverse executors needs to be all Callable while followings are not: {}".format(
                    f_get_non_callable_items(executor, non_callable_mask)))
            workload_executor, prep_executor, post_executor = executor
        else:
            workload_executor = executor
            prep_executor = map if prep_hook else None
            post_executor = map if post_hook else None

        if not callable(workload_hook):
            raise ValueError('{} hook parameter needs to be Callable instead of {}'.format(
                get_current_function_name(), type(hook).__name__))

        if not callable(workload_executor):
            raise ValueError('{} executor parameter needs to be Callable instead of {}'.format(
                get_current_function_name(), type(hook).__name__))

        # wrap hook to catch desired exceptions
        wrapped_workload_hook = partial(
            _wrap_with_catch_exception,
            hook=workload_hook, catch_exceptions=catch_exceptions,
            # enable log_func will activate the real first eyewitness of any error from hook.
            # but that's really duplicate since it will be reported in graph traverse main routing
            # and, invoker client side, also.
            #log_func=logger.debug,
            log_func=None,
        )

        num_nodes = len(self.adj_matrix)
        # nan means no link in the adjacent matrix
        depMatrix = np.logical_not(np.isnan(self.adj_matrix))

        # ------------------------------------------------------------
        # traverse depMatrix from start(inputs) to end(outputs).
        # markup those nodes which had been processed
        v_done = np.zeros(num_nodes, dtype=bool)
        depth_not_ready = False
        depth = 0
        succ = True
        results = [ExceptCatchRunResult(success=None, result=None)]*num_nodes
        while True:
            # calculate dependency
            v_num_inputs = np.sum(depMatrix, axis=0)

            # calculate next level(depth) nodes
            v_candidate_node = np.logical_xor(v_num_inputs == 0, v_done)

            # get index of next level(depth) nodes
            v_candidate_node_index = np.extract(v_candidate_node, np.arange(0, num_nodes, dtype=np.int32))

            # get graph node objects of next level(depth)
            candidate_nodes = f_compose_r(
                partial(map, partial(getitem, self)),
                list
            )(v_candidate_node_index)

            # process next level nodes
            cnt = 0

            # set depth in the first time
            if depth_not_ready or (len(candidate_nodes) > 0 and candidate_nodes[0].depth is None):
                depth_not_ready = True

                _ = f_compose_r(
                    partial(map, lambda _: setattr(_, "depth", depth)),
                    list,
                )(candidate_nodes)

            # compose input data for workload hook
            nodes = f_compose_r(
                partial(map, f_compose_r(
                    attrgetter("id", "depth", "obj"),
                    lambda _: TraverseHookInputNodeSpec(*_),
                )),
                list,
            )(candidate_nodes)

            # customized prep process
            if prep_hook:
                #_ = list(map(logger.debug, nodes))
                nodes = f_compose_r(
                    partial(
                        prep_executor,
                        prep_hook,
                    ),
                    list,
                )(nodes)
                #logger.debug("-----------------------------")
                #_ = list(map(logger.info, nodes))
                #logger.debug("------------#####------------")

            # ENGINE START...
            # NOTE: wrapped_results are from catch_exception_run wrapped hook
            wrapped_results = f_compose_r(
                partial(
                    workload_executor,
                    wrapped_workload_hook,
                ),
                list,
            )(nodes)

            succ_mask = f_compose_r(
                partial(map, attrgetter("success")),
                list,
            )(wrapped_results)

            # pick up finished results
            finished_results = f_compose_r(
                partial(zip, nodes),
                lambda seq: compress(seq, map(lambda _: _ is not None, succ_mask)),
                list,
            )(wrapped_results)
            #_ = list(map(logger.debug, finished_results))

            # count on finished results
            cnt += len(finished_results)

            def record_finished(item):
                node, wrapped_result = item
                idx = node.id

                # save succ results
                results[idx] = wrapped_result
                #logger.debug("results[{}] = {}".format(idx, wrapped_result))

                # remove dependencies to the processed node
                depMatrix[idx] = False
                v_done[idx] = True

            # customized post process
            if post_hook:
                if len(finished_results) == 0:
                    finished_nodes = []
                    finished_wrapped_results = []
                else:
                    finished_nodes, finished_wrapped_results = zip(*finished_results)

                finished_results = f_compose_r(
                    # extract real result from raw workload_hook instead of catch_except_run wrapped
                    partial(map, attrgetter("result")),
                    # combine with graph node again
                    partial(zip, finished_nodes),
                    # call post_hook
                    partial(
                        post_executor,
                        partial(f_star_call, post_hook),
                    ),
                    # re-wrap the post processed result as catch_error_run result
                    partial(zip, map(attrgetter("success"), finished_wrapped_results)),
                    partial(starmap, ExceptCatchRunResult),
                    # reconstruct the finished_results list by appending node prefix
                    partial(zip, finished_nodes),
                    list,
                )(finished_wrapped_results)
                #logger.debug("-----------------------------")
                #_ = list(map(logger.info, finished_results))
                #logger.debug("------------#####------------")

            # builtin post process
            _ = f_compose_r(
                partial(
                    map,
                    f_compose_r(
                        record_finished,
                    ),
                ),
                list,
            )(finished_results)

            # handle failed call
            if not all(succ_mask):
                failed_results = f_compose_r(
                    partial(zip, nodes),
                    lambda seq: compress(seq, map(lambda _: _ is False, succ_mask)),
                    list,
                )(wrapped_results)

                # log for debug
                for node, wrapped_result in failed_results:
                    idx, depth = attrgetter("id", "depth")(node)

                    logger.warning("--------------------------------------------------------------")
                    logger.warning("First eyewitness of graph traverse hook invoking error:")
                    for item, msg in zip(["hookFunc", "adj_index", "depth"],
                                         [repr(hook), idx, depth]):
                        logger.warning(">> {}: {}".format(item, msg))
                    for item, msg in zip(
                            ["exc", "tb"],
                            [str(wrapped_result.result.exception), wrapped_result.result.traceback]):
                        for i, line in enumerate(msg.splitlines()):
                            logger.warning(">> {}[{}]: {}".format(item, i, line))
                    logger.warning("--------------------------------------------------------------")

                if earlyStop and len(failed_results) > 0:
                    succ = False
                    break

            # traverse terminated when no candidate was found
            if cnt == 0:
                break
            elif earlyStop and not succ:
                logger.warning('Eearly stop')
                break
            else:
                depth += 1
        return results


class GraphNode:
    def __init__(self, graph, id, obj):
        self._graph = graph
        self._id = id
        self._depth = None
        self._obj = obj

    def __getstate__(self):
        logger.debug("Pickle dump {}({})".format(type(self).__name__, hex(id(self))))
        state = self.__dict__.copy()
        return state

    def __setstate__(self, state):
        self.__dict__.update(state)
        logger.debug("Pickle load {}({})".format(type(self).__name__, hex(id(self))))

    @property
    def graph(self):
        return self._graph

    @graph.setter
    def graph(self, value):
        if isinstance(value, Graph):
            self._graph = value
        else:
            raise ValueError('GraphNode.graph needs to be a Graph object instead of {}'.format(
                type(value).__name__,
            ))

    @property
    def id(self):
        return self._id

    @id.setter
    def id(self, value):
        self._id = value

    @property
    def obj(self):
        return self._obj

    @property
    def depth(self):
        return self._depth

    @depth.setter
    def depth(self, value):
        self._depth = value

    # TODO: not used yet
    @property
    def parents(self):
        """
        Get the parents of current graph node in the graph
        :return: a list of graphNode objects
        """
        ids = self.graph._get_parents(self.id)
        return self.graph[ids]

    # TODO: not used yet
    @property
    def children(self):
        """
        Get the children of current graph node in the graph
        :return: a list of graphNode objects
        """
        ids = self.graph._get_children(self.id)
        return self.graph[ids]
