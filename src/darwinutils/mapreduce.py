#! /usr/bin/env python3
# -*- coding:utf-8 -*-

from functools import partial
from concurrent.futures import ThreadPoolExecutor as _threadPoolExecutor
from concurrent.futures import ProcessPoolExecutor as _processPoolExecutor
from darwinutils.log import get_task_logger
from collections import Callable
from darwinutils.config import DARWIN_CONFIG
import sys, zlib, pickle


logger = get_task_logger(__name__)


"""
NOTE:
------------------------------------------------------------------------------------------------------------
1) The parameters, especially the function to be paralleled, for multi-process run needs to be pickable.
------------------------------------------------------------------------------------------------------------
https://docs.python.org/3/library/pickle.html#what-can-be-pickled-and-unpickled
>> The following types can be pickled:
>>
>> * None, True, and False
>> * integers, floating point numbers, complex numbers
>> * strings, bytes, bytearrays
>> * tuples, lists, sets, and dictionaries containing only picklable objects
>> * functions defined at the top level of a module (using def, NOT lambda)
>>                                                   ^^^^^^^^^^^^^^^^^^^^^
>> * built-in functions defined at the top level of a module
>> * classes that are defined at the top level of a module
>> * instances of such classes whose __dict__ or the result of calling __getstate__() is picklable (see section Pickling Class Instances for details).
"""


def _parallel_map(*args, executor_t=None, flatter=None, **kwargs):
    if executor_t is None:
        executor_t = _processPoolExecutor
    elif not callable(executor_t):
        raise ValueError('executor type needs to be callable instread of {}'.format(type(executor_t)))

    executor_kwargs = {}
    if "max_workers" in kwargs:
        executor_kwargs["max_workers"] = kwargs.pop("max_workers")

    # enforce flattening the map output if timeout was requested because the Timeout only works within the "with" ctx.
    if kwargs.get("timeout", 0) > 0 and flatter is None:
        flatter = list

    with executor_t(**executor_kwargs) as executor:
        r = executor.map(*args, **kwargs)
        if callable(flatter):
            r = flatter(r)
    return r


parallel_map_t = partial(_parallel_map, executor_t=_threadPoolExecutor)
parallel_map_p = partial(_parallel_map, executor_t=_processPoolExecutor)


def starmap_functor(starmap_func, args, type):
    """
    Wrap the input arguments to starmap function based on two types:
    type 1: starmap_func(*args)
    type 2: starmap_func(*args, **kwargs)
    :param starmap_func:
    :param args:
    :param type:
    :return:
    """
    if not isinstance(starmap_func, Callable):
        raise ValueError('Parameter "starmap_func" should be a method instead of {}'.format(type(starmap_func)))
    if not isinstance(args, (tuple, list)):
        raise ValueError('Parameter "args" should be a tuple or list instead of {}'.format(type(args)))

    #logger.debug('type={} args={}'.format(type, args))

    # TYPE 1
    if type == 1:
        return starmap_func(*args)

    # TYPE 2
    args_len = len(args)
    if args_len > 2 or args_len < 1:
        raise ValueError('Parameter "args" should follow "(*args{{, **kwargs}})" while current length is {}'.format(args_len))
    if args_len == 2:
        return starmap_func(*args[0], **args[1])
    return starmap_func(*args[0], **{})


def _parallel_starmap(fn, *args, type=1, **kwargs):
    return _parallel_map(partial(starmap_functor, fn, type=type), *args, **kwargs)


def gen_parallel_param_lst(to_be_sliced_lst, *args, data_as_param=False, compress_enable_flag=False, **kwargs):
    end_point = 0
    param_lst = []

    if isinstance(to_be_sliced_lst, tuple):
        data_len = list(map(lambda s: len(s), to_be_sliced_lst))
        if len(set(data_len)) != 1:
            logger.error("Not samed length of each list in tuple {}".format(data_len))
            return None
    else:
        to_be_sliced_lst = [to_be_sliced_lst]
    for idx in range(int(len(to_be_sliced_lst[0]) / DARWIN_CONFIG["data_parallel_block_size"]) +
                     (len(to_be_sliced_lst[0]) % DARWIN_CONFIG["data_parallel_block_size"] > 0) * 1):
        start_point = end_point
        end_point = min(end_point + DARWIN_CONFIG["data_parallel_block_size"], len(to_be_sliced_lst[0]) + 1)
        # Note, we must pass the idx even it is not used in the function since some function may need it to reduce the result to keep
        # the sequence. multiple thread return can not guaranty the return sequence so we need the idx to understand the logic sequence
        # Another point is that we shall avoid put the real list as parameter. start2map or starmap has the limitation for parameter len
        # it is better that for the long list, we only pass the index and the parallel implemeted function can access the self.data or
        # a global data
        if data_as_param:
            to_be_sliced_lst_param = list(map(lambda s: s[start_point:end_point], to_be_sliced_lst))
            if compress_enable_flag:
                if sys.getsizeof(to_be_sliced_lst_param) >= DARWIN_CONFIG.get("data_block_compress_size"):
                    kwargs["compress_flag"] = True
                    to_be_sliced_lst_param = list(map(lambda s: zlib.compress(pickle.dumps(s[start_point:end_point])), to_be_sliced_lst))
                    param_lst.append([[idx, *to_be_sliced_lst_param, *args], kwargs])
                else:
                    kwargs["compress_flag"] = False
                    param_lst.append([[idx, *to_be_sliced_lst_param, *args], kwargs])
            else:
                param_lst.append([[idx, *to_be_sliced_lst_param, *args], kwargs])
        else:
            param_lst.append([[idx, (start_point, end_point), *args], kwargs])
    return param_lst


parallel_starmap_t = partial(_parallel_starmap, type=1, executor_t=_threadPoolExecutor)
parallel_starmap_p = partial(_parallel_starmap, type=1, executor_t=_processPoolExecutor)
parallel_star2map_t = partial(_parallel_starmap, type=2, executor_t=_threadPoolExecutor)
parallel_star2map_p = partial(_parallel_starmap, type=2, executor_t=_processPoolExecutor)
