# -*- coding: utf-8 -*-


def get_celery_app(cfg_obj='conf.celery_config.BaseConfig'):
    """
    Create celery app object with a customized config
    refer to: https://stackoverflow.com/questions/4763072/why-cant-it-find-my-celery-config-file
    :param cfg_obj: Object is either an actual object or the name of a module to import.
    :type cfg_obj: object or model str
    :return: celery app object
    :rtype Celery
    """
    from celery import current_app
    from celery.app import default_app
    if current_app == default_app:
        from celery import Celery
        celery = Celery()
        celery.config_from_object(cfg_obj)
    else:
        celery = current_app

    return celery

