#! /usr/bin/env python3
# -*- coding:utf-8 -*-


from functools import (partial, reduce)


def f_echo(*args, **kwargs):
    if len(args) == 1 and len(kwargs) < 1:
        result = args[0]

    # special case to downgrade kwargs to args
    elif len(args) == 0 and len(kwargs) == 1:
        result, *_ = kwargs.values()

    elif len(kwargs) < 1:
        result = args

    else:
        result = args, kwargs

    return result


def f_call(f, *args, **kwargs):
    return f(*args, **kwargs)


def f_flip_call(f, *args, **kwargs):
    return f(*args[::-1], **kwargs)


def f_si_call(f, *args, **kwargs):
    return f()


def f_star_call(f, x):
    return f(*x)


def f_unstar_call(f, *args):
    return f(args)


def f_star2_call(f, x):
    if isinstance(x, dict):
        args = []
        kwargs = x
    else:
        args, kwargs = x
    return f(*args, **kwargs)


def f_unstar2_call(f, *args, **kwargs):
    return f([args, kwargs])


def f_group_call(functions, *args, map_functor=map, **kwargs):
    return partial(map_functor, partial(f_flip_call, f_star2_call, [args, kwargs]))(functions)


def f_group(*functions, map_functor=map):
    return partial(f_group_call, functions, map_functor=map_functor)


def _pee9shushapi4ubohz9ji7eih7daedohgoheexaeb9noutiesu4eek9quei7mohb_1(x=None, f=None, g=None):
    """ Internal functions, you do not need to remember it """
    return g(f(x))


def _uong4avoh3aekiqueeghac4quah3upeikohhiecooyahpei9waquahgh9cheiwej_r(f, g):
    """ Internal functions, you do not need to remember it """
    return partial(_pee9shushapi4ubohz9ji7eih7daedohgoheexaeb9noutiesu4eek9quei7mohb_1, f=f, g=g)


def _zai3eeyish7ge7pheighee9haengoisiuzaijahpah9saesoixahwei9xoch3ohr_l(f, g):
    """ Internal functions, you do not need to remember it """
    return partial(_pee9shushapi4ubohz9ji7eih7daedohgoheexaeb9noutiesu4eek9quei7mohb_1, f=g, g=f)


def f_compose_r(*functions):
    """
    Function composer, right to left
    :param functions:
    :return:
    """
    return reduce(
        _uong4avoh3aekiqueeghac4quah3upeikohhiecooyahpei9waquahgh9cheiwej_r,
        functions,
        f_echo)


def f_compose_l(*functions):
    """
    Function composer, left to right
    :param functions:
    :return:
    """
    return reduce(
        _zai3eeyish7ge7pheighee9haengoisiuzaijahpah9saesoixahwei9xoch3ohr_l,
        functions,
        f_echo)


def f_unpack_compose_r(*functions):
    """
    Function composer, right to left, unpack input arguments
    :param functions:
    :return:
    """
    return reduce(
        _uong4avoh3aekiqueeghac4quah3upeikohhiecooyahpei9waquahgh9cheiwej_r,
        map(partial(partial, f_star_call), functions),
        f_echo)


def f_unpack_compose_l(*functions):
    """
    Function composer, left to right, unpack input arguments
    :param functions:
    :return:
    """
    return reduce(
        _zai3eeyish7ge7pheighee9haengoisiuzaijahpah9saesoixahwei9xoch3ohr_l,
        map(partial(partial, f_star_call), functions),
        f_echo)


def f_list_rize(*args, _num_args=None):
    """
    List rize the input args:
    example:
    * f() -> []
    * f(1) -> [1]
    * f(1,2) -> [1,2]
    * f([1]) -> [[1]]
    * f([1], _num_args=1) -> [1], keep the behavior as attrgetter(0)([[1]])==[1]
    :param args:
    :param _num_args: do not re-list-rize a list when setting to 1
    :return:
    """
    # special case if caller actually don't want to list-rize.
    if _num_args == 1 and len(args) == 1 and isinstance(args[0], (list, tuple)):
        r = args[0]
    else:
        r = [*args]
    return r


def f_squeeze(data):
    while isinstance(data, (list, tuple)):
        # NOTE: empty list is also a terminal.
        if len(data) != 1:
            break
        data = data[0]
    return data


def irevert(iterable):
    """
    Revert an iterable
    :param iterable:
    :return:
    """
    '''
    from functools import reduce
    return reduce(lambda x, y: [y, *x], iterable, initial=[])
    '''
    return list(iterable)[::-1]


def f_timeit_call(f, *args, **kwargs):
    import time
    time_beg = time.time()
    r = f(*args, **kwargs)
    time_end = time.time()
    return r, time_end-time_beg


def f_zip_call(functions, arg_iterable, map_functor=map):
    if len(functions) != len(arg_iterable):
        raise ValueError("f_zip_call needs same number of <func, arg> pairs instead of {}. vs {}".format(
            len(functions), len(arg_iterable),
        ))
    return f_compose_r(
        partial(f_star_call, zip),
        partial(map_functor, partial(f_star_call, f_call)),
    )([functions, arg_iterable])


def f_zip(*functions, map_functor=map):
    return partial(f_zip_call, functions, map_functor=map_functor)


def i_cut(it, s):
    def _it_wrap(_it):
        while True:
            yield next(_it)[s]
    return _it_wrap(it)


def i_split(it, thread_safe=False):
    from itertools import (tee, chain, starmap)
    from darwinutils.decorator import threadsafe_iter
    it = iter(it)
    items = next(it)
    it = chain([items], it)
    return f_compose_r(
        # if required, use threadsafe_iter in case splitted iterators might be used by different threads.
        threadsafe_iter if thread_safe else iter,
        partial(f_flip_call, tee, len(items)),
        partial(f_flip_call, zip, range(len(items))),
        partial(starmap, i_cut),
        list,
    )(it)


def i_epoch(it, epoch=1):
    from itertools import (chain, tee)
    from collections import Iterator
    if not isinstance(it, Iterator):
        it = iter(it)
    return chain.from_iterable(tee(it, epoch))


def i_batch(it, batch=1, ignore_rest=False):
    from collections import Iterator
    if not isinstance(it, Iterator):
        it = iter(it)
    while True:
        try:
            seq = []
            for i in range(batch):
                seq.append(next(it))
            yield seq

        except StopIteration as e:
            if not ignore_rest and len(seq) > 0:
                yield seq
            break
