# -*- coding: utf-8 -*-
"""Create and return logger object for different scenarios
"""


# TODO: put DARWIN_CONFIG import above other import even the sys libs
# TODO: reserve for monkey patch which was not mandatory yet. is that really required?
from darwinutils.config import DARWIN_CONFIG
import sys
from functools import (
    lru_cache,
)
import traceback
import logging
import logging.config
from multiprocessing_logging import install_mp_handler
from conf.logging_config import logging_config
logging.config.dictConfig(logging_config)


def get_flask_logger():
    """
    get logger for a flask based restful service
    this must be called in the flask's app context, such as inside blueprint definition

    :return: logger object
    :rtype: object
    """
    from flask import current_app
    return current_app.logger


@lru_cache(maxsize=500)
def get_task_logger(name, service_name="darwin_service"):
    """
    get a logger for a celery worker task module

    :param name: module name where this function was invoked
    :type name: str
    :return: logger object
    :rtype: object
    """
    # TODO: migrate to use load_module_from_func_spec in helper
    """
    my_celery = getattr(__import__("darwin_service_data_extractor", fromlist=["celery_app"]), "celery_app")
    if my_celery.current_task:
        try:
            from celery.utils.log import get_task_logger as upstream_get_task_logger
            logger = upstream_get_task_logger(name)
        except AttributeError:
            exc_type, exc_value, exc_traceback = sys.exc_info()
            error_str = traceback.format_exception(exc_type, exc_value, exc_traceback)
            print("Failed to get logger handler. Task: {} and Attribute list: {}  Reason: [{}]".format(my_celery.current_task,
                                                                                                       dir(my_celery), "".join(error_str)))
            logger = logging.getLogger(name)
    else:
        logger = logging.getLogger(name)
    """
    logger = logging.getLogger(name)
    install_mp_handler(logger)
    return logger
