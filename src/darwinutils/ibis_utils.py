#! /usr/bin/env python3
# -*- coding: utf-8 -*-

"""
The utils is used to connect impala, hbase and hive
"""
import os
from abc import ABC
import traceback
from time import time
from collections import namedtuple
import pandas as pd
from pyhive import hive
import ibis
from darwinutils.log import get_task_logger
from darwinutils.config import DARWIN_CONFIG
logger = get_task_logger(__name__)

DBConnectorPair = namedtuple('DBConnectorPair', ['name', 'obj'])


def check_thrift_sasl_version():
    rst = os.popen("conda list | grep thrift_sasl")
    version_str = rst.read()
    if version_str is None or len(version_str) == 0:
        logger.warning("Cannot detect thrift_sasl and force to 'thrift_sasl               0.2.1                    py36_0'")
        version_str = "thrift_sasl               0.2.1                    py36_0"
    return version_str


class ABCDBConnector(ABC):
    def connect(self):
        pass

    def connect_database(self, database_name=None):
        pass

    def create_database(self, database_name):
        logger.error("In the project, we shall not create database")
        pass

    def drop_database(self, database_name):
        logger.error("In the project, we shall not create database")
        pass

    def create_table(self, table_name, database=None, table_obj=None, **kwargs):
        pass

    def connect_table(self, table_name, database=None):
        pass

    def exists_table(self, table_name, database=None):
        pass

    def get_table_schema(self, table_name, database=None):
        pass

    def list_databases(self, like=None):
        pass

    def list_tables(self, like=None, database=None):
        pass

    def fetch(self, sql_str, limit=None):
        pass

    def insert(self, dataframe, overwrite=False):
        pass

    def raw_sql(self, sql_str, fetch_rst=True, onebyone=False):
        pass

    def impala_table_refresh(self, table_name, database=None):
        pass

    def column_stats(self):
        pass

    def truncate_table(self, table_name, database=None):
        pass

    def drop_table(self, table_name, database=None):
        pass


class DBConnector(ABCDBConnector):
    def __init__(self, hdfs_host, hdfs_port, hive_host, hive_port, impala_host, impala_port, user, passwd,
                 database=None, auth_mechanism="PLAIN", use_hive=False):
        # Now, if we use impala, we must use thrift_sasl 0.2.1 but if we use hive, we need use a higher level thrift_sasl
        # they cannot be used at the same time.
        if use_hive:
            if check_thrift_sasl_version().find("0.2.1") > 0:
                logger.warning("Suggest use Impala but not hive over thrift_sasl 0.2.1")
            logger.info("Using Hive connector")
            self._use_hive = True
            self._db_conn = HiveConnector(hive_host, hive_port, database=database)
        else:
            if check_thrift_sasl_version().find("0.2.1") < 0:
                logger.warning("Using Impala connector but the thrift_sasl version is not right. Please careful")
            logger.info("Using Impala connector")
            self._use_hive = False
            self._db_conn = ImpalaConnector(hdfs_host=hdfs_host, hdfs_port=hdfs_port, impala_host=impala_host,
                                            impala_port=impala_port, user=user, passwd=passwd, database=database,
                                            auth_mechanism=auth_mechanism)

    def __del__(self):
        del self._db_conn

    def connect(self):
        return self._db_conn.connect()

    def connect_database(self, database_name=None):
        return self._db_conn.connect_database(database_name)

    def create_table(self, table_name, database=None, table_obj=None, **kwargs):
        return self._db_conn.create_table(table_name, database, table_obj, **kwargs)

    def connect_table(self, table_name, database=None):
        return self._db_conn.connect_table(table_name, database)

    def exists_table(self, table_name, database=None):
        return self._db_conn.exists_table(table_name, database)

    def get_table_schema(self, table_name, database=None):
        return self._db_conn.get_table_schema(table_name, database)

    def list_databases(self, like=None):
        return self._db_conn.list_databases(like)

    def list_tables(self, like=None, database=None):
        return self._db_conn.list_tables(like, database)

    def fetch(self, sql_str, limit=None):
        return self._db_conn.fetch(sql_str, limit)

    def insert(self, dataframe, overwrite=False):
        return self._db_conn.insert(dataframe, overwrite)

    def raw_sql(self, sql_str, fetch_rst=True, onebyone=False):
        return self._db_conn.raw_sql(sql_str, fetch_rst, onebyone=onebyone)

    def impala_table_refresh(self, table_name, database=None):
        return self._db_conn.impala_table_refresh(table_name, database)

    def column_stats(self):
        return self._db_conn.column_stats()

    def truncate_table(self, table_name, database=None):
        return self._db_conn.truncate_table(table_name, database)

    def drop_table(self, table_name, database=None):
        return self._db_conn.drop_table(table_name, database)


class ImpalaConnector(ABCDBConnector):
    def __init__(self, hdfs_host, hdfs_port, impala_host, impala_port, user, passwd,
                 database=None, auth_mechanism="PLAIN"):
        self._impala_host_name = impala_host
        self._impala_port = impala_port
        if isinstance(hdfs_host, str):
            self._hdfs_host_name = [hdfs_host]
        else:
            self._hdfs_host_name = hdfs_host
        self._hdfs_port = hdfs_port
        self._impala_user = user
        self._impala_password = passwd
        self._auth_mechanism=auth_mechanism
        self._hdfs_conn = None
        self._impala_conn = None
        self._max_excute_time = 0
        self._max_excute_time_cmd = ''
        self._impala_database_obj = DBConnectorPair(name=database, obj=None)
        self._impala_table_obj = DBConnectorPair(name='', obj=None)

    def __del__(self):
        if self._impala_conn is not None:
            self._impala_conn.close()

    def connect(self):
        if self._hdfs_conn is None:
            for hdfs_host in self._hdfs_host_name:
                try:
                    self._hdfs_conn = ibis.hdfs_connect(host=hdfs_host, port=self._hdfs_port,
                                                        timeout=DARWIN_CONFIG.get("pipeline_task_default_retry_timeout", 1800))
                    if self._hdfs_conn is not None:
                        self._hdfs_conn.ls("/")  # test connection
                        break
                except Exception as e:
                    self._hdfs_conn = None
        if self._hdfs_conn is None:
            logger.error("Failed to connect hdfs server by config: {} {}".format(self._hdfs_host_name, self._hdfs_port))
            return False

        if self._impala_conn is None:
            try:
                self._impala_conn = ibis.impala.connect(host=self._impala_host_name, port=self._impala_port,
                                                        hdfs_client=self._hdfs_conn, database=self._impala_database_obj.name,
                                                        user=self._impala_user, password=self._impala_password,
                                                        auth_mechanism=self._auth_mechanism,
                                                        timeout=DARWIN_CONFIG.get("pipeline_task_default_retry_timeout", 1800))
            except Exception as e:
                logger.exception("Connect Impala {}:{} for table {} by user {} failed with reason {}".format(
                    self._impala_host_name, self._impala_port, self._impala_database_obj.name, self._impala_user, str(e)))
                return False
        try:
            lst_dbs = self._impala_conn.list_databases()
            if self._impala_database_obj.name is not None:
                if self._impala_database_obj.name not in lst_dbs:
                    logger.warning("Required database {} is not in {} of Impala server {}:{}".format(self._impala_database_obj.name,
                                                                                                     lst_dbs,
                                                                                                     self._impala_host_name,
                                                                                                     self._impala_port))
        except Exception as e:
            logger.exception("Impala {}:{} access by user {} failed with reason {}".format(self._impala_host_name,
                                                                                           self._impala_port,
                                                                                           self._impala_user, str(e)))
            return False

        # initiate database part
        if self._impala_database_obj.name is not None:
            self.connect_database()
        return True

    def connect_database(self, database_name=None):
        succ = True
        if database_name == self._impala_database_obj.name:
            if self._impala_database_obj.name is not None:
                self._impala_database_obj = DBConnectorPair(name=database_name,
                                                            obj=self._impala_conn.database(database_name))
            else:
                logger.error("No valid database name for connect_database")
                succ = False
        else:
            if database_name is not None:
                self._impala_database_obj = DBConnectorPair(name=database_name,
                                                            obj=self._impala_conn.database(database_name))
            else:
                if self._impala_database_obj.obj is None:
                    self._impala_database_obj = DBConnectorPair(name=self._impala_database_obj.name,
                                                                obj=self._impala_conn.database(self._impala_database_obj.name))
        return succ

    def create_table(self, table_name, database=None, table_obj=None, **kwargs):
        if table_name is None:
            logger.error("table_name cannot be None")
            return False

        if database is not None:
            if not self.connect_database(database_name=database):
                logger.error("Failed to connect database {}".format(database))
                return False
        self._impala_database_obj.obj.create_table(table_name, table_obj, **kwargs)
        self.connect_table(table_name, database)
        return True

    def connect_table(self, table_name, database=None):
        if table_name is None:
            logger.error("table_name cannot be None")
            return None

        if database is not None:
            if not self.connect_database(database_name=database):
                logger.error("Failed to connect database {}".format(database))
                return None

        if table_name == self._impala_table_obj.name:
            if self._impala_table_obj.obj is None:
                self._impala_table_obj = DBConnectorPair(name=table_name,
                                                         obj=self._impala_database_obj.obj.table(table_name))
        else:
            self._impala_table_obj = DBConnectorPair(name=table_name,
                                                     obj=self._impala_database_obj.obj.table(table_name))
        return self._impala_table_obj.obj

    def exists_table(self, table_name, database=None):
        if table_name is None:
            logger.error("table_name cannot be None")
            return False
        if self._impala_conn is not None:
            if database is not None:
                try:
                    flag = self._impala_conn.exists_table(table_name, database)
                except Exception as e:
                    logger.exception("exists_table failed with reason [{}]".format(str(e)))
                    flag = False
            elif self._impala_database_obj.name is not None:
                try:
                    flag = self._impala_conn.exists_table(table_name, self._impala_database_obj.name)
                except Exception as e:
                    logger.exception("exists_table failed with reason [{}]".format(str(e)))
                    flag = False
            else:
                logger.info("Database is not initialzed or passed")
                flag = False
        else:
            logger.error("Impala connection is not initialized")
            flag = False
        return flag

    def get_table_schema(self, table_name, database=None):
        if table_name is None:
            logger.error("table_name cannot be None")
            return None
        if self._impala_conn is not None:
            if database is not None:
                return self._impala_conn.get_schema(table_name, database)
            elif self._impala_database_obj.name is not None:
                return self._impala_conn.get_schema(table_name, self._impala_database_obj.name)
            else:
                logger.info("Database is not initialzed or passed")
                return None
        else:
            logger.info("Impala connection is not initialized")
            return None

    def list_databases(self, like=None):
        if self._impala_conn is not None:
            return self._impala_conn.list_databases(like=like)
        else:
            logger.info("Impala connection is not initialized")
            return None

    def list_tables(self, like=None, database=None):
        if self._impala_conn is not None:
            if database is not None:
                return self._impala_conn.list_tables(like=like, database=database)
            elif self._impala_database_obj.obj is not None:
                return self._impala_conn.list_tables(like=like, database=self._impala_database_obj.name)
            else:
                return self._impala_conn.list_tables(like=like)
        else:
            logger.info("Impala connection is not initialized")
            return None

    def fetch(self, sql_str, limit=None):
        if self._impala_conn is not None:
            cur = self._impala_conn.sql(sql_str)
            return cur.execute(limit=limit)
        else:
            logger.info("Impala table connection is not initialized")
            return None

    def insert(self, dataframe, overwrite=False):
        if not isinstance(dataframe, pd.DataFrame):
            logger.error("Only support insert pandas dataframe for impala")
            return False
        if self._impala_table_obj.obj is not None:
            self._impala_table_obj.obj.insert(obj=dataframe, overwrite=overwrite)
            return True
        else:
            logger.error("Impala table connection is not initialized")
            return False

    def raw_sql(self, sql_str, fetch_rst=True, onebyone=False):
        if not isinstance(sql_str, str):
            logger.error("raw_sql {} is not str type".format(sql_str))
            return False, None
        flag = True
        if self._impala_conn is not None:
            cur = self._impala_conn.raw_sql(sql_str, results=fetch_rst)
            if cur is not None:
                try:
                    if not onebyone:
                        rst = cur.fetchall()
                    else:
                        rst = []
                        while True:
                            tmp = cur.fetchone()
                            if tmp is None:
                                # read all
                                break
                            rst.append(tmp)
                except Exception as e:
                    logger.exception("fetchmany failed {} by sql {} {}".format(str(e), sql_str, dir(cur)))
                    flag = False
                    rst = None

            else:
                rst = None
            try:
                cur.release()
            except:
                pass
            if fetch_rst:
                return flag, rst
            else:
                return flag, None
        else:
            logger.error("Impala connection is not initialized")
            return False, None

    def impala_table_refresh(self, table_name, database=None):
        if table_name is None:
            logger.error("table_name cannot be None")
            return False
        if self._impala_conn is not None:
            self.connect_table(table_name, database=database)
            self._impala_table_obj.obj.refresh()
            return True
        else:
            logger.error("Impala table connection is not initialized")
            return False

    def column_stats(self):
        if self._impala_table_obj.obj is not None:
            return self._impala_table_obj.obj.column_stats()
        else:
            logger.info("Impala table connection is not initialized")
            return None

    def truncate_table(self, table_name, database=None):
        if table_name is None:
            logger.error("table_name cannot be None")
            return False
        flag = True
        if self._impala_conn is not None:
            if database is not None:
                try:
                    self._impala_conn.truncate_table(table_name, database)
                except Exception as e:
                    logger.exception("truncate_table failed with reason [{}]".format(str(e)))
                    flag = False
            elif self._impala_database_obj.name is not None:
                try:
                    self._impala_conn.truncate_table(table_name, self._impala_database_obj.name)
                except Exception as e:
                    logger.exception("truncate_table failed with reason [{}]".format(str(e)))
                    flag = False
            else:
                logger.info("Database is not initialzed or passed")
                flag = False
        else:
            logger.error("Impala connection is not initialized")
            flag = False
        return flag

    def drop_table(self, table_name, database=None):
        if table_name is None:
            logger.error("table_name cannot be None")
            return False
        flag = True
        if self._impala_conn is not None:
            if database is not None:
                try:
                    self._impala_conn.drop_table(table_name, database)
                except Exception as e:
                    logger.exception("drop_table failed with reason [{}]".format(str(e)))
                    flag = False
            elif self._impala_database_obj.name is not None:
                try:
                    self._impala_conn.drop_table(table_name, self._impala_database_obj.name)
                except Exception as e:
                    logger.exception("drop_table failed with reason [{}]".format(str(e)))
                    flag = False
            else:
                logger.error("Impala database connection is not initialized and cannot drop it.")
                flag = False
        else:
            logger.error("Impala connection is not initialized")
            flag = False
        if flag:
            if self._impala_table_obj.obj is not None and self._impala_table_obj.name == table_name:
                if database is None:
                    self._impala_table_obj = DBConnectorPair(name='', obj=None)
                elif database == self._impala_database_obj.name:
                    self._impala_table_obj = DBConnectorPair(name='', obj=None)
        return flag


class HiveConnector(ABCDBConnector):
    def __init__(self, hive_host, hive_port, user="hive", passwd=None, database=None, auth_mechanism="NOSASL"):
        self._hive_user = user
        self._hive_password = passwd
        self._auth_mechanism=auth_mechanism
        self._max_excute_time = 0
        self._max_excute_time_cmd = ''
        self._hive_host = hive_host
        self._hive_port = hive_port
        self._hive_database_obj = DBConnectorPair(name=database, obj=None)

    def __del__(self):
        if self._hive_database_obj.obj is not None:
            self._hive_database_obj.obj.close()

    def create_hive_plain_transport(self, host, port, username, password=None, timeout=1800):
        import sasl
        from thrift_sasl import TSaslClientTransport
        from thrift.transport.TSocket import TSocket

        socket = TSocket(host, port)
        if timeout is not None:
            socket.setTimeout(timeout * 1000)
        else:
            socket.setTimeout(timeout)

        sasl_auth = 'PLAIN'

        def sasl_factory():
            sasl_client = sasl.Client()
            sasl_client.setAttr('host', host)
            sasl_client.setAttr('username', username)
            # Password doesn't matter in NONE mode, just needs to be nonempty.
            if password is  None:
                sasl_client.setAttr('password', 'x')
            else:
                sasl_client.setAttr('password', password)
            sasl_client.init()
            return sasl_client

        return TSaslClientTransport(sasl_factory, sasl_auth, socket)

    def connect(self):
        # initiate hive part
        """
        if self._hive_database_obj.name is not None and self._hive_database_obj.name != '':
            self._hive_database_obj = DBConnectorPair(name=self._hive_database_obj.name,
                                                      obj=hive.Connection(host=self._hive_host, port=self._hive_port,
                                                                          database=self._hive_database_obj.name,
                                                                          username=self._hive_user))
        else:
            self._hive_database_obj = DBConnectorPair(name='',
                                                      obj=hive.Connection(host=self._hive_host, port=self._hive_port,
                                                                          username=self._hive_user))
        """
        if self._hive_database_obj.name is not None and self._hive_database_obj.name != '':
            self._hive_database_obj = DBConnectorPair(name=self._hive_database_obj.name,
                                                      obj=
                                                      hive.connect(thrift_transport=self.create_hive_plain_transport(
                                                          host=self._hive_host,
                                                          port=self._hive_port,
                                                          username=self._hive_user,
                                                          timeout=DARWIN_CONFIG.get("pipeline_task_default_retry_timeout", 1800)
                                                      ), database=self._hive_database_obj.name))
        else:
            self._hive_database_obj = DBConnectorPair(name=self._hive_database_obj.name,
                                                      obj=
                                                      hive.connect(thrift_transport=self.create_hive_plain_transport(
                                                          host=self._hive_host,
                                                          port=self._hive_port,
                                                          username=self._hive_user,
                                                          timeout=DARWIN_CONFIG.get(
                                                              "pipeline_task_default_retry_timeout", 1800)
                                                      )))
        return True

    def connect_database(self, database_name=None):
        if database_name is None:
            if self._hive_database_obj.name is not None and self._hive_database_obj.name != '':
                return True
            else:
                logger.error("No database_name assigned")
                return False
        else:
            if self._hive_database_obj.name is not None and self._hive_database_obj.name != '':
                if self._hive_database_obj.name == database_name:
                    return True
                else:
                    self._hive_database_obj.obj.close()
                    self._hive_database_obj = DBConnectorPair(name=database_name,
                                                              obj=hive.Connection(host=self._hive_host,
                                                                                  port=self._hive_port,
                                                                                  database=database_name,
                                                                                  username=self._hive_user))
                    return True
            else:
                self._hive_database_obj = DBConnectorPair(name=database_name,
                                                          obj=hive.Connection(host=self._hive_host,
                                                                              port=self._hive_port,
                                                                              database=database_name,
                                                                              username=self._hive_user))
                return True

    def create_table(self, table_name, database=None, table_obj=None, **kwargs):
        logger.error("Hive connector dosenot implement create_table method. Please use raw_sql to create table")
        return False

    def connect_table(self, table_name, database=None):
        logger.error("Hive connector dosenot implement connect_table method. Please use raw_sql to create table")
        return False

    def exists_table(self, table_name, database=None):
        if table_name is None:
            logger.error("table_name cannot be None")
            return False
        tables = self.raw_sql(sql_str="show tables")
        return table_name in list(map(lambda s: s[0], tables))

    def get_table_schema(self, table_name, database=None):
        if table_name is None:
            logger.error("table_name cannot be None")
            return None
        if self._hive_database_obj.obj is not None:
            cursor = self._hive_database_obj.obj.cursor()
            cursor.execute('select * from table_name limit 100')
            return cursor.description()
        else:
            logger.info("Hive connection is not initialized")
            return None

    def list_databases(self, like=None):
        if self._hive_database_obj.obj is not None:
            if like is not None:
                databases = self.raw_sql(sql_str="show databases like {}".format(repr(like)))
            else:
                databases = self.raw_sql(sql_str="show databases")
            return list(map(lambda s: s[0], databases))
        else:
            logger.info("Hive connection is not initialized")
            return None

    def list_tables(self, like=None, database=None):
        if self._hive_database_obj.obj is not None:
            if like is not None:
                tables = self.raw_sql(sql_str="show tables like {}".format(repr(like)))
            else:
                tables = self.raw_sql(sql_str="show tables")
            return list(map(lambda s: s[0], tables))
        else:
            logger.info("Hive connection is not initialized")
            return None

    def fetch(self, sql_str, limit=None):
        if self._hive_database_obj.obj is not None:
            if limit is not None:
                return pd.read_sql(sql_str, self._hive_database_obj.obj, params={"limit": limit})
            else:
                return pd.read_sql(sql_str, self._hive_database_obj.obj)
        else:
            logger.info("Hive connection is not initialized")
            return None

    def insert(self, sql_str, overwrite=False):
        if not isinstance(sql_str, str):
            logger.error("Only support str insert for hive")
            return False
        if self._hive_database_obj.obj is not None:
            rst = self.raw_sql(sql_str, fetch_rst=False)
            return rst[0]
        else:
            logger.info("Hive connection is not initialized")
            return False

    def raw_sql(self, sql_str, fetch_rst=True, onebyone=False):
        if not isinstance(sql_str, str):
            logger.error("raw_sql {} is not str type".format(sql_str))
            return False, None
        flag = True
        if self._hive_database_obj.obj is not None:
            cursor = self._hive_database_obj.obj.cursor()
            cursor.execute(sql_str)
            if fetch_rst:
                if not onebyone:
                    rst = cursor.fetchall()
                else:
                    rst = None
                    while True:
                        #logger.info("A sql: {}".format(sql_str))
                        tmp = cursor.fetchmany(size=DARWIN_CONFIG.get("raw_sql_fetch_size", 100000))
                        if tmp is None:
                            break
                        else:
                            if rst is None:
                                rst = tmp
                            else:
                                rst.extend(tmp)
                            if len(tmp) < DARWIN_CONFIG.get("raw_sql_fetch_size", 100000):
                                # Done
                                break
                        #logger.info("B sql: {} records {}".format(sql_str, len(rst)))
            else:
                rst = None
        else:
            logger.info("Hive connection is not initialized")
            return False, None
        return flag, rst

    def column_stats(self):
        logger.error("Hive connector dosenot implement column_stats method. Please use raw_sql to create table")
        return None

    def truncate_table(self, table_name, database=None):
        logger.error("Hive connector dosenot implement column_stats method. Please use raw_sql to create table")
        return False

    def drop_table(self, table_name, database=None):
        if table_name is None:
            logger.error("table_name cannot be None")
            return False
        flag = True
        if self._hive_database_obj.obj is not None:
            if database is not None:
                cursor = self._hive_database_obj.obj.cursor()
                cursor.execute("use {}".format(database))
                cursor.execute("drop table {}".format(table_name))
            else:
                cursor = self._hive_database_obj.obj.cursor()
                cursor.execute("use {}".format(self._hive_database_obj.name))
                cursor.execute("drop table {}".format(table_name))
        else:
            logger.error("Hive connection is not initialized")
            flag = False
        return flag
