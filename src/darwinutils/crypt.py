#! /usr/bin/env python3
# -*- coding:utf-8 -*-


"""
Public-key algorithm based crypt module.
https://cryptography.io/en/latest/hazmat/primitives/asymmetric/rsa/
"""


from functools import (
    partial,
    reduce,
)
from operator import (
    methodcaller,
    itemgetter,
    getitem,
    concat,
    attrgetter,
)
from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives import (serialization, hashes)
from cryptography.hazmat.primitives.asymmetric import (rsa, dsa, padding)
from darwinutils.fn import (
    f_call,
    f_compose_l,
    f_compose_r,
    f_echo,
    f_flip_call,
    f_group,
    f_list_rize,
    f_star_call,
    f_unstar_call,
    f_zip,
)


__all__ = [
    "gen_key",
    "dumps_key",
    "loads_public_key",
    "loads_private_key",
    "sign_with_private_key",
    "verify_with_public_key",
    "encrypt_with_public_key",
    "decrypt_with_private_key",
]


"""
Generate a key pair
:param type_: crypto.TYPE_{RSA,DSA}
:param size: key size
:return: PKey object
"""
gen_key = partial(f_unstar_call, f_compose_r(
    f_zip(
        partial(getitem, {
            "rsa": partial(f_flip_call, partial(rsa.generate_private_key, 65537), default_backend()),
            "dsa": partial(f_flip_call, dsa.generate_private_key, default_backend()),
        }),
        f_echo,
    ),
    list,
    partial(f_star_call, f_call),
))


"""
Dump a key pair
:param key: key to dump
:param cipher: cipher to encrypt the pem output content
:param passphrase: passphrase to ecrypt the pem output content
:return: [<private_key>, <public_key>]
"""
dumps_key = partial(f_unstar_call, f_compose_r(
    f_group(
        # private key
        f_compose_r(
            f_zip(
                f_echo,
                f_compose_r(
                    serialization.BestAvailableEncryption,
                    partial(
                        methodcaller,
                        "private_bytes",
                        serialization.Encoding.PEM,
                        serialization.PrivateFormat.PKCS8,
                    ),
                ),
            ),
            partial(f_star_call, partial(f_flip_call, f_call)),
        ),
        # public keys
        f_compose_r(
            itemgetter(0),
            methodcaller("public_key"),
            methodcaller(
                "public_bytes",
                serialization.Encoding.PEM,
                serialization.PublicFormat.SubjectPublicKeyInfo,
            ),
        ),
    ),
    partial(map, methodcaller("decode", "ascii")),
    tuple,
))


"""
Load a public key
:param buffer: public key in pem format
:return: a PKey <public_key> object
"""
loads_public_key = f_compose_r(
    methodcaller("encode", "ascii"),
    partial(f_flip_call, serialization.load_pem_public_key, default_backend()),
)


"""
Load a private key
:param buffer: private key in pem format
:param passphrase: passphrase to decrypt the pem output content
:return: a PKey <private_key> object
"""
loads_private_key = partial(f_unstar_call, f_compose_r(
    f_group(
        # passphrase
        itemgetter(1),
        # data
        f_compose_r(
            itemgetter(0),
            methodcaller("encode", "ascii"),
        ),
    ),
    list,
    partial(f_star_call, partial(f_flip_call, serialization.load_pem_private_key, default_backend())),
))


"""
Sign a msg with a private key with a specific digest
:param key: private key in pem format
:param msg: msg to be signed
:param digest: digest method
:return: the signature of msg
    sig = pri_key.sign(
        msg,
        padding.PSS(
            mgf=padding.MGF1(hashes.SHA256()),
            salt_length=padding.PSS.MAX_LENGTH,
        ),
        hashes.SHA256(),
    )
"""
sign_with_private_key = partial(f_unstar_call, f_compose_r(
    f_zip(
        # key
        attrgetter("sign"),
        # msg: list rize so that it could be concat with other digest args
        f_list_rize,
        # digest
        f_compose_r(
            str.upper,
            partial(getattr, hashes),
            f_call,
            f_group(
                f_compose_r(
                    padding.MGF1,
                    partial(f_flip_call, f_echo, padding.PSS.MAX_LENGTH),
                    partial(f_star_call, padding.PSS),
                ),
                f_echo,
            ),
            list,
        ),
    ),
    list,
    f_group(
        itemgetter(0),
        f_compose_r(
            itemgetter(slice(1, None)),
            partial(reduce, concat),
        ),
    ),
    list,
    partial(f_star_call, f_star_call),
))


"""
Verify a signed msg with a public key with a specific digest
:param key: private key in pem format
:param sig: signature of the target msg to be verified
:param msg: msg to be verified
:return: None if succ, or raise exception
    r = pub_key.verify(
        sig,
        msg2,
        padding.PSS(
            mgf=padding.MGF1(hashes.SHA256()),
            salt_length=padding.PSS.MAX_LENGTH,
        ),
        hashes.SHA256(),
    )
"""
verify_with_public_key = partial(f_unstar_call, f_compose_r(
    f_zip(
        # key
        attrgetter("verify"),
        # signature: list rize so that it could be concat with other args
        f_list_rize,
        # msg: list rize so that it could be concat with other args
        f_list_rize,
        # digest
        f_compose_r(
            str.upper,
            partial(getattr, hashes),
            f_call,
            f_group(
                f_compose_r(
                    padding.MGF1,
                    partial(f_flip_call, f_echo, padding.PSS.MAX_LENGTH),
                    partial(f_star_call, padding.PSS),
                ),
                f_echo,
            ),
            list,
        ),
    ),
    list,
    f_group(
        itemgetter(0),
        f_compose_r(
            itemgetter(slice(1, None)),
            partial(reduce, concat),
        ),
    ),
    list,
    partial(f_star_call, f_star_call),
))


"""
Encrypt/decrypt a msg with a public/private key with a specific digest
:param key: public/private key in pem format
:param msg: msg to be encrypted/decrypted
:param digest: digest to be used during encrypt and decrypt
:return: encrypted/decrypted msg
>>> message = b"encrypted data"
>>> ciphertext = public_key.encrypt(
...     message,
...     padding.OAEP(
...         mgf=padding.MGF1(algorithm=hashes.SHA256()),
...         algorithm=hashes.SHA256(),
...         label=None
...     )
... )
>>> plaintext = private_key.decrypt(
...     ciphertext,
...     padding.OAEP(
...         mgf=padding.MGF1(algorithm=hashes.SHA256()),
...         algorithm=hashes.SHA256(),
...         label=None
...     )
... )
>>> plaintext == message
True
"""
_f_f_crypt = f_compose_r(
    attrgetter,
    f_list_rize,
    partial(f_flip_call, concat, [
        # msg: list rize so that it could be concat with other args
        f_list_rize,
        # digest
        f_compose_r(
            str.upper,
            partial(getattr, hashes),
            f_call,
            f_group(
                # padding.OAEP(mgf=)
                padding.MGF1,
                # padding.OAEP(algorithm=)
                f_echo,
            ),
            list,
            # padding.OAEP(label=None)
            partial(f_flip_call, concat, [None]),
            partial(f_star_call, padding.OAEP),
            f_list_rize,
        ),
    ]),
    partial(f_star_call, f_zip),
    partial(
        f_compose_l,
        partial(f_star_call, f_star_call),
        f_group(
            itemgetter(0),
            f_compose_r(
                itemgetter(slice(1, None)),
                partial(reduce, concat),
            ),
        ),
        list,
    )
)


encrypt_with_public_key, decrypt_with_private_key = f_compose_r(
    partial(map, f_compose_r(
        _f_f_crypt,
        partial(partial, f_unstar_call),
    )),
    list,
)(["encrypt", "decrypt"])
