#! /usr/bin/env bash


PROG_CLI=`command -v $0`
PROG_NAME=${0##*/}
PROG_DIR=${0%/*}
PROG_VERSION=0.1.0


[ "$DEBUG" = "true" ] && set -x
# TODO: explicitly define DEFAULT_sudo before init_op issue would be fixed
[ "${USER:-`id -u -n`}" = "root" ] || DEFAULT_sudo=sudo


function check_sudo_perm() {
    eval "${sudo:+${sudo} -n} true 2>/dev/null"
}
function get_darwin_cfg() {
    python -c "if True:

        from functools import partial
        from operator import (itemgetter, methodcaller)
        from darwinutils.config import DARWIN_CONFIG as cfg
        from darwinutils.fn import (f_star_call, f_compose_r)

        f_f = f_compose_r(
            methodcaller(\"split\", \".\"),
            partial(map, itemgetter),
            partial(f_star_call, f_compose_r),
        )
        print(f_f(\"$1\")(cfg))
    "
}
function get_slurm_cfg() {
    if [ -z "$SLURM_CONF" -o ! -f "$SLURM_CONF" ]; then return 1; fi

    local key=$1

    grep "^${key}=" $SLURM_CONF | tail -n1 | cut -d= -f2-
}
function __test_get_slurm_cfg() {
    local err_cnt=0

    local _tst_conf=`mktemp /tmp/XXXXXX`
    local _SLURM_CONF=$SLURM_CONF
    SLURM_CONF=$_tst_conf

    # generate test slurm.conf
    echo "hello=bye" >$_tst_conf
    echo "hello=world" >$_tst_conf
    echo "#hello=world" >>$_tst_conf

    test `get_slurm_cfg hello` = "world" || { ((err_cnt+=1)); log_error "Fail to run sub-test 1"; }

    # restore original SLURM_CONF
    rm -f $_tst_conf
    SLURM_CONF=$_SLURM_CONF
    test $err_cnt -eq 0
}
function reorder_channels_by_conda_forge() {
    local -a _channels=("${!1}")
    local -a _known_conda_forge_channels=("${!2}")

    local -a conda_forge_channels=`set_intersection _channels[@] _known_conda_forge_channels[@]`
    local -a other_channels=`set_difference _channels[@] known_conda_forge_channels[@]`
    local -a _channels=`array_concat conda_forge_channels[@] other_channels[@]`
    # log for debug
    # declare -p conda_forge_channels >&2
    # declare -p other_channels >&2
    # declare -p _channels >&2
    declare_p_val _channels
}


source log.sh
source utils.sh


conda=_shadow_cmd_conda
setup_os_flags


#===========================================================
# Pre-defined environment variable and options
#
# do not modify as_root, unless you know what it really means!!!
DEFAULT_as_root=${as_root:-"false"}
#-----------------------------------------------------------
# Anaconda related
#
DEFAULT_use_conda="true"
DEFAULT_conda_install_home=`ls -1d $HOME/anaconda3 2>/dev/null || ls -1d /opt/anaconda3 2>/dev/null || echo $HOME/anaconda3`
DEFAULT_CONDA_CHANNELS=(
"defaults"
"conda-forge"
"bioconda"
#"https://mirrors.tuna.tsinghua.edu.cn/anaconda/pkgs/main"
#"https://mirrors.tuna.tsinghua.edu.cn/anaconda/pkgs/free"
#"https://mirrors.tuna.tsinghua.edu.cn/anaconda/cloud/conda-forge"
#"https://mirrors.tuna.tsinghua.edu.cn/anaconda/cloud/bioconda"
)
DEFAULT_conda_install_flags_extra=("--channel-priority")
# constant variable which records the known conda-forge urls to be used for use_openblas feature
known_conda_forge_channels=(
"conda-forge"
"https://mirrors.tuna.tsinghua.edu.cn/anaconda/cloud/conda-forge"
)


#-----------------------------------------------------------
# Pypi related
#
# NOTE: you can setup your own pypi mirror by reading following doc.
# https://aboutsimon.com/blog/2012/02/24/Create-a-local-PyPI-mirror.html
DEFAULT_PIP_EXTRA_INDEX_URLS=(
"https://pypi.tuna.tsinghua.edu.cn/simple"
)
DEFAULT_pip_install_flags_extra=""


#-----------------------------------------------------------
# Darwin related
#
DEFAULT_darwin_conf_home=${DARWIN_CONF_HOME:-$(get_darwin_cfg home 2>/dev/null || echo "$PROG_DIR/../conf")}
if [ ! -d "$DEFAULT_darwin_conf_home" ]; then
    log_error "Fail to detect DARWIN_CONF_HOME, please specify explicitly. Abort!"
    log_info "For example: \"export DARWIN_CONF_HOME=\$HOME/workspace/darwin-platform/src/conf\""
    exit 1
fi
if [ -n "$DEFAULT_darwin_conf_home" -a -d $DEFAULT_darwin_conf_home/.. ]; then
    DEFAULT_DARWIN_HOME=${DEFAULT_darwin_conf_home}/..
else
    # this is really a strong assumption!
    DEFAULT_DARWIN_HOME=$HOME/workspace/darwin-platform/src
fi
DEFAULT_DARWIN_3rd_party_patch_dirs=(
${DEFAULT_darwin_conf_home}/../3rd_party
)

DEFAULT_DARWIN_CONDA_VE_NAME=${CONDA_DEFAULT_ENV:-darwin}
DEFAULT_DARWIN_CONDA_DEPS_FILE=${DEFAULT_darwin_conf_home:-"${DEFAULT_DARWIN_HOME}/conf"}/package-list.txt
DEFAULT_DARWIN_PIP_DEPS_FILE=${DEFAULT_darwin_conf_home:-"${DEFAULT_DARWIN_HOME}/conf"}/requirements.txt
DEFAULT_num_gpus=${num_gpus:-`ls /dev/nvidia[0-9]* 2>/dev/null | wc -l | awk '{print $1}'`}
DEFAULT_install_target=${install_target:-all}
DEFAULT_use_mkl=${use_mkl:-false}
# default to use openblas version of tensorflow, numpy, sklearn and etc softwares
# due to a conflict issue between tf-mkl with opencv. and,
# tf-gpu does not work in os w/o nvidia-driver, and
# tf-eigen has performance problem as identified by HeBing.
if [ "${DEFAULT_num_gpus}" -gt 0 ]; then
    DEFAULT_use_openblas=${use_openblas:-false}
else
    DEFAULT_use_openblas=${use_openblas:-true}
fi
# mkl, eigen or gpu, https://github.com/AnacondaRecipes/tensorflow_recipes
if [ -n "$tf_variant" ]; then
    DEFAULT_tf_variant=$tf_variant
elif [ "$DEFAULT_num_gpus" -gt 0 ]; then
    DEFAULT_tf_variant=gpu
elif [ "$DEFAULT_use_mkl" = "true" ]; then
    DEFAULT_tf_variant=mkl
fi


# ------------------------------------------------------------------------------
# Slurm related
#
# DEFAULT_SLURM_CONF=${SLURM_CONF:-`get_darwin_cfg "default_slurm_conf" 2>/dev/null` || echo "$DEFAULT_darwin_conf_home/slurm.cfg"`}


#===========================================================
# It's time to parse command line
#
source getopt.sh


# required by post setup based on a given darwin installation.
DARWIN_CONF_HOME=${DARWIN_CONF_HOME:-$darwin_conf_home}
[ -n "$DARWIN_CONF_HOME" ] && export DARWIN_CONF_HOME


#===========================================================
# post-process of building auto variables and env.
#
#-----------------------------------------------------------
# Anaconda related
#
# rebuild conda install flags
if [ ${#CONDA_CHANNELS[@]} -gt 0 ]; then
    if [ "$use_openblas" = "true" ]; then
        declare -a CONDA_CHANNELS=`reorder_channels_by_conda_forge CONDA_CHANNELS[@] known_conda_forge_channels[@]`
    fi

    conda_install_flags_extra+=(`echo "${CONDA_CHANNELS[@]}" | sed -e 's/ / --channel /g' -e 's/^/--channel /g'`)
    conda_install_flags_extra+=("--override-channels")
fi
conda_env_name=$DARWIN_CONDA_VE_NAME


#-----------------------------------------------------------
# Pypi related
#
if [ ${#PIP_EXTRA_INDEX_URLS[@]} -gt 0 ]; then
    pip_install_flags_extra=(`echo "${PIP_EXTRA_INDEX_URLS[@]}" | sed -e 's/ / --extra-index-url /g' -e 's/^/--extra-index-url /g'`)
fi

#===========================================================
# parameter validation
#
if [ $num_gpus -le 0 -a "$tf_variant" = "gpu" ]; then
    log_error "num_gpus=$num_gpus conflicts with tf_variant=$tf_variant"
    exit 1
fi


#===========================================================
# Feature toolkit functions
#
function _magic_tf_variant() {
    # append tf_variant magic
    if [ -n "$tf_variant" ]; then
        sed -e 's/tensorflow/tensorflow-'$tf_variant'/g'
    else
        cat -
    fi | \
    # prevent predefined cuda related package
    if [ "$tf_variant" != "gpu" ]; then
        grep -vE "cudatoolkit|cudnn"
    else
        cat -
    fi
}
function filter_pkg_lst_per_tag() {
    function get_pkg_lines() {
        if [ -n "$pkg_lines" ]; then
            echo "$pkg_lines"
        elif [ -n "$pkg_lst_file" ]; then
            cat $pkg_lst_file
        else
            cat -
        fi
    }
    local pkg_lst_file=""
    local include_implicit=false
    local args=()
    while [ -n "$1" ];
    do
        if $G_expr_bin "$1" : "--pkg_lst_file$" >/dev/null; then
            shift; pkg_lst_file=$1; shift;
        elif $G_expr_bin "$1" : "--pkg_lst_file=.*" >/dev/null; then
            pkg_lst_file=`echo $1 | cut -d= -f2-`; shift;
        elif $G_expr_bin "$1" : "--include_implicit$" >/dev/null; then
            shift; include_implicit=true;
        elif $G_expr_bin "$1" : "\--include_implicit=.*" >/dev/null; then
            include_implicit=`echo $1 | cut -d= -f2-`; shift;
        else
            args+=($1); shift;
        fi
    done
    local tag=${args[0]}; shift

    local pkg_lines=""
    # cache input pkg lines
    if [ $include_implicit = "true" -a -z "$pkg_lst_file" ]; then
        # TODO: use /dev/stdin (?)
        pkg_lines=$(</dev/stdin)
    fi
    if [ -n "$tag" ]; then
        # find out pkgs which was explicitly tagged.
        get_pkg_lines | \
        if [ "$tag" = "all" ]; then
            grep -A 1 -E "^# tags: .*@"
        else
            grep -A 1 -E "^# tags: .*@${tag},|^# tags: .*@${tag} *$"
        fi | \
        grep -vE "^# tags:|^--"
    fi
    if [ -z "$tag" -o "$include_implicit" = "true" ]; then
        # find out pkgs which was not tagged explicitly
        get_pkg_lines | \
        grep -B 1 -E "^[^# ]" | \
        grep -vE "^--" | \
        awk '{ if ($0 ~ /^# tags:/) { getline; } else { print $0; }; }' | grep -v "^#"
    fi
}
function __test_filter_pkg_lst_per_tag() {
    local err_cnt=0

    if [ -z "$@" ]; then
        cat $DARWIN_CONDA_DEPS_FILE | filter_pkg_lst_per_tag all

    elif [ -n "$1" -a -f "$1" ]; then
        local pkg_lst_file=$1
        shift

        cat $pkg_lst_file | filter_pkg_lst_per_tag $@
    else
        filter_pkg_lst_per_tag --include_implicit $@
    fi || { ((err_cnt+=1)); log_error "Fail sub-test 1"; }

    test $err_cnt -eq 0
}


#===========================================================
# Feature functions
#
function usage() {
    echo
    echo "Usage: $0 cmd={usage | install_all | setup_all | check_all } [OPTION=value]..."
    echo
    echo "Options:"
    echo "  - DARWIN_CONF_HOME: \"conf\" directory of darwin"
    echo "        default to \"$DEFAULT_darwin_conf_home\""
    echo
    echo "  - DARWIN_CONDA_VE_NAME:"
    echo "        default to \"$DEFAULT_DARWIN_CONDA_VE_NAME\""
    echo
    echo "  - LOG_LEVEL:"
    echo "        debug | info | warn | error, default to \"${DEFAULT_LOG_LEVEL:-${LOG_LEVEL}}\""
    echo
    echo "  - tf_variant: Variant of tensorflow would be selected from anaconda"
    echo "        gpu | mkl | eigen, default to \"${DEFAULT_tf_variant}\""
    echo
    echo "  - install_target: target depends softwares to be installed"
    echo "        all | dev, default to \"${DEFAULT_install_target}\""

    echo
}
function install_darwin_deps() {
    local conda_env_name=$DARWIN_CONDA_VE_NAME && \

    # install_anaconda && \
    setup_conda_flags && \
    #
    # TODO: remove following "conda config" if "--channel" had been cool down
    # use on-demand "--channel" instead of "conda config"
    #
    # if ! $conda config --show channels | grep -qiw conda-forge; then
    #     $conda config --append channels conda-forge
    # fi && \
    # if ! $conda config --show channels | grep -qiw bioconda; then
    #     $conda config --append channels bioconda
    # fi && \
    #
    # strictly enforce mkl or nomkl
    #
    if [ "$use_mkl" = "true" ]; then
        if $conda config --show disallowed_packages | grep -qiw "mkl"; then
            $conda config --remove disallowed_packages mkl
        fi
    else
        if ! $conda config --show disallowed_packages | grep -qiw "mkl"; then
            $conda config --append disallowed_packages mkl
      fi
    fi && \
    if [ -n "${conda_env_prefix}" ]; then
        conda_create_env --user --prefix ${conda_env_prefix}/$DARWIN_CONDA_VE_NAME python==3.6
    else
        conda_create_env --user --name $DARWIN_CONDA_VE_NAME python==3.6
    fi && \
    setup_pip_flags && \

    print_title "Check and install \"Darwin\" depends anaconda package in environment \"$DARWIN_CONDA_VE_NAME\"..." && \
    f_pkg_txt=$DARWIN_CONDA_DEPS_FILE && \
    f_pip_txt=$DARWIN_PIP_DEPS_FILE && \
    local -a pkgs=() && \
    pkgs+=(`filter_pkg_lst_per_tag --pkg_lst_file=$f_pkg_txt --include_implicit=true $install_target | _magic_tf_variant | sed -e 's/^/conda:10:/g'`) && \
    pkgs+=(`filter_pkg_lst_per_tag --pkg_lst_file=$f_pip_txt --include_implicit=true $install_target | _magic_tf_variant | sed -e 's/^/pip:20:/g'`) && \
    # exclude thoe deps which osx does not support, such as p7zip
    if [ "$is_osx" = "true" ]; then
        local osx_not_exists=("conda:10:p7zip")
        local -a pkgs=`set_difference pkgs[@] osx_not_exists[@]`
        unset osx_not_exists

        if ! command -v 7za >/dev/null 2>&1; then
            log_error "p7zip should be installed before calling this script."
            false
        fi
    fi && \
    if [ "$use_mkl" = "false" ]; then
        # use "nomkl" meta package from anaconda to request no mkl.
        pkgs+=("conda:10:nomkl")
    fi && \
    # solve mkl and openblas dance
    # https://conda-forge.org/docs/blas.html#openblas-mkl-dance
    # https://github.com/conda-forge/pygridgen-feedstock/issues/10
    if [ "$use_openblas" = "true" ]; then
        pkgs+=(
            'conda:10:blas=*=openblas'
            "rpm:8:mesa-libGL"
            "deb:8:libgl1-mesa-glx"
        )
    fi && \
    # remove non-openblas enforcely if required.
    blas_ver_bld="`$conda list -n $DARWIN_CONDA_VE_NAME blas | grep ^blas | awk '{print $2,$3}'`" && \
    if [ -n "$blas_ver_bld" -a "`echo "$blas_ver_bld" | cut -d' ' -f2`" != "openblas" ]; then
        log_warn "Remove existing \"blas\"==\"$blas_ver_bld\""
        $conda uninstall -n $DARWIN_CONDA_VE_NAME --yes blas --force
    fi && \
    if do_and_verify \
        'eval pkg_verify ${pkgs[@]}' \
        'eval pkg_install ${pkgs[@]}' \
        "true"; then
        pkg_list_installed ${pkgs[@]} | log_lines debug
    else
        log_error "Fail to create conda env \"$DARWIN_CONDA_VE_NAME\" for darwin"
        false
    fi
}
function install_slurm() {
    print_title "Check and install \"Slurm\" ..." && \
    local -a pkgs=() && \
    # slurm meta package
    pkgs+=(
        "deb:8:slurm-wlm"
        #"deb:8:slurmd"
        #"deb:8:slurmctld"
        #"deb:8:slurm-client"
        "deb:8:munge"
    ) && \
    if do_and_verify \
        'eval pkg_verify ${pkgs[@]}' \
        'eval pkg_install ${pkgs[@]}' \
        "true"; then
        pkg_list_installed ${pkgs[@]} | log_lines debug
    else
        log_error "Fail to install \"slurm\""
        false
    fi
}
function install_3rd_party_patches() {
    declare -a setup_shs=()

    function _run_patch_install() { $1 cmd=install; }

    for setup_sh_dir in ${DARWIN_3rd_party_patch_dirs[@]}
    do
        for setup_sh in `find $setup_sh_dir -maxdepth 2 -name "setup.sh" -type f`
        do
            [ -f "$setup_sh" -a -x "$setup_sh" ] || continue
            setup_shs+=($setup_sh)
        done
    done
    if [ ${#setup_shs[@]} -gt 0 ]; then
        declare _does_activate="false"
        if [ "$use_conda" = "true" -a "$CONDA_DEFAULT_ENV" != "$DARWIN_CONDA_VE_NAME" ]; then
            $conda activate $DARWIN_CONDA_VE_NAME && \
           _does_activated="true" && \
           setup_pip_flags
        fi && \

        declare -a pkgs=() && \
        if ! $is_osx; then
            pkgs+=("patch" "rsync")
            if do_and_verify "pkg_verify ${pkgs[@]}" "pkg_install ${pkgs[@]}" "true"; then
                pkg_list_installed ${pkgs[@]}
            else
                log_error "Fail to install patching pkgs \"`filter_pkgs ${pkgs[@]} | xargs`\""
                false
            fi
        fi && \

        for_each_op _run_patch_install ${setup_shs[@]} && \

        if [ "$_does_activate" = "true" ]; then
            _shadow_cmd_conda deactivate && \
            setup_pip_flags
        fi
    fi
}
function install_all() {
    true install_slurm && \
    install_darwin_deps && \
    true install_3rd_party_patches && \

    setup_all && \
    check_all && \
    true
}
function setup_all() {
    true setup_cgroup && \
    true setup_slurm_mn && \
    true
}
function setup_slurm_cn() {
    # TODO: NotImplemented
    true
}
function setup_slurm_mn() {
    setup_slurm_crypto_openssl && \
    true
}
function setup_slurm_crypto_openssl() {
    # TODO: slurm in osx not implemented yet.
    if [ "$is_osx" = "true" ]; then return 0; fi

    local _does_activate="false"
    if [ "$use_conda" = "true" -a "$CONDA_DEFAULT_ENV" != "$DARWIN_CONDA_VE_NAME" ]; then
        $conda activate $DARWIN_CONDA_VE_NAME && \
       _does_activated="true" && \
       setup_pip_flags
    fi

    # due to a design shortage in DARWIN_CONFIG, an empty file needs to be exists prior the real setup.
    local f=$DARWIN_CONF_HOME/slurm.key
    [ -f $f ] || touch $f
    f=$DARWIN_CONF_HOME/slurm.crt
    [ -f $f ] || touch $f

    local slurm_job_credential_private_key=${slurm_job_credential_private_key:-`get_darwin_cfg default_slurm_job_credential_private_key`}
    local slurm_job_credential_public_certificate=${slurm_job_credential_public_certificate:-`get_darwin_cfg default_slurm_job_credential_public_certificate`}
    if [ \( -n "$slurm_job_credential_private_key" -a ! -s "$slurm_job_credential_private_key" \) -o \
         \( -n "$slurm_job_credential_public_certificate" -a ! -s "$slurm_job_credential_public_certificate" \) \
    ]; then
        # TODO: should support as_root
        openssl genrsa -out $slurm_job_credential_private_key 1024 && \
        openssl rsa    -in  $slurm_job_credential_private_key -pubout -out $slurm_job_credential_public_certificate && \
        chmod go-rwx $slurm_job_credential_private_key $slurm_job_credential_public_certificate

    elif [ -z "$slurm_job_credential_private_key" -o -z "$slurm_job_credential_public_certificate" ]; then
        log_error "Fail to get slurm_job_credential_{private_key,public_certificate} from slurm.conf"
        {
            declare -p slurm_job_credential_private_key
            declare -p slurm_job_credential_public_certificate
        } | log_lines debug
        false
    fi
    local rc=$?

    if [ "$_does_activate" = "true" ]; then
        _shadow_cmd_conda deactivate && \
        setup_pip_flags
    fi
    (exit $rc)
}
function check_all() {
    local err_cnt=0

    true check_cpu_affinity_issue || { ((err_cnt+=1)); }
    true check_cgroup || { ((err_cnt+=1)); }

    test $err_cnt -eq 0
}
function check_cpu_affinity_issue() {
    if [ "$is_osx" = "true" ]; then return 0; fi

    declare _does_activate="false"
    if [ "$use_conda" = "true" -a "$CONDA_DEFAULT_ENV" != "$DARWIN_CONDA_VE_NAME" ]; then
        $conda activate $DARWIN_CONDA_VE_NAME && \
       _does_activated="true" && \
       setup_pip_flags
    fi

    print_title "Check cpu affinity issue exists in current setup" && \
    if ! python -c 'if True:
        import os
        pid=os.getpid()

        import tensorflow
        a=os.sched_getaffinity(pid)

        import cv2
        b=os.sched_getaffinity(pid)

        import sys
        rc=a==b
        print(rc)
        sys.exit(rc)
    ' | grep -qix "True"; then
        log_error "#"
        log_error "# -------------------------------------------------------------------"
        log_error "# python cpu affinity issue exists with current tensorflow and cv2!!!"
        log_error "# -------------------------------------------------------------------"
        log_error "#"
        false
    fi
    local rc=$?

    if [ "$_does_activate" = "true" ]; then
        _shadow_cmd_conda deactivate && \
        setup_pip_flags
    fi
    (exit $rc)
}
function check_cgroup() {
    if [ "$is_osx" = "true" ]; then return 0; fi

    # check if Cgroup python module really works
    declare _does_activate="false" && \
    if [ "$use_conda" = "true" -a "$CONDA_DEFAULT_ENV" != "$DARWIN_CONDA_VE_NAME" ]; then
        $conda activate $DARWIN_CONDA_VE_NAME && \
       _does_activated="true" && \
       setup_pip_flags
    fi

    print_title "Check cpu affinity issue exists in current setup" && \
    if ! python -c 'if True:
        import os
        from cgroups import Cgroup

        pid = os.getpid()
        cg = Cgroup("test_in_setup_sh_{}".format(pid), hierarchies=["memory"])
        cg.set_memory_limit(limit=10*1024, unit="megabytes")
        cg.add(pid)
        try:
            # TODO: ignore delete error(?)
            cg.delete()
        except Exception:
            pass
        print("True")
    ' | grep -qix "True"; then
        log_error "#"
        log_error "# -------------------------------------------------------------------"
        log_error "# Fail to validate memory cgroup memory:${USER:-`id -u -n`}"
        log_error "# -------------------------------------------------------------------"
        log_error "#"
        false
    fi
    rc=$?

    if [ "$_does_activate" = "true" ]; then
        _shadow_cmd_conda deactivate && \
        setup_pip_flags
    fi
    (exit $rc)
}
function setup_cgroup() {
    if [ "$is_osx" = "true" ]; then return 0; fi

    local USER=${USER:-`id -u -n`}
    local GROUP=${GROUP:-`id -g -n`}

    # "cgroups" python module default to use "/" as the prefix.
    local cg_path_prefix="/"

    print_title "Setup memory cgroup ${cg_path}" && \
    # cgroup on /sys/fs/cgroup/memory type cgroup (rw,nosuid,nodev,noexec,relatime,memory)
    local cg_mnt_memory=`mount -v -t cgroup | awk '$6 ~ /memory/{print $3}'` && \
    # "cgroups" python module default to use "$USER" as the target cgroup name
    local cg_path=${cg_mnt_memory}${cg_path_prefix}${USER} && \
    # check and create the cgroup
    if ! do_and_verify "$sudo test -d $cg_path" "$sudo mkdir -p $cg_path" "true"; then
        log_error "#"
        log_error "# -------------------------------------------------------------------"
        log_error "# Fail to create memory cgroup in ${cg_path}"
        log_error "# -------------------------------------------------------------------"
        log_error "#"
        false
    fi && \
    # check and setup ownership of the cgroup
    if ! do_and_verify "test -O $cg_path" "$sudo chown -R ${USER}:${GROUP} $cg_path" "true"; then
        log_error "#"
        log_error "# -------------------------------------------------------------------"
        log_error "# Fail to create memory cgroup in ${cg_path}"
        log_error "# -------------------------------------------------------------------"
        log_error "#"
        false
    fi
}


# Run registered initialization ops
run_initialize_ops


if [ -n "$cmd" ] && declare -F $cmd >/dev/null 2>&1; then
    $cmd $@
    exit $?
else
    usage
fi
