#! /usr/bin/env python3
# -*- coding: utf-8 -*-
"""
The file is deprecated
"""

from darwinutils.impala_utils import ImpalaConnector
from darwinutils.impala_utils import DEFAULT_IMPALA_CONFIG
from data_preprocess.data_distribution import DataDistribution
import pandas as pd
import numpy as np
from darwinutils.log import get_task_logger
from datetime import  datetime
logger = get_task_logger(__name__)

date_format_check = {
    "bsns_lcnc_efct_dt": "%Y%m%d",
    "mrchnt_crt_dt": "%Y%m%d",
    "MRCHNT_CRT_TM": "%H%M%S",
    "D1_HLDY_PAY_OPN_DT": "%Y%m%d",
    "upd_dt": "%Y%m%d",
}

rsk_mno_csv_path = "data/rsk_mno.csv"
my_connector = ImpalaConnector(**DEFAULT_IMPALA_CONFIG)
my_connector.connect()

################The followed part shall be re-organized
data_process = DataDistribution(my_connector)
'''

for key, value in date_format_check.items():
    data_process.check_date_format(key, value)


diff_rst, special_case_mno_lst = data_process.cal_date_difference("mrchnt_crt_dt", "bsns_lcnc_efct_dt",
                                                                  date_format_check["mrchnt_crt_dt"],
                                                                  date_format_check["bsns_lcnc_efct_dt"])
logger.info("Min, Max, Mean,Var, (hist, bin_edge): {}".format(data_process.get_data_distribution(diff_rst)))

if len(special_case_mno_lst) > 0:
    shot_rsk_lst = data_process.get_risk_mno_lst(special_case_mno_lst, pd.read_csv(rsk_mno_csv_path)["mno"].tolist())
    if len(shot_rsk_lst) > 0:
        logger.info("Get {} rsk merchant\n{}".format(len(shot_rsk_lst), shot_rsk_lst))
    else:
        logger.info("Shot none merchant from {} special cases".format(len(special_case_mno_lst)))

issue_mno_lst = data_process.check_date_format(name="MRCHNT_CRT_TM", format=date_format_check["MRCHNT_CRT_TM"])
hour_lst, mno_list = data_process.get_day_hour(name="MRCHNT_CRT_TM", format=date_format_check["MRCHNT_CRT_TM"])
logger.debug("22:00 - 7:00 register merchant number {}\n{}".format(
    len(np.where((np.array(hour_lst) > 22) | (np.array(hour_lst) <= 7))[0]),
    mno_list[np.where((np.array(hour_lst) > 22) | (np.array(hour_lst) <= 7))[0]]
))

total_number_lst, mno = data_process.sum_columns(["app_qr_cnt", "tbl_card_cnt", "xiao_fang_cnt", "oth_pos_cnt"])
logger.info("Min, Max, Mean,Var, (hist, bin_edge): {}".format(data_process.get_data_distribution(total_number_lst)))

parent_lst, count_lst = data_process.count_by_group("PRNT_MRCHNT_KEY_WRD",
                                                    condition="real_mrchnt_flg = 'Y' and PRNT_MRCHNT_KEY_WRD !='' "
                                                              "and PRNT_MRCHNT_KEY_WRD != ' ' ")
logger.info("Min, Max, Mean,Var, (hist, bin_edge): {}".format(data_process.get_data_distribution(count_lst)))

parent_lst, count_lst = data_process.count_by_group("MCC_KEY_WRD", condition="real_mrchnt_flg = 'Y'")
logger.info("Min, Max, Mean,Var, (hist, bin_edge): {}".format(data_process.get_data_distribution(count_lst)))

issue_mno_lst = data_process.check_date_format(name="mrchnt_crt_dt", format=date_format_check["mrchnt_crt_dt"])
diff_lst, mno_list = data_process.cal_date_difference_with_special_date(special_date=datetime(2019, 3, 13),
                                                                        name="mrchnt_crt_dt",
                                                                        format=date_format_check["mrchnt_crt_dt"],
                                                                        special_diff_threshold=0)
logger.info("Min, Max, Mean,Var, (hist, bin_edge): {}".format(data_process.get_data_distribution(diff_lst)))
if len(mno_list) > 0:
    logger.warning("Get unexpected day difference with 2018:\n{}".format(mno_list))

parent_lst, count_lst = data_process.count_by_group("CRPRTN_CARD_NO_CFRTXT",
                                                    condition="real_mrchnt_flg = 'Y' and CRPRTN_CARD_NO_CFRTXT !='' "
                                                              "and CRPRTN_CARD_NO_CFRTXT != ' ' ")
logger.info("Min, Max, Mean,Var, (hist, bin_edge): {}".format(data_process.get_data_distribution(count_lst)))

parent_lst, count_lst = data_process.count_by_group("CRPRTN_CARD_NO_CFRTXT")
logger.info("Min, Max, Mean,Var, (hist, bin_edge): {}".format(data_process.get_data_distribution(count_lst)))

parent_lst, count_lst = data_process.count_by_group("STLMNT_ID_CARD_NO",
                                                    condition="real_mrchnt_flg = 'Y' and CRPRTN_CARD_NO_CFRTXT !='' "
                                                              "and CRPRTN_CARD_NO_CFRTXT != ' ' ")
logger.info("Min, Max, Mean,Var, (hist, bin_edge): {}".format(data_process.get_data_distribution(count_lst)))
parent_lst, count_lst = data_process.count_by_group("STLMNT_ID_CARD_NO")
logger.info("Min, Max, Mean,Var, (hist, bin_edge): {}".format(data_process.get_data_distribution(count_lst)))


parent_lst, count_lst = data_process.count_by_group("DATA_CMPLT_STS")
logger.info("Min, Max, Mean,Var, (hist, bin_edge): {}".format(data_process.get_data_distribution(count_lst)))
logger.info(parent_lst)

for value in parent_lst:
    df = data_process.fetch_value_mno_by_condition(condition="real_mrchnt_flg = 'Y' and DATA_CMPLT_STS={}".format(repr(value)))
    shot_rsk_lst = data_process.get_risk_mno_lst(df["mno"].tolist(), pd.read_csv(rsk_mno_csv_path)["mno"].tolist())
    if len(shot_rsk_lst) > 0:
        logger.info("Get {} rsk merchant for {}\n{}".format(len(shot_rsk_lst), value, shot_rsk_lst))
    else:
        logger.info("Shot none merchant from {}".format(value))


crprtn_status = ["crprtn_bsns_lcnc_chck_flag", "crprtn_chck_flag", "oprtr_chck_flag"]
for name in crprtn_status:
    parent_lst, count_lst = data_process.count_by_group(name)
    logger.info("==================={}=====================".format(name))
    logger.info("Min, Max, Mean,Var, (hist, bin_edge): {}".format(data_process.get_data_distribution(count_lst)))
    logger.info(parent_lst)

issue_mno_lst = data_process.check_date_format(name="D1_HLDY_PAY_OPN_DT", format=date_format_check["D1_HLDY_PAY_OPN_DT"],
                                               condition="real_mrchnt_flg = 'Y' and D1_HLDY_PAY_FLG='Y'")
if issue_mno_lst is None or len(issue_mno_lst) != 0:
    logger.info("D1_HLDY_PAY_OPN_DT format has issue:\n{}".format(issue_mno_lst))

diff_rst, special_case_mno_lst = data_process.cal_date_difference("D1_HLDY_PAY_OPN_DT", "mrchnt_crt_dt",
                                                                  date_format_check["D1_HLDY_PAY_OPN_DT"],
                                                                  date_format_check["mrchnt_crt_dt"],
                                                                  condition="real_mrchnt_flg = 'Y' and D1_HLDY_PAY_FLG='Y'",
                                                                  special_diff_threshold=0)
logger.info("Min, Max, Mean,Var, (hist, bin_edge): {}".format(data_process.get_data_distribution(diff_rst)))

if len(special_case_mno_lst) > 0:
    shot_rsk_lst = data_process.get_risk_mno_lst(special_case_mno_lst, pd.read_csv(rsk_mno_csv_path)["mno"].tolist())
    if len(shot_rsk_lst) > 0:
        logger.info("Get {} rsk merchant\n{}".format(len(shot_rsk_lst), shot_rsk_lst))
    else:
        logger.info("Shot none merchant from {} special cases".format(len(special_case_mno_lst)))

diff_lst, mno_list = data_process.cal_date_difference_with_special_date(special_date=datetime(2019, 3, 13),
                                                                        name="D1_HLDY_PAY_OPN_DT",
                                                                        format=date_format_check["D1_HLDY_PAY_OPN_DT"],
                                                                        special_diff_threshold=0,
                                                                        condition="real_mrchnt_flg = 'Y' and D1_HLDY_PAY_FLG='Y'"
                                                                        )
logger.info("Min, Max, Mean,Var, (hist, bin_edge): {}".format(data_process.get_data_distribution(diff_lst)))
if len(mno_list) > 0:
    logger.warning("Get unexpected day difference with 2018:\n{}".format(mno_list))

group_by_fields_lst = ["STLMNT_PERIOD", "PYMNT_DAYS", "MIN_STLMNT_AMT", "MIN_RTN_AMT"]
for name in group_by_fields_lst:
    parent_lst, count_lst = data_process.count_by_group(name)
    logger.info("{}: Min, Max, Mean,Var, (hist, bin_edge): {}".format(name, data_process.get_data_distribution(count_lst)))
    logger.info("{} vs {}".format(parent_lst, count_lst))



parent_lst, count_lst = data_process.count_by_group("CRPRTN_STLMNT_FLG", condition="real_mrchnt_flg = 'Y' and NRML_MRCHNT_DGRE in ('10', '11')")
#logger.info("{}: Min, Max, Mean,Var, (hist, bin_edge): {}".format("CRPRTN_STLMNT_FLG", data_process.get_data_distribution(count_lst)))
logger.info("{} vs {}".format(parent_lst, count_lst))


df = data_process.fetch_value_mno_by_condition("stlmnt_id_card_no", condition="real_mrchnt_flg = 'Y' and length(stlmnt_id_card_no)=18")
df['stlmnt_id_card_no'] = 2019 - pd.to_numeric(df['stlmnt_id_card_no'].str.slice(6, 10))
logger.info("Min, Max, Mean,Var, (hist, bin_edge): {}".format(data_process.get_data_distribution(df['stlmnt_id_card_no'].values.tolist())))
age_lst = df['stlmnt_id_card_no'].values
special_lst_idx = np.where((age_lst > 70) | (age_lst < 16))[0]
logger.info(len(special_lst_idx))

parent_lst, count_lst = data_process.count_by_group("STLMNT_CARD_NO", condition="real_mrchnt_flg = 'Y' and STLMNT_CARD_NO not in ('', ' ')")
logger.info("{}: Min, Max, Mean,Var, (hist, bin_edge): {}".format("STLMNT_CARD_NO", data_process.get_data_distribution(count_lst)))
logger.info("Total: {}".format(len(parent_lst)))
if len(parent_lst) < 50:
    logger.info("{} vs {}".format(parent_lst, count_lst))

card_no = parent_lst[np.argmax(count_lst)]
df = data_process.fetch_value_mno_by_condition('STLMNT_ID_CARD_NO', condition="real_mrchnt_flg = 'Y' and STLMNT_CARD_NO={}".format(repr(card_no)))
logger.info("ID number: {} - {}".format(len(df), np.unique(df["STLMNT_ID_CARD_NO".lower()].values)))
'''

'''
df = data_process.fetch_value_mno_by_condition(['mrchnt_key_wrd', 'upd_dt'],
                                               condition="chng_tp='2'",
                                               with_mno=False,
                                               table='prod_fct_ip_mrchnt_stlmnt_acct_chng_h')
groupped_rst = df.groupby(by='mrchnt_key_wrd')
till_now_lst = []
for group_name, tmp_df in groupped_rst:
    try:
        tmp_df = tmp_df.sort_values(by=["upd_dt"], ascending=False)
        till_now_lst.append((datetime(2019, 3, 14) -
                             datetime.strptime(tmp_df["upd_dt"].values[0], date_format_check["upd_dt"])).days)
    except:
        logger.exception("group_name: {} df: {}".format(group_name, tmp_df))
logger.info("{}: Min, Max, Mean,Var, (hist, bin_edge): {}".format("upd_dt till now", data_process.get_data_distribution(till_now_lst)))
'''

'''
df = data_process.fetch_value_mno_by_condition(['mrchnt_key_wrd', 'upd_dt'],
                                               condition="chng_tp='2'",
                                               with_mno=False,
                                               table='prod_fct_ip_mrchnt_stlmnt_acct_chng_h')
merchant_df = data_process.fetch_value_mno_by_condition(['mrchnt_crt_dt', 'mrchnt_key_wrd'])
groupped_rst = df.groupby(by='mrchnt_key_wrd')
till_now_lst = []
mrchnt_key_wrd_lst_stlmnt = set(groupped_rst.groups.keys())
mrchnt_key_wrd_lst_mrchnt = set(merchant_df["mrchnt_key_wrd"].values)
intersect_lst = mrchnt_key_wrd_lst_stlmnt.intersection(mrchnt_key_wrd_lst_mrchnt)

for group_name in intersect_lst:
    tmp_df = groupped_rst.get_group(group_name)
    tmp_df = tmp_df.sort_values(by=["upd_dt"], ascending=False)
    if len(tmp_df) > 1:
        try:
            till_now_lst.append((datetime.strptime(tmp_df["upd_dt"].values[0], date_format_check["upd_dt"]) -
                                 datetime.strptime(tmp_df["upd_dt"].values[1], date_format_check["upd_dt"])).days)
        except:
            logger.exception("group_name: {} df: {}".format(group_name, tmp_df))
            exit(1)
    else:
        tmp_merchant_df = merchant_df.loc[merchant_df["mrchnt_key_wrd"]==group_name]
        try:
            till_now_lst.append((datetime.strptime(tmp_df["upd_dt"].values[0], date_format_check["upd_dt"]) -
                                 datetime.strptime(tmp_merchant_df["mrchnt_crt_dt".lower()].values[0], date_format_check["mrchnt_crt_dt"])).days)
        except:
            logger.exception("group_name: {} df: {} merchan df: {}".format(group_name, tmp_df, tmp_merchant_df))
            exit(1)
logger.info("{}: Min, Max, Mean,Var, (hist, bin_edge): {}".format("upd_dt till now",
                                                                      data_process.get_data_distribution(till_now_lst)))
'''

'''
parent_lst, count_lst = data_process.count_by_group("mrchnt_key_wrd", table="prod_fct_ip_mrchnt_cmpln_h", condition='')
logger.info("{}: Min, Max, Mean,Var, (hist, bin_edge): {}".format("mrchnt_key_wrd", data_process.get_data_distribution(count_lst)))
logger.info("Total: {}".format(len(parent_lst)))
if len(parent_lst) < 50:
    logger.info("{} vs {}".format(parent_lst, count_lst))


parent_lst, count_lst = data_process.sum_by_group("tx_amt", "mrchnt_key_wrd", table="prod_fct_ip_mrchnt_cmpln_h", condition='')
logger.info("{}: Min, Max, Mean,Var, (hist, bin_edge): {}".format("mrchnt_key_wrd", data_process.get_data_distribution(count_lst)))
logger.info("Total: {}".format(len(parent_lst)))
if len(parent_lst) < 50:
    logger.info("{} vs {}".format(parent_lst, count_lst))
'''

'''
sign_fields = {"SIGN_PROVICE_NM":"INSTL_PRVNC_NM", "sign_city_nm":"INSTL_CITY_NM", "SIGN_CNTY_NM":None}
diff_lst = []

for sign_field_name, install_field_name in sign_fields.items():
    sign_field_name = sign_field_name.lower()
    if install_field_name is not None:
        install_field_name = install_field_name.lower()
    sign_df = data_process.fetch_value_mno_by_condition(["mrchnt_key_wrd", sign_field_name],
                                                            table="prod_fct_ip_mrchnt_sign_h",
                                                            condition="amap_rtrn_rslt='Y'",
                                                            with_mno=False)
    if install_field_name is not None:
        merchant_df = data_process.fetch_value_mno_by_condition([install_field_name, 'mrchnt_key_wrd'])
    else:
        merchant_df = data_process.fetch_value_mno_by_condition(['mrchnt_key_wrd'])
    groupped_rst = sign_df.groupby(by='mrchnt_key_wrd')
    cnt_lst = []
    mrchnt_key_wrd_lst_sign = set(groupped_rst.groups.keys())
    mrchnt_key_wrd_lst_mrchnt = set(merchant_df["mrchnt_key_wrd"].values)
    intersect_lst = mrchnt_key_wrd_lst_sign.intersection(mrchnt_key_wrd_lst_mrchnt)

    for group_name in intersect_lst:
        tmp_df = groupped_rst.get_group(group_name)
        cnt_lst.append(len(set(list(tmp_df[sign_field_name.lower()].values))))
        if install_field_name is not None:
            if tmp_df[sign_field_name.lower()].values[0] != merchant_df.loc[merchant_df['mrchnt_key_wrd']==group_name, install_field_name].values[0]:
                diff_lst.append(1)
            else:
                diff_lst.append(0)

    logger.info("{}: Min, Max, Mean,Var, (hist, bin_edge): {}".format(sign_field_name, data_process.get_data_distribution(cnt_lst)))
    logger.info("Max: {} {}".format(list(intersect_lst)[np.argmax(cnt_lst)], max(cnt_lst)))

    if install_field_name is not None:
        logger.info("{}: Min, Max, Mean,Var, (hist, bin_edge): {}".format(sign_field_name,
                                                                          data_process.get_data_distribution(diff_lst)))

'''
'''
check_fields = ["DRCT_AGNT_KEY_WRD", "ROOT_AGNT_KEY_WRD", "BRC_AGNT_KEY_WRD"]
for field_name in check_fields:
    parent_lst, count_lst = data_process.count_by_group(field_name)
    logger.info("{}: Min, Max, Mean,Var, (hist, bin_edge): {}".format(field_name, data_process.get_data_distribution(count_lst)))
    if len(parent_lst) < 50:
        logger.info("{} vs {}".format(parent_lst, count_lst))
'''
'''
check_fields = ["DRCT_ALLY_KEY_WRD", "root_ally_key_wrd"]
for field_name in check_fields:
    parent_lst, count_lst = data_process.count_by_group(field_name,
                                                        condition="real_mrchnt_flg='Y' and {} not in ('', ' ')".format(field_name))
    logger.info("{}: Min, Max, Mean,Var, (hist, bin_edge): {}".format(field_name, data_process.get_data_distribution(count_lst)))
    if len(parent_lst) < 50:
        logger.info("{} vs {}".format(parent_lst, count_lst))

    df = data_process.fetch_value_mno_by_condition(condition="real_mrchnt_flg='Y' and {} not in ('', ' ')".format(field_name))
    mno_lst = df['mno'].values
    shot_rsk_lst = data_process.get_risk_mno_lst(mno_lst, pd.read_csv(rsk_mno_csv_path)["mno"].tolist())
    if len(shot_rsk_lst) > 0:
        logger.info("Get {} rsk merchant\n{}".format(len(shot_rsk_lst), shot_rsk_lst))
    else:
        logger.info("Shot none merchant from {} special cases".format(len(mno_lst)))
'''

field_name = "PRNT_MRCHNT_KEY_WRD"
parent_lst, count_lst = data_process.count_by_group(field_name)
logger.info("{}: Min, Max, Mean,Var, (hist, bin_edge): {}".format(field_name, data_process.get_data_distribution(count_lst)))
if len(parent_lst) < 50:
    logger.info("{} vs {}".format(parent_lst, count_lst))
