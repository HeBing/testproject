#! /usr/bin/env python3
# -*- coding: utf-8 -*-
import os
import argparse
from darwinutils.json_op import Json
import json
from pipeline.pipeline import PipeLine
from pipeline.pipetask import PipeTaskRetry, PipeTask
from darwinutils.log import get_task_logger
logger = get_task_logger(__name__)

"""
Example:
python data_extractor.py --pipeline_file /home/tester/workspace/riskAI/src/cmd/pipeline/data_extractor/example.json --input_dict '{"input": "/home/tester/workspace/riskAI/src/cmd/pipeline/data_extractor/example_sql_file.txt"}'
"""


def valid_pipeline_format(pipeline):
    if not isinstance(pipeline, list):
        return False
    return all(list(map(lambda s: isinstance(s, dict), pipeline)))


def main(args):
    if not os.path.exists(args.pipeline_file):
        logger.error("File {} not existed".format(args.pipeline_file))
        exit(1)
    try:
        pipeline = Json.file_to_json_without_comments(os.path.abspath(args.pipeline_file))
    except:
        logger.exception("Cannot load pipeline file {} as json format".format(args.pipeline_file))
        exit(1)
    if not valid_pipeline_format(pipeline):
        logger.error("Pipeline file {} content is not right: \n{}".format(args.pipeline_file, json.dumps(pipeline, indent=2)))
        exit(1)

    input_dict={}
    if args.input_dict is not None:
        try:
            input_dict = json.loads(args.input_dict)
        except:
            logger.exception("Failed to transfer input_dict string {} to dict type".format(args.input_dict))
            exit(1)
    elif args.input_dict_file is not None:
        if not os.path.exists(os.path.abspath(args.input_dict_file)):
            logger.error("File {} not existed".format(args.input_dict_file))
            exit(1)
        try:
            input_dict = Json.file_to_json_without_comments(os.path.abspath(args.input_dict_file))
        except:
            logger.exception("Failed to transfer input_dict string {} to dict type".format(args.input_dict))
            exit(1)

    try:
        p = PipeLine(pipeline)
        task = PipeTask(p)
        logger.info("Start execute pipeline:\n")
        r = task(**input_dict)
        logger.info("Complete pipeline. Result:\n{}".format(r.get("output")))
        exit(0)
    except Exception as e:
        logger.exception("Failed to execute pipeline {} with reason {}".format(args.pipeline_file, str(e)))
        exit(1)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '--pipeline_file',
        type=str,
        help='To be excuted piepline file full path',
        required=True
    )
    exclusive_group = parser.add_mutually_exclusive_group(required=True)
    input_dict = exclusive_group.add_argument(
        '--input_dict',
        type=str,
        help='It is the input which align with pipeline define. If input has file path, it must be full path. \n'
             'For example: '
             '\'{"input": /home/tester/workspace/riskAI/src/cmd/pipeline/data_extractor/example_sql_file.txt}\'',
    )
    input_dict_file = exclusive_group.add_argument(
        '--input_dict_file',
        type=str,
        help='It is the full path file which include the input which align with pipeline define. '
    )

    args_flag, unparsed = parser.parse_known_args()
    main(args_flag)
