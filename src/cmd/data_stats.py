#! /usr/bin/env python3
# -*- coding: utf-8 -*-
import os
import argparse
from darwinutils.json_op import Json
import pandas as pd
import json
import numpy as np
from sklearn.preprocessing import OrdinalEncoder
from data_preprocess.sql_executor import sql_fetch
from data_preprocess.data_distribution import DataDistribution
from data_preprocess.plot_stats import PlotStats
from darwinutils.mapreduce import parallel_starmap_p
from darwinutils.log import get_task_logger
from darwinutils.config import DARWIN_CONFIG
logger = get_task_logger(__name__)

"""
Example:
python data_stats.py --config=examples/data_stats_example.json
"""


def valid_config_format(config):
    """
    {
        "table": "prod_dim_ip_mrchnt_real_h_withflag",
        "feature_columns": ["mrchnt_lgcl_dlt_flg", "mrchnt_sts", "qrc_mrchnt_flg", "d1_hldy_pay_opn_dt", "stlmnt_id_card_no", "tbl_card_cnt"],
        "label_column": "flag",
        "label_negative_value": "正常"
    }
    :param config:
    :return:
    """
    if not isinstance(config, dict):
        return False
    keywords = ["feature_columns", "label_column"]
    for key in keywords:
        if config.get(key) is None:
            return False
    if config.get("table") is None and config.get("file") is None:
        return False
    return True


def main(args):
    if not os.path.exists(args.config):
        logger.error("File {} not existed".format(args.config))
        exit(1)
    try:
        config = Json.file_to_json_without_comments(os.path.abspath(args.config))
    except:
        logger.exception("Cannot load config file {} as json format".format(args.config))
        exit(1)
    if not valid_config_format(config):
        logger.error("Config file {} content is not right: \n{}".format(args.config, json.dumps(config, indent=2)))
        exit(1)

    columns = config["feature_columns"]
    if config.get("label_column") is not None:
        if config["label_column"] not in columns:
            # do not use set since we need keep sequence
            columns.append(config["label_column"])

    condition = ""
    if config.get("sql_condition") is not None and len(config.get("sql_condition")) > 0:
        if config["sql_condition"].find("where") >= 0:
            condition = config.get("sql_condition")
        else:
            condition = "where {}".format(config.get("sql_condition"))
    data = None
    if config.get("table") is not None:
        sql_str = "select {} from {} {}".format(", ".join(columns), config["table"], condition)
        logger.info("Fetch requried data")
        try:
            data = sql_fetch(sql_str)
        except Exception as e:
            logger.exception("Failed to execute sql_fetch for {} with reason {}".format(sql_str, str(e)))
            exit(1)
    else:
        if os.path.exists(os.path.abspath(config.get("file"))):
            sql_str = os.path.abspath(config.get("file"))
            #logger.info(os.path.splitext(config.get("file")))
            if os.path.splitext(config.get("file"))[1] in [".csv"]:
                try:
                    data = pd.read_csv(os.path.abspath(config.get("file")))
                except Exception as e:
                    logger.exception("Failed read csv {} with reason {}".format(os.path.abspath(config.get("file")), str(e)))
                    exit(1)
            else:
                try:
                    data = pd.read_excel(os.path.abspath(config.get("file")))
                except Exception as e:
                    logger.exception("Failed read excel {} with reason {}".format(os.path.abspath(config.get("file")), str(e)))
                    exit(1)
            if len(columns) > 0:
                data = data[columns]
        else:
            logger.error("File {} is not existed".format(config.get("file")))
            exit(1)
    if data is not None:
        logger.info("Stats the fetched data")
        if config.get("label_column") is not None:
            #logger.info(data.columns)
            if config.get("label_column") not in data.columns.tolist():
                logger.error("Config error label {} not in data {}".format(config.get("label_column"), data.columns))
                exit(1)
            label_idx = data.columns.values.tolist().index(config.get("label_column"))
            label_value = list(data[config.get("label_column")].unique())
            if 0 not in label_value:
                logger.info("Label column {} is not encoded to 0,1 type but container {}. Force encoder it".format(
                    config.get("label_column"), label_value
                ))
                if config.get("label_negative_value") is None:
                    logger.warning("label_negative_value is not configured in config file {}. We will use Alpha order".format(
                        args.config
                    ))
                    categories = 'auto'
                elif config.get("label_negative_value") not in label_value:
                    logger.warning(
                        "label_negative_value {} is not fetched data. We will use Alpha order".format(
                            config.get("label_negative_value")
                        ))
                    categories = 'auto'
                else:
                    label_value.remove(config.get("label_negative_value"))
                    categories = [[config.get("label_negative_value")] + label_value]
                enc = OrdinalEncoder(categories=categories)
                enc.fit(data[config.get("label_column")].values.reshape(-1, 1))
                data[config.get("label_column")] = enc.transform(data[config.get("label_column")].values.reshape(-1, 1)).reshape(-1, )
        else:
            label_idx = None
        stats_rst = None
        column_types = config.get("feature_type")
        logger.info("Total stats {} columns {} rows to be stats".format(len(data.columns.values), len(data)))
        try:
            stats_rst = DataDistribution.get_all_stats(data, label_idx, bins=args.bins, pearson_top_n=args.pearsonTopN,
                                                       pearson_threshold=args.pearsonThreshold,
                                                       column_types=column_types, chimerge_flag=args.chimerge_flag
                                                       )
        except Exception as e:
            logger.exception("Failed to analysis dataframe which from {}".format(sql_str))
            exit(1)
        if stats_rst is None or len(stats_rst) == 0:
            logger.error("Donot get any stats result of dataframe which from {} and shape is {} and label id is {}".format(
                sql_str, np.shape(data), label_idx
            ))
            exit(1)
        if os.path.exists(args.output):
            import shutil
            logger.warning("Path {} existed. Force delete it".format(args.output))
            shutil.rmtree(args.output)
        os.makedirs(os.path.join(args.output, "columns"))
        logger.info("Plot the stats result")

        param_lst = []
        for column_name in columns:
            if args.chimerge_flag == "only":
                param_lst.append([stats_rst[column_name], column_name, None])
            else:
                param_lst.append([stats_rst[column_name], column_name,data[column_name].values])
        trance_layer_lst = list(parallel_starmap_p(PlotStats.plot_tables_charts_for_data_dist, param_lst,
                                                max_workers=DARWIN_CONFIG.get("parallel_job_limit", 10)))
        param_lst = []
        for trace_lst, layout, column_name in trance_layer_lst:
            if trace_lst is not None:
                param_lst.append([trace_lst, layout, os.path.join(os.path.join(os.path.abspath(args.output), "columns"),
                                                                  "{}.html".format(column_name))])
            else:
                logger.warning("Column {} cannot be plot".format(column_name))
        parallel_starmap_p(PlotStats.plot, param_lst, max_workers=DARWIN_CONFIG.get("parallel_job_limit", 10))

        #
        columns = generate_summary_report(os.path.abspath(args.output), stats_rst=stats_rst, columns=columns, label_column=config.get("label_column"))
        generate_report_html(os.path.abspath(args.output), columns=columns, column_desc_files=config.get("column_desc_files"))
        logger.info("All charts output to {}".format(os.path.abspath(args.output)))
    else:
        logger.error("Cannot get any data by sql str {}".format(sql_str))
        exit(1)


def generate_summary_report(full_path, stats_rst, columns, label_column):
    sorted_columns = columns
    missing_value_columns = []
    dominant_value_columns = []
    outlier_alert_columns = []
    pearson_with_label_values = []
    pearson_eachother_dict = {}
    for column_name in columns:
        if stats_rst[column_name].get("general_table") is not None:
            tmp_rst = stats_rst[column_name].get("general_table")
            if tmp_rst.get("Missing") is not None and tmp_rst["Missing"].values[0] > 0:
                missing_value_columns.append(column_name)
            if tmp_rst.get("Dominant") is not None and tmp_rst["Dominant"].values[0] != 0:
                dominant_value_columns.append(column_name)
            if tmp_rst.get("OutlierRatio") is not None and not tmp_rst["OutlierRatio"].isna().any():
                if tmp_rst["OutlierRatio"].values[0] >= DARWIN_CONFIG["outlier_ratio_threshold"]:
                    outlier_alert_columns.append(column_name)
        if stats_rst[column_name].get("coefficient_table") is not None and column_name != label_column:
            tmp_rst = stats_rst[column_name].get("coefficient_table")
            incides = np.where(tmp_rst["Name"].values == "{} : {}".format(column_name, label_column))[0]
            if len(incides) == 1:
                pearson_with_label_values.append(abs(tmp_rst.loc[incides[0], "Coefficient"]))
            else:
                pearson_with_label_values.append(0)
            pearson_eachother_indices = np.where(np.abs(tmp_rst["Coefficient"].values) > 0.9)[0]
            if len(pearson_eachother_indices) > 0:
                names = tmp_rst.loc[pearson_eachother_indices, "Name"]
                for name in names:
                    keys = name.split(":")
                    if pearson_eachother_dict.get(keys[0].strip()) is not None:
                        if keys[1].strip() not in pearson_eachother_dict.get(keys[0].strip()):
                            pearson_eachother_dict[keys[0].strip()].append(keys[1].strip())
                    elif pearson_eachother_dict.get(keys[1].strip()) is not None:
                        if keys[0].strip() not in pearson_eachother_dict.get(keys[1].strip()):
                            pearson_eachother_dict[keys[1].strip()].append(keys[0].strip())
                    else:
                        pearson_eachother_dict[keys[0].strip()] = [keys[1].strip()]
        else:
            pearson_with_label_values.append(0)
    if len(pearson_with_label_values) > 0:
        incides = np.argsort(pearson_with_label_values)[::-1]
        sorted_columns = list(np.array(columns)[incides])
    output_file = {
        "missing_value_columns": missing_value_columns,
        "dominant_value_columns": dominant_value_columns,
        "outlier_alert_columns": outlier_alert_columns,
        "potential_dumplicated_columns": pearson_eachother_dict
    }
    with open(os.path.join(full_path, "summary.json"), 'w') as f:
        json.dump(output_file, f, indent=2)
    return sorted_columns


def generate_report_html(full_path, columns, column_desc_files=None):
    report_tuples = []
    column_desc = None
    if column_desc_files is not None:
        for file_path in column_desc_files:
            if os.path.exists(os.path.abspath(file_path)):
                try:
                    df = pd.read_excel(os.path.abspath(file_path))
                    if "var_name" in df.columns.tolist() and "VARNAME" in df.columns.tolist():
                        df = df[["VARNAME", "var_name"]]
                        if column_desc is None:
                            column_desc = df
                        else:
                            column_desc = pd.concat([column_desc, df], ignore_index=True)
                    else:
                        logger.error("var_name or VARNAME not in file {} heads {}".format(file_path, df.columns.tolist()))
                except:
                    logger.error("Cannot load column desc file {}".format(file_path))
    if column_desc is not None:
        column_desc.to_csv("tmp.csv", index=False)
    for column_name in columns:
        link = "columns/{}.html".format(column_name)
        column_desc_str = ""
        if column_desc is not None:
            #column_desc_str = column_desc["VARNAME"][column_desc["var_name"].str.lower().isin(column_name.lower())]
            #logger.info(column_desc.var_name.str.lower().values)
            indices = np.where(column_desc.var_name.str.lower().values == column_name.lower())[0]
            if len(indices) > 1:
                logger.warning("There are multiple same name var_name {} - {}. Pickup first one".format(column_name,
                                                                                                        column_desc.loc[indices, "VARNAME"]))
                column_desc_str = column_desc.loc[indices[0], "VARNAME"]
            elif len(indices) == 0:
                if column_name not in ["label", "flag"]:
                    logger.warning("Cannot find var_name {} from column desc files {}".format(column_name, column_desc_files))
                column_desc_str = ""
            else:
                column_desc_str = column_desc.loc[indices[0], "VARNAME"]
        report_tuples.append((link, column_name, column_desc_str))

    html = '<html><body>'  # add anything else in here or even better
    for r in report_tuples:
        html += '<a href="%s">%s(%s)</a><br>' % (r[0], r[1], r[2])
    with open(os.path.join(full_path, "index.html"), "w") as f:
        f.write(html)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '--config',
        type=str,
        help='To be excuted piepline file full path',
        required=True
    )
    parser.add_argument(
        '--bins',
        type=int,
        default=6,
        help='How many bins to be used for continuous data analysis. Default number is 10',
        required=False
    )
    parser.add_argument(
        '--pearsonTopN',
        type=int,
        default=3,
        help='How many top pearson coefficient number to be retrieved',
        required=False
    )
    parser.add_argument(
        '--pearsonThreshold',
        type=float,
        default=0.3,
        help='Sensitive threshold for pearson coefficient',
        required=False
    )
    """
    parser.add_argument(
        '--columnBatchSize',
        type=int,
        default=10,
        help='How many columns to be stats at the sametime',
        required=False
    )
    """
    parser.add_argument(
        '--output',
        type=str,
        default="stats",
        help='Output path for the generated html files',
        required=False
    )
    parser.add_argument(
        '--chimerge_flag',
        type=str,
        choices=["without", "with", "only"],
        default="without",
        help='Draw chimerge flag with/without/only',
        required=False
    )
    args_flag, unparsed = parser.parse_known_args()
    main(args_flag)
