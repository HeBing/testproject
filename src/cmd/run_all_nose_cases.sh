#! /usr/bin/env bash


PROG_VERSION=0.1.0
PROGCLI=`command -v $0`
PROGNAME=${PROGCLI##*/}
PROGDIR=${PROGCLI%/*}


# add in top in order to see getopt.sh's internal log
LOG_LEVEL=${LOG_LEVEL:-debug}
source $PROGDIR/log.sh
source $PROGDIR/utils.sh

set -o pipefail

DEFAULT_run_whole_file=false
DEFAULT_clean_first=false

DEFAULT_log_dir=${log_dir:-auto_run_logs}
DEFAULT_f_log_all=log.all

DEFAULT_f_pass_lst=pass.lst
DEFAULT_f_fail_lst=fail.lst
DEFAULT_f_fail_lst_legacy=test_result.log

DEFAULT_suite_prefix=platform_test_suite_
DEFAULT_suite_ext=.txt
DEFAULT_suite_name=full

DEFAULT_nose_logging_level=`echo ${LOG_LEVEL} | upper`
DEFAULT_nose_process_timeout=1800
DEFAULT_dry_run=false


# parse the cli arguments
source $PROGDIR/getopt.sh


# house clean setup
declare saved_stdout=7
declare saved_stderr=8
declare -a house_clean_pids=()
function house_clean() {
    if [ ${#house_clean_pids[@]} -gt 0 ]; then
        echo "house clean pids ${house_clean_pids[@]}" >&${saved_stderr}
        # TODO: try to sync last content to the processes which will be killed
        sync
        kill ${house_clean_pids[@]}
    fi
}
trap "house_clean" EXIT
function setup_oob_console_monitor() {
    # ------------------------------------------
    # setup out-of-band console montior, so that
    # the console output can be saved silently
    #
    # define saved file for console output
    local f_log_out=${1:-/tmp/log.console.$USER.$$.out}
    local f_log_err=${2:-/tmp/log.console.$USER.$$.err}
    if [ -n "$3" ]; then
        if declare -p saved_stdout >/dev/null; then
            saved_stdout=$3
        else
            declare -g saved_stdout=$3
        fi
    elif [ -z "$saved_stdout" ]; then
        log_error "setup_oob_console_monitor needs 3rd argument to be a valid stdout fd number"
        return 1
    fi
    if [ -n "$4" ]; then
        if declare -p saved_stderr >/dev/null; then
            saved_stderr=$4
        else
            declare -g saved_stderr=$4
        fi
    elif [ -z "$saved_stderr" ]; then
        log_error "setup_oob_console_monitor needs 3rd argument to be a valid stderr fd number"
        return 1
    fi
    local track_pid=$$

    # dup/save stderr and stdout
    eval "exec ${saved_stdout}>&1"
    eval "exec ${saved_stderr}>&2"

    # launch monitor process
    tail --pid $track_pid -f $f_log_out >&${saved_stdout} &
    house_clean_pids+=($!)
    if [ "$f_log_out" != "$f_log_err" ]; then
        tail --pid $track_pid -f $f_log_err >&${saved_stderr} &
        house_clean_pids+=($!)
    fi

    # redirect stdout and stderr
    if [ "$f_log_out" != "$f_log_err" ]; then
        exec 1>>$f_log_out 2>>$f_log_err
    else
        exec 1>>$f_log_out 2>&1
    fi

    # log for debug
    # linux and osx compatible ps syntax
    ps -o pid,ppid,command -p ${house_clean_pids[@]} | sed -e 's/^/[mon_pids] >> /g' | log_lines debug
    #
    # end of out-of-band console monitor setup
    # ------------------------------------------
}
function run_nose_cases_validate_parameter() {
    # ------------------------
    # validate input parameter
    #
    # globalize test suite file name for subsequent run_nose_cases call
    declare -g f_suite=${suite_prefix}${suite_name}${suite_ext}

    if [ ${suite_name} = "_fail" -a -f "$log_dir/$f_fail_lst" ]; then
        log_info "Generate dynamic test suite \"${suite_name}\" from previous failed run."
        cp $log_dir/$f_fail_lst ${f_suite}
    fi
    if [ ! -f ${f_suite} ]; then
        log_error "Test suite file \"${f_suite}\" does not exist"
        return 1
    fi
}
function run_nose_cases_prepare_directory() {
    # ------------------------
    # prepare directory
    #
    if [ "$clean_first" = "true" ]; then
        if cd $log_dir 2>/dev/null; then
            files=`ls -1d *:*.log 2>/dev/null`
            if [ -n "$files" ]; then
                rm -f $files
            fi
            cd - >/dev/null
        fi

        # backup pass case list first
        if [ -f $log_dir/$f_pass_lst ]; then
            cp $log_dir/$f_pass_lst $log_dir/$f_pass_lst.old
            echo -n >$log_dir/$f_pass_lst
        fi
    fi
    # backup fail case list first
    if [ -f $log_dir/$f_fail_lst ]; then
        cp $log_dir/$f_fail_lst $log_dir/$f_fail_lst.old
        echo -n >$log_dir/$f_fail_lst
    fi

    # create log_dir if needed
    if [ ! -d $log_dir ]; then mkdir -p $log_dir; fi
    touch $log_dir/$f_fail_lst
    touch $log_dir/$f_pass_lst

    # truncate aggregated log file
    if [ -f $log_dir/$f_log_all ]; then
        echo -n >$log_dir/$f_log_all
    else
        touch $log_dir/$f_log_all
    fi
}
function run_nose_cases_inspect_test_suite() {
    # ----------------------------------------------------------
    # find out all individual test cases which had not passed.
    #
    # define global array for all run_nose_cases flow
    declare -ag all_target_cases=()
    declare -ag all_passed_cases=()

    # ------------------------
    # mandatory constant
    #
    # NOTE: nose test case should not use GPU in case it competing with celery task in same node
    export CUDA_VISIBLE_DEVICES=${CUDA_VISIBLE_DEVICES:-""}

    local f
    for f in `cat ${f_suite} | grep -v "^#"`
    do
        local top_level_case_name=`basename $f .py`
        local lines="`nosetests --collect-only -svx $top_level_case_name 2>&1`" || {
            log_error "Fail to harvest cases from \"$f\", abort!"
            echo "$lines" | sed -e 's/^/>> /g' | log_lines debug
            return 1
        }
        local n_cases_exp=`echo "$lines" | grep "Ran [0-9]\+ test" | awk '{print $2}'`

        local -a nose_cases=(`echo "$lines" | grep "^$top_level_case_name\." | cut -d' ' -f1 | sed -e 's/\./:/'`)
        local n_cases=${#nose_cases[@]}
        if [ $n_cases -eq 0 ] && echo "$lines" | grep -sq "^Failure:"; then
            log_error "Fail to harvest cases from \"$f\", abort!"
            echo "$lines" | sed -e 's/^/>> /g' | log_lines debug
            return 1
        fi
        log_debug "Found ${n_cases} new cases in \"$f\" besides existing ${#all_target_cases[@]}/$((${#all_target_cases[@]}+${#all_passed_cases[@]})) cases"
        if [ $n_cases_exp -ne $n_cases ]; then
            log_error "Some cases has non-standard case name in \"$top_level_case_name\""
            echo "$lines" | grep -v "^$top_level_case_name\." | sed -e 's/^/>> /g' | log_lines debug
            return 1
        fi
        declare p=""
        for p in ${nose_cases[@]}
        do
            if grep -sqx "$p" $log_dir/$f_pass_lst 2>/dev/null; then
                all_passed_cases+=($p)
                log_debug ">> Skip nose test \"$p\" which was already passed."
                continue
            fi
            all_target_cases+=($p)
        done
    done
    #echo "${all_target_cases[@]}" | tr ' ' '\n' | nl | sed -e 's/^/[all] 1>> /g' | log_lines debug
    #echo "${all_passed_cases[@]}" | tr ' ' '\n' | nl | sed -e 's/^/[pass] 1>> /g' | log_lines debug
}
function run_nose_cases_start() {
    # -----------------------------------------------
    # upsample to whole case file from individual case
    # in run_whole_file mode
    #
    # pick global array "all_target_cases" and "all_passed_cases" from previous inspect function
    #echo "${all_target_cases[@]}" | tr ' ' '\n' | nl | sed -e 's/^/[all] 2>> /g' | log_lines debug
    #echo "${all_passed_cases[@]}" | tr ' ' '\n' | nl | sed -e 's/^/[pass] 2>> /g' | log_lines debug
    if ! declare -p all_target_cases >/dev/null; then
        log_error "run_nose_cases_start needs \"all_target_cases\" variable be ready"
        return 1
    fi
    if ! declare -p all_passed_cases >/dev/null; then
        log_error "run_nose_cases_start needs \"all_passed_cases\" variable be ready"
        return 1
    fi

    if $run_whole_file; then
        _all_target_cases=(`echo "${all_target_cases[@]}" | tr ' ' '\n' | cut -d: -f1 -s | sort -u | xargs`)
    else
        _all_target_cases=(${all_target_cases[@]})
    fi

    # ------------------------
    # mandatory constant
    #
    # NOTE: nose test case should not use GPU in case it competing with celery task in same node
    export CUDA_VISIBLE_DEVICES=${CUDA_VISIBLE_DEVICES:-""}

    # -----------------------------------------------
    # Start running cases
    #
    let n_all_cases=${#_all_target_cases[@]}
    let icnt=${#all_passed_cases[@]}
    ((n_all_cases+=icnt))
    let fcnt=0
    declare passed_cases
    for p in ${_all_target_cases[@]}
    do
        declare _p=`echo "$p" | sed -e 's/:/./'`

        # calculate the real case number which will be run in this round
        if $run_whole_file; then
            _target_cases=(`echo "${all_target_cases[@]}" | tr ' ' '\n' | grep "^$p:" | xargs`)
        else
            _target_cases=($p)
        fi
        ((icnt+=${#_target_cases[@]}))

        # log for debug
        log_debug "Run nose test $icnt/$fcnt/${n_all_cases} \"$p\" ..."
        if $run_whole_file; then
            echo "${_target_cases[@]}" | tr ' ' '\n' | sed -e 's/^/>> /g' | log_lines debug
        fi

        # backdoor for dry-run after showes the case we plan to run
        if $dry_run; then
            continue
        fi

        # it's time to run the case
        # NOTE: do NOT use --nologcapture because we depends on logcapture to log extra fail/error lines
        SECONDS=0
        nosetests -xv --logging-level=${nose_logging_level} --process-timeout=${nose_process_timeout} $p 2>&1 | tee $log_dir/$p.log
        rc=$?
        local elapsed=`date -u -d @$SECONDS +"%T"`

        # parse test report/log
        local tst_rpt_summary=`tail -n1 $log_dir/$p.log 2>/dev/null`
        local tst_rpt_summary_errors=`echo "$tst_rpt_summary" | grep "errors=" | sed -e 's/^.*errors=\([0-9]\+\).*/\1/'`
        local tst_rpt_summary_failures=`echo "$tst_rpt_summary" | grep "failures=" | sed -e 's/^.*failures=\([0-9]\+\).*/\1/'`
        local -a tst_rpt_error_cases=(`grep "^ERROR: $_p" $log_dir/$p.log | awk '{print $2}' | sed -e 's/\./:/'`)
        local -a tst_rpt_fail_cases=(`grep  "^FAIL: $_p"  $log_dir/$p.log | awk '{print $2}' | sed -e 's/\./:/'`)

        # count error/fail from report/log and validate
        if [ -z "$tst_rpt_summary_errors" ]; then tst_rpt_summary_errors=0; fi
        if [ -z "$tst_rpt_summary_failures" ]; then tst_rpt_summary_failures=0; fi
        if [ $tst_rpt_summary_errors -ne ${#tst_rpt_error_cases[@]} ]; then
            log_warn "Inconsistent report \"errors\"=$tst_rpt_summary_errors vs. \"#ERROR:\"=${#tst_rpt_error_cases[@]} in case $p"
        fi
        if [ $tst_rpt_summary_failures -ne ${#tst_rpt_fail_cases[@]} ]; then
            log_warn "Inconsistent report \"failures=\"$tst_rpt_summary_failures vs. \"#FAIL:\"=${#tst_rpt_fail_cases[@]} in case $p"
        fi

        # record tested cases
        local -a tst_rpt_bad_cases=`set_union tst_rpt_error_cases[@] tst_rpt_fail_cases[@]`
        local -a tst_rpt_pass_cases=`set_difference _target_cases[@] tst_rpt_bad_cases[@]`
        if [ ${#tst_rpt_pass_cases[@]} -gt 0 ]; then
            echo "${tst_rpt_pass_cases[@]}" | tr ' ' '\n' >>$log_dir/$f_pass_lst
            all_passed_cases+=($passed_cases)
            log_info "Run nose test $icnt/$fcnt/${n_all_cases} \"$p\" ... succ in ${elapsed}"
        fi
        if [ ${#tst_rpt_bad_cases[@]} -gt 0 ]; then
            echo "${tst_rpt_bad_cases[@]}" | tr ' ' '\n' >>$log_dir/$f_fail_lst
            ((fcnt+=${#tst_rpt_bad_cases[@]}))
            log_error "Run nose test $icnt/$fcnt/${n_all_cases} \"$p\" ... fail in ${elapsed}"
        fi
    done
}
function run_nose_cases_summary() {
    local beg=$1
    local delta=`date "+%s"`
    ((delta-=beg))
    local elapsed=`date -u -d @$delta +"%T"`

    # ----------------------
    # evaluate test run result
    #
    rst=`cat $log_dir/$f_fail_lst | wc -l | awk '{print $1}'`
    if [ ${rst} -ne 0 -a "${dry_run}" == "false" ]; then
        log_error "Test fail in $elapsed"
        log_error "Please refer $log_dir/$f_fail_lst to get detail"
        cat $log_dir/$f_fail_lst 2>/dev/null | sed -e 's/^/>> /g' | log_lines error

        # associate legacy fail list to morden
        if [ ! -L $log_dir/$f_fail_lst_legacy ]; then
            if [ -f $log_dir/$f_fail_lst_legacy ]; then rm -f $log_dir/$f_fail_lst_legacy; fi
            ln -s $log_dir/$f_fail_lst $log_dir/$f_fail_lst_legacy
        fi
        (exit $rst)
    else
        log_info "Test pass in $elapsed"
    fi
}
function run_nose_cases_per_suite() {
    local beg=`date "+%s"`

    run_nose_cases_validate_parameter && \
    run_nose_cases_prepare_directory && \
    setup_oob_console_monitor $log_dir/$f_log_all $log_dir/$f_log_all 7 8 && \
    run_nose_cases_inspect_test_suite && \
    run_nose_cases_start && \
    run_nose_cases_summary $beg
}

run_nose_cases_per_suite
