#! /usr/bin/env python3
# -*- coding:utf-8 -*-

"""Configuration file for default standalone logger
"""


import os
from socket import (SOCK_DGRAM, SOCK_STREAM)
import datetime


logging_config = {
    'version': 1,
    'disable_existing_loggers': True,
    'formatters': {
        'verbose': {
            'format': "[%(asctime)s] %(levelname)s [%(name)s:%(lineno)s] [%(process)d:%(thread)d] %(message)s",
            'datefmt': "%Y-%m-%d %H:%M:%S"
        },
        'standard': {
            'format': "[%(asctime)s] %(levelname)s [%(name)s:%(lineno)s] %(message)s",
            'datefmt': "%Y-%m-%d %H:%M:%S"
        },
        'simple': {
            'format': '%(levelname)s %(message)s'
        },
    },
    'handlers': {
        'null': {
            'level': 'INFO',
            'class': 'logging.NullHandler',
        },
        'console': {
            'level': 'INFO',
            'class': 'logging.StreamHandler',
            'formatter': 'verbose'
        },
        'file': {
            'level': 'INFO',
            'class': 'logging.handlers.RotatingFileHandler',
            # TODO: for celery and flask env, their logger should already resolved the multi-process safe(?)
            ## multi-processes safe
            #'class': 'cloghandler.ConcurrentRotatingFileHandler',
            'maxBytes': 1024 * 1024 * 10,
            'backupCount': 50,
            'delay': True,
            'filename': os.path.join(
                os.environ.get('DARWIN_LOG_DIR', default='/tmp'),
                'darwin.log',
                #'darwin.log.{}'.format(datetime.datetime.now().strftime("%Y%m%d_%H%M%S"))
            ),
            'formatter': 'verbose'
        },
    },
    'loggers': {
        '': {
            'handlers': ['console'],
            'level': 'INFO',
        },
    }
}
