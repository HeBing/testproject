#生成环境配置：<br />
 "###### impala configurations #####": false,<br />
  "impala_config": {<br />
    "host": "10.1.30.218",<br />
    "port": 21050,<br />
    "user": "impala",<br />
    "passwd": "96cEj260C8Vs",<br />
    "database": "dwsrc"<br />
  },<br />
  "###### hdfs configurations #####": false,<br />
  "hdfs_config": {<br />
    "host": ["10.1.30.210", "10.1.30.209"],<br />
    "port": 50070<br />
  },<br />
  "###### hive configurations #####": false,<br />
  "hive_config": {<br />
    "host": "10.1.30.213",<br />
    "port": 10000<br />
  },<br />
  "sql_timeout": 1200,<br />
  <br />
#测试环境配置：<br />
 "impala_config": {<br />
    "host": "172.16.136.90",<br />
    "port": 21050,<br />
    "user": "impala",<br />
    "passwd": "8a4AFf90a0A",<br />
    "database": "dwsrc"<br />
  },<br />
  "###### hdfs configurations #####": false,<br />
  "hdfs_config": {<br />
    "host": "172.16.136.93",<br />
    "port": 50070<br />
  },<br />
  "###### hive configurations #####": false,<br />
  "hive_config": {<br />
    "host": "172.16.136.89",<br />
    "port": 10000<br />
  },<br />
  "sql_timeout": 1200,<br />
#RC测试环境配置：<br />
  "impala_config": {<br />
    "host": "172.16.133.166",<br />
    "port": 21050,<br />
    "user": "app",<br />
    "passwd": "123456",<br />
    "database": "dwsrc"<br />
  },<br />
  "###### hdfs configurations #####": false,<br />
  "hdfs_config": {<br />
    "host": ["172.16.133.166","172.16.133.167"],<br />
    "port": 50070<br />
  },<br />
  "###### hive configurations #####": false,<br />
  "hive_config": {<br />
    "host": "172.16.133.167",<br />
    "port": 10000<br />
  },<br />
  "sql_timeout": 1200,<br />
