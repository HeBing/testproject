#! /usr/bin/env python3
# -*- coding:utf-8 -*-


import os
from enum import Enum
import time
import uuid
import pickle
from queue import (Queue, Empty)
from operator import (
    itemgetter,
    attrgetter,
    is_not,
    methodcaller,
)
from functools import (
    partial,
)
import zlib
from darwinutils.config import DARWIN_CONFIG
from darwinutils.helper import (
    get_current_function_name,
)
from darwinutils.log import get_task_logger
from darwinutils.helper import (
    load_func_from_spec,
    f_compose_l,
    f_compose_r,
    f_star_call,
    f_si_call,
    f_group,
    f_timeit_call,
    f_list_rize,
    f_flip_call,
    reduce_uuid,
    loop_run,
    f_echo,
    f_unstar2_call,
)
from pipeline.common import (
    PLVariableSpec,
    PLCacheSpec,
    puas,
)


logger = get_task_logger(__name__)


"""
Get a pipeline variable definition spec from a loose string definition

:param _: "<var_pl_name>{:<var_func_name>}" style variable spec definition string
:type _: str
:return: a pipeline variable definition spec
:rtype: PLVariableSpec
"""
f_get_pl_variable_spec_from_str = f_compose_r(
    partial(str.split, sep=':'),
    itemgetter(slice(0, 2)),
    lambda _: _ * (3-len(_)),
    partial(zip, PLVariableSpec._fields),
    dict,
    lambda _: PLVariableSpec(**_)
)


pl_var_encoder_seq = [
    pickle.dumps,
]
pl_var_decoder_seq = [
    pickle.loads,
]
if DARWIN_CONFIG["pipeline_result_compress_method"]:
    pl_var_encoder_seq.append(partial(zlib.compress, level=zlib.Z_BEST_SPEED))
    pl_var_decoder_seq.append(zlib.decompress)
pl_var_encoder = f_star_call(f_compose_r, pl_var_encoder_seq)
pl_var_decoder = f_star_call(f_compose_l, pl_var_decoder_seq)


class CtrlMagic(Enum):
    """
    The pipeline control magic data to distinguish normal data used by workload function
    NOTE: the value of each enum must be int32 so that it could be mapped in grpc channel
    """

    # client request stop instance in data channel
    CMD_STOP_INSTANCE = 1
    # when workload function returns insufficient output, fill in this value to keep the shape and distinguish
    # with the actual output from workload function.
    MISSING_VALUE = 2

    # for testing purpose: client send this value to simulate sending real ValueError
    ValueError_VALUE = 3


MISSING_VALUE_TRANSITION = CtrlMagic.MISSING_VALUE
MISSING_VALUE_TRANSITION_TYPE = type(CtrlMagic.MISSING_VALUE)


def pl_var_translate_none(value):
    """
    Convert magic transition value for missing value of workload output to None
    # NOTE: when workload function return insufficient output, warm node service will fill in MISSING_VALUE_TRAINSTION
    #       to keep the output shape
    :param value:
    :return:
    """
    if isinstance(value, MISSING_VALUE_TRANSITION_TYPE) and value == MISSING_VALUE_TRANSITION:
        r = None
    else:
        r = value
    return r


class PipeVariableInterface:
    def __init__(self, name):
        self._name_spec = None

        self.name = name

    def __getstate__(self):
        """
        Customize for pickle dump
        :return:
        """
        logger.debug("Pickle dump {}({}) with name {}".format(type(self).__name__, hex(id(self)), self.name))
        state = self.__dict__.copy()

        # remove derived and unpicklable attributes
        pass

        return state

    def __setstate__(self, state):
        """
        Customize for pickle load
        :param state:
        :return:
        """
        # restore derived and unpicklable attributes
        pass

        self.__dict__.update(state)
        logger.debug("Pickle load {}({}) with name {}".format(type(self).__name__, hex(id(self)), self.name))

    @property
    def name(self):
        return self._name_spec.pl_name

    @name.setter
    def name(self, value):
        if isinstance(value, str):
            value = f_get_pl_variable_spec_from_str(value)

        elif not isinstance(value, PLVariableSpec):
            raise ValueError("PipeVariable name needs to be either str or PLVariableSpec instead of {}".format(
                type(value).__name__,
            ))
        self._name_spec = value

    """ alias of name_spec.func_name """
    @property
    def func_name(self):
        fn_name = self._name_spec.func_name
        return fn_name if fn_name else self.name

    @property
    def value(self):
        raise NotImplementedError("{} not implemented yet".format(get_current_function_name()))

    @value.setter
    def value(self, value):
        raise NotImplementedError("{} not implemented yet".format(get_current_function_name()))


class PipeVariableBase(PipeVariableInterface):
    def __init__(self, name):
        self._value = None
        super().__init__(name)

    @property
    def value(self):
        return self._value

    @value.setter
    def value(self, value):
        self._value = value


class PipeVariableCelery(PipeVariableInterface):
    """
    Multi-process based PipeLine variable
    """
    def __init__(self, name, sess_id=None, key_func=None, timeout=None, expire_time=None):
        self._sess_id = None
        super().__init__(name)

        # it acts as a local cache of celery result
        self._value = None

        self._sess_id = sess_id
        self._key_func = key_func
        self._timeout = timeout

    def __getstate__(self):
        state = self.__dict__.copy()
        del state["_value"]
        return state

    def __setstate__(self, state):
        self.__dict__.update(state)
        self._value = None

    @property
    def sess_id(self):
        if self._sess_id is None:
            self._sess_id = str(uuid.uuid4())
            logger.debug("Generate immutable uuid {}".format(self._sess_id))
        return self._sess_id

    @property
    def uuid(self):
        return str(uuid.uuid3(uuid.UUID(self.sess_id), str(self._name_spec)))

    @property
    def timeout(self):
        if self._timeout is None:
            self._timeout = DARWIN_CONFIG['pipeline_result_default_timeout']
        return self._timeout

    @classmethod
    def get_value(cls, var_uuid, *args, timeout=None, **kwargs):
        # import celery to initialize backend though its not directly used
        # from darwin_service import celery as my_celery
        _ = load_func_from_spec(DARWIN_CONFIG.service_name, "celery_app")
        from celery.result import AsyncResult
        r = AsyncResult(id=var_uuid)

        is_ready = f_compose_r(
            attrgetter("ready"),
            partial(
                f_flip_call,
                loop_run,
                # lambda r, i_cnt, eta: r,
                partial(f_unstar2_call, f_compose_r(
                    itemgetter(0),
                    itemgetter(0),
                    f_echo,
                )),
                sleep_interval=0.001,
                timeout=timeout,
            ),
        )(r)

        if is_ready:
            result = r.result
        else:
            raise TimeoutError("Cannot get celery variable {} in {}s".format(
                var_uuid, timeout,
            ))
        """
        # deprecated old get_value()
        from celery.result import AsyncResult
        r = AsyncResult(id=var_uuid)

        # TODO: support running pipeline client call in flask-celery framework
        from celery.result import allow_join_result
        with allow_join_result():
            result = r.get(*args, timeout=timeout, **kwargs)
        """
        return result

    @classmethod
    def put_value(cls, var_uuid, value, *args, timeout=None, **kwargs):
        from pipeline.tasks import save_data
        # NOTE: input value was a combined value with uuid but the output expects to be value only
        # NOTE: save_data will only return the "value" field in the input value.
        t = save_data.apply_async(args=[value], kwargs={}, task_id=var_uuid)

        result_backend_verification = DARWIN_CONFIG['pipeline_result_backend_verification']
        if result_backend_verification is True:
            r = cls.get_value(var_uuid, timeout=timeout)
            """
            # deprecated old put_value's verification
            from celery.result import allow_join_result
            with allow_join_result():
                r = t.get(*args, timeout=timeout, **kwargs)
            """
        else:
            _, r = value
        return r

    @property
    def value(self):
        if self._value is None:
            self._value = type(self).get_value(self.uuid, timeout=self.timeout)
        return self._value

    @value.setter
    def value(self, value):
        if self._value is not None:
            raise IOError("Cannot change immutable variable")

        # send uuid to remote also for verfication to make sure it could be retrieved.
        _value = type(self).put_value(self.uuid, (self.uuid, value), timeout=self.timeout)

        if self._key_func is not None and self._key_func(value) != self._key_func(_value):
            raise IOError("Data inconsistent after serialized to celery {}".format(self._sess_id))

        logger.debug("Variable \"{}\" was assigned with value {} in session {}".format(
            self.name,
            _value,
            self.sess_id,
        ))
        self._value = _value


class PipeVariableRedis(PipeVariableInterface):
    """
    Multi-process based PipeLine variable
    """
    def __init__(self, name, sess_id=None, key_func=None, timeout=None, expire_time=None):
        self._sess_id = None
        self._redis_cli = None
        super().__init__(name)

        # it acts as a local cache of celery result
        self._value = None

        self._sess_id = sess_id
        self._key_func = key_func
        self._timeout = timeout
        self._expire_time = expire_time

    def __getstate__(self):
        state = self.__dict__.copy()
        del state["_value"]
        del state["_redis_cli"]
        return state

    def __setstate__(self, state):
        self.__dict__.update(state)
        self._value = None
        self._redis_cli = None

    @property
    def redis_cli(self):
        if self._redis_cli is None:
            self._redis_cli = load_func_from_spec(DARWIN_CONFIG.service_name, "redis_client")
        return self._redis_cli

    @property
    def sess_id(self):
        if self._sess_id is None:
            self._sess_id = str(uuid.uuid4())
            logger.debug("Generate immutable uuid {}".format(self._sess_id))
        return self._sess_id

    @property
    def uuid(self):
        return str(uuid.uuid3(uuid.UUID(self.sess_id), str(self._name_spec)))

    @property
    def timeout(self):
        if self._timeout is None:
            self._timeout = DARWIN_CONFIG["pipeline_result_default_timeout"]
        return self._timeout

    @property
    def expire_time(self):
        if self._expire_time is None:
            self._expire_time = DARWIN_CONFIG['pipeline_result_expire_time']
        return self._expire_time

    @classmethod
    def get_value(cls, var_uuid, *args, timeout=None, **kwargs):
        _redis = load_func_from_spec(DARWIN_CONFIG.service_name, "redis_client")

        result = f_compose_r(
            partial(partial, _redis.get),
            #partial(partial, _redis.exists),
            partial(
                f_flip_call,
                loop_run,
                # lambda r, i_cnt, eta: r is not None,
                partial(f_unstar2_call, f_compose_r(
                    itemgetter(0),
                    itemgetter(0),
                    partial(f_flip_call, is_not, None),
                )),
                sleep_interval=0.001,
                timeout=timeout,
            ),
            pl_var_decoder,
        )(var_uuid)

        """
        # deprecated old get_value
        for try_cnt in range(2):
            # try getting by key
            result = _redis.get(var_uuid)
            if result is not None:
                result = pl_var_decoder(result)
                break

            # wait for the result in a channel once.
            if try_cnt == 0:
                q = Queue(1)

                # TODO: filter and load "type"="message" and "channel"=var_uuid msgs only
                msg_handler = f_compose_r(
                    itemgetter("data"),
                    pl_var_decoder,
                )

                if DARWIN_CONFIG.debug_performance:
                    msg_handler = f_compose_r(
                        f_group(
                            partial(f_si_call, time.time),
                            partial(f_timeit_call, msg_handler),
                        ),
                        partial(f_star_call, PLCacheSpec),
                    )
                ps = _redis.pubsub()
                kwargs = {
                    var_uuid: f_compose_r(
                        msg_handler,
                        q.put,
                    ),
                }
                ps.subscribe(**kwargs)
                time_beg = time.time()
                mq_t = ps.run_in_thread(sleep_time=0.001)

                try:
                    payload = q.get(True, timeout)
                    if DARWIN_CONFIG.debug_performance:
                        enqueue_time, (payload, loads_duration) = payload
                        logger.critical("perf: wait pl var .{} for {:.6f}s. and load for {:.6f}s".format(
                            var_uuid[puas], enqueue_time-time_beg, loads_duration,
                        ))

                except Empty:
                    payload = None

                finally:
                    ps.unsubscribe(list(kwargs.keys()))
                    #mq_t.join()

                if payload is None:
                    raise TimeoutError("Did not get response from session .{} in {}s".format(
                        var_uuid[puas], timeout,
                    ))
        """
        return result

    @classmethod
    def put_value(cls, var_uuid, value, *args, timeout=None, ex=None, **kwargs):
        if value is None:
            return None

        _redis = load_func_from_spec(DARWIN_CONFIG.service_name, "redis_client")
        r = _redis.set(var_uuid, pl_var_encoder(value), *args, ex=ex, nx=True, **kwargs)
        if r is None:
            raise IOError("Cannot change immutable variable. Key .{} already exists in redis.".format(
                var_uuid
            ))

        """
        # deprecated old put_value's notification
        # notify subscriber that value is ready.
        payload = 1
        _ = _redis.publish(var_uuid, pl_var_encoder(payload))
        """

        result_backend_verification = DARWIN_CONFIG['pipeline_result_backend_verification']
        if result_backend_verification is True:
            r = cls.get_value(var_uuid, timeout=timeout)
        else:
            r = value
        return r

    @property
    def value(self):
        if self._value is None:
            try:
                beg = time.time()
                self._value = type(self).get_value(self.uuid, timeout=self.timeout)
            except TimeoutError as _e:
                logger.error("Timeout when getting pl var redis {}/{} after {}/{}".format(
                    self.name, self.uuid, time.time()-beg, beg,
                ))
                raise

            if DARWIN_CONFIG.debug_performance:
                end = time.time()
                logger.critical("perf: pl var redis \"{}\"/.{}, get value for sess .{} in {:.6f}s from data {}".format(
                    self.name, self.uuid[puas],
                    self.sess_id[puas],
                    end - beg,
                    type(self._value).__name__,
                ))
        return pl_var_translate_none(self._value)

    @value.setter
    def value(self, value):
        if self._value is not None:
            raise IOError("Cannot change immutable variable. Variable {}/.{} already has value assigned.".format(
                self.name, self.uuid,
            ))

        # send uuid to remote also for verification to make sure it could be retrieved.
        try:
            beg = time.time()
            _value = type(self).put_value(self.uuid, value, timeout=self.timeout, ex=self.expire_time)
        except TimeoutError as _e:
            logger.error("Timeout when setting pl var redis {}/{} after {}/{}".format(
                self.name, self.uuid,
                time.time()-beg, beg,
                self.redis_cli.exists(self.uuid)
            ))
            raise

        if self._key_func is not None and self._key_func(value) != self._key_func(_value):
            raise ValueError("Data inconsistent after serialized to redis {}".format([self._sess_id, value, _value]))

        if DARWIN_CONFIG.debug_performance:
            end = time.time()
            logger.critical("perf: pl var redis \"{}\"/.{}, set value for sess .{} in {:.6f}s with data {}".format(
                self.name, self.uuid[puas],
                self.sess_id[puas],
                end - beg,
                type(_value).__name__,
            ))
        self._value = _value
