#! /usr/bin/env python3
# -*- coding:utf-8 -*-


import os
import sys
import signal
import types
from enum import Enum
import uuid
import pickle
import traceback
import math
import random
import time
import threading
import logging
from threading import (
    Thread,
    Event,
)
from queue import (
    Queue,
    Empty,
)
from concurrent.futures import (
    ProcessPoolExecutor,
    ThreadPoolExecutor,
)
import grpc
from inspect import Signature
from collections import (
    Iterable,
    Iterator,
    Callable,
    deque,
    OrderedDict,
    ChainMap,
)
from operator import (
    getitem,
    itemgetter,
    attrgetter,
    concat,
    mul,
    not_,
    lt,
    gt,
    eq,
    is_not,
    contains,
    methodcaller,
    sub,
    neg,
    delitem,
)
from functools import (
    partial,
    partialmethod,
    reduce,
    lru_cache,
)
from itertools import (
    chain,
    compress,
    starmap,
    zip_longest,
)
from multiprocessing import Process
import zerorpc
import gevent
from darwinutils.log import get_task_logger
from darwinutils.config import DARWIN_CONFIG
from darwinutils.decorator import (
    ExceptCatchRunResultErrorMsg,
)
from darwinutils.fn import (
    f_call,
    f_compose_l,
    f_compose_r,
    f_echo,
    f_flip_call,
    f_group,
    f_list_rize,
    f_si_call,
    f_star_call,
    f_star2_call,
    f_timeit_call,
    f_unstar_call,
    f_zip,
)
from darwinutils.helper import (
    enforce_gc,
    find_free_port,
    get_celery_task_state,
    get_current_function_name,
    get_dict_filter_function_per_keys,
    get_free_celery_task_id,
    get_local_source_ip_address,
    inspect_func,
    load_func_from_spec,
    loop_run,
    redis_extlock,
    redis_lock,
    redis_unlock,
    reduce_uuid,
)
from darwinutils.mapreduce import (
    parallel_map_t,
    parallel_map_p,
    parallel_starmap_t,
    parallel_starmap_p,
)

from protocol.basic_pb2 import (
    VarArray,
    VarList,
    VarMap,
)
from protocol.utils import (
    VarList_,
    VarMap_,
)
from protocol.utils import loads as pb_loads
from protocol.utils import dumps as pb_dumps
from protocol.pipeline_services_pb2 import (
    TaskRequest,
    TaskResponse,
)
from protocol.pipeline_services_pb2_grpc import (
    add_PipeLineServicer_to_server,
    PipeLineServicer,
    PipeLineStub,
)
from pipeline.pipevariable import (
    f_get_pl_variable_spec_from_str,
    MISSING_VALUE_TRANSITION,
    PipeVariableCelery,
    PipeVariableRedis,
    pl_var_decoder,
    pl_var_encoder,
    pl_var_translate_none,
)
from pipeline.common import (
    PLFuncModuleSpec,
    PLNodeSessDataSpec,
    PLCacheSpec,
    PLCtrlCmdSpec,
    puas,
)
from pipeline.tasks import (
    run_pl_warm_node_with_gpu,
    run_pl_warm_node_with_cpu,
)


logger = get_task_logger(__name__)
_rpc_client_heartbeat = DARWIN_CONFIG["pipeline_rpc_client_heartbeat"]


@lru_cache(maxsize=1000)
def _get_zrpc_client(endpoint, tid, pid):
    logger.debug("establish zrpc connection to {} from thread {} in process {}".format(endpoint, tid, pid))
    c = zerorpc.Client(heartbeat=_rpc_client_heartbeat)
    try:
        c.connect(endpoint)
    except Exception as _e:
        logger.exception("Fail to connect to zrpc endpoint {}".format(endpoint))
        raise
    return c


def get_zrpc_client(endpoint):
    pid = os.getpid()
    tid = threading.current_thread().ident
    return _get_zrpc_client(endpoint, tid, pid)


@lru_cache(maxsize=100)
def _get_grpc_client(endpoint, tid, pid):
    logger.debug("establish grpc connection to {} from thread {} in process {}".format(endpoint, tid, pid))
    try:
        channel = grpc.insecure_channel(endpoint)
        stub = PipeLineStub(channel)
    except Exception as _e:
        logger.exception("Fail to connect to grpc endpoint {}".format(endpoint))
        raise
    return stub


def get_grpc_client(endpoint):
    pid = os.getpid()
    tid = threading.current_thread().ident
    # TODO: assume grpc client can be shared between thread. (?)
    return _get_grpc_client(endpoint, tid=0, pid=pid)


hb_data_encoder = pickle.dumps
hb_data_decoder = pickle.loads
grpc_data_decoder_srv = f_compose_r(
    # decode kwargs from a VarMap attribute in the standard request pb msg from invoker
    attrgetter("task_kwargs"),
    VarMap_.loads,
)
grpc_data_decoder_cli = f_compose_r(
    # decode kwargs from a VarMap attribute in the standard response pb msg from server
    attrgetter("content"),
    VarList_.loads,
)
grpc_data_encoder_srv = f_compose_r(
    # encode pl_node's return list to pb msg VarList
    partial(VarList_.dumps, to_bytes=False),
    # compose as standard response
    lambda _: TaskResponse(content=_),
)
grpc_data_encoder_cli = f_compose_r(
    # encode pl_node's input kwargs dict to pb msg VarMap
    partial(VarMap_.dumps, to_bytes=False),
    # compose as standard request
    lambda _: TaskRequest(task_kwargs=_),
)
is_redis_accepted_channel, is_zrpc_accepted_channel, is_grpc_accepted_channel = f_compose_r(
    partial(map, f_compose_r(
        partial(contains, DARWIN_CONFIG["pipeline_warm_node_call_accepted_channels"]),
    )),
)(["redis", "zrpc", "grpc"])
is_rpc_accepted_channel = is_zrpc_accepted_channel or is_grpc_accepted_channel


def inject_timeout(data, _timeout=None):
    data["_timeout"] = _timeout
    return data


def pl_load_func_from_spec(func_module_spec):
    """
    Load function definition from module
    :param func_module_spec: a function definition
    :type func_module_spec: Union(PLFuncModuleSpec, PLFuncModuleSpec compatible iterable)
    :return: a function object
    :rtype: Union(Callable, Exception)
    """
    if isinstance(func_module_spec, (list, tuple)):
        func_module_spec = PLFuncModuleSpec(*func_module_spec)

    from_, import_ = attrgetter("from_", "import_")(func_module_spec)
    from darwinutils.helper import load_func_from_spec
    return load_func_from_spec(from_, import_)


"""
Load and inspect a function
alias of : inspect_func(load_func_from_spec(x))
:param func_spec:
:type func_spec: PLFuncModuleSpec
:return: inspect.Signature
"""
load_and_inspect_func_from_spec = f_compose_r(pl_load_func_from_spec, inspect_func)


"""
Get a variable's pl_name from its spec definition string
NOTE:
* var's "pl_name" means its variable name in pipeline domain
* var's "func_name" means its argument name in function domain

:param _: "<var_pl_name>{:<var_func_name>}" style variable spec definition string
:type _: str
:return: var's pl_name string
"""
f_get_var_pl_name_from_spec_str = partial(
    map,
    f_compose_r(
        f_get_pl_variable_spec_from_str,
        attrgetter("pl_name")
    )
)

"""
Get a pipeline node's input/output variables' name

:param _: a PipeNode object
:type _: PipeNode
:return: a list of variables' pl_name (var name in pipeline domain)
"""
f_get_pl_node_input_var_names, f_get_pl_node_output_var_names = f_compose_r(
    partial(map, attrgetter),
    partial(map, partial(f_compose_l, f_get_var_pl_name_from_spec_str)),
)(["inputs", "outputs"])


f_f_expand = f_compose_r(
    partial(partial, mul),
    partial(f_compose_r, f_list_rize),
)

"""
Generate a function to wait for a pl inst's start up

NOTE:
* ==
f_f_wait_for_pl_inst_start_up = lambda pl_inst: partial(
    pl_inst.wait_for_state,
    type(pl_inst).STATE.started,
    sleep_interval=0.01,
    timeout=pl_inst.pl_node.start_time,
)
Be careful! zhuang bi you dai jia, waste 1h ph

:param _: a PipeNodeWarmInstance object
:type _: PipeNodeWarmInstance
:return: a partial call to the object's wait_for_state
f_f_wait_for_pl_inst_start_up = f_compose_r(
    f_group(
        # func
        attrgetter("wait_for_state"),
        # varargs
        f_compose_r(
            f_group(
                f_compose_r(
                    type,
                    attrgetter("STATE.started"),
                ),
            ),
            list,
        ),
        # varkwargs
        f_compose_r(
            f_group(
                partial(f_si_call, partial(f_echo, 0.01)),
                attrgetter("pl_node.start_time"),
            ),
            list,
            partial(zip, [
                "sleep_interval",
                "timeout",
            ]),
            dict,
        ),
    ),
    list,
    f_group(itemgetter(0), itemgetter(1, 2)),
    partial(partial, f_star_call, f_star2_call),
)
"""


def handle_msg(msg, q, hint_func=None):
    data = pl_var_decoder(msg['data'])
    enqueue_time = time.time()
    if isinstance(data, PLNodeSessDataSpec) and hint_func is not None:
        hint_func([data.sess_id[puas], enqueue_time])
    q.put(PLCacheSpec(enqueue_time, data))
    return True


class PipeNode(Callable):
    TYPE = Enum(
        "TYPE",
        (
            "cold",
            "warm",
        ),
        qualname="PipeNode.TYPE",
    )

    @classmethod
    def create_from_node_spec(cls, node_spec, **kwargs):
        pl_func_module_spec = PLFuncModuleSpec(*node_spec.get("func"))
        return cls(
            name=node_spec.get("name"),
            type_=node_spec.get("type"),
            outputs=node_spec.get("outputs"),
            inputs=node_spec.get("inputs"),
            func_module_spec=pl_func_module_spec,
            extra_args=node_spec.get("extra_args", []),
            extra_kwargs=node_spec.get("extra_kwargs", {}),
            flags=node_spec.get("flags", []),
            **kwargs,
        )

    def __init__(self,
                 name,
                 type_,
                 outputs=None,
                 inputs=None,
                 func_module_spec=None,
                 extra_args=None,
                 extra_kwargs=None,
                 flags=None,
                 ):
        """
        Constructor
        :param name: worker function name
        :param type_: cold = cold start everytime, warm = warm start
        :param outputs: mandatory
        :param inputs: optional, detect if not set
        :param func_module_spec: instruction about how to load worker function
        :param extra_args: fixed args for worker function
        :param extra_kwargs: fixed kwargs for worker function
        :param flags: flexible flags which is more like 弹幕
        """
        # variables which associated with directly initialized attributes
        self._name = name
        if type_ in PipeNode.TYPE.__members__:
            self._type = type_
        else:
            raise ValueError("{} is not a valid PipeNode type".format(type_))
        self._outputs = outputs
        self._inputs = inputs
        self._func_module_spec = func_module_spec
        self._extra_args = tuple(extra_args) if isinstance(extra_args, list) else extra_args
        self._extra_kwargs = extra_kwargs
        self._flags = flags

        # setter initialized attributes
        pass

        # variables which associated with derived attributes
        pass

        # cache variable
        self._c_func = None
        self._c_signature = None
        self._c_uuid = None
        self._c_flags = None
        self._c_kwflags = None
        self._c_fmt_flags = None
        self._c_spec = None
        self._c_log_prefix = None
        self._c_input_var_names = None
        self._c_input_arg_names = None
        self._c_output_var_names = None
        self._c_output_arg_names = None

    def __getstate__(self):
        logger.debug("Pickle dump {}({}) with name {}".format(type(self).__name__, hex(id(self)), self.name))
        state = self.__dict__.copy()

        # remove unpicklable attributes
        pass

        # remove cache attributes
        del state["_c_func"]
        del state["_c_signature"]
        del state["_c_uuid"]
        del state["_c_flags"]
        del state["_c_kwflags"]
        del state["_c_fmt_flags"]
        del state["_c_spec"]
        del state["_c_log_prefix"]
        del state["_c_input_var_names"]
        del state["_c_input_arg_names"]
        del state["_c_output_var_names"]
        del state["_c_output_arg_names"]

        return state

    def __setstate__(self, state):
        self.__dict__.update(state)

        # TODO: restore unpicklable or derivable attributes
        self._c_func = None
        self._c_signature = None
        self._c_uuid = None
        self._c_flags = None
        self._c_kwflags = None
        self._c_fmt_flags = None
        self._c_spec = None
        self._c_log_prefix = None
        self._c_input_var_names = None
        self._c_input_arg_names = None
        self._c_output_var_names = None
        self._c_output_arg_names = None

        logger.debug("Pickle load {}({}) with name {}".format(type(self).__name__, hex(id(self)), self.name))

    @property
    def spec(self):
        if self._c_spec is None:
            spec_fields = [
                ["name", f_echo],
                ["type", f_echo],
                ["inputs", f_echo],
                ["outputs", f_echo],
                ["func_module_spec:func", tuple],
                ["extra_args", tuple],
                ["extra_kwargs", f_compose_r(
                    dict.items,
                    partial(sorted, key=itemgetter(0)),
                    #OrderedDict,
                    dict,
                )],
                ["formatted_flags:flags", f_echo],
            ]
            field_names, field_handlers = partial(f_star_call, zip)(spec_fields)
            field_attr_names, field_spec_names = f_compose_r(
                partial(map, f_get_pl_variable_spec_from_str),
                partial(f_star_call, zip),
            )(field_names)
            self._c_spec = f_compose_r(
                f_star_call(attrgetter, field_attr_names),
                partial(zip, field_handlers),
                partial(map, partial(f_star_call, f_call)),
                partial(zip, field_spec_names),
                dict,
            )(self)
        return self._c_spec

    @property
    def name(self):
        return self._name if self._name else self.func_name

    @property
    def type(self):
        return self._type

    @property
    def flags(self):
        if self._c_flags is None:
            self._c_flags = f_compose_r(
                partial(filter, f_compose_r(
                    partial(f_flip_call, str.find, "="),
                    partial(gt, 0),
                )),
                partial(sorted, key=str),
            )(self._flags)
        return self._c_flags

    @property
    def kwflags(self):
        if self._c_kwflags is None:
            self._c_kwflags = f_compose_r(
                partial(filter, f_compose_r(
                    partial(f_flip_call, str.find, "="),
                    partial(lt, 0),
                )),
                partial(map, f_compose_r(
                    partial(f_flip_call, str.split, "=", maxsplit=1)
                )),
                dict,
            )(self._flags)
        return self._c_kwflags

    @property
    def formatted_flags(self):
        if self._c_fmt_flags is None:
            self._c_fmt_flags = f_compose_r(
                attrgetter("flags", "kwflags"),
                partial(zip, [
                    f_echo,
                    f_compose_r(
                        dict.items,
                        partial(map, f_compose_r(
                            partial(str.join, "="),
                        )),
                        list,
                    ),
                ]),
                partial(map, partial(f_star_call, f_call)),
                partial(reduce, concat),
                partial(sorted, key=str),
            )(self)
        return self._c_fmt_flags

    @property
    def uuid(self):
        if self._c_uuid is None:
            seeds = [
                self.name,
                self.type,
                self.inputs,
                self.outputs,
                self.func_module_spec,
                self.extra_args,
                # we MUST order the dict items before including it into the seed.
                f_compose_r(
                    methodcaller("items"),
                    partial(sorted, key=itemgetter(0)),
                    dict,
                )(self.extra_kwargs),
                # exclude flags which were started with "_" which will be considered uuid independent.
                f_compose_r(
                    partial(filter, f_compose_r(
                        partial(f_flip_call, str.startswith, "_"),
                        not_,
                    )),
                    list,
                )(self.formatted_flags),
            ]
            self._c_uuid = str(uuid.uuid3(
                uuid.UUID(DARWIN_CONFIG["pipeline_uuid_namespace"]),
                f_compose_r(
                    partial(map, str),
                    partial(reduce, concat),
                )(seeds),
            ))
        return self._c_uuid

    """alias of pl_func_module_spec.as_"""
    @property
    def func_name(self):
        return self.func_module_spec.as_ if self.func_module_spec.as_ else self.func_module_spec.import_

    @property
    def func(self):
        """
        Get function object
        :return: Callable function object
        """
        if self._c_func is None:
            """ DO IT ONLY ON-DEMAND """
            result = pl_load_func_from_spec(self.func_module_spec)
            if not isinstance(result, Callable):
                if isinstance(result, Exception):
                    raise result
                else:
                    raise ValueError("Fail to load func with return {}".format(result))
            self._c_func = result
        return self._c_func

    @property
    def signature(self):
        """
        Get function's full argument spec
        :return:
        """
        if self._c_signature is None:
            result = inspect_func(self.func, *self.extra_args, **self.extra_kwargs)
            # """ DO IT ONLY ON-DEMAND """
            # if self._func is None:
            #     """ load and inspect in another process to avoid unnecessary injection to current process """
            #     from darwinutils.mapreduce import parallel_map_p
            #     result, *_ = list(parallel_map_p(
            #         load_and_inspect_func_from_spec,
            #         [self.func_module_spec],
            #         max_workers=1,
            #     ))
            # else:
            #     """ inspect loaded function, if there was """
            #     result = inspect_func(self._func)

            if not isinstance(result, Signature):
                if isinstance(result, Exception):
                    raise result
                else:
                    raise ValueError("Fail to inspect func with return {}".format(result))
            self._c_signature = result
        return self._c_signature

    @property
    def inputs(self):
        if (self._c_input_var_names is None or self._c_input_arg_names is None) and \
                self._inputs is not None:
            self._c_input_var_names, self._c_input_arg_names = f_compose_r(
                partial(map, f_compose_r(
                    # [pl_var1_spec_obj, ...]
                    f_get_pl_variable_spec_from_str,
                    # ["<pl_in_var1_name", ...], ["pl_in_arg1_name",...]
                    attrgetter("pl_name", "func_name"),
                )),
                partial(f_star_call, zip),
            )(self._inputs)
        return self._inputs

    @property
    def outputs(self):
        if (self._c_output_var_names is None or self._c_output_arg_names is None) and \
                self._outputs is not None:
            self._c_output_var_names, self._c_output_arg_names = f_compose_r(
                partial(map, f_compose_r(
                    # [pl_var1_spec_obj, ...]
                    f_get_pl_variable_spec_from_str,
                    # ["<pl_in_var1_name", ...], ["pl_in_arg1_name",...]
                    attrgetter("pl_name", "func_name"),
                )),
                partial(f_star_call, zip),
            )(self._outputs)
        return self._outputs

    @property
    def func_module_spec(self):
        return self._func_module_spec

    @property
    def extra_args(self):
        return self._extra_args

    @property
    def extra_kwargs(self):
        return self._extra_kwargs

    @property
    def log_prefix(self):
        if self._c_log_prefix is None:
            self._c_log_prefix = "pl {} node, \"{}\"/.{}".format(
                self.type, self.name, self.uuid[puas],
            )
        return self._c_log_prefix

    @property
    def input_var_names(self):
        if self._c_input_var_names is None:
            _ = self.inputs
        return self._c_input_var_names

    @property
    def input_arg_names(self):
        if self._c_input_arg_names is None:
            _ = self.inputs
        return self._c_input_arg_names

    @property
    def output_var_names(self):
        if self._c_output_var_names is None:
            _ = self.outputs
        return self._c_output_var_names

    @property
    def output_arg_names(self):
        if self._c_output_arg_names is None:
            _ = self.outputs
        return self._c_output_arg_names

    def __repr__(self):
        return "{}.{}({})".format(
            type(self).__name__,
            type(self).create_from_node_spec.__name__,
            repr(self.spec),
        )

    def clone(self):
        """
        Clone a pipenode
        :return:
        """
        return eval(repr(self))

    def __call__(self, *args, _catched_exceptions=[], **kwargs):
        """
        PipeNode of cold type call
        :param args:
        :param _catched_exceptions:
        :param kwargs:
        :return:
        """
        if DARWIN_CONFIG.debug_performance:
            time_beg = time.time()
        """
        # log for debug
        _ = f_compose_r(
            partial(f_star_call, zip),
            partial(map, f_compose_r(
                "cold_exec >> {}".format,
                logger.info,
            )),
            list,
        )([
            ["func", "extra_args", "extra_kwargs", "args", "kwargs"],
            [self.func, self.extra_args, self.extra_kwargs, args, kwargs],
        ])
        """
        try:
            result = partial(self.func, *self.extra_args, **self.extra_kwargs)(*args, **kwargs)
        except Exception as _e:
            if f_compose_r(
                partial(map, f_compose_r(
                    partial(isinstance, _e),
                )),
                any,
            )(_catched_exceptions):
                result = ExceptCatchRunResultErrorMsg(exception=_e, traceback=traceback.format_exc())
            else:
                raise

        if DARWIN_CONFIG.debug_performance:
            time_end = time.time()
            logger.critical("perf: {}, call in {:.6f}s.".format(self.log_prefix, time_end-time_beg))
        return result


class PipeNodeWarm(PipeNode):
    def __init__(self, *args, pl_uuid, **kwargs):
        if pl_uuid is None:
            raise ValueError("{} cannot be created with invalid PL uuid {}.".format(
                type(self).__name__, pl_uuid,
            ))
        self._pl_uuid = pl_uuid

        self._c_queue_ids = None
        self._c_max_instances = None
        self._c_instances = None
        self._c_gc_interval = None
        self._c_hb_interval = None
        self._c_start_time = None
        self._c_hb_lead_time = None
        self._c_max_batch_size = None
        self._c_max_pool_workers = None

        super().__init__(*args, **kwargs)

        if self.type != PipeNode.TYPE.warm.name:
            raise ValueError("{} cannot be instantiated with {} type of node spec.".format(
                type(self).__name__, self.type
            ))

    def __getstate__(self):
        state = super().__getstate__()
        del state["_c_queue_ids"]
        del state["_c_max_instances"]
        del state["_c_instances"]
        del state["_c_gc_interval"]
        del state["_c_hb_interval"]
        del state["_c_start_time"]
        del state["_c_hb_lead_time"]
        del state["_c_max_batch_size"]
        del state["_c_max_pool_workers"]
        return state

    def __setstate__(self, state):
        super().__setstate__(state)
        self._c_queue_ids = None
        self._c_max_instances = None
        self._c_instances = None
        self._c_gc_interval = None
        self._c_hb_interval = None
        self._c_start_time = None
        self._c_hb_lead_time = None
        self._c_max_batch_size = None
        self._c_max_pool_workers = None

    def __repr__(self):
        return "{}.{}({}, pl_uuid={})".format(
            type(self).__name__,
            type(self).create_from_node_spec.__name__,
            repr(self.spec),
            repr(self.pl_uuid),
        )

    @property
    def gc_interval(self):
        if self._c_gc_interval is None:
            default_gc_interval = DARWIN_CONFIG["default_pipeline_warm_node_gc_interval"]
            self._c_gc_interval = f_compose_r(
                attrgetter("kwflags"),
                f_star_call(
                    f_group,
                    partial(map, partial(partial, f_flip_call, dict.get))([
                        "gc_interval",
                        "_gc_interval",
                    ]),
                ),
                partial(filter, partial(f_flip_call, is_not, None)),
                list,
                lambda _: int(_[0]) if len(_) > 0 else default_gc_interval,
            )(self)
        return self._c_gc_interval

    @property
    def pl_uuid(self):
        """
        The uuid of the parent pipeline which this pipe node belongs to
        :return:
        """
        return self._pl_uuid

    @property
    def queue_id(self):
        """
        The default/1st queue id which clients might request service from.
        :return:
        :rtype str
        """
        return self.queue_ids[0]

    @property
    def queue_ids(self):
        """
        All queue ids of its instances which clients might request service from
        NOTE: this attribute is calculated by algorithm and does not be persisted.
        :return:
        :rtype: list
        """
        if self._c_queue_ids is None:
            # NOTE: it's very important to define the relationship of pl_node "uuid" and its "first_queue_id"
            # now, first_queue_id is an up-level dimension of uuid per its belonging pipeline.
            # with this design, a "same" pl node will have different queue ids if it belongs to different pipeline.
            # TODO: another approach is to share the queue_ids between pipeline for a "same" pl node.
            first_queue_id = reduce_uuid(self.pl_uuid, self.uuid)

            self._c_queue_ids = f_compose_r(
                attrgetter("max_instances"),
                partial(range, 1),
                partial(map, f_compose_r(
                    partial(reduce_uuid, first_queue_id),
                )),
                list,
                partial(concat, [first_queue_id]),
            )(self)
        return self._c_queue_ids

    @property
    def max_instances(self):
        """
        The maximal number of instances to which the pl node can scale
        # TODO: should we add min as the lower bound?
        :return:
        :rtype int
        """
        if self._c_max_instances is None:
            default_max_instances = 1
            self._c_max_instances = f_compose_r(
                attrgetter("kwflags"),
                f_star_call(
                    f_group,
                    partial(map, partial(partial, f_flip_call, dict.get))([
                        "max_instances",
                        "_max_instances",
                    ]),
                ),
                partial(filter, partial(f_flip_call, is_not, None)),
                list,
                lambda _: int(_[0]) if len(_) > 0 else default_max_instances,
            )(self)
        return self._c_max_instances

    @property
    def instances(self):
        """
        The pl instances which this pl node can serve the clients
        :return: list of pl inst object
        :rtype list
        """
        if self._c_instances is None:
            self._c_instances = f_compose_r(
                attrgetter("max_instances"),
                range,
                partial(map, partial(type(self).get_instance_class(), self)),
                list,
            )(self)
        return self._c_instances

    @property
    def log_prefix(self):
        if self._c_log_prefix is None:
            self._c_log_prefix = "pl {} node, \"{}\"/.{}/.{}".format(
                self.type, self.name, self.uuid[puas], self.queue_id[puas],
            )
        return self._c_log_prefix

    @property
    def max_batch_size(self):
        """
        The max number of batch size to which the pl node instance can bundle
        :return:
        :rtype int
        """
        if self._c_max_batch_size is None:
            default_max_batch_size = 1
            self._c_max_batch_size = f_compose_r(
                attrgetter("kwflags"),
                f_star_call(
                    f_group,
                    partial(map, partial(partial, f_flip_call, dict.get))([
                        "max_batch_size",
                        "_max_batch_size",
                    ]),
                ),
                partial(filter, partial(f_flip_call, is_not, None)),
                list,
                lambda _: int(_[0]) if len(_) > 0 else default_max_batch_size,
            )(self)
        return self._c_max_batch_size

    @property
    def max_pool_workers(self):
        """
        The maximal number of workers for parallel execution queue
        :return:
        :rtype int
        """
        if self._c_max_pool_workers is None:
            default_max_pool_workers = DARWIN_CONFIG["default_pipeline_warm_node_max_pool_workers"]
            self._c_max_pool_workers = f_compose_r(
                attrgetter("kwflags"),
                f_star_call(
                    f_group,
                    partial(map, partial(partial, f_flip_call, dict.get))([
                        "max_pool_workers",
                        "_max_pool_workers",
                    ]),
                ),
                partial(filter, partial(f_flip_call, is_not, None)),
                list,
                lambda _: int(_[0]) if len(_) > 0 else default_max_pool_workers,
            )(self)
        return self._c_max_pool_workers


    def cold_exec(self, *args, **kwargs):
        """
        Execute warm node's workload function in local as if its a code node
        NOTE: this was used to initialize the warm node workload as an iterator in the main loop
        :param args:
        :param kwargs:
        :return: a generator object to presents the output iterator
        :rtype generator
        """
        return super().__call__(*args, **kwargs)

    def __call__(self, *args, _catched_exceptions=[], _target_instance=None, **kwargs):
        """
        warm node client stub call to a target instance
        :param args:
        :param _catched_exceptions:
        :param _target_instance:
        :param kwargs:
        :return:
        """
        raise NotImplementedError("{} not implemented yet".format(get_current_function_name()))

    @classmethod
    def get_instance_class(cls):
        """
        Get the default adaptable pl instance class type this type of pl node can work with.
        :return:
        """
        # TODO: change to classproperty :-)
        raise NotImplementedError("{} not implemented yet".format(get_current_function_name()))

    def parse_instances(self, instances):
        """
        Parse the instance locate spec and get the target instance ids
        :param instances:
        :return:
        """
        f_slice_revert = lambda _: (self.max_instances + _) if _ < 0 else _
        f_round_float = lambda _: round(_*(self.max_instances-1))

        # calculate the target instance if requested by caller
        if isinstance(instances, float):
            if 0 > instances or 1 < instances:
                raise ValueError("Float instances has to be [0, 1] instead of {}".format(
                    instances,
                ))
            instances = f_round_float(instances)

        elif isinstance(instances, int):
            instances = f_slice_revert(instances)
            if 0 > instances or self.max_instances <= instances:
                raise ValueError("Integer instances has to be [0, {}] instead of {}".format(
                    self.max_instances, instances,
                ))

        elif isinstance(instances, Iterable):
            instances = f_compose_r(
                partial(map, f_slice_revert),
                list,
            )(instances)
            if min(instances) < 0 or max(instances) >= self.max_instances:
                raise ValueError("Instance id should in range of [0, {}). min({}) and max({}) are out of range".format(
                    self.max_instances, min(instances), max(instances),
                ))

        elif isinstance(instances, slice):
            beg, end = f_compose_r(
                f_group(
                    f_compose_r(
                        attrgetter("start"),
                        lambda _: 0 if _ is None else _,
                    ),
                    f_compose_r(
                        attrgetter("stop"),
                        lambda _: self.max_instances if _ is None else _,
                    ),
                ),
                partial(map, f_compose_r(
                    f_slice_revert,
                )),
                list,
                f_group(
                    min,
                    max,
                )
            )(instances)
            # TODO: do not support slice's step, always assume 1
            if isinstance(beg, float):
                beg = round(beg*(self.max_instances-1))
            if isinstance(end, float):
                end = round(end*(self.max_instances-1))
            if 0 > beg or self.max_instances < end:
                raise ValueError("Instance id should in range of [0, {}) instead of slice {}".format(
                    self.max_instances, instances,
                ))
            instances = list(range(self.max_instances))[slice(beg, end, 1)]

        elif instances is not None:
            raise TypeError("instances has to be int or float instead of {}".format(
                type(instances).__name__,
            ))
        return instances

    def instance_ctrl(self, func, instances):
        """
        Meta function to redirect ctrl function call to target instances
        :param func:
        :param instances:
        :return: a tupled function returns with (<instance_id_1, func_result_1>, {...})
        """
        return f_compose_r(
            # get ids of target instances
            self.parse_instances,
            # list rize in case the target is only
            partial(f_list_rize, _num_args=1 if not isinstance(instances, (int, float)) else None),
            # manipulate friendly resp tuples
            f_group(
                # pass instance ids
                f_echo,
                # fire ctrl to instance and get result
                f_compose_r(
                    partial(parallel_map_t, f_compose_r(
                        partial(getitem, self.instances),
                        func,
                    )),
                    list,
                ),
            ),
            # identify ctrl resp with its originated instance id
            partial(f_star_call, zip),
            list,
        )(instances)

    # shortcut helper function
    def start(self, instances, *args, **kwargs):
        return self.instance_ctrl(methodcaller("start", *args, **kwargs), instances)

    # shortcut helper function
    def stop(self, instances, *args, **kwargs):
        return self.instance_ctrl(methodcaller("stop", *args, **kwargs), instances)

    # shortcut helper function
    def state(self, instances, *args, **kwargs):
        return self.instance_ctrl(attrgetter("state"), instances)

    # shortcut helper function
    def wait_for_state(self, instances, *args, **kwargs):
        return self.instance_ctrl(methodcaller("wait_for_state", *args, **kwargs), instances)

    # shortcut helper function
    def status(self, instances, *args, **kwargs):
        return self.instance_ctrl(attrgetter("status"), instances)

    # shortcut helper function
    def run_in_thread(self, instances, *args, **kwargs):
        return self.instance_ctrl(methodcaller("run_in_thread", *args, **kwargs), instances)

    @property
    def active_instances(self):
        """
        Get the ids of active instances
        :return: a list if instance id
        :rtype list
        """
        instance_class = type(self).get_instance_class()
        return f_compose_r(
            attrgetter("max_instances"),
            range,
            self.state,
            partial(filter, f_compose_r(
                itemgetter(1),
                partial(eq, instance_class.STATE.started),
            )),
            list,
            partial(f_star_call, zip),
            list,
            itemgetter(0),
        )(self)


class PipeNodeWarmInstance:
    CMD_TYPE = Enum(
        "CMD_TYPE",
        (
            "status",
            "stop",
        ),
        qualname="PipeNodeWarmInstance.CMD_TYPE",
    )
    STATE = Enum(
        "STATE",
        (
            "pending",
            "started",
        ),
        qualname="PipeNodeWarmInstance.STATE",
    )
    MODE = Enum(
        "MODE",
        (
            "server",
            "client",
        ),
        qualname="PipeNodeWarmInstance.MODE",
    )
    CONTAINER_TYPE = Enum(
        "CONTAINER_TYPE",
        (
            "celery",
            "thread",
            "process",
        ),
        qualname="PipeNodeWarmInstance.CONTAINER_TYPE",
    )

    def __init__(self, pl_node, instance_id):
        # attributes from external
        if not isinstance(pl_node, PipeNodeWarm):
            raise ValueError("PipeNodeWarmInstance must be initialized with a PipeNodeWarm object instead of {}".format(
                type(pl_node).__name__,
            ))
        self._pl_node = pl_node

        if 0 > instance_id or pl_node.max_instances <= instance_id:
            raise ValueError("instance id, {}, is out of range: [{}, {}).".format(
                instance_id, 0, pl_node.max_instances,
            ))
        self._instance_id = instance_id

        # auto attributes which are calculated from other attributes and varialbes, and cached only
        self._uuid = None
        self._c_log_prefix = None

        # attributes which has default value
        self._state = type(self).STATE.pending
        self._mode = type(self).MODE.client

    def __getstate__(self):
        state = self.__dict__.copy()
        # remove cached attributes
        del state["_uuid"]
        del state["_state"]
        del state["_mode"]
        del state["_c_log_prefix"]
        return state

    def __setstate__(self, state):
        self.__dict__.update(state)
        self._uuid = None
        self._state = type(self).STATE.pending
        self._mode = type(self).MODE.client
        self._c_log_prefix = None

    def __repr__(self):
        return "{}({}, instance_id={})".format(
            type(self).__name__,
            repr(self.pl_node),
            repr(self.instance_id),
        )

    def clone(self):
        """
        Clone a pipenode instance
        :return:
        """
        return eval(repr(self))

    @property
    def pl_node(self):
        """
        Parent pl node to which this instance belongs
        :return:
        """
        return self._pl_node

    @property
    def instance_id(self):
        return self._instance_id

    @property
    def uuid(self):
        """
        The uuid from which client can request service
        # NOTE: this id was calculated by algorithm and predictable.
        :return:
        """
        if self._uuid is None:
            self._uuid = self.pl_node.queue_ids[self.instance_id]
        return self._uuid

    @property
    def state(self):
        """
        The abbreviation of instance state
        :return:
        :rtype PipeNodeWarmInstance.STATE
        """
        return self._state

    @state.setter
    def state(self, value):
        """
        NOTE: this is only for internal use by instance server.
        :param value:
        :return:
        """
        if value != self._state:
            self._state = value

    @property
    def mode(self):
        """
        Mode of instance
        NOTE: we use the mode to distinguish and isolate the code, or
              we might need to use more complex design pattern.
        :return:
        :rtype PipeNodeWarmInstance.MODE
        """
        return self._mode

    @property
    def is_server(self):
        """
        helper func to mode property
        :return:
        """
        return self.mode == type(self).MODE.server

    @property
    def is_client(self):
        """
        helper func to mode property
        :return:
        """
        return self.mode == type(self).MODE.client

    @property
    def log_prefix(self):
        if self._c_log_prefix is None:
            self._c_log_prefix = "{} {}".format(
                self.pl_node.log_prefix, self.log_prefix_short,
            ).replace("node,", "inst,")
        return self._c_log_prefix

    @property
    def log_prefix_short(self):
        return "{}/.{}".format(self.instance_id, self.uuid[puas])

    def wait_for_state(self, state, sleep_interval=0.001, timeout=None):
        """
        wait the instance to be in a specific state
        :param state:
        :param sleep_interval:
        :param timeout:
        :return:
        """
        return loop_run(
            partial(getattr, self, "state"),
            partial(
                f_unstar_call,
                f_compose_r(
                    itemgetter(0),
                    partial(eq, state),
                ),
            ),
            sleep_interval=sleep_interval,
            timeout=timeout,
        )

    def run(self):
        """
        Main thread which will wait and join all known threads
        :return:
        """
        raise NotImplementedError("{} not implemented yet".format(get_current_function_name()))

    def request_ctrl(self, name, payload=None, *args, timeout=None, **kwargs):
        """
        meta function to re-direct ctrl request from requester to its proper handler function
        :param name: ctrl cmd name
        :param payload:
        :param args:
        :param timeout:
        :param kwargs:
        :return:
        """
        assert self.is_client, "pl inst \"request_ctrl\" cannot be explicitly called in non client mode {}".format(self.mode)

        members = type(self).CMD_TYPE.__members__
        if isinstance(name, str):
            if name in members:
                name = members[name]
            else:
                raise ValueError("Invalid ctrl command {}.".format(name))

        elif not isinstance(name, type(self).CMD_TYPE):
            raise TypeError("name in request_ctrl needs to be CMD_TYPE instead of {}".format(type(name).__name__))
        return methodcaller(
            "_handle_ctrl_req_{}".format(name.name),
            payload=payload,
            *args,
            timeout=timeout,
            **kwargs,
        )(self)

    def response_ctrl(self, sess_id, ctrl_msg, *args, timeout=None, **kwargs):
        """
        meta function to re-direct ctrl response from broker main thread to its proper handler function
        :param sess_id:
        :param ctrl_msg:
        :param args:
        :param timeout:
        :param kwargs:
        :return:
        """
        assert self.is_server, "pl inst \"response_ctrl\" cannot be explicitly called in non server mode {}".format(self.mode)

        if not isinstance(ctrl_msg, PLCtrlCmdSpec):
            raise TypeError("ctrl_msg in response_ctrl needs to be PLCtrlCmdSpec type instead of {}".format(
                type(ctrl_msg).__name__,
            ))
        logger.debug("{}, worker responds ctrl cmd \"{}\" from sess .{}.".format(
            self.log_prefix, ctrl_msg.name, sess_id[puas],
        ))
        return methodcaller(
            "_handle_ctrl_resp_{}".format(ctrl_msg.name),
            ctrl_msg.payload, sess_id,
            *args,
            timeout=timeout,
            **kwargs,
        )(self)


class PipeQueue(Queue, Iterator):
    """
    A one consumer - one producer implementation Queue for PipeNode in order to
    wrap a remote queue as a generator for warm pl node's workload function
    """
    def __init__(self, pl_inst=None, maxsize=0):
        self._pl_inst = pl_inst
        self._working_cache = None
        self._c_is_batch = None
        self._should_stop = False
        super().__init__(maxsize=maxsize)

    def __getstate__(self):
        state = self.__dict__.copy()
        del state["_working_cache"]
        del state["_c_is_batch"]
        return state

    def __setstate__(self, state):
        self.__dict__.update(state)
        self._working_cache = None
        self._c_is_batch = None

    @property
    def pl_inst(self):
        """
        the pl instance which this queue is working for and belongs to
        :return:
        """
        return self._pl_inst

    @property
    def working_cache(self):
        """
        The cached ongoing workloads
        :return:
        """
        if self._working_cache is None:
            self._working_cache = deque()
        return self._working_cache

    @property
    def is_batch(self):
        if self._c_is_batch is None:
            if self.pl_inst is None:
                self._c_is_batch = False
            else:
                self._c_is_batch = self.pl_inst.pl_node.max_batch_size > 1
        return self._c_is_batch

    @property
    def should_stop(self):
        return self._should_stop

    def __next__(self):
        """
        Iterator of threading Queue
        iterating data's sess key will be cached so that the result of workload func for the real data
        could be associated with this key for later post processing later.

        # !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        NOTE: I assume that only one consumer of this Iterator interface!!!
        # !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

        :return:
        """
        if self.should_stop:
            self.task_done()
            raise self.should_stop

        # cache the queue size
        if self.is_batch:
            batch_size_exp = self.pl_inst.pl_node.max_batch_size
        else:
            batch_size_exp = 1

        data_batch = []

        i_cnt = 0
        while i_cnt < batch_size_exp:
            enqueue_time, data = self.get()
            if DARWIN_CONFIG.debug_performance and \
                    self.pl_inst is not None and \
                    isinstance(data, PLNodeSessDataSpec):
                sess_id = data.sess_id
                logger.critical("perf: {}, data sess .{} wait for {:.6f}s".format(
                    self.pl_inst.log_prefix,
                    sess_id[puas],
                    time.time()-enqueue_time,
                ))
            if i_cnt == 0:
                q_size = self.qsize()+1
                batch_size_exp = min(batch_size_exp, q_size)

            i_cnt += 1

            if isinstance(data, StopIteration):
                self._should_stop = data
                break

            elif isinstance(data, PLNodeSessDataSpec):
                sess_id, data = data
                self.working_cache.append(PLCacheSpec(time.time(), sess_id))
                data_batch.append(data)

            else:
                # log for debug only
                # NOTE: duplicate IMPORTANT but harmless msg!!!
                _ = list(map(lambda _: logger.warning("Unknown data was received and will be dropped: {}".format(data)),
                             range(10)))

        batch_size_cur = len(data_batch)
        if batch_size_cur < 1 and self.should_stop:
            # stop signal is the only one in queue, let's stop immediately
            self.task_done()
            raise self.should_stop

        elif self.should_stop:
            # requeue a stop signal in case the de-queued stop signal was the last one which will have no chance
            # to trigger next iter cycle.
            self.put(PLCacheSpec(time.time(), self.should_stop))

        # no batch return
        if not self.is_batch:
            assert batch_size_cur == 1, \
                "should not get more {} batch of data if batch was not requested".format(batch_size_cur)
            return data_batch[0]

        # batch return
        return data_batch


class PipeNodeWarmInstanceBase(PipeNodeWarmInstance):
    def __init__(self, *args, **kwargs):
        self._q_data = None
        self._t_data = None
        super().__init__(*args, **kwargs)

    def __getstate__(self):
        state = super().__getstate__()
        del state["_q_data"]
        del state["_t_data"]
        return state

    def __setstate__(self, state):
        super().__setstate__(state)
        self._q_data = None
        self._t_data = None

    @property
    def q_data(self):
        """
        Queue for data channel
        :return:
        """
        if self._q_data is None:
            self._q_data = PipeQueue(self)
        return self._q_data

    @property
    def t_data(self):
        """
        Thread/task for data channel worker
        :return:
        """
        return self._t_data

    def run_data(self):
        """
        data channel worker
        :return:
        """
        assert self.is_server, "pl inst \"run_ctrl\" cannot be explicitly called in non server mode {}".format(self.mode)

        n_output_vars = len(self.pl_node.outputs)

        # main loop
        start_cnt = 0
        it = None
        time_end = None
        gc_interval = self.pl_node.gc_interval
        while True:
            try:
                if it is None:
                    logger.info("Start warm node {} stream's workload run at {} time(s)".format(
                        self.uuid, start_cnt,
                    ))
                    if DARWIN_CONFIG.debug_performance:
                        time_beg = time.time()

                    # assemble PipeNode worker to the stream and get its stream's output iterator
                    it = self.pl_node.cold_exec(self.q_data)

                    if DARWIN_CONFIG.debug_performance:
                        time_end = time.time()
                        logger.critical("perf: {}, init in {:.6f}s.".format(self.log_prefix, time_end-time_beg))
                    # count the "warm" restart
                    start_cnt += 1

                # wait and extract output from the stream iterator which is actually the worker's response for
                # input data.
                outputs = next(it)

                # gc was tightly associated with data worker thread.
                # let's do it with defined policy
                time_last = time_end
                time_end = time.time()
                if time_last is not None and self.pl_node.gc_interval > 0:
                    gc_interval -= time_end - time_last
                    if gc_interval <= 0.0:
                        enforce_gc()
                        gc_interval = self.pl_node.gc_interval

                # prepare timeit perf data to help benchmarking.
                enqueue_times, sess_ids, = zip(*list(self.q_data.working_cache))
                n_sess_ids = len(sess_ids)

            except StopIteration as _e:
                logger.debug("{}: Data worker is terminating".format(self.log_prefix))
                break

            except Exception as _e:
                if DARWIN_CONFIG.debug_performance:
                    time_end = time.time()

                err_msg = ExceptCatchRunResultErrorMsg(exception=_e, traceback=traceback.format_exc())

                # all the working sessions are affected by this exceptions.
                enqueue_times, sess_ids = zip(*list(self.q_data.working_cache))
                n_sess_ids = len(sess_ids)
                f_expand_vertical = f_f_expand(n_sess_ids)
                f_expand_horizontal = f_f_expand(n_output_vars)

                # broadcast the exceptional error message to all output variables and sessions
                # fake the output as if every output variable is a same ExceptCatchRunResultErrorMsg object
                outputs = err_msg
                if n_sess_ids > 1:
                    outputs = f_expand_vertical(outputs)
                if n_output_vars > 1:
                    if n_sess_ids > 1:
                        outputs = f_compose_r(
                            partial(map, f_expand_horizontal),
                            list,
                        )(outputs)
                    else:
                        outputs = f_expand_horizontal(outputs)

                # let's restart the warm node's workload hook in next cycle
                it = None

                logger.warning("--------------------------------------------------------------")
                logger.warning("First eyewitness of warm node invoking error:")
                for item, msg in zip(
                        ["name", "spec", "instance_id"],
                        [self.pl_node.name, self.pl_node.spec, self.instance_id]):
                    logger.warning(">> {}: {}".format(item, msg))
                for item, msg in zip(
                        ["exc", "tb", "affected_sessions"],
                        [str(err_msg.exception), err_msg.traceback, "\n".join(sess_ids)]):
                    for i, line in enumerate(msg.splitlines()):
                        logger.warning(">> {}[{}]: {}".format(item, i, line))
                logger.warning("--------------------------------------------------------------")

            if DARWIN_CONFIG.debug_performance:
                _ = f_compose_r(
                    partial(f_star_call, zip),
                    list,
                    partial(map, f_compose_r(
                        partial(
                            f_star_call,
                            lambda sess_id, enqueue_time: logger.critical(
                                "perf: {}, run for data sess .{} in {:.6f}s at {:.6f}".format(
                                    self.log_prefix, sess_id[puas],
                                    time_end - enqueue_time, time_end,
                                )),
                        ),
                    )),
                    list,
                )([sess_ids, enqueue_times])

            # ensure the output has shape of [#sessions, #output_variables]
            if n_sess_ids < 2 and self.pl_node.max_batch_size < 2:
                outputs = f_list_rize(outputs)
            if n_output_vars < 2:
                outputs = f_compose_r(
                    partial(map, f_list_rize),
                    list,
                )(outputs)
            #logger.info("process outputs from warm workload func as {}".format(outputs))

            n_outputs = len(outputs)
            if n_outputs > n_sess_ids:
                logger.warning("{}: Extra return! {} session batched, but {} returned, crop later {} useless".format(
                    self.log_prefix, n_sess_ids, n_outputs, n_outputs-n_sess_ids,
                ))
                if DARWIN_CONFIG.verbose_level > 2:
                    logger.debug("{}: Extra return which will be dropped are: {}".format(
                        self.log_prefix, outputs[n_sess_ids:],
                    ))
                del outputs[n_sess_ids:]
                n_outputs = n_sess_ids

            # fill in(None) insufficient return or crop useless return for each session
            f_cook_resp = partial(f_unstar_call, f_compose_r(
                # calc difference of the number of output vars
                f_zip(
                    # enumerate the outputs to keep the index in the data flow so that
                    # log for debug node can have the session index for easy of display.
                    enumerate,
                    # func generation func about length calculation
                    f_compose_r(
                        partial(partial, sub),
                        partial(f_compose_r, f_compose_r(
                            itemgetter(1),
                            len,
                        )),
                        partial(f_group, f_echo),
                        partial(f_compose_l, list),
                        partial(partial, map),

                    ),
                ),
                partial(f_star_call, partial(f_flip_call, f_call)),
                # identify unmatched outputs which has less return or more return than expected
                partial(filter, f_compose_r(
                    itemgetter(1),
                    partial(eq, 0),
                    not_,
                )),
                list,
                f_group(
                    # handle less return
                    f_compose_r(
                        partial(filter, f_compose_r(
                            itemgetter(1),
                            partial(lt, 0),
                        )),
                        partial(map, f_compose_r(
                            f_group(
                                # log for debug
                                #
                                # syntax:
                                # [(sess_idx, [val1{, val2...}]), len_diff]
                                #
                                # example:
                                # fill: [(1, [1, 3]), 1]
                                partial(
                                    f_star_call,
                                    lambda vals, diff: logger.warning("{}: sess .{} fill extra {} None".format(
                                        self.log_prefix, sess_ids[vals[0]][puas], diff,
                                    )),
                                ),
                                # fill op for insufficient return
                                f_compose_r(
                                    f_zip(
                                        f_compose_r(
                                            itemgetter(1),
                                            attrgetter("extend"),
                                        ),
                                        # NOTE: use a transition value indicate it's really a faked output
                                        partial(mul, [MISSING_VALUE_TRANSITION]),
                                    ),
                                    list,
                                    partial(f_star_call, f_call),
                                ),
                            ),
                            list,
                            # drop debug output
                            itemgetter(1),
                        )),
                        list,
                    ),
                    # handle useless return
                    f_compose_r(
                        partial(filter, f_compose_r(
                            itemgetter(1),
                            partial(gt, 0),
                        )),
                        partial(map, f_compose_r(
                            f_group(
                                # log for debug
                                #
                                # syntax:
                                # [(sess_idx, [val1{, val2...}]), len_diff]
                                #
                                # example:
                                # crop: [(1, [1, 3, 4]), -1]
                                partial(
                                    f_star_call,
                                    lambda vals, diff: logger.warning("{}: sess .{} crop useless {} return values".format(
                                        self.log_prefix, sess_ids[vals[0]][puas], diff,
                                    )),
                                ),
                                # crop op for useless return
                                f_compose_r(
                                    f_zip(
                                        f_compose_r(
                                            itemgetter(1),
                                            partial(partial, delitem),
                                        ),
                                        partial(f_flip_call, slice, None),
                                    ),
                                    list,
                                    partial(f_star_call, f_call),
                                ),
                            ),
                            list,
                            # drop debug output
                            itemgetter(1),
                        )),
                        list,
                    ),
                ),
                list,
            ))
            if DARWIN_CONFIG.debug_performance:
                f_cook_resp = partial(f_timeit_call, f_cook_resp)
            r = f_cook_resp(outputs, n_output_vars)
            if DARWIN_CONFIG.debug_performance:
                _, dur = r
                logger.critical("perf: {}, cook {} resp(s). for data sess(s) {} in {:.6f}s".format(
                    self.log_prefix,
                    len(sess_ids),
                    f_compose_r(
                        partial(map, f_compose_r(
                            itemgetter(puas),
                            ".{}".format,
                        )),
                        ", ".join,
                    )(sess_ids),
                    dur,
                )),

            if n_outputs < n_sess_ids:
                logger.warning("{}: Insufficient return! {} session batched, but {} returned, filling None".format(
                    self.log_prefix, n_sess_ids, n_outputs,
                ))
                # NOTE: use transition value to indicate it's really a faked output
                outputs.extend([[MISSING_VALUE_TRANSITION]*n_output_vars]*(n_sess_ids-n_outputs))
                n_outputs = n_sess_ids

            # pair to (<sess_id, output>, ...)
            pairs = list(zip(sess_ids, outputs))
            remain_pairs = self.response_data(pairs)
            assert len(remain_pairs) == 0

            self.q_data.working_cache.clear()

    def response_data(self, sess_output_pairs):
        """
        Response request for data service
        NOTE: this is the base class of all processing flow and nothing will be handled instead of passing them
              to the child of processing chain.
        :param sess_output_pairs:
        :return:
        """
        return sess_output_pairs

    def run_data_in_thread(self):
        """
        start data channel worker in a separate thread
        :return:
        """
        assert self.is_server, "pl inst \"run_data_in_thread\" cannot be explicitly called in non server mode {}".format(self.mode)
        t_data = Thread(
            target=self.run_data,
            args=[],
        )
        t_data.start()
        self._t_data = t_data
        return t_data

    def run(self):
        """
        Main thread which will wait and join all known threads
        :return:
        """
        # switch to server mode
        self._mode = type(self).MODE.server

        _ = self.run_data_in_thread()

        if self.t_data is not None:
            self.t_data.join()


class PayloadEvent(Event):
    """
    threading.Event with payload
    TODO: payload set/get was just a "payload" of event set/get.
    TODO: take care by yourself if you use this payload independently.
    """
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._payload = None

    @property
    def payload(self):
        return self._payload

    def set(self, payload=None):
        self._payload = payload
        return super().set()

    def clear(self):
        self._payload = None
        return super().clear()

    def async_wait(self, timeout=None, sleep_interval=0.0):
        return loop_run(
            self.is_set,
            partial(f_unstar_call, itemgetter(0)),
            timeout=timeout,
            sleep_interval=sleep_interval,
            sleep_func=gevent.sleep,
        )


class PipeNodeWarmInstanceRpc(PipeNodeWarmInstanceBase, PipeLineServicer):
    SUB_CHANNEL_TYPES = Enum(
        "SUB_CHANNEL_TYPES",
        (
            "zrpc",
            "grpc",
        ),
        qualname="PipeNodeWarmInstanceRpc.SUB_CHANNEL_TYPES",
    )
    def __init__(self, *args, **kwargs):
        self._sess_queues = None
        self._rpc_host = None

        # zerorpc
        self._zrpc_server = None
        self._zrpc_port = None
        self._c_zrpc_endpoint = None
        self._t_zrpc = None
        self._zrpc_worker_ready = None

        # gRPC
        self._grpc_server = None
        self._grpc_port = None
        self._c_grpc_endpoint = None
        self._t_grpc = None
        self._grpc_worker_ready = None

        self._c_input_arg_filter_function = None
        super().__init__(*args, **kwargs)

    def __getstate__(self):
        state = super().__getstate__()
        del state["_sess_queues"]
        del state["_rpc_host"]

        # zerorpc
        del state["_zrpc_server"]
        del state["_zrpc_port"]
        del state["_c_zrpc_endpoint"]
        del state["_t_zrpc"]
        del state["_zrpc_worker_ready"]

        # gRPC
        del state["_grpc_server"]
        del state["_grpc_port"]
        del state["_c_grpc_endpoint"]
        del state["_t_grpc"]
        del state["_grpc_worker_ready"]

        del state["_c_input_arg_filter_function"]
        return state

    def __setstate__(self, state):
        super().__setstate__(state)
        self._sess_queues = None
        self._rpc_host = None

        # zerorpc
        self._zrpc_server = None
        self._zrpc_port = None
        self._c_zrpc_endpoint = None
        self._t_zrpc = None
        self._zrpc_worker_ready = None

        # gRPC
        self._grpc_server = None
        self._grpc_port = None
        self._c_grpc_endpoint = None
        self._t_grpc = None
        self._grpc_worker_ready = None

        self._c_input_arg_filter_function = None

    @property
    def sess_queues(self):
        """
        sync queue objects for rpc server(s) call to wait for result
        :return:
        """
        assert self.is_server, "sess_queues cannot be explicitly get in non-server mode {}".format(self.mode)

        if self._sess_queues is None:
            self._sess_queues = {}
        return self._sess_queues

    @property
    def rpc_host(self):
        if self._rpc_host is None and self.is_server:
            self._rpc_host = get_local_source_ip_address(DARWIN_CONFIG["redis_host"])
        return self._rpc_host

    @property
    def zrpc_client(self):
        assert self.is_client, "zrpc_client cannot be explicitly get in non-client mode {}".format(self.mode)

        zrpc_endpoint = self.zrpc_endpoint
        # zrpc_client cannot be shared in multi-threads environment
        return get_zrpc_client(zrpc_endpoint)

    @property
    def zrpc_server(self):
        assert self.is_server, "zrpc_server cannot be explicitly get in non-server mode {}".format(self.mode)
        return self._zrpc_server

    @property
    def zrpc_port(self):
        return self._zrpc_port

    @property
    def zrpc_endpoint(self):
        if self._c_zrpc_endpoint is None:
            # locally cache the zrpc_XXXX for operation
            # NOTE: it's very possible that zrpc_XXXX was not ready yet in client mode.
            # NOTE: requester to "zrpc_endpoint" attribute needs to handle the null return by itself!!!
            # TODO: should sync wait for the zrpc_XXXX ready from its service announcement?
            rpc_host = self.rpc_host
            zrpc_port = self.zrpc_port
            if rpc_host and zrpc_port:
                self._c_zrpc_endpoint = "tcp://{}:{}".format(rpc_host, zrpc_port)
        return self._c_zrpc_endpoint

    @property
    def t_zrpc(self):
        return self._t_zrpc

    @property
    def grpc_client(self):
        assert self.is_client, "grpc_client cannot be explicitly get in non-client mode {}".format(self.mode)

        grpc_endpoint = self.grpc_endpoint
        # TODO: assume grpc_client can be shared in multi-threads environment
        return get_grpc_client(grpc_endpoint)

    @property
    def grpc_server(self):
        assert self.is_server, "grpc_server cannot be explicitly get in non-server mode {}".format(self.mode)
        return self._grpc_server

    @property
    def grpc_port(self):
        return self._grpc_port

    @property
    def grpc_endpoint(self):
        if self._c_grpc_endpoint is None:
            # locally cache the grpc_XXXX for operation
            # NOTE: it's very possible that grpc_XXXX was not ready yet in client mode.
            # NOTE: requester to "grpc_endpoint" attribute needs to handle the null return by itself!!!
            # TODO: should sync wait for the grpc_XXXX ready from its service announcement?
            rpc_host = self.rpc_host
            grpc_port = self.grpc_port
            if rpc_host and grpc_port:
                self._c_grpc_endpoint = "{}:{}".format(rpc_host, grpc_port)
        return self._c_grpc_endpoint

    @property
    def t_grpc(self):
        return self._t_grpc

    @property
    def input_arg_filter_function(self):
        if self._c_input_arg_filter_function is None:
            self._c_input_arg_filter_function = get_dict_filter_function_per_keys(self.pl_node.input_arg_names)
        return self._c_input_arg_filter_function

    def response_data(self, sess_output_pairs):
        """
        Response data requests from rpc channel
        :param sess_output_pairs:
        :return:
        """
        assert self.is_server, "response_data cannot be explicitly called in non-server mode {}".format(self.mode)

        # let parent response first, we handle the remains
        pairs = super().response_data(sess_output_pairs)
        n_sess_ids = len(pairs)

        # fast path to quick return:
        # * if nothing left for us to handle
        # * if no any Xrpc is not an accepted channel, ignore since no data should be send in rpc queue
        if n_sess_ids < 1 or not is_rpc_accepted_channel:
            return pairs

        # find out mask of sessions from rpc channel
        rpc_sess_ids_mask = f_compose_r(
            partial(map, f_compose_r(
                itemgetter(0),
                partial(contains, self.sess_queues),
            )),
            tuple,
        )(pairs)

        # filter out rpc pairs
        rpc_pairs = tuple(compress(pairs, rpc_sess_ids_mask))

        # action to "send" result to session
        _ = f_compose_r(
            partial(map, f_compose_r(
                f_zip(
                    # sess_id -> sess_queue.set (PayloadEvent.set(payload))
                    f_compose_r(
                        partial(getitem, self.sess_queues),
                        attrgetter("set"),
                    ),
                    # wrap output as cache item before putting into queue
                    f_compose_r(
                        f_group(
                            partial(f_si_call, time.time),
                            f_echo,
                        ),
                        partial(f_star_call, PLCacheSpec),
                    ),
                ),
                list,
                # kick wrapped output into its corresponding sess_queue
                partial(f_star_call, f_call),
            )),
            list,
        )(rpc_pairs)

        # clean up for this round
        _ = f_compose_r(
            range,
            partial(map, partial(f_si_call, self.q_data.task_done)),
            tuple,
        )(len(rpc_pairs))

        return tuple(compress(pairs, map(not_, rpc_sess_ids_mask)))

    def rpc_server_call(self, data, _timeout=None, f_encoder=None, f_decoder=None, rpc_source=None):
        """
        Warm node instance rpc server call
        :param data:
        :param _timeout:
        :param f_encoder: function to encode result before return to caller
        :param f_decoder: function to decode input data from caller
        :param rpc_source: the source of rpc where the invoker comes from. support "zrpc", "grpc" for now
        :return:
        """
        assert self.is_server, "rpc_server_call cannot be explicitly called in non-server mode {}".format(self.mode)

        if f_decoder:
            data = f_decoder(data)

        sess_id = str(uuid.uuid4())

        data = PLNodeSessDataSpec(sess_id, data)

        q_sess = PayloadEvent()
        self.sess_queues[sess_id] = q_sess

        enqueue_time_to_data = time.time()
        if DARWIN_CONFIG.debug_performance:
            logger.critical("put {} req to sess .{} in q_data at {}".format(
                rpc_source, sess_id[puas], enqueue_time_to_data,
            ))
        self.q_data.put(PLCacheSpec(enqueue_time_to_data, data))

        # extract data and perf tagging
        # TODO: handle timeout in server side?
        # TODO: timeout in cli will raise TimeoutError. any better solution?
        q_sess.async_wait(timeout=_timeout, sleep_interval=0.0001)
        enqueue_time, data = q_sess.payload

        if DARWIN_CONFIG.debug_performance:
            dequeue_time_fr_sess = time.time()
            logger.critical("get resp for {} sess .{} from q_sess at {}".format(
                rpc_source, sess_id[puas], dequeue_time_fr_sess,
            ))
            logger.critical("perf: {}, {} data sess .{} resp sit-in-q for {:.6f}s".format(
                self.log_prefix, rpc_source, sess_id[puas], dequeue_time_fr_sess-enqueue_time,
            ))
        if f_encoder:
            data = f_encoder(data)
        return data

    def run_zrpc(self):
        assert self.is_server, "run_zrpc cannot be explicitly called in non-server mode {}".format(self.mode)

        # NOTE: I uses "redis_host" as the beacon and assume that's all clients are in same ip network as the beacon
        self._zrpc_port = find_free_port(self.rpc_host)

        zrpc_server = zerorpc.Server({
            "rpc_server_call": self.zrpc_server_call,
        })
        zrpc_server.bind(self.zrpc_endpoint)
        logger.info("bind zrpc to {}".format(self.zrpc_endpoint))

        # NOTE: _zrpc_worker_ready MUST be created in the main run(...)
        self._zrpc_worker_ready.set()

        if is_zrpc_accepted_channel and DARWIN_CONFIG["apply_gevent_monkey_patch"] is not True:
            # TODO: use a workaround(?) to nicely close zrpc server
            # https://stackoverflow.com/questions/21140384/zerorpc-python-server-exceptions-when-attempting-to-stop-or-close
            gevent.signal(signal.SIGTERM, zrpc_server.stop)

        self._zrpc_server = zrpc_server

        zrpc_server.run()

    def run_zrpc_in_thread(self):
        assert self.is_server, "run_zrpc_in_thread cannot be explicitly called in non-server mode {}".format(self.mode)

        t_zrpc = Thread(
            target=self.run_zrpc,
            args=[],
        )
        t_zrpc.start()
        self._t_zrpc = t_zrpc
        return t_zrpc

    def run_grpc(self):
        assert self.is_server, "run_grpc cannot be explicitly called in non-server mode {}".format(self.mode)

        # NOTE: I uses "redis_host" as the beacon and assume that's all clients are in same ip network as the beacon
        self._grpc_port = find_free_port(self.rpc_host)

        grpc_server = grpc.server(ThreadPoolExecutor(max_workers=self.pl_node.max_pool_workers))
        add_PipeLineServicer_to_server(self, grpc_server)

        # TODO: use insecure_port for now. support secure_port() in future.
        grpc_server.add_insecure_port(self.grpc_endpoint)
        logger.info("bind grpc to {}".format(self.grpc_endpoint))

        # NOTE: _grpc_worker_ready MUST be created in the main run(...)
        # TODO: is it a good time to set this flag, should we do it in close-loop?
        self._grpc_worker_ready.set()

        self._grpc_server = grpc_server

        grpc_server.start()

    def run_grpc_in_thread(self):
        assert self.is_server, "run_grpc_in_thread cannot be explicitly called in non-server mode {}".format(self.mode)

        t_grpc = Thread(
            target=self.run_grpc,
            args=[],
        )
        t_grpc.start()
        self._t_grpc = t_grpc
        return t_grpc

    def zrpc_stub_call(self, *args, _timeout=None, **kwargs):
        """
        Warm node instance zrpc client call
        :param args:
        :param _timeout:
        :param kwargs:
        :return:
        """
        data = self.input_arg_filter_function(kwargs)
        #_ = list(map(lambda x: logger.info("input zrpc| {}".format(x)), data.items()))

        data_enc = pl_var_encoder(data)

        result_enc = self.zrpc_client.rpc_server_call(data_enc, _timeout=_timeout)
        result = pl_var_decoder(result_enc)
        return result

    def grpc_stub_call(self, *args, _timeout=None, **kwargs):
        """
        Warm node instance grpc client call
        for normal call, args for pl node was in kwargs.
        for stream call, args for pl node was in args[0] which must be a generator
        :param args:
        :param _timeout: timeout for both client and server side TODO: (?)
        :param kwargs: node args for normal call
        :return:
        """
        if len(args) > 0:
            request = args[0]
            is_stream_call = isinstance(request, types.GeneratorType)
        else:
            is_stream_call = False

        if _timeout:
            pre_process_func = f_compose_r(
                self.input_arg_filter_function,
                partial(inject_timeout, _timeout=_timeout),
                grpc_data_encoder_cli
            )
        else:
            pre_process_func = f_compose_r(
                self.input_arg_filter_function,
                grpc_data_encoder_cli
            )

        post_process_func = grpc_data_decoder_cli

        if is_stream_call:
            f_run_task = self.grpc_client.RunTasks
            pre_process_func = None  # GeneralDataProcessLayer(process_func=pre_process_func)
            post_process_func = None  # GeneralDataProcessLayer(process_func=post_process_func)
        else:
            request = kwargs
            f_run_task = self.grpc_client.RunTask

        #"""
        result = f_compose_r(
            pre_process_func,
            f_run_task,
            post_process_func,
        )(request)
        #"""

        """
        logger.info("request={}".format([type(request), request]))

        request_enc1 = self.input_arg_filter_function(request)
        logger.info("request_enc1={}".format([type(request_enc1), request_enc1]))

        request_enc = grpc_data_encoder_cli(request_enc1)
        #request_enc = pre_process_func(request)
        logger.info("request_enc={}".format([type(request_enc), request_enc]))

        _request = VarMap_.loads(request_enc.task_kwargs)
        logger.info("_request={}".format([type(_request), _request]))

        result_enc = f_run_task(request_enc)
        logger.info("result_enc={}".format([type(result_enc), result_enc]))

        result = post_process_func(result_enc)
        logger.info("result={}".format([type(result), result]))
        """
        return result

    def __call__(self, *args, _channel=None, _timeout=None, **kwargs):
        """
        meta RPC channel stub call
        :param args:
        :param _channel:
        :param _timeout:
        :param kwargs:
        :return:
        """
        if _channel is None:
            _channel = DARWIN_CONFIG["pipeline_warm_node_call_default_channel"]

        if _channel == "zrpc":
            f_rpc_stub_call = self.zrpc_stub_call
        elif _channel == "grpc":
            f_rpc_stub_call = self.grpc_stub_call
        else:
            raise TypeError("Unsupported rpc sub-channel \"{}\"".format(_channel))

        result = f_rpc_stub_call(*args, _timeout=_timeout, **kwargs)
        return result

    def zrpc_server_call(self, data, _timeout=None):
        """
        rpc server call from zerorpc
        :param data: encoded input byte stream for workload function from zerorpc client/stub invoker
        :type data: bytes
        :param _timeout: timeout from client/stub or default in the cfg.
        :type _timeout: Union(float, NoneType)
        :return: encoded output byte stream for caller
        """
        return partial(
            self.rpc_server_call,
            f_encoder=pl_var_encoder,
            f_decoder=pl_var_decoder,
            rpc_source="zrpc",
        )(data, _timeout=_timeout)

    def grpc_server_call(self, data, context, _timeout=None):
        """
        rpc server call from gRPC
        :param data: raw data for workload function from gRPC client/stub invoker
        :type data: TaskRequest
        :param context: gRPC server side runtime context
        :param _timeout: timeout from client/stub or default in the cfg
        :type _timeout: Union(float, NoneType)
        :return:
        """
        return partial(
            self.rpc_server_call,
            f_encoder=grpc_data_encoder_srv,
            f_decoder=grpc_data_decoder_srv,
            rpc_source="grpc",
        )(data, _timeout=_timeout)

    def RunTask(self, request, context):
        assert isinstance(request, TaskRequest)

        _timeout = request._timeout
        # pb does not handle python None, client maps None to 0.
        if math.isclose(_timeout, 0.0, abs_tol=1e-5):
            _timeout = None
        return self.grpc_server_call(request, context, _timeout=_timeout)

    def RunTasks(self, request_iterator, context):
        for request in request_iterator:
            yield self.RunTask(request, context)


class PipeNodeWarmInstanceRedis(PipeNodeWarmInstanceRpc):
    def __init__(self, *args, **kwargs):
        self._c_uuid_ctrl = None
        self._q_ctrl = None
        self._t_ctrl = None
        self._t_broker = None
        self._broker_ps = None
        self._redis_cli = None
        self._hb_data = None
        self._hb_token = None
        self._hb_key = None
        self._status = None
        self._ctrl_flow_ready = None
        self._beacon_locked = False
        super().__init__(*args, **kwargs)

    def __getstate__(self):
        state = super().__getstate__()
        del state["_c_uuid_ctrl"]
        del state["_q_ctrl"]
        del state["_t_ctrl"]
        del state["_t_broker"]
        del state["_broker_ps"]
        del state["_redis_cli"]
        del state["_hb_data"]
        del state["_hb_token"]
        del state["_hb_key"]
        del state["_status"]
        del state["_ctrl_flow_ready"]
        del state["_beacon_locked"]
        return state

    def __setstate__(self, state):
        super().__setstate__(state)
        self._c_uuid_ctrl = None
        self._q_ctrl = None
        self._t_ctrl = None
        self._t_broker = None
        self._broker_ps = None
        self._redis_cli = None
        self._hb_data = None
        self._hb_token = None
        self._hb_key = None
        self._status = None
        self._ctrl_flow_ready = None
        self._beacon_locked = False

    @property
    def uuid_ctrl(self):
        """
        uuid for ctrl channel
        :return:
        """
        if self._c_uuid_ctrl is None:
            self._c_uuid_ctrl = reduce_uuid(self.uuid, "pl_inst_ctrl")
        return self._c_uuid_ctrl

    @property
    def q_ctrl(self):
        """
        Queue for ctrl channel
        :return:
        """
        if self._q_ctrl is None:
            self._q_ctrl = Queue()
        return self._q_ctrl

    @property
    def t_ctrl(self):
        """
        Thread/task for ctrl channel worker
        :return:
        """
        return self._t_ctrl

    @property
    def t_broker(self):
        """
        Thread/task for broker
        :return:
        """
        return self._t_broker

    @property
    def redis_cli(self):
        """
        A shared redis client to be easy of coding
        :return:
        """
        if self._redis_cli is None:
            self._redis_cli = load_func_from_spec(DARWIN_CONFIG.service_name, "redis_client")
        return self._redis_cli

    @property
    def hb_token(self):
        """
        Token to identify the life of this instance during hb(heartbeat)
        :return:
        """
        if self._hb_token is None:
            self._hb_token = uuid.uuid4()
        return self._hb_token

    @property
    def hb_key(self):
        """
        HB beacon name to distinguish with other pl instances.
        :return:
        """
        if self._hb_key is None:
            self._hb_key = "darwin_hb.pl.{}-pn.{}.{}-pi{}.{}".format(
                self.pl_node.pl_uuid[puas],
                self.pl_node.uuid[puas], self.pl_node.queue_id[puas],
                self.instance_id, self.uuid[puas],
            )
        return self._hb_key

    def _predict_is_hb_not_expire(self, value):
        """
        Predict if given time will expire the heartbeat
        :param value:
        :return:
        """
        timeout = self.pl_node.hb_interval
        # give server a lead time to expire the hb
        if self.is_server:
            timeout -= self.pl_node.hb_lead_time
        return value is not None and (time.time() - value) < self.pl_node.hb_interval

    @property
    def beacon(self):
        """
        if the beacon is there
        :return:
        """
        r = True
        # 灯亮就不查了
        if not self.beacon_lamp:
            # TODO: tooooooo verbose, must be removed when commit
            #logger.debug("check hb beacon")
            r = self.redis_cli.exists(self.hb_key)
        return r

    @property
    def beacon_lock_key(self):
        """
        ID of distribute beacon lock in redis
        :return:
        """
        return "{}_lock".format(self.hb_key)

    @property
    def beacon_locked(self):
        """
        If beacon was locked
        :return: False or a valid Redlock lock object
        """
        assert self.is_server, "beacon_lock cannot be explicitly checked in non-server mode {}".format(self.mode)
        return self._beacon_locked

    @beacon_locked.setter
    def beacon_locked(self, value):
        """
        Smart lock/unlock/extend beacon lock
        :param value:
        :return:
        """
        assert self.is_server, "beacon_lock cannot be explicitly set in non-server mode {}".format(self.mode)
        if value is True:
            self.lock_beacon()
        elif value is False:
            self.unlock_beacon()

    def lock_beacon(self, ttl=None):
        """
        Lock beacon
        :param ttl:
        :return:
        """
        assert self.is_server, "lock_beacon cannot be explicitly called in non-server mode {}".format(self.mode)
        if ttl is None:
            ttl = self.pl_node.hb_interval + DARWIN_CONFIG["pipeline_ctrl_cmd_default_timeout"]

        if DARWIN_CONFIG.debug_performance:
            beg = time.time()

        # extend the lock ttl if already locked, or
        # acquire the new lock
        if self.beacon_locked and self.redis_cli.exists(self.beacon_lock_key):
            r = redis_extlock(self.beacon_locked, ttl)
            assert r, "Extend a locked existing lock should not fail"

            if DARWIN_CONFIG.debug_performance:
                dur = time.time() - beg
                logger.critical("perf: {}, extend hb lock in {:.6f}s".format(self.log_prefix, dur))
        else:
            self._beacon_locked = redis_lock(
                self.beacon_lock_key, ttl,
                retry_count=3, retry_delay=0.2,
                redis_cli=self.redis_cli,
            )
            if DARWIN_CONFIG.debug_performance:
                dur = time.time() - beg
                logger.critical("perf: {}, acquire hb lock in {:.6f}s".format(self.log_prefix, dur))
        return self._beacon_locked

    def unlock_beacon(self):
        """
        Unlock beacon
        :return:
        """
        assert self.is_server, "unlock_beacon cannot be explicitly called in non-server mode {}".format(self.mode)
        if self._beacon_locked:
            redis_unlock(self._beacon_locked)
            self._beacon_locked = False

    @property
    def beacon_lamp(self):
        """
        if the beacon is valid/alive
        :return:
        """
        r = False
        if self._hb_data is not None:
            last_hb_time, payload = self._hb_data
            r = self._predict_is_hb_not_expire(last_hb_time)
        return r

    def _get_remote_hb_data(self):
        """
        Get hb data from beacon
        :return:
        """
        # this is a hot function. enable perf debug in a higher verbose level to reduce
        # abusing debugging.
        is_hb_perf_debug = DARWIN_CONFIG.debug_performance and DARWIN_CONFIG.verbose_level > 1
        if is_hb_perf_debug:
            beg = time.time()
        data = self.redis_cli.get(self.hb_key)
        if data is not None:
            data = hb_data_decoder(data)
            if is_hb_perf_debug:
                dur = time.time() - beg
                # hb data is too verbose, trace only necessary.
                if DARWIN_CONFIG.verbose_level > 1:
                    logger.critical("perf: {}, load hb data in {:.6f}s".format(self.log_prefix, dur))
            if not isinstance(data, PLCacheSpec):
                data = None
            else:
                a_time, payload = data
                logger.debug("{}: receive hb_data {}".format(
                    self.log_prefix, data,
                ))
                # TODO: do something to the received hb_token???
                if "hb_token" in payload:
                    del payload["hb_token"]
                    data = PLCacheSpec(a_time, payload)
        return data

    def _set_remote_hb_data(self, data):
        """
        Set hb data to beacon
        :param value:
        :return:
        """
        a_time, payload = data
        if a_time is None:
            a_time = time.time()

        _payload = {
            "hb_token": self.hb_token,
        }
        _payload.update(payload)

        f = partial(self.redis_cli.set, self.hb_key, hb_data_encoder(PLCacheSpec(a_time, _payload)))
        if DARWIN_CONFIG.debug_performance:
            f = partial(f_timeit_call, f)

        r = f()

        if DARWIN_CONFIG.debug_performance:
            r, dur = r
            # hb data is too verbose, trace only necessary.
            if DARWIN_CONFIG.verbose_level > 1:
                logger.critical("perf: {}, save hb data in {:.6f}s".format(self.log_prefix, dur))
        logger.debug("{}: write hb_data {}".format(
            self.log_prefix, _payload,
        ))

    def _del_remote_hb_data(self):
        logger.debug("{}, delete hb data.".format(self.log_prefix))
        self.redis_cli.delete(self.hb_key)

    @property
    def hb_data(self):
        """
        NOTE: hb data is not controlled by beacon lamp, instead, it controls lamp
        :return:
        """
        # retrieve from remote if no lamp
        for i in range(2):
            if self.beacon_lamp:
                break
            elif i == 0:
                self._hb_data = self._get_remote_hb_data()
            elif i == 1:
                logger.debug("{}, expire hb data.".format(self.log_prefix))
                self._hb_data = None

        return self._hb_data

    @hb_data.setter
    def hb_data(self, value):
        assert self.is_server, "hb_data cannot be explicitly set in non server mode {}".format(self.mode)

        is_req_stop = isinstance(value, StopIteration)
        is_req_auto_refresh = value is None

        # magic backdoor to house clean hb_data after service shutdown
        if is_req_stop:
            self._del_remote_hb_data()
            value = None
        else:
            # auto refreshing hb_data is requested by explicitly assigning none.
            if is_req_auto_refresh:
                # --------------------------------
                # collect hb_data first
                # TODO: be careful NOT NOT NOT to include heavy data into hb!!!
                resp_status = self._handle_ctrl_resp_status({}, None)

                # keep full blood by kicking beacon lamp
                # TODO: be careful NOT NOT NOT to include heavy data into hb!!!
                value = {
                    "status": resp_status.payload,
                    "srv_host_ports": [self.rpc_host, self.zrpc_port, self.grpc_port],
                }

            if not isinstance(value, PLCacheSpec):
                value = PLCacheSpec(time.time(), value)

            # set remotely if no lamp
            if not self.beacon_lamp or is_req_auto_refresh:
                self._set_remote_hb_data(value)

        # set locally after beacon detection
        self._hb_data = value

    @property
    def hb_time(self):
        """
        alias of hb_time in hb_data
        :return:
        """
        _hb_data = self.hb_data
        if _hb_data is not None:
            r = _hb_data.enqueue_time
        else:
            r = None
        return r

    @property
    def hb_payload(self):
        """
        alias of hb_payload in hb_data
        :return:
        """
        _hb_data = self.hb_data
        if _hb_data is not None:
            r = _hb_data.payload
        else:
            r = None
        return r

    @property
    def status(self):
        """
        Get status of pl inst, it's an alias of hb_data.payload
        NOTE:
        * Getter is only for client.
        * Though remote can resp the status query, we only use the passive cache in the beacon.
        :return:
        """
        assert self.is_client, \
            "pl inst \"status\" cannot be explicitly query in non client mode {}".format(self.mode)

        _hb_payload = self.hb_payload
        if _hb_payload is not None:
            # aggressively trust the beacon lamp!
            _status = _hb_payload["status"]
        else:
            _status = None
        return _status

    @status.setter
    def status(self, value):
        """
        NOTE: for pl inst server side only
        :param value:
        :return:
        """
        assert self.is_server,\
            "pl inst \"status\" cannot be explicitly set in non server mode {}".format(self.mode)

        is_enforce = "_enforce" in value
        if not self.beacon_lamp or is_enforce:
            if is_enforce:
                del value["_enforce"]
            if self._status is None:
                # TODO: should we make a copy?
                self._status = value
            else:
                self._status.update(value)

    # "state" getter is now alias of "status".state in this derived pl inst class
    @PipeNodeWarmInstance.state.getter
    def state(self):
        """
        hb data payload first, and then request ctrl from remote
        :return:
        """
        _status = self.status
        if _status is not None:
            # NOTE: pickle class enum in cython was complicate, let's covert it to str
            state_str = _status["result"]["state"]
            return type(self).STATE[state_str]
        else:
            return type(self).STATE.pending

    @PipeNodeWarmInstanceRpc.rpc_host.getter
    def rpc_host(self):
        if self.is_server:
            return PipeNodeWarmInstanceRpc.rpc_host.fget(self)

        _hb_payload = self.hb_payload
        if _hb_payload is not None:
            # aggressively trust the beacon lamp!
            self._rpc_host, self._zrpc_port, self._grpc_port = _hb_payload["srv_host_ports"]
        else:
            self._rpc_host = None
        return self._rpc_host

    @PipeNodeWarmInstanceRpc.zrpc_port.getter
    def zrpc_port(self):
        if self.is_server:
            return PipeNodeWarmInstanceRpc.zrpc_port.fget(self)

        # re-direct to rpc_host getter to reuse get logic
        _ = self.rpc_host
        return self._zrpc_port

    @PipeNodeWarmInstanceRpc.grpc_port.getter
    def grpc_port(self):
        if self.is_server:
            return PipeNodeWarmInstanceRpc.grpc_port.fget(self)

        # re-direct to rpc_host getter to reuse get logic
        _ = self.rpc_host
        return self._grpc_port

    def run_data(self):
        _ = super().run_data()
        # notify ctrl channel to stop also
        self.q_ctrl.put(PLCacheSpec(time.time(), StopIteration("stop_aft_data_flow_termination")))

    def response_data(self, sess_output_pairs):
        """
        Response data requests from broker data channel
        :param sess_ids:
        :param outputs:
        :return:
        """
        # let parent reponse first, we handle the remains
        pairs = super().response_data(sess_output_pairs)
        n_sess_ids = len(pairs)

        # fast path to quick return if nothing left for us to handle
        if n_sess_ids < 1:
            return []

        sess_ids, outputs = zip(*pairs)
        # Get output var definition specs for this PipeNode
        output_var_specs = f_compose_r(
            attrgetter("pl_node.outputs"),
            partial(map, f_get_pl_variable_spec_from_str),
            list,
        )(self)

        # prepare output variables which will be used to send result to caller.
        variable_class = type(self.pl_node).get_variable_class()
        output_vars_lst = f_compose_r(
            partial(map, f_compose_r(
                lambda sess_id: map(lambda spec: variable_class(spec, sess_id=sess_id), output_var_specs),
                list,
            )),
            list,
        )(sess_ids)
        # log for debug
        """
        # show the output variables' name and its beloing session
        _ = f_compose_r(
            partial(map, f_compose_r(
                partial(map, attrgetter("name", "_sess_id")),
                list,
                logger.info,
            )),
            list,
        )(output_vars_lst)
        # show the output variables' value which will be send back
        _ = f_compose_r(
            enumerate,
            partial(map, f_compose_r(
                partial(f_star_call, "raw_output[{}]: {}".format),
                logger.info
            )),
            list,
        )(outputs)
        """

        # send data back to remote invoker
        # calculate set instructions
        pairs = f_compose_r(
            # flatten the data
            partial(map, partial(reduce, concat)),
            partial(f_star_call, lambda vars, vals: [vars, ["value"]*len(vars), vals]),
            partial(f_star_call, zip),
            list,
        )([output_vars_lst, outputs])
        # log for debug, again
        # show the output variables' name and its beloing session, after it was paired
        # sample output:
        # [('id', '.eaded2/.f6a103'), ('name', 'c'), ('val', [[0.95, 0.17], [0.89, 0.84]])]
        #          ^^^^^^^ ^^^^^^^                            ^^^^^^^^^^^^^^^^^^^^^^^^^^^^
        #         sess_id  var's uuid                         value of variable which will be send back
        _ = f_compose_r(
            # --------------------------
            # debug and verbose>1
            #
            f_echo if DARWIN_CONFIG.verbose_level > 2 else partial(f_si_call, list),
            # --------------------------
            partial(map, f_compose_r(
                itemgetter(0, 2),
                f_zip(
                    # pl_var
                    f_compose_r(
                        f_group(
                            f_compose_r(
                                attrgetter("_sess_id", "uuid"),
                                partial(map, itemgetter(puas)),
                                list,
                                partial(f_star_call, ".{}/.{}".format),
                                partial(f_echo, "id"),
                                f_list_rize,
                            ),
                            f_compose_r(
                                attrgetter("name"),
                                partial(f_echo, "name"),
                                f_list_rize,
                            ),
                        ),
                        partial(reduce, concat),
                    ),
                    # pl_var's value
                    f_compose_r(
                        f_group(
                            f_echo,
                        ),
                        partial(zip, ["val"]),
                        list,
                    ),
                ),
                list,
                partial(reduce, concat),
                logger.debug,
            )),
            list,
        )(pairs)

        # execute value set instructions
        f_setattr = setattr
        if DARWIN_CONFIG.debug_performance:
            f_setattr = partial(f_timeit_call, f_setattr)

        f_setattr_all = f_compose_r(
            partial(
                # use parallel_t to parallel io
                parallel_starmap_t,
                f_setattr,
            ),
            list,
        )
        if DARWIN_CONFIG.debug_performance:
            f_setattr_all = partial(f_timeit_call, f_setattr_all)

        results = f_setattr_all(pairs)
        if DARWIN_CONFIG.debug_performance:
            results, dur_all = results
            logger.critical("perf: {}, Data worker resp to {} vars in {} sess in {:.6f}s".format(
                self.log_prefix,
                len(pairs), n_sess_ids,
                dur_all,
            ))
            if DARWIN_CONFIG.verbose_level > 1:
                _, durs_per_var = f_star_call(zip, results)
                _ = f_compose_r(
                    f_zip(
                        f_compose_r(
                            partial(map, itemgetter(0)),
                            list,
                        ),
                        f_echo,
                    ),
                    list,
                    partial(f_star_call, zip),
                    list,
                    partial(map, partial(f_star_call, lambda pl_var, duration: logger.critical(
                        "perf: {}, Data worker resp to \"{}\"/.{}/.{} in {:.6f}s".format(
                            self.log_prefix,
                            pl_var.name, pl_var.sess_id[puas], pl_var.uuid[puas],
                            duration,
                        )))),
                    list,
                )([pairs, durs_per_var])

        # clean up for this round
        _ = f_compose_r(
            range,
            partial(map, partial(f_si_call, self.q_data.task_done)),
            tuple,
        )(len(sess_ids))

        # assume all handled and no remains
        return []

    def run_ctrl(self):
        """
        Ctrl channel worker
        :return:
        """
        assert self.is_server,\
            "pl inst \"run_ctrl\" cannot be explicitly called in non server mode {}".format(self.mode)

        poll_interval = self.pl_node.hb_interval - self.pl_node.hb_lead_time
        is_ctrl_flow_ready = False
        is_zrpc_worker_ready_or_not_required = not is_zrpc_accepted_channel
        is_grpc_worker_ready_or_not_required = not is_grpc_accepted_channel
        enforce_hb = False
        while True:
            """
            # Update pl inst service dashboard
            """
            # refresh the state so that the "status" ctrl cmd can have consistent state
            # note: this must be set directly w/o triggering any side action.
            self._state = type(self).STATE.started

            # cache ctrl_flow_ready event into local variable to improve performance in case threading.Event is shit.
            # NOTE: this event will be set during first external "status" ctrl req which was suppose to be issued
            #       by main thread of ourself.
            if not is_ctrl_flow_ready:
                is_ctrl_flow_ready = self._ctrl_flow_ready.is_set()
                if is_ctrl_flow_ready:
                    logger.debug("{}: ctrl flow is ready".format(self.log_prefix))

            # cache zrpc_worker_ready event into local variable to improve performance in case threading.Event is shit.
            # NOTE: this event will be set after run_zrpc_in_thread had zrpc service ready
            if not is_zrpc_worker_ready_or_not_required:
                is_zrpc_worker_ready_or_not_required = self._zrpc_worker_ready.is_set()
                if is_zrpc_worker_ready_or_not_required:
                    logger.debug("{}: zrpc worker is ready".format(self.log_prefix))

            # cache grpc_worker_ready event into local variable to improve performance in case threading.Event is shit.
            # NOTE: this event will be set after run_grpc_in_thread had grpc service ready
            if not is_grpc_worker_ready_or_not_required:
                is_grpc_worker_ready_or_not_required = self._grpc_worker_ready.is_set()
                if is_grpc_worker_ready_or_not_required:
                    logger.debug("{}: grpc worker is ready".format(self.log_prefix))

            # do NOT NOT NOT set hb beacon before the ctrl flow is confirmed ready. or,
            # client might loss resp if it sends req earlier than broker is ready to dispatch msg
            if (
                    not self.beacon_lamp or
                    enforce_hb
            ) and (
                    is_ctrl_flow_ready and
                    is_zrpc_worker_ready_or_not_required and
                    is_grpc_worker_ready_or_not_required
            ):
                # magic call to request auto refreshing hb data in hb_interval
                self.hb_data = None
                enforce_hb = False
            """
            # end of update pl inst service dashboard
            """

            # wait and extract next ctrl cmd data
            try:
                data = self.q_ctrl.get(True, poll_interval)
            except Empty as _e:
                logger.debug("{}: Oops, an empty ctrl loop".format(self.log_prefix))
                enforce_hb = True
                continue

            # extract data and perf tagging
            enqueue_time, data = data
            if DARWIN_CONFIG.debug_performance and isinstance(data, PLNodeSessDataSpec):
                sess_id = data.sess_id
                logger.critical("perf: {}, ctrl sess .{} sit-in-q for {:.6f}s".format(
                    self.log_prefix, sess_id[puas], time.time()-enqueue_time,
                ))

            # backdoor ctrl cmd from data channel worker about its termination
            # note: ctrl worker being terminated by data worker through this backdoor is the official way to
            #       to stop ctrl worker in order to provide full ctrl of pl instance service management.
            if isinstance(data, StopIteration):
                logger.debug("{}: Ctrl worker is terminating".format(self.log_prefix))
                self.q_ctrl.task_done()
                break

            elif not isinstance(data, PLNodeSessDataSpec):
                logger.warning("{}: Invalid type of ctrl cmd \"{}\" will be dropped.".format(
                    self.log_prefix, type(data).__name__,
                ))
                self.q_ctrl.task_done()
                continue

            # dispatch ctrl msg to its responding handler
            f = partial(self.response_ctrl, data.sess_id, data.data)
            if DARWIN_CONFIG.debug_performance:
                f = partial(f_timeit_call, f)
            r = f()
            if DARWIN_CONFIG.debug_performance:
                r, dur = r
                if isinstance(data.data, PLCtrlCmdSpec):
                    cmd_name = data.data.name
                else:
                    cmd_name = type(data.data).__name__
                logger.critical("perf: {}, run for ctrl sess .{}/\"{}\" in {:.6f}s".format(
                    self.log_prefix, sess_id[puas], cmd_name, dur,
                ))
            # kick queue about the finishing of ctrl msg handling
            self.q_ctrl.task_done()

    def run_ctrl_in_thread(self):
        """
        start ctrl channel worker in a separate thread
        :return:
        """
        assert self.is_server, "pl inst \"run_ctrl_in_thread\" cannot be explicitly called in non server mode {}".format(self.mode)
        t_ctrl = Thread(
            target=self.run_ctrl,
            args=[],
        )
        t_ctrl.start()
        self._t_ctrl = t_ctrl
        return t_ctrl

    def run_broker_in_thread(self):
        """
        start msg broker in a separate thread
        :return:
        """
        assert self.is_server, "pl inst \"run_broker_in_thread\" cannot be explicitly called in non server mode {}".format(self.mode)
        f_f_hint_func = partial(
            partial,
            str.format,
            "{0}, {1} sess .{2[0]} received at {2[1]:.6f}",
            self.log_prefix,
        )
        f_hint_func_data, f_hint_func_ctrl = partial(map, f_compose_r(
            f_f_hint_func,
            partial(f_compose_l, logger.debug),
        ))(["data", "ctrl"])

        channel_data = self.uuid
        channel_ctrl = self.uuid_ctrl
        kwargs = {
            channel_data: partial(handle_msg, q=self.q_data, hint_func=f_hint_func_data),
            channel_ctrl: partial(handle_msg, q=self.q_ctrl, hint_func=f_hint_func_ctrl),
        }
        ps = self.redis_cli.pubsub()
        ps.subscribe(**kwargs)
        t_broker = ps.run_in_thread(sleep_time=0.001)
        logger.debug("{}: serve data channel at {}".format(self.log_prefix, self.uuid))
        logger.debug("{}: serve ctrl channel at {}".format(self.log_prefix, self.uuid_ctrl))

        self._t_broker = t_broker
        self._broker_ps = ps
        return t_broker

    def run(self):
        """
        Main thread which will wait and join all known threads
        :return:
        """
        pid = os.getpid()

        # switch to server mode
        self._mode = type(self).MODE.server
        #logger.info("rpc_host={}".format(self.rpc_host))

        # create for ctrl channel worker
        self._ctrl_flow_ready = Event()

        # redis data channel worker before the broker
        _ = self.run_data_in_thread()

        # zrpc data channel worker before the broker
        if is_zrpc_accepted_channel:
            # create for zrpc channel worker
            self._zrpc_worker_ready = Event()
            _ = self.run_zrpc_in_thread()

        # grpc data channel worker before the broker
        if is_grpc_accepted_channel:
            # create for grpc channel worker
            self._grpc_worker_ready = Event()
            _ = self.run_grpc_in_thread()

        # run ctrl after data channels are all ready, though not ensured by thread scheduler
        _ = self.run_ctrl_in_thread()
        # broker worker last so that workers can be ready before dispatcher/broker
        # TODO: It's required for redis pub/sub because data before the broker will be lost
        if is_redis_accepted_channel:
            _ = self.run_broker_in_thread()

        # ------------------------------------------------------------
        # trigger and enforce 1st status update about registration
        #
        pl_inst_cli = self.clone()
        # request ctrl worker to handle first status update/registration
        f_target = partial(
            pl_inst_cli.request_ctrl,
            type(self).CMD_TYPE.status,
            payload={
                "_ignore_return": True,
            },
            # must send rudely to trigger the initial registration
            rudely=True,
        )
        # check if beacon is there
        f_cond = partial(
            f_si_call,
            partial(
                getattr,
                pl_inst_cli,
                "beacon",
            ),
        )
        # TODO: should we endless wait for the beacon
        f = partial(loop_run, f_target, f_cond, sleep_interval=0.001, timeout=-1)
        if DARWIN_CONFIG.debug_performance:
            f = partial(f_timeit_call, f)

        r = f()

        if DARWIN_CONFIG.debug_performance:
            _, dur = r
            logger.critical("perf: {}, main thread wait beacon for {:.6f}s".format(self.log_prefix, dur))
        del pl_inst_cli
        #
        # end of trigger and enforce 1st status update about registration
        # ------------------------------------------------------------

        stop_graceful_time = DARWIN_CONFIG["pipeline_warm_node_default_stop_graceful_time"]
        try:
            if self.t_data is not None:
                # t_data should always be stoppable
                self.t_data.join(
                    #timeout=stop_graceful_time
                )
                if self.t_data.isAlive():
                    raise TimeoutError("t_data")
            if self.t_ctrl is not None:
                # t_ctrl should always be stoppable
                self.t_ctrl.join(
                    #timeout=stop_graceful_time,
                )
                if self.t_ctrl.isAlive():
                    raise TimeoutError("t_ctrl")

            if self._broker_ps is not None and is_redis_accepted_channel:
                self._broker_ps.unsubscribe([self.uuid, self.uuid_ctrl])
            if self._t_broker is not None:
                # t_broker should always be stoppable
                self.t_broker.join(
                    #timeout=stop_graceful_time,
                )
                if self.t_broker.isAlive():
                    raise TimeoutError("t_broker")

            if is_grpc_accepted_channel:
                if self._grpc_server is not None:
                    logger.info("Stop grpc server at {}".format(self.grpc_endpoint))
                    self._grpc_server.stop(
                        grace=stop_graceful_time,
                    )

                if self._t_grpc is not None:
                    self.t_grpc.join(
                        timeout=stop_graceful_time,
                    )
                    if self.t_grpc.isAlive():
                        raise TimeoutError("t_grpc")

            if is_zrpc_accepted_channel:
                # TODO: remove this rude housekeeping after monkey patch had been enforced for zrpc
                if DARWIN_CONFIG["apply_gevent_monkey_patch"] is not True:
                    logger.info("sig term self {} to terminate zrpc".format(pid))
                    os.kill(pid, signal.SIGTERM)

                elif self._zrpc_server is not None:
                    logger.info("Stop zrpc server at {}".format(self.zrpc_endpoint))
                    # only issue stop when monkey_patch was applied
                    self._zrpc_server.stop()

                if self._t_zrpc is not None:
                    # TODO: t_zrpc is very possible cannot be terminated by itself
                    self.t_zrpc.join(
                        timeout=stop_graceful_time,
                    )
                    if self.t_zrpc.isAlive():
                        raise TimeoutError("known t_zrpc timeout, don't worry.")
            logger.info("All services were terminated")

        except TimeoutError as _e:
            logger.exception("harmless timeout, don't worry.")
        finally:
            # TODO: remove this rude housekeeping after monkey patch had been enforced for zrpc
            if is_zrpc_accepted_channel and DARWIN_CONFIG["apply_gevent_monkey_patch"] is not True:
                logger.info("I am going to terminate my self {} after zrpc srv's out of service w/o monkey patch".format(
                    pid,
                ))
                sys.exit(0)

    def _run_in_local(self, executor_func, executor_name=None, timeout=None, wait_for_start=True):
        """
        start msg broker locally in a separate executor (thread or process)
        :return:
        """
        assert self.is_client, \
            "pl inst \"run_in_{}\" cannot be explicitly called in non client mode {}".format(
                executor_name, self.mode,
            )
        if not isinstance(executor_func, Callable):
            raise TypeError("executor_func needs to be a callable w/o arguments instead of {}".format(
                type(executor_func).__name__,
            ))
        if executor_name is None:
            executor_func = repr(executor_func)

        t = executor_func()
        t.start()

        if wait_for_start:
            if timeout is None:
                timeout = self.pl_node.start_time

            # wait for pl inst start up
            # NOTE: you have to clone a client to wait for state
            cli = self.clone()
            f = partial(
                cli.wait_for_state,
                type(cli).STATE.started,
                sleep_interval=0.01,
                timeout=timeout,
            )

            if DARWIN_CONFIG.debug_performance:
                f = partial(f_timeit_call, f)

            r = f()
            del cli

            if DARWIN_CONFIG.debug_performance:
                r, dur = r
                logger.critical("perf: {}, start in {} spends {:.6f}s".format(
                    self.log_prefix,
                    executor_name, dur,
                ))
        return t

    def run_in_thread(self, timeout=None, wait_for_start=True):
        return self._run_in_local(
            partial(Thread, target=self.clone().run, args=[]),
            executor_name="thread",
            timeout=timeout,
            wait_for_start=wait_for_start,
        )

    def run_in_process(self, timeout=None, wait_for_start=True):
        return self._run_in_local(
            partial(Process, target=self.clone().run, args=[]),
            executor_name="process",
            timeout=timeout,
            wait_for_start=wait_for_start,
        )

    def run_in_celery(self, timeout=None, wait_for_start=True, task_id=None):
        assert self.is_client,\
            "pl inst \"run_in_celery\" cannot be explicitly called in non client mode {}".format(self.mode)

        route = {
            True: run_pl_warm_node_with_gpu,
            False: run_pl_warm_node_with_cpu,
        }

        """
        if self.state == type(self).STATE.started:
            return True

        elif self.state != type(self).STATE.pending:
            raise ValueError("Unknown PL node instance state {}".format(self.state))
        """

        executor = f_compose_r(
            attrgetter("pl_node.flags"),
            partial(f_flip_call, contains, "gpu"),
            partial(getitem, route),
            attrgetter("apply_async")
        )(self)

        if task_id is None:
            task_id = str(uuid.uuid4())
            logger.debug("{}: Run in celery with a {} task id {}".format(
                self.log_prefix, "new", task_id,
            ))
        else:
            logger.debug("{}: Run in celery with a {} task id {}".format(
                self.log_prefix, "manual", task_id,
            ))
        executor_args = f_compose_r(
            partial(zip, [
                "args",
                "task_id",
            ]),
            dict,
            lambda _: [[], _],
        )([
            [self],
            task_id,
        ])
        #logger.info(executor_args)

        _ = f_star2_call(executor, executor_args)

        if wait_for_start:
            if timeout is None:
                timeout = self.pl_node.start_time

            # wait for pl inst start up
            # NOTE: you have to clone a client to wait for state
            cli = self.clone()
            f = partial(
                cli.wait_for_state,
                type(cli).STATE.started,
                sleep_interval=0.01,
                timeout=timeout,
            )

            if DARWIN_CONFIG.debug_performance:
                f = partial(f_timeit_call, f)

            r = f()
            del cli

            if DARWIN_CONFIG.debug_performance:
                r, dur = r
                logger.critical("perf: {}, start in celery spends {:.6f}s".format(self.log_prefix, dur))
        return task_id

    def start(self, timeout=None, container=None, wait_for_start=True, track_results=None):
        assert self.is_client, "pl inst \"start\" cannot be explicitly called in non client mode {}".format(self.mode)
        beg = time.time()
        if self.state == type(self).STATE.started:
            logger.debug("{} was already started and will not start again".format(self.log_prefix))
            return True

        if timeout is None:
            timeout = self.pl_node.start_time
        if container is None:
            container = DARWIN_CONFIG["pipeline_warm_node_default_container"]
        if isinstance(container, str):
            container = type(self).CONTAINER_TYPE[container]

        # generate distribute lock key for this warm inst startup
        start_key = reduce_uuid(self.uuid, "ctrl_start_quram")

        # log for perf
        if DARWIN_CONFIG.debug_performance:
            beg_lock = time.time()
            logger.critical("perf: {}, pre-state check for warm inst start in {:.6f}s".format(
                self.log_prefix, beg_lock - beg,
            ))

        # start locking for startup
        lock = redis_lock(start_key, self.pl_node.hb_interval+timeout)
        if not lock:
            logger.debug("{} was also being started by others. Let's be gentle.".format(self.log_prefix))
            return True

        elif self.state == type(self).STATE.started:
            logger.debug("{} was just started".format(self.log_prefix))
            redis_unlock(lock)
            return True

        method = "run_in_{}".format(container.name)

        # prepare startup method
        f = methodcaller(method, timeout=timeout, wait_for_start=wait_for_start)
        if DARWIN_CONFIG.debug_performance:
            f = partial(f_timeit_call, f)
        # issue the >>> *** <<< starting w/ distribute lock
        t = f(self)
        if DARWIN_CONFIG.debug_performance:
            t, dur_wo_lock = t

        # release the lock after start call
        redis_unlock(lock)

        if DARWIN_CONFIG.debug_performance:
            aft_lock = time.time()
            logger.critical("perf: {}, start warm inst in {:.6f}s with lock cost {:.6f}s".format(
                self.log_prefix, dur_wo_lock, aft_lock-beg_lock-dur_wo_lock,
            ))

        # hidden output to send real start return result to caller
        if isinstance(track_results, list):
            track_results.append([self, t])
        return self.state == type(self).STATE.started

    def stop(self, payload=None, timeout=None, wait_for_stop=True):
        assert self.is_client, "pl inst \"stop\" cannot be explicitly called in non client mode {}".format(self.mode)
        if self.state == type(self).STATE.pending:
            return True

        if payload is None:
            payload = StopIteration("stop by ctrl cmd from client")
        r = self.request_ctrl(type(self).CMD_TYPE.stop, payload=payload)

        if wait_for_stop:
            if timeout is None:
                timeout = DARWIN_CONFIG["pipeline_ctrl_cmd_default_timeout"]+self.pl_node.hb_interval

            # wait for pl inst stop
            # NOTE: you have to clone a client to wait for state
            cli = self.clone()
            f = partial(
                cli.wait_for_state,
                type(cli).STATE.pending,
                sleep_interval=0.01,
                timeout=timeout,
            )

            if DARWIN_CONFIG.debug_performance:
                f = partial(f_timeit_call, f)

            r = f()
            del cli

            if DARWIN_CONFIG.debug_performance:
                r, dur = r
                logger.critical("perf: {}, stop spends {:.6f}s".format(self.log_prefix, dur))
        return r

    def _send_ctrl_request(
            self,
            msg,
            timeout=None, sess_id=None,
            rudely=False, result_expire=None, ignore_return=False,
    ):
        """
        Helper function to send ctrl cmd's request to service instance
        :param msg:
        :param timeout:
        :param sess_id:
        :return:
        """
        assert self.is_client, "pl inst \"send_ctrl_request\" cannot be explicitly called in non client mode {}".format(self.mode)
        if timeout is None:
            timeout = DARWIN_CONFIG["pipeline_ctrl_cmd_default_timeout"]

        # before sending request to channel, wait for hb beacon appears which means the service had registered.
        if not rudely:
            r, dur = f_timeit_call(
                loop_run,
                partial(getattr, self, "beacon"),
                partial(
                    f_unstar_call,
                    itemgetter(0),
                ),
                sleep_interval=0.001,
                timeout=timeout,
            )
            # TODO: should we add more graceful time instead of 0
            timeout = max(0, timeout-dur)

        if sess_id is None:
            sess_id = str(uuid.uuid4())
            if isinstance(msg, PLCtrlCmdSpec):
                msg_name = msg.name
            else:
                msg_name = type(msg).__name__
            logger.debug("Generate sess id {} for ctrl request {}".format(sess_id, msg_name))

        f_send_msg = f_compose_r(
            partial(PLNodeSessDataSpec, sess_id),
            pl_var_encoder,
            partial(self.redis_cli.publish, self.uuid_ctrl),
        )
        f = partial(f_send_msg, msg)

        if DARWIN_CONFIG.debug_performance:
            f = partial(f_timeit_call, f)
        r = f()
        if DARWIN_CONFIG.debug_performance:
            r, dur = r
            logger.critical("perf: {}, send call req to inst {} with ctrl sess .{} in {:.6f}s".format(
                self.pl_node.log_prefix, self.log_prefix_short,
                sess_id[puas], dur,
            ))

        if ignore_return:
            resp = None
        else:
            # create response receiving ack pl variable
            pl_variable_class = type(self.pl_node).get_variable_class()
            if result_expire is None:
                result_expire = DARWIN_CONFIG["pipeline_ctrl_result_default_expire_time"]
            pl_var_resp = pl_variable_class("x", sess_id=sess_id, timeout=timeout, expire_time=result_expire)

            resp = pl_var_resp.value

            # explicitly free
            del pl_var_resp

        return resp

    def _send_ctrl_response(self, resp, sess_id, timeout=None):
        """
        Helper function to send ctrl cmd's response back to caller
        :param resp:
        :param sess_id:
        :param timeout:
        :return:
        """
        assert self.is_server, "pl inst \"send_ctrl_response\" cannot be explicitly called in non server mode {}".format(self.mode)
        assert sess_id is not None, "sess_id should not be None when sending ctrl response back"

        if timeout is None:
            timeout = DARWIN_CONFIG["pipeline_ctrl_cmd_default_timeout"]

        # create response sending pl variable
        pl_variable_class = type(self.pl_node).get_variable_class()
        expire_time = DARWIN_CONFIG["pipeline_ctrl_result_default_expire_time"]
        pl_var_resp = pl_variable_class("x", sess_id=sess_id, timeout=timeout, expire_time=expire_time)
        pl_var_resp.value = resp
        return resp

    def response_ctrl(self, sess_id, ctrl_msg, *args, timeout=None, **kwargs):
        """
        Dispatch known and implemented ctrl request to its defined handler. Ack
        :param sess_id:
        :param ctrl_msg:
        :param args:
        :param timeout:
        :param kwargs:
        :return:
        """
        assert self.is_server, "pl inst \"response_ctrl\" cannot be explicitly called in non server mode {}".format(self.mode)

        if not isinstance(ctrl_msg, PLCtrlCmdSpec):
            """ let parent handle unknown type of ctrl_msg """
            return super().response_ctrl(sess_id, ctrl_msg, *args, timeout=timeout, **kwargs)

        cmd_name, payload = ctrl_msg

        is_valid_cmd = cmd_name in type(self).CMD_TYPE.__members__
        has_cmd_handler = hasattr(self, "_handle_ctrl_req_{}".format(cmd_name))

        if not is_valid_cmd:
            logger.warning("{}: Unknown ctrl cmd \"{}\" from sess .{} and will be dropped.".format(
                self.log_prefix, cmd_name, sess_id[puas],
            ))
            resp = PLCtrlCmdSpec(
                cmd_name,
                {
                    "result": TypeError("Unknown ctrl cmd"),
                    "pl_inst_uuid": self.uuid,
                    "timestamp": time.time(),
                }
            )
        elif not has_cmd_handler:
            logger.warning("{}: Un-implemented ctrl cmd \"{}\" from sess .{} will be dropped.".format(
                self.log_prefix, cmd_name, sess_id[puas],
            ))
            resp = PLCtrlCmdSpec(
                cmd_name,
                {
                    "result": TypeError("Un-implemented ctrl cmd"),
                    "pl_inst_uuid": self.uuid,
                    "timestamp": time.time(),
                }
            )
        else:
            """ let parent handle known and implemented types of ctrl_msg """
            return super().response_ctrl(sess_id, ctrl_msg, *args, timeout=timeout, **kwargs)
        return self._send_ctrl_response(resp, sess_id, timeout=timeout)

    def _handle_ctrl_req_status(self, payload, *args, timeout=None, rudely=False, **kwargs):
        """
        Request status from remote service instance
        :param payload:
        :param args:
        :param timeout:
        :param kwargs:
        :return:
        """
        ignore_return = isinstance(payload, (dict, ChainMap)) and "_ignore_return" in payload
        if not isinstance(payload, dict):
            raise TypeError("payload of \"status\" request msg needs to be a dict instead of {}".format(
                type(payload).__name__,
            ))
        msg = PLCtrlCmdSpec(
            type(self).CMD_TYPE.status.name,
            ChainMap(payload, {
                "timestamp": time.time(),
            }),
        )
        return self._send_ctrl_request(msg, timeout=timeout, rudely=rudely, ignore_return=ignore_return)

    def _handle_ctrl_resp_status(self, payload, sess_id, *args, timeout=None, **kwargs):
        """
        Response status request from remote client
        :param payload:
        :param sess_id:
        :param args:
        :param timeout:
        :param kwargs:
        :return:
        """
        is_internal_hb_collecting_call = sess_id is None
        if not is_internal_hb_collecting_call and not self._ctrl_flow_ready.is_set():
            logger.debug("{}: Ctrl flow is ready".format(self.log_prefix))
            self._ctrl_flow_ready.set()

        ignore_return = isinstance(payload, (dict, ChainMap)) and "_ignore_return" in payload
        if ignore_return:
            del payload["_ignore_return"]

        if not self.beacon_lamp:
            logger.debug("{}: Update status".format(self.log_prefix))
            self.status = {
                # TODO: is enforce mandatory if lamp is off.
                "_enforce": True,
                # NOTE: pickle class enum in cython was complicate, let's covert it to str
                "state": self._state.name,
                # TODO: add other light weight attributes
            }

        resp = PLCtrlCmdSpec(
            type(self).CMD_TYPE.status.name,
            {
                "result": self._status,
                "pl_inst_uuid": self.uuid,
                "timestamp": time.time(),
                "req_payload": payload,
            },
        )
        # shortcut for internal call
        if is_internal_hb_collecting_call:
            return resp

        if ignore_return:
            resp = None
        else:
            resp = self._send_ctrl_response(resp, sess_id, timeout=timeout)
        return resp

    def _handle_ctrl_req_stop(self, payload, *args, timeout=None, rudely=False, **kwargs):
        if not isinstance(payload, StopIteration):
            raise TypeError("payload of \"stop\" request msg needs to be a StopIteration instead of {}".format(
                type(payload).__name__,
            ))
        msg = PLCtrlCmdSpec(
            type(self).CMD_TYPE.stop.name,
            {
                "signal": payload,
                "timestamp": time.time(),
            },
        )
        # NOTE: the result of stop req needs to be checked from client side instead of trusting
        # the return from remote.
        return self._send_ctrl_request(msg, timeout=timeout, rudely=rudely, ignore_return=True)

    def _handle_ctrl_resp_stop(self, payload, sess_id, *args, timeout=None, **kwargs):
        resp = PLCtrlCmdSpec(
            type(self).CMD_TYPE.stop.name,
            {
                "result": "cmd_issued",
                "pl_inst_uuid": self.uuid,
                "timestamp": time.time(),
                "req_payload": payload,
            }
        )
        # NOTE: ack ctrl req first, and then trigger the real stop procedure in backend
        # TODO: in case the stop procedure might be handled in another threads which block acking ctrl requester.
        r = self._send_ctrl_response(resp, sess_id, timeout=timeout)
        #
        self.q_data.put(PLCacheSpec(time.time(), payload["signal"]))
        return r


class PipeNodeWarmRedis(PipeNodeWarm):
    @classmethod
    def get_instance_class(cls):
        return PipeNodeWarmInstanceRedis

    @classmethod
    def get_variable_class(cls):
        pl_var_cls = DARWIN_CONFIG["pipeline_result_backend"]
        if pl_var_cls == "redis":
            pl_var_cls = PipeVariableRedis
        elif pl_var_cls == "celery":
            pl_var_cls = PipeVariableCelery
        else:
            raise EnvironmentError("Unsupported pipeline_result_backend {}".format(pl_var_cls))
        return pl_var_cls

    def __init__(self, *args, **kwargs):
        self._c_timeout = None
        self._redis_cli = None
        super().__init__(*args, **kwargs)

    def __getstate__(self):
        state = super().__getstate__()
        del state["_c_timeout"]
        del state["_redis_cli"]
        return state

    def __setstate__(self, state):
        super().__setstate__(state)
        self._c_timeout = None
        self._redis_cli = None

    @property
    def timeout(self):
        if self._c_timeout is None:
            self._c_timeout = self.kwflags.get("_timeout")
        return self._c_timeout

    @property
    def redis_cli(self):
        if self._redis_cli is None:
            self._redis_cli = load_func_from_spec(DARWIN_CONFIG.service_name, "redis_client")
        return self._redis_cli

    @property
    def hb_interval(self):
        if self._c_hb_interval is None:
            default_hb_interval = DARWIN_CONFIG["default_pipeline_warm_node_heartbeat_interval"]
            self._c_hb_interval = f_compose_r(
                attrgetter("kwflags"),
                f_star_call(
                    f_group,
                    partial(map, partial(partial, f_flip_call, dict.get))([
                        "hb_interval",
                        "_hb_interval",
                    ]),
                ),
                partial(filter, partial(f_flip_call, is_not, None)),
                list,
                lambda _: int(_[0]) if len(_) > 0 else default_hb_interval,
            )(self)
        return self._c_hb_interval

    @property
    def start_time(self):
        if self._c_start_time is None:
            default_start_time = DARWIN_CONFIG["default_pipeline_warm_node_start_time"]
            self._c_start_time = f_compose_r(
                attrgetter("kwflags"),
                f_star_call(
                    f_group,
                    partial(map, partial(partial, f_flip_call, dict.get))([
                        "start_time",
                        "_start_time",
                    ]),
                ),
                partial(filter, partial(f_flip_call, is_not, None)),
                list,
                lambda _: int(_[0]) if len(_) > 0 else default_start_time,
            )(self)
        return self._c_start_time

    @property
    def hb_lead_time(self):
        if self._c_hb_lead_time is None:
            _hb_lead_time = DARWIN_CONFIG["pipeline_warm_node_heartbeat_lead_time"]
            if isinstance(_hb_lead_time, float):
                _hb_lead_time *= self.hb_interval
            self._c_hb_lead_time = _hb_lead_time
        return self._c_hb_lead_time

    def generate_output_vars(self, sess_id, timeout=None):
        if timeout is None:
            timeout = self.timeout
        elif timeout < 0:
            timeout = None

        return f_compose_r(
            attrgetter("outputs"),
            partial(map, f_compose_r(
                f_get_pl_variable_spec_from_str,
                partial(type(self).get_variable_class(), sess_id=sess_id, timeout=timeout),
            )),
            list,
        )(self)

    def __call__(self, *args, _channel=None, **kwargs):
        if _channel is None:
            _channel = DARWIN_CONFIG["pipeline_warm_node_call_default_channel"]
        return methodcaller("call_from_{}".format(_channel), *args, **kwargs)(self)

    def call_from_redis(self, *args, _catched_exceptions=[], _target_instance=None, _timeout=None, **kwargs):
        """
        warm node client stub run
        :param args:
        :param kwargs:
        :return:
        """
        if DARWIN_CONFIG.debug_performance:
            time_beg = time.time()

        # IMPORTANT! IMPORTANT! IMPORTANT!
        # make an unique session key for each warm_run so that the output variables can be differentiated.
        sess_id = str(uuid.uuid4())
        logger.debug("{}: New session id .{} for incoming call".format(self.log_prefix, sess_id[puas]))

        # get warm node services' queue id lists
        # TODO: assume all instances are "active" and available for call. Change to specific "active" instance later.
        active_instance_ids = list(range(len(self.queue_ids)))

        # calculate the target instance if requested by caller
        _target_instance = self.parse_instances(_target_instance)

        # TODO: we might close this backdoor to end user. but admin can still use data channel to request stop
        # request close warm node service by sending "stop" signal msg
        # for multi-instance warm node service, send signal to the group
        if len(args) > 0 and isinstance(args[0], StopIteration):
            def _stop_queue(id_, msg):
                pl_inst = self.instances[id_]
                logger.debug("Request stopping {} by \"{}\" from data channel".format(
                    pl_inst.log_prefix, repr(msg),
                ))
                return pl_inst.stop(msg)

            if _target_instance is None:
                _target_instances = active_instance_ids
            elif isinstance(_target_instance, (list, tuple)):
                _target_instances = _target_instance
            else:
                _target_instances = [_target_instance]

            data = args[0]
            r = f_compose_r(
                # TODO: parallel stop??
                partial(map, partial(f_flip_call, _stop_queue, data)),
                list,
            )(_target_instances)
            return r

        if _target_instance is None:
            # TODO: randomly pick a warm node service's to balance workload
            _target_instance = random.choice(active_instance_ids)

        pl_inst = self.instances[_target_instance]

        # TODO: Request all instances to be active.
        # TODO: same logic as "call_from_redis", needs to optimize
        if _target_instance not in active_instance_ids:
            if DARWIN_CONFIG["pipeline_task_auto_start_warm_nodes"] is True:
                logger.info("{}: request to be started immediately".format(
                    pl_inst.log_prefix,
                ))
                pl_inst.start()
            else:
                # Usually, we only call an instance in active_instances list.
                # but, it would be possible that its hb data was expired but server was busy and not have
                # chance to update hb in the hb_interval. so, instead of enforcing starting or fail the call,
                # we gracefully wait another hb_interval for the updated hb, or, we fail with timeout.
                #
                # NOTE: recommend tuning your pl parameters, such as hb_interval for your workload
                #
                logger.warning("{}: wait an extra hb_interval for its updated heartbeat. {}".format(
                    pl_inst.log_prefix,
                    "recommend tuning or increasing the hb_interval!",
                ))
                f = partial(
                    pl_inst.wait_for_state,
                    PipeNodeWarmInstance.STATE.started,
                    sleep_interval=0.01,
                    timeout=self.hb_interval,
                )
                if DARWIN_CONFIG.debug_performance:
                    f = partial(f_timeit_call, f)
                r = f()
                if DARWIN_CONFIG.debug_performance:
                    r, dur = r
                    logger.critical("perf: {}, wait for its ready spends extra {:.6f}s".format(
                        pl_inst.log_prefix, dur,
                    ))

            active_instance_ids.append(_target_instance)

        queue_id = self.queue_ids[_target_instance]

        # [pl_var1_spec_obj, ...]
        input_var_specs = f_compose_r(
            attrgetter("inputs"),
            partial(map, f_get_pl_variable_spec_from_str),
        )(self)

        # ["<pl_in_var1_name", ...], ["pl_in_arg1_name",...]
        input_var_names, input_arg_names = f_compose_r(
            partial(map, attrgetter("pl_name", "func_name")),
            partial(f_star_call, zip),
        )(input_var_specs)
        #_ = list(map(logger.info, input_var_names))
        #_ = list(map(logger.info, kwargs.keys()))

        data = f_compose_r(
            partial(map, partial(getitem, kwargs)),
            partial(zip, input_arg_names),
            dict,
        )(input_arg_names)
        #_ = list(map(lambda x: logger.info("input redis| {}".format(x)), data.items()))

        f_send_msg = f_compose_r(
            partial(PLNodeSessDataSpec, sess_id),
            pl_var_encoder,
            partial(self.redis_cli.publish, queue_id),
        )
        f = partial(f_send_msg, data)

        if DARWIN_CONFIG.debug_performance:
            f = partial(f_timeit_call, f)
        r = f()
        if DARWIN_CONFIG.debug_performance:
            r, dur = r
            logger.critical("perf: {}, send call req to inst {} with data sess .{} in {:.6f}s".format(
                self.log_prefix, self.instances[_target_instance].log_prefix_short,
                sess_id[puas], dur,
            ))

        # prepare output variable objects
        output_vars = self.generate_output_vars(sess_id, timeout=_timeout)

        # prepare function of retrieving response
        # * per instance function
        f_get_var_val = attrgetter("value")
        if DARWIN_CONFIG.debug_performance:
            f_get_var_val = partial(f_timeit_call, f_get_var_val)

        # * overall function: get value from redis with parallel_t
        f_get_vars_val = f_compose_r(
            partial(parallel_map_t, f_get_var_val),
            tuple,
        )
        if DARWIN_CONFIG.debug_performance:
            f_get_vars_val = partial(f_timeit_call, f_get_vars_val)

        # get it now.
        values = f_get_vars_val(output_vars)

        # analysis performance info
        if DARWIN_CONFIG.debug_performance:
            values, dur_overall = values
            logger.critical("perf: {}, get resp from inst {} with data sess .{} in {:.6f}s".format(
                self.log_prefix, self.instances[_target_instance].log_prefix_short,
                sess_id[puas], dur_overall,
            ))
            values, durs_per_var = list(zip(*values))
            # show more detailed perf log per each variable
            if DARWIN_CONFIG.verbose_level > 1:
                for output_var, dur_per_var in zip(output_vars, durs_per_var):
                    logger.critical("perf: {}, get resp of var \"{}\"/.{} from inst {} with data sess .{} in {:.6f}s".format(
                        self.log_prefix,
                        output_var.name, output_var.uuid[puas],
                        self.instances[_target_instance].log_prefix_short,
                        sess_id[puas], dur_per_var,
                    ))

        # re-raise the exceptions from remote which are not targetted to be catched, if there was
        exception_values = f_compose_r(
            partial(filter, f_compose_r(
                lambda _: isinstance(_, ExceptCatchRunResultErrorMsg),
            )),
            list,
        )(values)

        # find out un-catched exceptions
        uncatched_exception_values = f_compose_r(
            partial(filter, f_compose_r(
                attrgetter("exception"),
                lambda exc: partial(map, f_compose_r(
                    partial(isinstance, exc),
                ))(_catched_exceptions),
                any,
                not_,
            )),
            list,
        )(exception_values)

        # raise un-catched exceptions to abort.
        if len(uncatched_exception_values) > 0:
            raise uncatched_exception_values[0].exception from None

        # or, pass through the catched exception gracefully to caller as the result
        # even there might be lots of catched exceptions from different output variables, pick one of them
        # as the output is sufficient.
        if len(exception_values) > 0:
            err_msg = exception_values[0]
            """
            logger.warning("--------------------------------------------------------------")
            logger.warning("First eyewitness of warm node invoking errors from remote:")
            for item, msg in zip(["name", "spec"], [self.name, self.spec]):
                logger.warning(">> {}: {}".format(item, msg))
            for item, msg in zip(
                    ["exc", "tb"],
                    [str(err_msg.exception), err_msg.traceback]):
                for i, line in enumerate(msg.splitlines()):
                    logger.warning(">> {}[{}]: {}".format(item, i, line))
            logger.warning("--------------------------------------------------------------")
            # TODO: how to stage traceback?
            raise err_msg.exception from None
            """
            values = err_msg

        # squeeze normal output
        elif len(output_vars) == 1:
            values = values[0]

        if DARWIN_CONFIG.debug_performance:
            time_end = time.time()
            logger.critical("perf: {}, call to inst {} in {:.6f}s at {:.6f}".format(
                self.log_prefix,
                self.instances[_target_instance].log_prefix_short,
                time_end-time_beg,
                time_end,
            ))
        return values

    def call_from_rpc(self, *args, _sub_channel=None, _catched_exceptions=[], _target_instance=None, _timeout=None, **kwargs):
        """
        warm node client stub run
        :param args:
        :param kwargs:
        :return:
        """
        if DARWIN_CONFIG.debug_performance:
            time_beg = time.time()

        # get warm node services' queue id lists
        # TODO: assume all instances are "active" and available for call. Change to specific "active" instance later.
        active_instance_ids = list(range(len(self.queue_ids)))

        # calculate the target instance if requested by caller
        _target_instance = self.parse_instances(_target_instance)

        # TODO: we might close this backdoor to end user. but admin can still use data channel to request stop
        # request close warm node service by sending "stop" signal msg
        # for multi-instance warm node service, send signal to the group
        if len(args) > 0 and isinstance(args[0], StopIteration):
            def _stop_queue(id_, msg):
                pl_inst = self.instances[id_]
                logger.debug("Request stopping {} by \"{}\" from data channel".format(
                    pl_inst.log_prefix, repr(msg),
                ))
                return pl_inst.stop(msg)

            if _target_instance is None:
                _target_instances = active_instance_ids
            elif isinstance(_target_instance, (list, tuple)):
                _target_instances = _target_instance
            else:
                _target_instances = [_target_instance]

            data = args[0]
            r = f_compose_r(
                # TODO: parallel stop??
                partial(map, partial(f_flip_call, _stop_queue, data)),
                list,
            )(_target_instances)
            return r

        if _target_instance is None:
            # TODO: randomly pick a warm node service's to balance workload
            _target_instance = random.choice(active_instance_ids)

        pl_inst = self.instances[_target_instance]

        # TODO: Request all instances to be active.
        # TODO: same logic as "call_from_redis", needs to optimize
        if _target_instance not in active_instance_ids:
            if DARWIN_CONFIG["pipeline_task_auto_start_warm_nodes"] is True:
                logger.info("{}: request to be started immediately".format(
                    pl_inst.log_prefix,
                ))
                pl_inst.start()
            else:
                # Usually, we only call an instance in active_instances list.
                # but, it would be possible that its hb data was expired but server was busy and not have
                # chance to update hb in the hb_interval. so, instead of enforcing starting or fail the call,
                # we gracefully wait another hb_interval for the updated hb, or, we fail with timeout.
                #
                # NOTE: recommend tuning your pl parameters, such as hb_interval for your workload
                #
                logger.warning("{}: wait an extra hb_interval for its updated heartbeat. {}".format(
                    pl_inst.log_prefix,
                    "recommend tuning or increasing the hb_interval!",
                ))
                f = partial(
                    pl_inst.wait_for_state,
                    PipeNodeWarmInstance.STATE.started,
                    sleep_interval=0.01,
                    timeout=self.hb_interval,
                )
                if DARWIN_CONFIG.debug_performance:
                    f = partial(f_timeit_call, f)
                r = f()
                if DARWIN_CONFIG.debug_performance:
                    r, dur = r
                    logger.critical("perf: {}, wait for its ready spends extra {:.6f}s".format(
                        pl_inst.log_prefix, dur,
                    ))

            active_instance_ids.append(_target_instance)

        if _sub_channel is None:
            _sub_channel = DARWIN_CONFIG["pipeline_warm_node_call_default_channel"]

        if _sub_channel == "zrpc":
            f_rpc_stub_call = pl_inst.zrpc_stub_call
        elif _sub_channel == "grpc":
            f_rpc_stub_call = pl_inst.grpc_stub_call
        else:
            raise TypeError("Unsupported rpc sub-channel \"{}\"".format(_sub_channel))

        values = f_rpc_stub_call(*args, _timeout=_timeout, **kwargs)
        values = tuple(map(pl_var_translate_none, values))

        # squeeze normal output
        if len(self.output_var_names) == 1:
            values = values[0]

        if DARWIN_CONFIG.debug_performance:
            time_end = time.time()
            logger.critical("perf: {}, call to inst {} in {:.6f}s at {:.6f}".format(
                self.log_prefix,
                self.instances[_target_instance].log_prefix_short,
                time_end-time_beg,
                time_end,
            ))
        return values

    call_from_grpc = partialmethod(call_from_rpc, _sub_channel="grpc")
    call_from_zrpc = partialmethod(call_from_rpc, _sub_channel="zrpc")
