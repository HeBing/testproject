#! /usr/bin/env python3
# -*- coding:utf-8 -*-


import logging
import collections
from typing import (
    Any,
    List,
    Mapping,
    Union,
)
from operator import (
    add,
    attrgetter,
    concat,
    contains,
    delitem,
    eq,
    getitem,
    itemgetter,
    not_,
)
from functools import (partial, reduce)
from itertools import (compress)
import time
from queue import (
    Queue,
    Empty,
)
from threading import (
    Event,
    Thread,
)
from darwinutils.config import DARWIN_CONFIG
from darwinutils.fn import (
    f_compose_r,
    f_list_rize,
    f_si_call,
    f_star_call,
    f_timeit_call,
    f_zip,
)
from darwinutils.helper import (
    get_current_function_name,
    f_exception_catch_run,
)
from darwinutils.log import get_task_logger
from darwinutils.graph import (
    Graph,
    ExceptCatchRunResultErrorMsg,
    TraverseHookSpec,
    TraverseEngineSpec,
)
from darwinutils.mapreduce import (
    parallel_map_t,
    parallel_map_p,
)
from pipeline.pipeline import PipeLine
from pipeline.pipenode import (
    f_get_pl_node_input_var_names,
    f_get_pl_node_output_var_names,
    f_get_var_pl_name_from_spec_str,
    PayloadEvent,
    PipeNode,
)
from pipeline.pipevariable import (
    f_get_pl_variable_spec_from_str,
    PipeVariableBase,
)
from pipeline.common import (
    PLCombinedInOutVariablesNodeSpec,
)


MIN_TIMEOUT = 1e-5
logger = get_task_logger(__name__)
_f_get_f_get_pl_graph_xxx_var_names = lambda f: f_compose_r(
    partial(
        map,
        f_compose_r(
            attrgetter('obj'),
            f,
        ),
    ),
    partial(reduce, add),
    set,
    sorted,
)
f_get_pl_graph_input_var_names, f_get_pl_graph_output_var_names = map(
    _f_get_f_get_pl_graph_xxx_var_names, [
        f_get_pl_node_input_var_names,
        f_get_pl_node_output_var_names,
    ],
)
f_set_pl_vars_value = f_compose_r(
    partial(map, lambda item: setattr(item[0], "value", item[1])),
    #partial(parallel_map_t, f_compose_r(
    #    f_zip(
    #        f_compose_r(
    #            itemgetter(0),
    #            partial(partial, setattr),
    #        ),
    #        f_compose_r(
    #            itemgetter(1),
    #            f_list_rize,
    #            partial(concat, ["value"]),
    #        ),
    #    ),
    #    partial(f_star_call, f_star_call),
    #)),
    list
)


def workload_hook(traverse_node, warm_node_catched_exceptions=[]):
    adj_index, depth, node = traverse_node
    pl_node, input_vars, output_vars = node

    if DARWIN_CONFIG.debug_performance:
        time_beg = time.time()

    logger.debug("workload hook id={}, depth={}".format(adj_index, depth))

    # {"<pl_in_var1_name>": pl_in_var1_obj, ...}, {"<pl_out_var1_name>": pl_out_var1_obj, ...}
    # this will help re-order the variables to align with the order in input sequence
    input_vars_dict, output_vars_dict = f_compose_r(
        partial(map, f_compose_r(
            partial(map, lambda var: (attrgetter("name")(var), var)),
            dict,
        )),
    )([input_vars, output_vars])
    #_ = list(map(logger.info, input_vars_dict.items()))
    #_ = list(map(logger.info, output_vars_dict.items()))

    # [pl_var1_spec_obj, ...]
    input_var_specs = f_compose_r(
        attrgetter("inputs"),
        partial(map, f_get_pl_variable_spec_from_str),
    )(pl_node)

    # ["<pl_in_var1_name", ...], ["pl_in_arg1_name",...]
    input_var_names, input_arg_names = f_compose_r(
        partial(map, attrgetter("pl_name", "func_name")),
        lambda _: zip(*_),
    )(input_var_specs)

    # reorder the input pl variables with the order in its input definition, though it might be same.
    input_vars_reordered = f_compose_r(
        partial(map, partial(getitem, input_vars_dict)),
        list,
    )(input_var_names)

    # the input variables' value with same order in its input definition
    input_arg_values = f_compose_r(
        partial(map, attrgetter("value")),
        list,
    )(input_vars_reordered)

    kwargs = dict(zip(input_arg_names, input_arg_values))

    # append catched_exceptions kwargs to warm node
    if pl_node.type == PipeNode.TYPE.warm.name:
        kwargs['catched_exceptions'] = warm_node_catched_exceptions

    #_ = list(map(logger.debug, kwargs.items()))

    # -------------------------------------------
    #                                    --+
    #                @      &       _ _ /
    # OPEN FIRE! //###########=====+ + + + + +
    #           //  iƸ     \\\
    #          //           \\\
    # -------------------------------------------
    # TODO: inject partial args from pl definition
    output_arg_values = pl_node(**kwargs)

    # TODO: warm start might require sync result in worker tasks???
    """
    # the output var names which we expect to return for this firing
    output_var_names = f_compose_r(
        attrgetter("outputs"),
        partial(map, f_get_pl_variable_spec_from_str),
        partial(map, attrgetter("pl_name")),
        list,
    )(pl_node)
    if len(output_var_names) == 1:
        output_arg_values = [output_arg_values]

    # reorder the output pl variables with the order in its output definition, though it might be same.
    output_vars_reordered = f_compose_r(
        partial(map, partial(getitem, output_vars_dict)),
        list,
    )(output_var_names)

    # set the output value back to pl vars
    _ = f_set_pl_vars_value(zip(output_vars_reordered, output_arg_values))

    # TODO: debug only and not mandatory to return any values to traverse caller.
    return list(zip(output_var_names, map(lambda val: type(val).__name__, output_arg_values)))
    """
    if DARWIN_CONFIG.debug_performance:
        time_end = time.time()
        logger.critical("perf: {}, traverse workload hook for node {} at depth {} spends {:.6f}s".format(
            pl_node.log_prefix,
            adj_index, depth,
            time_end-time_beg,
        ))
    return output_arg_values


def post_hook(traverse_node, result):
    adj_index, depth, node = traverse_node
    pl_node, input_vars, output_vars = node

    if DARWIN_CONFIG.debug_performance:
        time_beg = time.time()

    logger.debug("post hook id={}, depth={}".format(adj_index, depth))
    if isinstance(result, ExceptCatchRunResultErrorMsg):
        logger.debug("error post in")
        return result

    # alias for convenience
    output_arg_values = result

    # the output var names which we expect to return for this firing
    output_var_names = f_compose_r(
        attrgetter("outputs"),
        partial(map, f_get_pl_variable_spec_from_str),
        partial(map, attrgetter("pl_name")),
        list,
    )(pl_node)
    #_ = list(map(logger.debug, output_var_names))

    if len(output_var_names) == 1:
        output_arg_values = [output_arg_values]

    # {"<pl_out_var1_name>": pl_out_var1_obj, ...}
    output_vars_dict = f_compose_r(
        partial(map, lambda var: (attrgetter("name")(var), var)),
        dict,
    )(output_vars)
    #_ = list(map(logger.debug, output_vars_dict.items()))

    # reorder the output pl variables with the order in its output definition, though it might be same.
    output_vars_reordered = f_compose_r(
        partial(map, partial(getitem, output_vars_dict)),
        list,
    )(output_var_names)

    # set the output value back to pl vars
    _ = f_set_pl_vars_value(zip(output_vars_reordered, output_arg_values))

    if DARWIN_CONFIG.debug_performance:
        time_end = time.time()
        logger.critical("perf: {}, traverse post hook for node {} at depth {} spends {:.6f}s".format(
            pl_node.log_prefix,
            adj_index, depth,
            time_end-time_beg,
        ))
    # TODO: debug only and not mandatory to return any values to traverse caller.
    return list(zip(output_var_names, map(lambda val: type(val).__name__, output_arg_values)))


class PipeTask(collections.Callable):
    def __init__(self, pl):
        # derived attributes need to be initialized as None
        self._pl_vars = None
        self._signature = None
        self._graph = None

        if not isinstance(pl, PipeLine):
            raise ValueError('pl must be a PipeLine object instead of {}'.format(type(pl).__name__))
        self._pl = pl

        # trigger calculating derived attributes immediately
        _ = self.pl_vars

    def __getstate__(self):
        logger.debug("Pickle dump {}({})".format(type(self).__name__, hex(id(self))))
        state = self.__dict__.copy()

        # remove derived attributes
        del state["_pl_vars"]
        del state["_signature"]
        del state["_graph"]

        return state

    def __setstate__(self, state):
        self.__dict__.update(state)

        # restore unpickle or derived attributes
        self._pl_vars = None
        self._signature = None
        self._graph = None

        logger.debug("Pickle load {}({})".format(type(self).__name__, hex(id(self))))

    @property
    def pl(self):
        return self._pl

    @property
    def pl_vars(self):
        """
        Get(create) pipeline variables
        NOTE: create the variables in the first call.
        :return:
        :rtype: list
        """
        if self._pl_vars is None:
            logger.debug("Create pipetask's variables {} at {}".format(
                type(self).__name__, self.pl.var_names, hex(id(self))))
            self._pl_vars = list(map(PipeVariableBase, self.pl.var_names))
        return self._pl_vars

    @property
    def graph(self):
        """
        Get(create) pipeline graph which customized for task execution engine.
        NOTE: create the graph in the first call.
        :return: a Graph object with combined node object which included pl_node and its graph adj_idx and depth
        :rtype: Graph
        """
        if self._graph is None:
            logger.debug("Create {}'s combined graph at {}".format(type(self).__name__, hex(id(self))))
            adj_matrix = self.pl.graph.adj_matrix

            # extract pl nodes from graph
            pl_nodes = f_compose_r(
                partial(map, attrgetter("obj")),
                list,
            )(self.pl.graph.nodes)

            # map pl nodes' input output var names to real pl var objects
            pl_node_input_vars, pl_node_output_vars = f_compose_r(
                partial(map, attrgetter("inputs", "outputs")),
                partial(map, f_compose_r(
                    partial(map, f_compose_r(
                        f_get_var_pl_name_from_spec_str,
                        partial(map, self._get_pl_var_by_name),
                        list
                    )),
                    list
                )),
                list,
                lambda _: zip(*_),
            )(pl_nodes)

            # log for debug functor
            _f_log = f_compose_r(
                partial(map, f_compose_r(
                    partial(map, f_compose_r(
                        attrgetter("name"),
                    )),
                    list,
                    logger.debug,
                )),
                list,
            )
            #_f_log(pl_node_input_vars)
            #_f_log(pl_node_output_vars)

            pl_combined_nodes = f_compose_r(
                partial(map, lambda _: PLCombinedInOutVariablesNodeSpec(*_)),
                list,
            )(zip(pl_nodes, pl_node_input_vars, pl_node_output_vars))

            graph = Graph(adj_matrix=adj_matrix, nodes=pl_combined_nodes)
            self._graph = graph
        return self._graph

    @property
    def signature(self):
        """
        Get the signature of pipeline callable
        :return:
        :rtype: inspect.Signature
        """
        # TODO: to replace FullArgSpec of node's func spec
        raise NotImplementedError("{} not implemented yet".format(get_current_function_name()))

    def _get_pl_var_by_name(self, name):
        """
        Get pipeline variable(s) per name(s)
        :param name:
        :type name: Union(str, list)
        :return:
        """
        result = f_compose_r(
            partial(filter, f_compose_r(
                attrgetter("name"),
                partial(eq, name),
            )),
            list,
        )(self.pl_vars)
        if len(result) == 0:
            raise KeyError("Pipeline does not have variable with name of {}".format(name))
        return result[0]

    def __call__(
            self,
            *args,
            graph_traverse_executor=None,
            graph_traverse_catched_exceptions=None,
            __auto_start_warm_nodes=None,
            **kwargs,
    ):
        # python functor has problem to accept "__XXX" parameter
        if "__auto_start_warm_nodes" in kwargs:
            __auto_start_warm_nodes = kwargs.pop("__auto_start_warm_nodes")

        logger.debug({"graph_traverse_executor": graph_traverse_executor})
        if graph_traverse_executor is None:
            # TODO: it seems parallel call to warm service will have celery/redis error!
            graph_traverse_executor = TraverseEngineSpec(workload=map, prep=map, post=map)
        if not isinstance(graph_traverse_executor, TraverseEngineSpec) and \
                isinstance(graph_traverse_executor, (list, tuple)):
            graph_traverse_executor = TraverseEngineSpec(*graph_traverse_executor)

        logger.debug({"graph_traverse_catched_exceptions": graph_traverse_catched_exceptions})
        if graph_traverse_catched_exceptions is None:
            graph_traverse_catched_exceptions = [Exception]

        if __auto_start_warm_nodes is None:
            auto_start_warm_nodes = DARWIN_CONFIG["pipeline_task_auto_start_warm_nodes"]
        else:
            auto_start_warm_nodes = __auto_start_warm_nodes

        if auto_start_warm_nodes is True:
            f = self.pl.start_warm_nodes
            if DARWIN_CONFIG.debug_performance:
                f = partial(f_timeit_call, f)
            r = f()
            if DARWIN_CONFIG.debug_performance:
                r, dur = r
                # lower level pl inst will show the starting dur, let's bump the verbose level
                # so that we only show this up-level dur when necessary
                if DARWIN_CONFIG.verbose_level > 1:
                    logger.critical("perf: {}, start warm nodes in {:.6f}s".format(
                        self.pl.log_prefix, dur,
                    ))
            if len(kwargs.keys()) == 0:
                logger.debug("{}: internally start all warm nodes")
                return r

        root_input_var_names = self.pl.input_var_names
        #_ = list(map(logger.debug, root_input_var_names))

        root_input_vars = f_compose_r(
            partial(map, self._get_pl_var_by_name),
            list,
        )(root_input_var_names)
        #_ = list(map(logger.debug, root_input_vars))

        root_input_var_values = f_compose_r(
            partial(map, kwargs.get),
            list,
        )(root_input_var_names)
        #_ = list(map(logger.debug, zip(root_input_var_names,
        #                               map(lambda val: type(val).__name__, root_input_var_values))))

        # feed the values to input variables
        _ = f_set_pl_vars_value(zip(root_input_vars, root_input_var_values))

        # ENGINE START!
        # NOTE:
        # - all cold node will have exceptions catched in generic graph traverse with "graph_traverse_catched_exception"
        # - warm node has exception raised in remote. local stub can only get a wrapped error msg. we pass the
        #   graph_traverse_catched_exceptions to make its behavior being similar as cold by passing the catched exceptions
        #   list to warm node's caller.
        f_traverse = partial(
            self.graph.traverse,
            hook=TraverseHookSpec(
                workload=partial(
                    workload_hook,
                    warm_node_catched_exceptions=graph_traverse_catched_exceptions,
                ),
                prep=None,
                post=post_hook,
            ),
            executor=graph_traverse_executor,
            catch_exceptions=graph_traverse_catched_exceptions,
        )
        if DARWIN_CONFIG.debug_performance:
            f_traverse = partial(f_timeit_call, f_traverse)
        result = f_traverse()
        if DARWIN_CONFIG.debug_performance:
            result, dur = result
            logger.critical("perf: {}, task traverse spends {:.6f}s".format(
                self.pl.log_prefix, dur,
            ))
        #_ = list(map(logger.error, result))
        succ, results = zip(*result)
        if not all(succ):
            error_results = f_compose_r(
                partial(map, lambda _: _ is False),
                partial(compress, enumerate(result)),
                list,
            )(succ)
            #_ = list(map(logger.error, error_results))

            # raise any one of catched exceptions, all errors must be happened in same depth.
            exception_results = f_compose_r(
                partial(filter,
                        lambda item: item[1].success is False and isinstance(item[1].result.exception, Exception)),
                list,
            )(error_results)
            if len(exception_results) > 0:
                result = exception_results[0][1].result

                # log for extra debug which is duplicate with graph.traverse's exc handling
                """
                for item, msg in zip(["exc", "tb"], [str(result.exception), result.traceback]):
                    for i, line in enumerate(msg.splitlines()):
                        logger.error(">> {}[{}]: {}".format(item, i, line))
                """
                raise result.exception from None
            else:
                raise ValueError("Why there are unsuccessful hook run but not exceptional breaked???!!!")

        """
        output_kwargs = f_compose_r(
            f_get_pl_graph_output_var_names,
            f_group(
                f_echo,
                f_compose_r(
                    # TODO: replace with parallel_map_t for parallel
                    partial(map, f_compose_r(
                        self._get_pl_var_by_name,
                        attrgetter("value"),
                    )),
                    list,
                ),
            ),
            partial(f_star_call, zip),
            dict,
        )(self.pl.graph.leaves)
        """

        leaf_output_var_names = f_get_pl_graph_output_var_names(self.pl.graph.leaves)
        #_ = list(map(logger.debug, leaf_output_var_names))

        leaf_output_vars = f_compose_r(
            partial(map, self._get_pl_var_by_name),
            list,
        )(leaf_output_var_names)
        #_ = list(map(logger.debug, leaf_output_vars))

        leaf_output_var_values = f_compose_r(
            partial(map, attrgetter("value")),
            list,
        )(leaf_output_vars)
        output_kwargs = dict(zip(leaf_output_var_names, leaf_output_var_values))
        #_ = list(map(lambda item: logger.debug((item[0], type(item[1]).__name__)), output_kwargs.items()))

        return output_kwargs


class PipeTaskRetry(PipeTask):
    """
    PipeTask which supports retry
    """
    def _timeout_call(
            self,
            *args,
            _timeout: float = None,
            _level: int = None,
            _beg: float = None,
            _q_result: Queue = None,
            **kwargs,
    ):
        """
        Call with an overall task timeout
        :param args: staged args for pipetask
        :param _timeout:
        :param _level: internal variable for recursive call tracking
        :param _beg: internal variable for recursive call tracking
        :param _q_result: internal variable for recursive call tracking
        :param kwargs: staged kwargs for pipetask
        :return:
        """
        if _beg is None:
            _beg = time.time()
        if _q_result is None:
            _q_result = Queue(maxsize=1)
        if _level is None:
            _level = 0

        f_work = partial(super().__call__, *args, **kwargs)

        # python cannot trace 1e-4 accurately, so. I assume no timeout at all.
        if isinstance(_timeout, (float, int)):
            succ = _timeout >= MIN_TIMEOUT
        elif _timeout is None:
            succ = True
        else:
            raise TypeError("_timeout should be int or float instead of {}".format(_timeout))

        # valid input _timeout
        if succ:
            e_start = Event()

            f_pre = e_start.set
            f_post = f_compose_r(
                f_list_rize,
                partial(concat, [_level]),
                partial(_q_result.put, block=False),
            )
            f = f_compose_r(
                partial(f_si_call, f_pre),
                partial(f_si_call, f_work),
                f_post,
            )
            t = Thread(target=f)

            t.start()

            if _timeout is None:
                __timeout = _timeout
            else:
                delta = time.time() - _beg
                __timeout = _timeout - delta
                logger.debug("{}, call e_start with _timeout {:.6f}s".format(
                    self.pl.log_prefix, __timeout,
                ))
                succ = __timeout >= MIN_TIMEOUT
            if succ:
                succ = e_start.wait(__timeout)

        # pipeline started successfully
        if succ:
            if _timeout is None:
                __timeout = _timeout
            else:
                delta = time.time() - _beg
                __timeout = _timeout - delta
                logger.debug("{}, call e_finish with _timeout {:.6f}s".format(
                    self.pl.log_prefix, __timeout,
                ))
                succ = __timeout >= MIN_TIMEOUT
            if succ:
                f = partial(_q_result.get, block=True, timeout=__timeout)
                f = partial(f_exception_catch_run, f, catch_exceptions=[Empty])
                succ, r = f()

        # pipeline got succ result
        delta = time.time() - _beg
        if succ:
            _q_result.task_done()
            level_from, r = r
            logger.log(
                logging.INFO if level_from != 1 or _level != 1 else logging.DEBUG,
                "{}, task succ after {}/{}/{} retry in {:.6f}s since {}".format(
                    self.pl.log_prefix, level_from, _level, _q_result.maxsize,
                    delta, _beg,
                ),
            )
        else:
            # TODO: may deduce the wasted time into the real task execution timeout?
            # TODO: if the retry_timeout would be smaller than the task's real timeout.
            # _retry_timeout = retry_timeout - delta
            err_msg = "{}, task {} try timeout after wasting {:.6f}s since {}".format(
                self.pl.log_prefix, _level, delta, _beg,
            )
            logger.warning(err_msg)
            raise TimeoutError(err_msg)

        # TODO: how to houseclean the thread for 1st task?
        return r

    def __call__(
            self,
            *args,
            _timeout: Union[None, float, List[Union[float, None]]] = None,
            **kwargs,
    ) -> Mapping[str, Any]:
        """
        Call with recursive retry timeouts
        :param args: args for pipetask
        :param _timeout: retry timeout chains
        :param kwargs: kwargs for pipetask
        :return:
        """
        # There are three internal arguments which don't want to be shared by external caller.
        # These internal arguments also make this __call__ and its dependent _timeout_call function be stateless
        #
        # :param _level: internal variable for recursive call tracking
        # :param _beg: internal variable for recursive call tracking
        # :param _q_result: internal variable for recursive call tracking
        #
        # extract internal parameter, "_beg", first to make sure every ticks were counted!
        _beg = kwargs.pop("_beg", None)
        if _beg is None:
            _beg = time.time()

        # extract other internal parameters
        _q_result = kwargs.pop("_q_result", None)
        _level = kwargs.pop("_level", None)

        # validate parameter
        if _timeout is None:
            _timeout = DARWIN_CONFIG["pipeline_task_default_retry_timeout"]

        # _timeout syntax:
        # -----
        # scala
        # 1.  n := [n, None] -> goto 3).
        # 2.  None := super()
        # -----
        # array
        # 3.  [..., x] := self([...]) + timeout_call(x)
        # 4.  [n] := timeout_call(n)
        #
        if isinstance(_timeout, (int, float)):
            _timeout = [_timeout, None]

        elif _timeout is None:
            return super().__call__(*args, **kwargs)

        elif isinstance(_timeout, (list, tuple)):
            pass

        else:
            raise TypeError("_timeout is not valid {}".format(_timeout))

        # split the timeout list
        *_timeout, _timeout_retry = _timeout
        n_timeout_count = len(_timeout)

        if _level is None:
            _level = n_timeout_count + 1
        if _q_result is None:
            _q_result = Queue(maxsize=_level)

        # dispatch the 1st round task
        succ = has_valid_1st_round = n_timeout_count > 0
        if has_valid_1st_round:
            f = partial(
                self.__call__,
                *args,
                _timeout=_timeout,
                _level=_level-1,
                _beg=_beg,
                _q_result=_q_result,
                **kwargs,
            )
            f = partial(
                f_exception_catch_run,
                f,
                catch_exceptions=[TimeoutError],
                log_func=None,
            )
            succ, r = f()

        # dispatch the 2nd retry task, if 1st was timeout
        if not succ:
            delta = time.time() - _beg
            logger.debug("{}, {} depth call with timeout {} after wasting {:.6f}s since {}".format(
                self.pl.log_prefix, n_timeout_count, _timeout_retry, delta, _beg,
            ))
            f = partial(
                self._timeout_call,
                *args,
                _timeout=_timeout_retry,
                _level=_level,
                _beg=_beg,
                _q_result=_q_result,
                **kwargs,
            )
            r = f()
        return r
