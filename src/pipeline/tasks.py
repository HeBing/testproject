#! /usr/bin/env python3
# -*- coding:utf-8 -*-


from functools import (
    partial,
    reduce,
)
from operator import (
    attrgetter,
    methodcaller,
    mul,
    concat,
    getitem,
)
from darwinutils.app import get_celery_app
from darwinutils.log import get_task_logger
from darwinutils.fn import (
    f_compose_r,
    f_group,
    f_list_rize,
    irevert,
    f_star_call,
)


logger = get_task_logger(__name__)
celery = get_celery_app()


_log_pl_warm_node_instance = f_compose_r(
    f_group(
        f_compose_r(
            attrgetter("pl_node.spec"),
            methodcaller("items"),
            list,
        ),
        f_compose_r(
            attrgetter("pl_node.pl_uuid"),
            f_list_rize,
            partial(zip, ["pl_uuid"]),
            list,
        ),
        f_compose_r(
            f_list_rize,
            partial(mul, 2),
            partial(zip, ["uuid", "instance_id"]),
            partial(map, f_compose_r(
                irevert,
                partial(f_star_call, getattr),
            )),
            partial(zip, ["uuid", "instance_id"]),
            list,
        ),
    ),
    partial(reduce, concat),
    partial(map, f_compose_r(
        ">> {}".format,
        logger.info,
    )),
    list,
)


@celery.task(bind=True)
def save_data(self, data):
    exp_id, payload = data
    assert exp_id==self.request.id, "save_data got inconsistent payload exp={} vs. real={}".format(exp_id, self.request.id)

    logger.info("Save payload as {}".format(self.request.id))
    return payload


@celery.task(ignore_result=True)
def run_pl_warm_node_with_gpu(pl_node_instance, num_gpus=1):
    from darwinutils.helper import gpu_balance
    balanced_gpu_uuids, _ = gpu_balance(num_gpus=num_gpus)

    # log for debug
    logger.info("Reserve GPU {} to run following pipeline warm node:".format(balanced_gpu_uuids))
    _log_pl_warm_node_instance(pl_node_instance)
    pl_node_instance.run_in_thread().join()


@celery.task(ignore_result=True)
def run_pl_warm_node_with_cpu(pl_node_instance, num_cpus=1):
    from darwinutils.helper import cpu_balance
    balanced_cpu_uuids, _ = cpu_balance(num_cpus=num_cpus)

    # log for debug
    logger.info("Reserve CPU {} to run following pipeline warm node:".format(balanced_cpu_uuids))
    _log_pl_warm_node_instance(pl_node_instance)
    pl_node_instance.run_in_thread().join()
