#! /usr/bin/env python3
# -*- coding:utf-8 -*-


from collections import namedtuple


"""pl_uuid_abbr_slice"""
puas = slice(None, 6)

# NOTE: "as_" is not absolutely functional necessary. it might be used as the PipeNode name if not set.
PLFuncModuleSpec = namedtuple("PLFuncModuleSpec", ["from_", "import_", "as_"])
PLNodeSessDataSpec = namedtuple("PLNodeSessDataSpec", ["sess_id", "data"])
PLCacheSpec = namedtuple("PLCacheSpec", ["enqueue_time", "payload"])

PLCombinedInOutVariablesNodeSpec = namedtuple(
    "PLCombinedInOutVariablesNodeSpec",
    ["pl_node", "input_vars", "output_vars"])

PLVariableSpec = namedtuple("PLVariableSpec", ["pl_name", "func_name"])
PLCtrlCmdSpec = namedtuple("PLCtrlCmdSpec", ["name", "payload"])
"""
PLNodeInstBeaconSpec = namedtuple("PLNodeInstStatusSpec", [
    # id of beacon owner
    "token",
    # PipeLineWarmInstance.STATE
    "state",
    # tracking payload for internal use
    "_track",
])
"""