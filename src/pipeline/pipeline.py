#! /usr/bin/env python3
# -*- coding:utf-8 -*-


import math
import time
import uuid
from itertools import (
    compress,
    chain,
    groupby,
    starmap,
)
from operator import (
    getitem,
    attrgetter,
    itemgetter,
    concat,
    setitem,
    add,
    eq,
    contains,
    not_,
    methodcaller,
)
from functools import (reduce, partial)
import numpy as np
from darwinutils.fn import (
    f_compose_r,
    irevert,
    f_echo,
    f_star_call,
    f_star2_call,
)
from darwinutils.config import DARWIN_CONFIG
from darwinutils.log import get_task_logger
from darwinutils.graph import (Graph)
from pipeline.common import (
    puas,
)
from pipeline.pipenode import (
    PipeNode,
    PipeNodeWarm,
    PipeNodeWarmRedis,
)
from pipeline.pipevariable import (
    f_get_pl_variable_spec_from_str,
)


"""
1. define the pl
2. statically load pl

3. boot warm nodes: pl.{start, stop}()
                    ^^^^^^^^^^^^^^^^
3.1 for each warm node: pl_warm_node.run(pl)
                        ^^^^^^^^^^^^^^^^^^^^

3.1.1 prepare warm node's input variables - 
* registered node as a consumer and only consumer of
  a remote queue which associated with this warm node in its belonging pipeline domain.
  this node's unique fingerprint(input vars' spec+args?) + belonging pipeline's fingerprint(uuid).

  queue_id = f_queue_id(node_uuid, pl_uuid)
             ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

3.2 run as service per each warm node
3.2.1 fork a new thread/process
3.2.1.1 initialize a generator to represent as a consumer of the registered input remote queue.
3.2.1.2 iterate remote generator for a set of input data, which might have shape of [n_session_keys, batch, n_in_vars]

3.2.1.3 initialize a "cache" likely local generator.
3.2.1.4 cache input data from remote queue into local queue
3.2.1.5 local cache based generator picks only [batch, n_in_vars] from local queue and produce as the generator output

3.2.1.6 pass the local generator as the input argument to a proxy function
3.2.1.7 proxy function calls the workload function and iterate its output. for example:
    for out_var1{, out_var2} in workload_func(in_generator):
    ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

3.2.1.8 combine all output variables' values, [batch, n_out_vars]
3.2.1.9 set combined output variables to the cache queue.
3.2.1.6 save the output variables to backend with its key = f_output_id(session key, queue_id)
        save_warm_node_result_task.async((output_var1_value{, output_var2_value}), task_id=session_key)
        ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^


3.3 run stub client: out_val1{, out_val2} = pl_node(in_arg1, in_arg2) same interface
                     ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
3.3.1 generate a session key (that one per each warm node)
3.3.2 wrap the session key with input data as a data chunk
3.3.3 send the data chunk to registered remote queue for the node in that pipeline
3.3.4 check if there is output with output_id as the key
        AsyncResult(id=session_key).get()
        ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^


4. code start pl
4.1. if indirect input, get it.
4.2. if input has queued target/warm start, push to it and get result asynchronously, or
4.3. if input is for code start, run func and get result synchronously.
4.4. leave result in celery as is

5. stop warn nodes.
5.1. push "stop" data into queue, or
5.2. revoke celery task enforce.
"""

logger = get_task_logger(__name__)


def get_started_warm_nodes(pl_warm_nodes, logical_op=f_echo):
    from celery.states import STARTED

    free_celery_task_ids = f_compose_r(
        partial(map, f_compose_r(
            methodcaller("get_free_celery_task_id"),
        )),
        list
    )(pl_warm_nodes)
    logger.debug(free_celery_task_ids)

    mask = f_compose_r(
        partial(map, f_compose_r(
            itemgetter(1),
            lambda _: _ in [STARTED],
            logical_op,
        )),
        list,
    )(free_celery_task_ids)
    logger.debug(mask)

    r = f_compose_r(
        partial(f_star_call, zip),
        lambda _: compress(_, mask),
        lambda _: zip(*_),
        list,
    )([pl_warm_nodes, free_celery_task_ids])
    logger.debug(r)
    return r


get_nonstarted_warm_nodes = partial(get_started_warm_nodes, logical_op=not_)


class PipeLine:
    def __init__(self, pl_nodes, engine=None):
        self._var_names = None
        self._input_var_names = None
        self._output_var_names = None
        self._graph = None
        self._engine = engine

        # derived attributes as cache only
        self._uuid = None
        self._c_log_prefix = None

        # TODO: only used to generate uuid, remove in future
        self._pl_nodes = pl_nodes
        self.graph = pl_nodes

    def __getstate__(self):
        logger.debug("Pickle dump {}({})".format(type(self).__name__, hex(id(self))))
        state = self.__dict__.copy()
        del state['_uuid']
        del state["_c_log_prefix"]
        return state

    def __setstate__(self, state):
        self.__dict__.update(state)
        self._uuid = None
        self._c_log_prefix = None
        logger.debug("Pickle load {}({})".format(type(self).__name__, hex(id(self))))

    @property
    def uuid(self):
        if self._uuid is None:
            self._uuid = str(uuid.uuid3(
                uuid.UUID(DARWIN_CONFIG["pipeline_uuid_namespace"]),
                str(self._pl_nodes),
                # TODO: switch to use normalized "pl_nodes"
                # TODO: needs to resolve self.uuid <--> self.graph loop referrence had been resolved.
                #f_compose_r(
                #    attrgetter("nodes"),
                #    partial(map, f_compose_r(
                #        attrgetter("spec"),
                #    )),
                #    tuple,
                #    repr,
                #)(self),
            ))
        return self._uuid

    @property
    def nodes(self):
        return list(map(attrgetter("obj"), self.graph.nodes))

    @property
    def warm_nodes(self):
        return f_compose_r(
            partial(filter, f_compose_r(
                attrgetter("type"),
                partial(eq, PipeNode.TYPE.warm.name)
            )),
            list,
        )(self.nodes)
    """
    def _add_node(self, node):
        if isinstance(node, dict):
            return PipeNode.create_from_node_spec(node)

        elif isinstance(node, PipeNode):
            return node

        elif isinstance(node, Iterable):
            nodes = list(node)
            node_type_mask = list(map(lambda obj: isinstance(obj, (dict, PipeNode)), nodes))
            if not all(node_type_mask):
                raise ValueError('Added nodes to pipeline should be type of PipeNode while followings are not: {}'.format(
                    list(map(lambda item: (item[0], type(item[1]).__name__), compress(enumerate(nodes), map(not_, node_type_mask))))
                ))
            self._nodes.extend(list(node))
            _ = list(map(self._add_node, nodes))

        else:
            raise ValueError('Added nodes to pipeline should be type of PipeNode instead of {}'.format(
                type(node).__name__,
            ))
    """

    @property
    def var_names(self):
        return self._var_names

    @property
    def input_var_names(self):
        return self._input_var_names

    @property
    def output_var_names(self):
        return self._output_var_names

    @property
    def graph(self):
        return self._graph

    @graph.setter
    def graph(self, value):
        if isinstance(value, Graph):
            self._graph = value

        elif isinstance(value, (list, tuple)):
            def create_pl_node_smartly(pl_node):
                if isinstance(pl_node, dict):
                    routes = {
                        PipeNode.TYPE.cold.name: PipeNode.create_from_node_spec,
                        PipeNode.TYPE.warm.name: partial(PipeNodeWarmRedis.create_from_node_spec, pl_uuid=self.uuid),
                    }
                    return routes[pl_node["type"]](pl_node)
                elif isinstance(pl_node, PipeNode):
                    return pl_node

            # convert to PipeNode
            nodes = f_compose_r(
                partial(map, create_pl_node_smartly),
                list,
            )(value)
            #lambda pl_node: PipeNode.create_from_node_spec(pl_node) if isinstance(pl_node, dict) else pl_node,
            type_err_mask = map(lambda node: not isinstance(node, PipeNode), nodes)
            if any(type_err_mask):
                raise ValueError('pl_nodes in PipeLine constructor should be type of PipeNode or spec dict while followings are not: {}'.format(
                    list(map(lambda item: (item[0], type(item[1]).__name__), compress(enumerate(nodes), type_err_mask)))
                ))
            #_ = list(map(lambda item: logger.debug((item[0], item[1].func_name)), enumerate(nodes)))

            # get PLVariableSpec lists of each nodes' input and output
            node_inputs, node_outputs = map(
                lambda att_name: list(map(
                    lambda var_strs: list(map(
                        f_get_pl_variable_spec_from_str,
                        var_strs if var_strs else [])),
                    map(attrgetter(att_name), nodes)
                )),
                ['inputs', 'outputs'])

            # figure out all pl variables to an ordered list
            node_input_vars, node_output_vars = map(
                lambda var_specs_list: list(map(
                    lambda var_specs: list(map(
                        attrgetter("pl_name"),
                        var_specs,
                    )),
                    var_specs_list)
                ),
                [node_inputs, node_outputs],
            )

            pl_input_var_names = set(reduce(concat, node_input_vars))
            pl_output_var_names = set(reduce(concat, node_output_vars))
            _intermidiate_var_names = pl_input_var_names.intersection(pl_output_var_names)

            pl_input_var_names = pl_input_var_names.difference(_intermidiate_var_names)
            self._input_var_names = list(sorted(pl_input_var_names))
            #_ = list(map(logger.debug, enumerate(pl_input_var_names)))

            pl_output_var_names = pl_output_var_names.difference(_intermidiate_var_names)
            self._output_var_names = list(sorted(pl_output_var_names))
            #_ = list(map(logger.debug, enumerate(pl_output_var_names)))

            # NOTE: order the var names to keep run output stable and debug easy, even though the functionality is same.
            pl_var_names = list(sorted(
                pl_input_var_names
                    .union(pl_output_var_names)
                    .union(_intermidiate_var_names)))
            self._var_names = pl_var_names
            #_ = list(map(logger.debug, enumerate(pl_var_names)))

            # vectoring variable name
            node_input_var_ids, node_output_var_ids = map(
                lambda vars_list: list(map(
                    lambda vars: list(map(
                        pl_var_names.index,
                        vars)
                    ),
                    vars_list)
                ),
                [node_input_vars, node_output_vars],
            )
            #_ = list(map(logger.debug, enumerate(node_input_var_ids)))
            #logger.info("------")
            #_ = list(map(logger.debug, enumerate(node_output_var_ids)))
            #logger.info("------")

            # var pairs, ordered
            var_consuming_pairs, var_providing_pairs = map(
                lambda ids_list: sorted(reduce(add, map(
                    lambda item: list(map(
                        lambda var_id: (item[0], var_id),
                        item[1])
                    ),
                    enumerate(ids_list)),
                    [],
                ), key=itemgetter(1)),
                [node_input_var_ids, node_output_var_ids]
            )
            #_ = list(map(logger.debug, var_consuming_pairs))
            #_ = list(map(logger.debug, var_providing_pairs))

            # var dict
            var_consuming_dict, var_providing_dict = map(
                lambda var_pairs: dict(map(
                    lambda item: (item[0], list(map(itemgetter(0), item[1]))),
                    groupby(var_pairs, key=itemgetter(1)))),
                [var_consuming_pairs, var_providing_pairs]
            )
            #_ = list(map(logger.debug, var_consuming_dict.items()))
            #logger.info("------")
            #_ = list(map(logger.debug, var_providing_dict.items()))
            #logger.info("------")

            # detect and raise error if there is variable who has multiple providers
            var_multiple_providing_pairs = list(filter(
                lambda item: item[1] != 1,
                map(lambda item: (item[0], len(item[1])), var_providing_dict.items())
            ))
            if len(var_multiple_providing_pairs) > 0:
                raise ValueError('There are invalid variables who has multiple provides in the graph. They are {}'.format(
                    list(map(lambda item: ((item[0], pl_var_names[item[0]]), var_providing_dict[item[0]]),
                             var_multiple_providing_pairs))
                ))

            num_nodes = len(nodes)
            adj_matrix = np.empty((num_nodes, num_nodes), dtype=np.float32)
            adj_matrix.fill(np.nan)

            _ = list(map(
                lambda item: list(map(
                    lambda j: setitem(adj_matrix, (item[0], j), 1.0),
                    #lambda j: logger.info("setitem(adj_matrix, ({}, {}), 1.0)".format(item[0], j)),
                    map(
                        lambda var_id: var_consuming_dict[var_id],
                        filter(partial(contains, var_consuming_dict), item[1])
                    )
                )),
                enumerate(node_output_var_ids),
            ))
            #logger.debug(adj_matrix)

            graph = Graph(adj_matrix=adj_matrix, nodes=nodes)
            #_ = graph.traverse(lambda node: logger.debug((node.id, node.depth, node.obj.func_name)))
            self._graph = graph

    @property
    def log_prefix(self):
        if self._c_log_prefix is None:
            self._c_log_prefix = "pl, .{}".format(
                self.log_prefix_short,
            )
        return self._c_log_prefix

    @property
    def log_prefix_short(self):
        return "{}".format(self.uuid[puas])

    def start_warm_nodes(self, *args, **kwargs):
        warm_nodes = self.warm_nodes
        from darwinutils.mapreduce import parallel_map_t
        map_func = parallel_map_t
        return f_compose_r(
            partial(map_func, f_compose_r(
                methodcaller("start", slice(None), *args, **kwargs),
            )),
            list,
        )(warm_nodes)

    def stop_warm_nodes(self, *args, **kwargs):
        warm_nodes = self.warm_nodes
        from darwinutils.mapreduce import parallel_map_t
        map_func = parallel_map_t
        return f_compose_r(
            partial(map_func, f_compose_r(
                methodcaller("stop", slice(None), *args, **kwargs),
            )),
            list,
        )(warm_nodes)
