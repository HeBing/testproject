import json
from urllib import request as urllib2
from datetime import datetime
from datetime import timedelta
import json

year = 2017
month = 1
day = 1
server_url = "http://www.easybots.cn/api/holiday.php?d="

date = datetime.strftime(datetime(year, month, day), "%Y%m%d")

holiday_lst = []
count = 0
while year < 2020:
    vop_url_request = urllib2.Request(server_url + date)
    vop_response = urllib2.urlopen(vop_url_request)
    vop_data = json.loads(vop_response.read())

    if vop_data[date] == '2':
        holiday_lst.append(date)
    count += 1
    date = datetime.strftime(datetime(year, month, day) + timedelta(days=count), "%Y%m%d")
    year = datetime.strptime(date, "%Y%m%d").year

with open("holiday.dat", 'w') as f:
    f.write(json.dumps(holiday_lst, indent=2))
print("Done")
