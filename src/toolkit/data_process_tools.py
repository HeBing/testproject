import os
import shutil
import pandas as pd
from glob import glob
from darwinutils.log import get_task_logger

logger = get_task_logger(__name__)

def copy_files(from_glob, to_folder):
    logger.info('Step1: copy files to customer to workspace')
    logger.info('from_glob = {}   to_folder = {}'.format(from_glob, to_folder))
    files = glob(from_glob)
    logger.info('\tfound files: \n\t{}'.format(files))
    if len(files) == 0:
        logger.error('\tfrom_glob {} is empty!'.format(from_glob))
    if not os.path.exists(to_folder):
        os.makedirs(to_folder)
    for file in files:
        logger.info('\tcopy from {} to {}'.format(file, to_folder))
        shutil.copy2(file, to_folder)
    return files

def merge_dfs_into_one(files, to_folder):
    logger.info('Step2: merge risk dfs to one file')
    df_list = []
    name_list = []
    for file in files:
        logger.info('\treading {}'.format(file))
        if not os.path.basename(file) == 'full_64_df.csv':
            name_list.append(os.path.basename(file).split('_')[1])
            df_list.append(pd.read_csv(file))
    data = pd.concat(df_list, axis=0, ignore_index=True)
    if len(name_list) > 1:
        new_df_file_name = 'full_{}_df.csv'.format('+'.join(name_list))
    elif len(name_list) == 1:
        new_df_file_name = 'full_{}_df.csv'.format(name_list[0])
    else:
        logger.error('No risk data!')
        exit(1)
    new_df_file_name = os.path.join(to_folder, new_df_file_name)
    logger.info('\twriting data to {}'.format(new_df_file_name))
    data.to_csv(new_df_file_name, index=False)
    good_df_data = os.path.join(to_folder, 'full_64_df.csv')
    return good_df_data, new_df_file_name

def pickup_evaluation_ids_file(good_df_data, risk_df_data):
    logger.info('Step3: pick up evaluation only merchant ids')
    cmd1 = 'python pickup_eva_only_merchant.py --data_file {} --ratio 0.1'.format(good_df_data)
    cmd2 = 'python pickup_eva_only_merchant.py --data_file {} --ratio 0.1'.format(risk_df_data)
    logger.info('\t{}'.format(cmd1))
    os.system(cmd1)
    logger.info('\t{}'.format(cmd2))
    os.system(cmd2)
    # copy evaluation id files to workspace
    good_evaluation_ids_file_local = '{}_evaluation_only.csv'.format(os.path.splitext(os.path.basename(good_df_data))[0])
    risk_evaluation_ids_file_local = '{}_evaluation_only.csv'.format(os.path.splitext(os.path.basename(risk_df_data))[0])
    workspace_folder = os.path.dirname(good_df_data)
    logger.info('copy from {} to {}'.format(good_evaluation_ids_file_local, workspace_folder))
    logger.info('copy from {} to {}'.format(risk_evaluation_ids_file_local, workspace_folder))
    shutil.copy2(good_evaluation_ids_file_local, workspace_folder)
    shutil.copy2(risk_evaluation_ids_file_local, workspace_folder)
    good_evaluation_ids_file = os.path.join(workspace_folder, os.path.basename(good_evaluation_ids_file_local))
    risk_evaluation_ids_file = os.path.join(workspace_folder, os.path.basename(risk_evaluation_ids_file_local))
    return good_evaluation_ids_file, risk_evaluation_ids_file

def generate_ea_data(good_df_data, risk_df_data, good_evaluation_ids_file, risk_evaluation_ids_file):
    logger.info('Step4: generate ea train & evaluation only data')
    output_folder = os.path.join(os.path.dirname(good_df_data), 'processed')
    cmd = 'python generate_ea_train_eva_only_data.py --good_df_file {} --risk_df_file {} --good_eva_only_ids_file {} ' \
          '--risk_eva_only_ids_file {} --output_folder {}'.format(good_df_data, risk_df_data, good_evaluation_ids_file, risk_evaluation_ids_file, output_folder)
    logger.info('\t{}'.format(cmd))
    os.system(cmd)
    return output_folder

def data_augment(workspace_folder):
    logger.info('Step5: data augment & dropout')
    with open(os.path.join(os.path.dirname(workspace_folder), 'full_64_df.csv')) as f:
        line = f.readline()
    with open(os.path.join(os.path.dirname(workspace_folder), 'ea_all_columns.csv'), 'w') as f:
        f.write(line)
    cmd = 'python data_augment.py --train_file {} --evaluation_file {} --sample_columns_file {}  --label_scope {} --label_name {} --augment_ratio {} --dropout --output_folder {}'.format(
        os.path.join(workspace_folder, 'data_ea_train', 'ea_all.csv'),
        os.path.join(workspace_folder, 'data_evaluation_only', 'evaluation_only_all_records_without_ids.csv'),
        os.path.join(os.path.dirname(workspace_folder), 'ea_all_columns.csv'),
        '1 2 3 4 5',
        'label',
        '0.1',
        os.path.join(workspace_folder, 'data_augment'))
    logger.info('\t{}'.format(cmd))
    os.system(cmd)
