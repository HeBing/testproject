#! /usr/bin/env python3
# -*- coding: utf-8 -*-

"""
All data analysis code shall be maintained in the file
"""
import os
from collections import ChainMap
import numpy as np
import pandas as pd

from darwinutils.json_op import Json
from darwinutils.log import get_task_logger
from data_preprocess.sql_executor import load_file, sql_fetch
from darwinutils.decorator import timeit
logger = get_task_logger(__name__)


def get_count(**kargs):
    """
    The function is only for test
    :param kargs:
    :return:
    """
    count = 0
    logger.debug("Total {} args".format(len(kargs.items())))
    for idx, (key, value) in enumerate(kargs.items()):
        if isinstance(value, (list, tuple, dict, pd.DataFrame)):
            logger.debug("{}: type {} num {}".format(idx, type(value), len(value)))
            count += len(value)
        elif value is not None:
            logger.debug("{}: type {} num {}".format(idx, type(value), value))
            count += int(value)
        else:
            count = -1
    return count


@timeit
def merge_dataframe_to_csv(output_file="merged.csv", **kargs):
    """
    The file is used to transfer multiple dataframe or list to one and save to a csv file. The function hardly keep a sequence,
    if you hope the output keep a sequence, please make sure your output variable name can be sorted

    :param kargs: {key:value, key:value} The different key:value pair come from differnt parents nodes. The function
    will try to merge them together. If the shape not same and then total failed. and return None
    :param output_file: string file which is the target csv file full path
    :return: True/False
    """
    if os.path.exists(output_file):
        logger.warning("Target file {} existed. We will overwrite it.".format(output_file))
        os.remove(output_file)
    # assume the kargs may include df and list at the same time
    input_key_names = list(kargs.keys())
    input_key_names.sort()

    df_type_dict = dict(ChainMap(*list(map(lambda s: {s[0]: s[1]},
                                           filter(lambda s: s[1] is not None and len(s[1]) > 0 and isinstance(s[1], pd.DataFrame),
                                                  kargs.items())))))

    lst_type_dict = dict(ChainMap(*list(map(lambda s: {s[0]: s[1]},
                                            filter(lambda s: s[1] is not None and len(s[1]) > 0 and isinstance(s[1], list),
                                                   kargs.items())))))

    if len(df_type_dict) > 0:
        columns = list(df_type_dict.values())[0].columns.values
    elif len(lst_type_dict) > 0:
        first_sample = list(lst_type_dict.values())[0]
        # the sample maybe combined or not sunch as [(1,2,3), (4,5,6)] or [[(1,2,3), (4,5,6)],[(7,8,9)]]
        if len(first_sample) > 0:
            if isinstance(first_sample[0], tuple):
                column_num = np.shape(first_sample)
            elif isinstance(first_sample[0], list) and len(first_sample[0]) > 0 and isinstance(first_sample[0][0], tuple):
                column_num = np.shape(first_sample[0])
            else:
                logger.error("Unsupported combine data format from {} which shape is {}".format(list(lst_type_dict.keys())[0],
                                                                                                np.shape(first_sample)))
                return None
        else:
            logger.error("Previous parent which output name is {} maybe failed".format(list(lst_type_dict.keys())[0]))
            return None
        column_num = column_num[-1]
        columns = list(map(lambda s: "c_{}".format(s), range(column_num)))
    else:
        logger.warning("There is nothing to be merged")
        return False

    last_df = None
    for var_name in input_key_names:
        if df_type_dict.get(var_name) is not None:
            if last_df is None:
                last_df = df_type_dict.get(var_name)
            else:
                try:
                    last_df = pd.concat([last_df, df_type_dict.get(var_name)], ignore_index=True)
                except Exception as e:
                    logger.exception("Cannot merge dataframe together: Current df shape {} and new df shape {} from {} and columsn {}".format(
                        np.shape(last_df), np.shape(df_type_dict.get(var_name)), var_name, columns
                    ))
                    return False
        else:
            if isinstance(lst_type_dict.get(var_name)[0], tuple):
                data_value = lst_type_dict.get(var_name)
            elif isinstance(lst_type_dict.get(var_name)[0], list):
                data_value = sum(lst_type_dict.get(var_name), [])
            else:
                logger.error("Get unsupported combined format for key {} and shape {} vs [{}]".format(var_name,
                                                                                                      np.shape(lst_type_dict.get(var_name)),
                                                                                                      type(np.shape(lst_type_dict.get(var_name)[0]))))
                return None
            try:
                tmp_df = pd.DataFrame(data_value, columns=columns)
            except:
                logger.exception("Failed to conver list shape {} from {} to dataframe with columns {}".format(
                    np.shape(lst_type_dict.get(var_name)), var_name, columns
                ))
                return False
            if last_df is None:
                last_df = tmp_df
            else:
                try:
                    last_df = pd.concat([last_df, tmp_df], ignore_index=True)
                except Exception as e:
                    logger.exception("Cannot merge dataframe together: Current df shape {} and new df shape {} from {} and columsn {}".format(
                        np.shape(last_df), np.shape(lst_type_dict.get(var_name)), var_name, columns
                    ))
                    return False
    try:
        last_df.to_csv(output_file, index=False)
    except Exception as e:
        logger.exception("Failed to write dataframe to file {} with reason {}".format(output_file, str(e)))
        return False
    return True


@timeit
def df_process_with_formula(df, process_file, file_type='excel'):
    '''
        按照公式计算dataframe
    :param df: dataframe
    :param process_file:
    :return:
    '''
    formula_lst = load_file(process_file, file_type)
    if formula_lst is None:
        logger.error("Failed to load sql cmd file {} file type {}".format(process_file, file_type))
        return None
    if len(formula_lst) == 0:
        logger.error("There is no valid sql str in file {} file type {}".format(process_file, file_type))
        return None

    df_res = None
    for idx, cmd in enumerate(formula_lst):
        if not pd.isna(cmd[0]) and not pd.isna(cmd[1]) and not pd.isna(cmd[2]):
            cmd[2] = cmd[2].lower().strip()
            cmd[0] = cmd[0].lower().strip()
            logger.info('processing {}/{} formula  {} : {}'.format(idx, len(formula_lst), cmd[2], cmd[0]))
            if cmd[1] == 'Y':
                if df_res is None:
                    df_res = df[[cmd[0]]]
                else:
                    df_res[cmd[2]] = df[cmd[0]]
            else:
                if df_res is None:
                    df_res = pd.DataFrame(df.eval(cmd[0]), columns=[cmd[2]])
                else:
                    try:
                        df_res[cmd[2]] = df.eval(cmd[0])
                    except Exception as e:
                        logger.exception("Failed processing formula:{} with [{}]".format(cmd, str(e)))
        else:
            logger.warning("Get Nan value  {}/{} formula".format(idx, len(formula_lst)))
    logger.info('Successfully processed all formula')
    return df_res


@timeit
def merge_df_lst_to_main_df(main_df_sql_file, df_lst, cmd_replace_dict_file=None, use_hive=False):
    '''
        merge df_lst to main_df
    :param main_df_sql_file:
    :param df_lst:
    :param cmd_replace_dict_file:
    :param on:
    :return:
    '''
    if not os.path.exists(main_df_sql_file):
        raise ValueError("File {} not existed".format(main_df_sql_file))
    with open(main_df_sql_file, 'r') as f:
        main_df_sql = f.read()
    replace_dict = Json.file_to_json(cmd_replace_dict_file)
    if replace_dict is not None:
        for k, v in replace_dict.items():
            main_df_sql = main_df_sql.replace(k, v)
    main_df = sql_fetch(main_df_sql, use_hive=use_hive)
    if df_lst is None or len(df_lst) == 0:
        raise ValueError("df_lst is {}!!!!".format(df_lst))
    for idx, df in enumerate(df_lst):
        if df is None:
            raise ValueError("!!!! It is a very crtical issue that previous pipe node failure. "
                             "It may cause you are getting unexpected data")

        logger.info('merging {}/{} df  shape1 = {}  shape2 = {}'.format(idx, len(df_lst), main_df.shape, df.shape))
        main_df = pd.merge(main_df, df, on='mrchnt_key_wrd', how='left')
    main_df.fillna(0)
    logger.info('Successfully merged all df')
    return main_df


@timeit
def save_file(df, output="output.csv"):
    if df is not None and isinstance(df, pd.DataFrame):
        if output is None:
            output = "output.csv"
        logger.info('write result to {}'.format(output))
        df.to_csv(output, index=False)
        return True
    else:
        logger.error("Input df is not valid")
        return False


@timeit
def concat_df_from_pipeline(sequence=None, **kwargs):
    # the function shall be only used for pipeline node start pipeline an then next node merge
    # the different pipeline result
    if sequence is None:
        sequence = list(kwargs.keys())
    df_lst = []
    for node in sequence:
        if node is not None and kwargs.get(node) is not None and kwargs.get(node).get("output") is not None and \
                isinstance(kwargs.get(node).get("output"), pd.DataFrame):
            df_lst.append(kwargs.get(node).get("output"))
        else:
            logger.warning("Donot get {} dataframe".format(node))
    if len(df_lst) > 0:
        try:
            return pd.concat(df_lst, ignore_index=True)
        except Exception as e:
            logger.exception("Failed to concat pipeline node result together by {}".format(str(e)))
            return None


@timeit
def merge_input_to_list(**kwargs):
    lst = []
    for k, v in kwargs.items():
        lst.append(v)
    return lst