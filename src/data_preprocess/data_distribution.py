#! /usr/bin/env python3
# -*- coding: utf-8 -*-

import pandas as pd
import numpy as np
from darwinutils.mapreduce import parallel_star2map_t, parallel_star2map_p
from darwinutils.log import get_task_logger
from darwinutils.decorator import timeit
logger = get_task_logger(__name__)


class DataDistribution(object):
    def __init__(self):
        pass

    @classmethod
    def get_miss_value_info(cls, data):
        """
        Get missing value information
        :param data: ndarry, list or dataframe
        :return: missing ratio, it maybe a list according to input shape
        """
        if data is None or len(data) == 0:
            return []
        if isinstance(data, pd.DataFrame):
            return data.isnull().sum(axis=0).values/len(data)
        elif isinstance(data, np.ndarray):
            return 1 - np.nansum(np.where(np.isnan(data), 0, 1), axis=0)/len(data)
        elif isinstance(data, list):
            data = np.array(data)
            return 1 - np.nansum(np.where(np.isnan(data), 0, 1), axis=0) / len(data)
        else:
            return []

    @classmethod
    def get_dominant_value_info(cls, data, threshold=0.9):
        """
        Get unique or domniant value information
        :param data: ndarray, list or dataframe
        :param threshold: threshold to determine dominant
        :return: [(True, Max Ratio)], it maybe a list according to input shape
        """
        if data is None or len(data) == 0:
            return [(False, 0.0)]
        rst = []
        if isinstance(data, (list, np.ndarray)):
            data = pd.DataFrame(data)
        if isinstance(data, pd.DataFrame):
            columns = data.columns.values
            #if only one column, groupby cannot work
            if len(columns) == 1:
                data = pd.concat([data, data], axis=1)
                data.columns = [columns[0], "copy"]
            for column in columns:
                ratio = data.groupby(column).count().iloc[:, 0].max()/len(data)
                rst.append((ratio >= threshold, ratio))
        else:
            rst = [(False, 0.0)]
        return rst

    @classmethod
    @timeit
    def get_top_pearson_coeff(cls, data, top_n=1, threshold=0.3, fixed_column_idx=None):
        """
        Get top n max pearson correlation coefficient column pair which value shall larger than threshold
        :param data: ndarray, list or dataframe
        :param top_n: how many return, if set to -1, it means all
        :param threshold: min value
        :param fixed_column_idx: if it is not None, it means calculate the column with others
        :return: list of column index pair [(0, 1), (0, 2), (1,2)], [0.9, 0.8, 0.7]
        """
        if data is None or len(data) == 0 or len(np.shape(data)) == 1:
            return [], []

        #if isinstance(data, (list, np.ndarray)):
        #    data = pd.DataFrame(data)
        #data.fillna(0, inplace=True)
        if isinstance(data, list):
            data = np.array(data)
        elif isinstance(data, pd.DataFrame):
            data = data.values

        if isinstance(data, np.ndarray):
            # we need first check whether need oridnal
            to_be_ordinal = []
            for idx in range(np.shape(data)[1]):
                if isinstance(data[:, idx][0], (np.str, np.object)):
                    try:
                        data[:, idx] = data[:, idx].astype(np.float)
                    except:
                        to_be_ordinal.append(idx)
            if len(to_be_ordinal) > 0:
                from sklearn.preprocessing import OrdinalEncoder
                enc = OrdinalEncoder()
                for name in to_be_ordinal:
                    if not isinstance(data[0, name], np.str):
                        logger.warning("Column idx {} type is {} shall not be re-ordinal".format(name, type(data[0, name])))
                    try:
                        enc.fit(data[:, name].astype(np.str).reshape(-1, 1))
                    except:
                        logger.exception("Failed for ordinal: {}".format(np.unique(data[:, name])))
                        raise ValueError
                    data[:, name] = enc.transform(data[:, name].astype(np.str).reshape(-1, 1)).reshape(-1,)
            try:
                data = data.astype(np.float)
            except Exception as e:
                logger.exception("Failed to transfer data to numeric with reason {}".format(str(e)))
                return [], []

            data = np.where(np.isnan(data), 0, data)
            #logger.info(data[:10])
            coef_matrix = np.triu(np.corrcoef(data, rowvar=False), 1)
            #if some column only have one value and then, the corrcoef become nan
            coef_matrix = np.where(np.isnan(coef_matrix), 0, coef_matrix)
            #logger.info("coef_matrix: \n{}".format(coef_matrix))
            if threshold == 0:
                threshold = 0.000001
            over_threshold_indices = np.where(np.abs(coef_matrix) >= threshold)
            if len(over_threshold_indices[0]) > 0:
                if fixed_column_idx is not None:
                    # we need just pick the fixed_column_idx
                    fixed_indices = np.append(np.where(over_threshold_indices[0] == fixed_column_idx),
                                              np.where(over_threshold_indices[1] == fixed_column_idx))
                    logger.debug("over_threshold_indices: {}\nfixed_indices: {}".format(over_threshold_indices, fixed_indices))
                    over_threshold_indices = (np.array(over_threshold_indices[0][[fixed_indices]]).reshape(-1,),
                                               np.array(over_threshold_indices[1][[fixed_indices]]).reshape(-1,))
                    logger.debug("over_threshold_indices: {}".format(over_threshold_indices))
                if top_n < 0:
                    sorted_indices = np.dstack(over_threshold_indices).reshape(-1, 2)[
                        np.argsort(coef_matrix[over_threshold_indices])[::-1]]
                else:
                    sorted_indices = np.dstack(over_threshold_indices).reshape(-1, 2)[
                                         np.argsort(coef_matrix[over_threshold_indices])[::-1]][:top_n]

                return sorted_indices, coef_matrix[sorted_indices[:, 0], sorted_indices[:, 1]]

        return [], []

    @classmethod
    def get_discrete_distribution(cls, data, label_idx=None):
        """
        Get the total number of each value.
        :param data: ndarray, list or dataframe
        :param label_idx: the column index of label in data
        :return: {col_idx: [values, numbers, not label_0 numbers]}
        """
        if data is None or len(data) == 0:
            return {}
        if label_idx is not None and (len(np.shape(data)) == 1 or label_idx >= np.shape(data)[1]):
            logger.error("Input label_idx {} not right. Data shape is {}".format(label_idx, np.shape(data)))
            return {}
        rst = {}
        if isinstance(data, (list, np.ndarray)):
            data = pd.DataFrame(data)
        if isinstance(data, pd.DataFrame):
            columns = data.columns.values
            if label_idx is not None:
                try:
                    data[columns[label_idx]] = pd.to_numeric(data[columns[label_idx]])
                except:
                    logger.warning("To calculate discrete_distribution require label be numeric")
                    label_idx = None
            # if only one column, groupby cannot work
            if len(columns) == 1:
                data = pd.concat([data, data], axis=1)
                data.columns = [columns[0], "copy"]
            for column in columns:
                if label_idx is None:
                    groupby_df = data.groupby(column).count()
                    rst[str(column)] = [groupby_df.index.values, groupby_df.iloc[:, 0].values, [0]*len(groupby_df.index.values)]
                else:
                    if column == columns[label_idx]:
                        continue

                    groupby_df = data.groupby(column)
                    values = []
                    numbers = []
                    no_label_0_numbers = []
                    for group_item in groupby_df:
                        values.append(group_item[0])
                        numbers.append(len(group_item[1]))
                        no_label_0_numbers.append(group_item[1][columns[label_idx]][group_item[1][columns[label_idx]] != 0].count())
                    rst[str(column)] = [np.array(values), np.array(numbers), np.array(no_label_0_numbers)]
        return rst

    @classmethod
    def get_continuous_distribution(cls, data, label_idx=None, bins=10):
        """
        Get the Min, Max, Mean, Median, Var and 10Bins stats
        :param data: ndarray, list or dataframe
        :param label_idx: the column index of label in data
        :param bins: bin number. default is 10
        :return: {col_idx: {general: [Min, Max, Mean, Median, Var], bins:{[bins, number, no_label_0_number]}}
        """
        if data is None or len(data) == 0:
            return {}
        if label_idx is not None and (len(np.shape(data)) == 1 or label_idx >= np.shape(data)[1]):
            logger.error("Input label_idx {} not right. Data shape is {}".format(label_idx, np.shape(data)))
            return {}
        column_names = None
        if isinstance(data, pd.DataFrame):
            column_names = data.columns.values
            data = data.values
        elif isinstance(data, list):
            data = np.array(data)

        rst = {}
        if isinstance(data, np.ndarray):
            try:
                data = np.array(data, dtype=np.float)
            except:
                logger.exception("Input data which type is {} {} cannot transfer to np.float".format(type(data),
                                                                                                 type([data[0]])))
                return {}
            if column_names is None:
                column_names = list(map(str, range(np.shape(data)[1])))
            else:
                column_names = list(map(str, column_names))
            # we need first check whether need oridnal
            to_be_ordinal = []
            for idx in range(np.shape(data)[1]):
                if isinstance(data[:, idx][0], (np.str, np.object)):
                    try:
                        data[:, idx] = data[:, idx].astype(np.float)
                    except:
                        to_be_ordinal.append(idx)
            if len(to_be_ordinal) > 0:
                from sklearn.preprocessing import OrdinalEncoder
                enc = OrdinalEncoder()
                enc.fit(data[:, to_be_ordinal])
                data[:, to_be_ordinal] = enc.transform(data[:, to_be_ordinal])
            if len(np.shape(data)) == 1:
                data = np.array([data])
            #logger.info(data)
            data = np.where(np.isnan(data), 0, data)
            for col_idx in range(np.shape(data)[1]):
                if label_idx is not None:
                    if label_idx == col_idx:
                        continue
                rst[column_names[col_idx]] = {"general": [np.min(data[:, col_idx]), np.max(data[:, col_idx]), np.mean(data[:, col_idx]),
                                                 np.median(data[:, col_idx]), np.var(data[:, col_idx])]}
                number, bin_lst = np.histogram(data[:, col_idx], bins=bins)
                if label_idx is not None:
                    no_label_0_lst = []
                    # avoid loss number of last bin
                    bin_lst[-1] += 1
                    for idx in range(bins):
                        label_array = data[np.where((data[:, col_idx] >= bin_lst[idx]) & (data[:, col_idx] < bin_lst[idx+1]))[0], label_idx]
                        no_label_0_lst.append(len(np.where(label_array != 0)[0]))
                    no_label_0_lst = np.array(no_label_0_lst)
                    bin_lst[-1] -= 1
                    rst[column_names[col_idx]]["bins"] = [bin_lst, number, no_label_0_lst]
                else:
                    rst[column_names[col_idx]]["bins"] = [bin_lst, number, [0]*(len(bin_lst)-1)]
        return rst

    @classmethod
    @timeit
    def get_chimerge(cls, data, label_idx, bin=10):
        """
        Get ChiMerge value
        :param data: ndarray, list or dataframe
        :param confidence_val: confidence level. default will not samle over 95%
        :param bin: bin number. Now, we shall fix to 10
        :param sample: sample number. default will not sample
        :return the chimerge scope and stats {col_idx: [[low, high, pos_num, neg_num], [low, high, pos_num, neg_num],...]} pos is the non 0 label
        """
        from darwinutils.config import DARWIN_CONFIG
        from random import sample
        if data is None or len(data) == 0 or len(np.shape(data)) == 1:
            return {}
        if isinstance(data, (list, np.ndarray)):
            data = pd.DataFrame(data)
        rst = {}
        if isinstance(data, pd.DataFrame):
            data = data.fillna(0)
            if len(data) > DARWIN_CONFIG.get("chimerge_sample_size", 100000):
                data = data.iloc[sample(range(len(data)), DARWIN_CONFIG.get("chimerge_sample_size", 100000)), :]
            for column in data.columns.values:
                if column == data.columns.values[label_idx]:
                    continue
                rst[str(column)] = cls.chimerge(data, column, data.columns.values[label_idx], bin)
                #logger.info("{} \n{}".format(column, rst[str(column)]))
                for bin_idx in range(bin):
                    samples = data[(rst[str(column)][bin_idx][0] <= data[column]) & (data[column] <= rst[str(column)][bin_idx][1])]
                    pos_num = samples[data.columns.values[label_idx]][samples[data.columns.values[label_idx]] != 0].count()
                    rst[str(column)][bin_idx].extend([pos_num, len(samples) - pos_num])
        return rst

    @staticmethod
    @timeit
    def chimerge(data, feature, label, bins=10):
        """
        Calculate chi-merge and return stats
        :param data: data frame of whole table
        :param feature: feature column name
        :param label: label column name
        :param bins: bin number
        :return: [[low, up], [low, up],...]
        """
        from collections import Counter
        distinct_vals = sorted(set(data[feature]))  # Sort the distinct values
        labels = sorted(set(data[label]))  # Get all possible labels
        empty_count = {l: 0 for l in labels}  # A helper function for padding the Counter()
        intervals = [[distinct_vals[i], distinct_vals[i]] for i in
                     range(len(distinct_vals))]  # Initialize the intervals for each attribute
        while len(intervals) > bins:  # While loop
            chi = []
            for i in range(len(intervals) - 1):
                # Calculate the Chi2 value
                obs0 = data[data[feature].between(intervals[i][0], intervals[i][1])]
                obs1 = data[data[feature].between(intervals[i + 1][0], intervals[i + 1][1])]
                total = len(obs0) + len(obs1)
                count_0 = np.array([v for i, v in {**empty_count, **Counter(obs0[label])}.items()])
                count_1 = np.array([v for i, v in {**empty_count, **Counter(obs1[label])}.items()])
                count_total = count_0 + count_1
                expected_0 = count_total * sum(count_0) / total
                expected_1 = count_total * sum(count_1) / total
                chi_ = (count_0 - expected_0) ** 2 / expected_0 + (count_1 - expected_1) ** 2 / expected_1
                chi_ = np.nan_to_num(chi_)  # Deal with the zero counts
                chi.append(sum(chi_))  # Finally do the summation for Chi2
            min_chi = min(chi)  # Find the minimal Chi2 for current iteration
            for i, v in enumerate(chi):
                if v == min_chi:
                    min_chi_index = i  # Find the index of the interval to be merged
                    break
            new_intervals = []  # Prepare for the merged new data array
            skip = False
            done = False
            for i in range(len(intervals)):
                if skip:
                    skip = False
                    continue
                if i == min_chi_index and not done:  # Merge the intervals
                    t = intervals[i] + intervals[i + 1]
                    new_intervals.append([min(t), max(t)])
                    skip = True
                    done = True
                else:
                    new_intervals.append(intervals[i])
            intervals = new_intervals
        return intervals

    @classmethod
    def get_discrete_continuous_columns(cls, data):
        """
        I am not sure the method is ok or not. We may change it in project
        :param data: ndarray, list or dataframe
        :return: {"discrete": [col_idx, col_idx, ...], "continuous": [col_idx, col_idx]}
        """
        from darwinutils.config import DARWIN_CONFIG
        if data is None or len(data) == 0 or len(np.shape(data)) == 1:
            return {}
        if isinstance(data, (list, np.ndarray)):
            data = pd.DataFrame(data)
        columns = data.columns.values
        # if only one column, groupby cannot work
        if len(columns) == 1:
            data = pd.concat([data, data], axis=1)
            data.columns = [columns[0], "copy"]
        rst = {}
        if isinstance(data, pd.DataFrame):
            for idx, column_name in enumerate(columns):
                try:
                    data[column_name] = data[column_name].fillna(0)
                    data[column_name] = pd.to_numeric(data[column_name])
                    if all(data[column_name].astype(float) == data[column_name].astype(int)) and \
                            (data[column_name].astype(str).str.len().median() >= DARWIN_CONFIG.get(
                                "str_value_length_median", 6) or len(data.groupby(column_name)) <= DARWIN_CONFIG.get("continuous_value_min_num", 20)):
                        if rst.get("discrete") is None:
                            rst["discrete"] = [idx]
                        else:
                            rst["discrete"].append(idx)
                    else:
                        if rst.get("continuous") is None:
                            rst["continuous"] = [idx]
                        else:
                            rst["continuous"].append(idx)
                except:
                    #logger.exception("column: {}".format(column_name))
                    if rst.get("discrete") is None:
                        rst["discrete"] = [idx]
                    else:
                        rst["discrete"].append(idx)
        return rst

    @classmethod
    def get_all_stats(cls, data, label_idx, bins=10, pearson_top_n=3, pearson_threshold=None, column_types=None,
                      chimerge_flag="without"):
        """
        The function is a big function to generate all required stats information for Suixingfu project. To be generated
        information as followed:
        1. Check each column whether discrete or continuous
        2. Generate table
        Table1:
            Discrete: Name, Missing_Ratio, Dominant
            Continuous: Name, Missing_Ratio, Dominant, Min, Max, Mean, Median, Var

        Table2:
            Name, Coefficient and include the column with label
            c1:c2, 0.345
            c1:c3, 0.31

        Chart1:
        Continuous: Box for the column

        Chart2:
        Discrete: Bar-Chart of each column which describe Each value number and positive sample number in each value
        Continuous: Bar-Chart of each column which describe Each Bin value number and positive sample number in each value

        Chart3:
        Continuous: ChiMerge Stats Bar-Chart for positive and negative number

        :param data: ndarray, list or dataframe
        :param label_idx: index of label in data. If there is no, it shall be None
        :param pearson_threshold:
        :param pearson_top_n:
        :param bins:
        :param chimerge_flag: draw or not or only ["without", "with", "only"]
        :return: A big dict to organize the above information and the dict format as followed:
        {
            column_name: {
                            type: discrete
                            general_table: dataframe
                            coefficient_table: dataframe
                            box_chart: None
                            value_bar_chart: dataframe
                            chimerge_bar_chart: None
                        }
        }
        """
        from darwinutils.config import DARWIN_CONFIG
        from random import sample
        if data is None or len(data) == 0 or len(np.shape(data)) == 1:
            logger.error("Input data type is {}".format(type(data)))
            return {}
        if isinstance(data, (list, np.ndarray)):
            data = pd.DataFrame(data)

        rst = {}
        if isinstance(data, pd.DataFrame):
            rst = cls.comprise_general_table(data, label_idx, column_types=column_types)
            if len(rst) == 0:
                logger.error("Failed to get rst from comprise_general_table with label_idx {}".format(label_idx))
                return rst
            logger.info("Done comprise_general_table")
            if chimerge_flag != "only":
                rst = cls.comprise_pearson_table(data, label_idx, full_dict=rst, top_n=pearson_top_n,
                                                 pearson_threshold=pearson_threshold)
                if len(rst) == 0:
                    logger.warning("Failed to get rst from comprise_pearson_table with label_idx {}".format(label_idx))
                logger.info("Done comprise_pearson_table")
                param_lst = []
                for idx, column_name in enumerate(data.columns.values):
                    if idx != label_idx:
                        param_lst.append([[data[[column_name, data.columns.values[label_idx]]]],
                                          {"label_idx": 1,
                                           "col_type": rst[str(column_name)]["type"],
                                           "bins": bins}])
                    else:
                        param_lst.append([[data[[column_name]]],
                                          {"label_idx": None,
                                           "col_type": rst[str(column_name)]["type"],
                                           "bins": bins}])
                logger.info("Start {} comprise_value_bar_chart jobs with max limit {}".format(
                    len(param_lst), DARWIN_CONFIG.get("parallel_job_limit", 10)
                ))
                value_bar_rst = list(parallel_star2map_p(cls.comprise_value_bar_chart, param_lst,
                                                         max_workers=DARWIN_CONFIG.get("parallel_job_limit", 10)))
                for value_bar_dict in value_bar_rst:
                    if len(value_bar_dict) > 0:
                        for key, value in value_bar_dict.items():
                            rst[key].update(value)
                logger.info("Done comprise_value_bar_chart")
                """
                value_bar_rst = []
                for idx, column_name in enumerate(data.columns.values):
                    if idx != label_idx:
                        value_bar_rst.append(cls.comprise_value_bar_chart(data[[column_name, data.columns.values[label_idx]]], label_idx=1,
                                                     col_type=rst[str(column_name)]["type"], bins=bins))
                    else:
                        value_bar_rst.append(cls.comprise_value_bar_chart(data[[column_name, data.columns.values[label_idx]]], label_idx=1,
                                                     col_type=rst[str(column_name)]["type"], bins=bins))
                for value_bar_dict in value_bar_rst:
                    if len(value_bar_dict) > 0:
                        for key, value in value_bar_dict.items():
                            rst[key].update(value)
                logger.info("Done comprise_value_bar_chart")
                """
            if chimerge_flag != "without":
                # get qualified chimerge_bar_chart candidates
                chimerge_candidates = list(
                    filter(lambda s: rst[s]["type"] == "continuous" and s != data.columns.values[label_idx], rst.keys()))
                param_lst = []
                if len(data) > DARWIN_CONFIG.get("chimerge_sample_size", 100000):
                    sampled_data = data.iloc[sample(range(len(data)), DARWIN_CONFIG.get("chimerge_sample_size", 100000)), :]
                for column_name in chimerge_candidates:
                    param_lst.append([[sampled_data[[column_name, sampled_data.columns.values[label_idx]]]],
                                      {"label_idx": 1,
                                       "col_type": rst[str(column_name)]["type"],
                                       "bins":bins}])
                logger.info("Start {} comprise_chimerge_bar_chart jobs with max limit {}".format(
                    len(param_lst), DARWIN_CONFIG.get("parallel_job_limit", 10)
                ))
                chimerge_rst = list(parallel_star2map_p(cls.comprise_chimerge_bar_chart, param_lst,
                                                        max_workers=DARWIN_CONFIG.get("parallel_job_limit", 10)))
                for chimerge_dict in chimerge_rst:
                    if len(chimerge_dict) > 0:
                        for key, value in chimerge_dict.items():
                            rst[key].update(value)

                logger.info("Done comprise_chimerge_bar_chart")

        return rst

    @classmethod
    def comprise_chimerge_bar_chart(cls, data, label_idx, col_type, full_dict=None, bins=10):
        """

        :param data: data frame
        :param label_idx: label index of data. If there is no label, it shall be None
        :param col_type: discrete or continuous
        :param full_dict: previous rst dict
        :return:
        """
        if full_dict is not None:
            rst = full_dict
        else:
            rst = {}
        if data is None or not isinstance(data, pd.DataFrame):
            logger.error("Input data type is {}".format(type(data)))
            return rst

        if col_type == "discrete":
            for name in data.columns.values:
                rst[str(name)]["chimerge_bar_chart"] = None
        else:
            # {col_name: [[low, high, pos_num, neg_num], [low, high, pos_num, neg_num],...]}
            chimerge_info = cls.get_chimerge(data, label_idx, bin=bins)
            if len(chimerge_info) == 0:
                logger.error("Failed get chimerge_info and label_idx {} bins {} type {}".format(
                    label_idx, bins, col_type
                ))
                return rst
            for name, item in chimerge_info.items():
                chimerge_table = {"bins": [], "positive": [], "negative": []}
                for info in item:
                    chimerge_table["bins"].append("{} - {}".format(round(info[0], 2), round(info[1], 2)))
                    chimerge_table["positive"].append(info[2])
                    chimerge_table["negative"].append(info[3])
                if len(rst) == 0:
                    rst[str(name)] = {}
                rst[str(name)]["chimerge_bar_chart"] = pd.DataFrame.from_dict(chimerge_table)
        return rst

    @classmethod
    def comprise_value_bar_chart(cls, data, label_idx, col_type, full_dict=None, bins=10):
        """

        :param data: data series
        :param label_idx: label index of data. If there is no label, it shall be None
        :param col_type: discrete or continuous
        :param full_dict: previous rst dict
        :return:
        """
        if full_dict is not None:
            rst = full_dict
        else:
            rst = {}
        if data is None or not isinstance(data, pd.DataFrame):
            logger.error("Input data type is {}".format(type(data)))
            return rst

        if col_type == 'discrete':
            # {col_name: [values, numbers, not label_0 numbers]}
            #logger.info(data)
            dist_info = cls.get_discrete_distribution(data, label_idx)
        else:
            # {col_name: {general: [Min, Max, Mean, Median, Var], bins:{[bins, number, no_label_0_number]}}
            dist_info = cls.get_continuous_distribution(data, label_idx, bins)
        if len(dist_info) == 0:
            logger.error("Failed get distribution and label_idx {} bins {} type {}".format(
                label_idx, bins, col_type
            ))
            return rst
        #logger.info(dist_info)
        for name, item in dist_info.items():
            if label_idx is not None:
                if col_type == 'discrete':
                    dist_table = {"values": item[0], "total": item[1], "positive": item[2]}
                else:
                    values = []
                    for i in range(len(item["bins"][0])-1):
                        values.append("{} - {}".format(round(item["bins"][0][i], 3), round(item["bins"][0][i+1], 3)))
                    dist_table = {"bins": values, "total": item["bins"][1], "positive": item["bins"][2]}
            else:
                if col_type == 'discrete':
                    dist_table = {"values": item[0], "total": item[1]}
                else:
                    values = []
                    for i in range(len(item["bins"][0])-1):
                        values.append("{} - {}".format(round(item["bins"][0][i], 3), round(item["bins"][0][i+1], 3)))
                    dist_table = {"bins": values, "total": item["bins"][1]}
            if rst.get(str(name)) is None:
                rst[str(name)] = {}
            rst[str(name)]["value_bar_chart"] = pd.DataFrame.from_dict(dist_table)
        return rst

    @classmethod
    def get_outlier_scale(cls, data, continuous_discrete_info):
        """
        Get outlier scale value: max((max_outlier - box_plot_max)/(box_plot_max - box_plot_min) and (box_plot_max - box_plot_min)/(box_plot_min - min_outlier))
        :param data: ndarry, list or dataframe
        :return: missing ratio, it maybe a list according to input shape
        """
        if data is None or len(data) == 0:
            logger.error("Get None data for get_outlier_scale")
            return []
        if isinstance(data, pd.DataFrame):
            data = data.values
        elif isinstance(data, list):
            data = np.array(data)

        if isinstance(data, np.ndarray):
            # we need first check whether need oridnal
            to_be_computed = continuous_discrete_info.get("continuous", [])
            return_value = np.zeros(np.shape(data)[1])
            if len(to_be_computed) > 0:
                q3 = np.percentile(data[:, to_be_computed], 75, axis=0)
                q1 = np.percentile(data[:, to_be_computed], 25, axis=0)
                iqr = q3 - q1
                iqr = np.where(iqr == 0, 0.00001, iqr)  # avoid devide 0
                # box_max = q3 + 1.5*iqr
                # box_min = q1 - 1.5*iqr
                pair_value = np.array([(q1 - np.min(data[:, to_be_computed], axis=0))/(4*iqr),
                                       (np.max(data[:, to_be_computed], axis=0) - q3)/(4*iqr)])
                rst = np.max(pair_value, axis=0)
                return_value[to_be_computed] = rst
            return return_value
        else:
            logger.error("Get unexpected data type {} for get_outlier_scale".format(type(data)))
            return []

    @classmethod
    def comprise_general_table(cls, data, label_idx, full_dict=None, column_types=None):
        """
        :param data: data frame
        :param label_idx: label index of data. If there is no label, it shall be None
        :param full_dict: previous rst dict
        :param column_types:
        :return:
        """
        if full_dict is not None:
            rst = full_dict
        else:
            rst = {}
        if data is None or not isinstance(data, pd.DataFrame):
            logger.error("Input data type is {}".format(type(data)))
            return rst
        missing_value_info = cls.get_miss_value_info(data)
        dominating_value_info = cls.get_dominant_value_info(data)
        # above function will not fillna
        continuous_discrete_info = cls.get_discrete_continuous_columns(data)

        if len(continuous_discrete_info) == 0 or len(missing_value_info) == 0 or len(dominating_value_info) == 0:
            logger.error("Failed get basic information for input data with label idx {} and return result:\n"
                         "continuous_discrete_info: {}\nmissing_value_info: {}\ndominating_value_info: {}".format(
                label_idx, continuous_discrete_info, missing_value_info, dominating_value_info
            ))
            return rst
        outlier_value_info = cls.get_outlier_scale(data, continuous_discrete_info)
        if len(outlier_value_info) == 0:
           logger.error("Failed get basic information for input data with label idx {} and return result:\n"
                        "outlier_value_info: {}".format(label_idx, outlier_value_info))
           return rst
        if column_types is not None and len(column_types) > 0:
            for key, value in column_types.items():
                if key in data.columns.values:
                    key_idx = data.columns.tolist().index(key)
                    if value == "discrete" and key_idx in continuous_discrete_info.get("continuous"):
                        continuous_discrete_info["continuous"].remove(key_idx)
                        if continuous_discrete_info.get("discrete") is None:
                            continuous_discrete_info["discrete"] = [key_idx]
                        else:
                            continuous_discrete_info["discrete"].append(key_idx)
                    elif value == "continuous" and key_idx in continuous_discrete_info.get("discrete"):
                        continuous_discrete_info["discrete"].remove(key_idx)
                        if continuous_discrete_info.get("continuous") is None:
                            continuous_discrete_info["continuous"] = [key_idx]
                        else:
                            continuous_discrete_info["continuous"].append(key_idx)

        for idx, name in enumerate(data.columns.values):
            if idx in continuous_discrete_info["discrete"]:
                # create discrete table
                rst[str(name)] = {"type": "discrete"}
                general_table = {
                    "Name": [name],
                    "Missing": [round(missing_value_info[idx], 4)],
                    "Dominant": [0 if not dominating_value_info[idx][0] else round(dominating_value_info[idx][1], 4)],
                    #"OutlierRatio": [round(outlier_value_info[idx], 4)]
                }
                rst[str(name)]["general_table"] = pd.DataFrame.from_dict(general_table)
            else:
                rst[str(name)] = {"type": "continuous"}
                if label_idx is not None:
                    continuous_info = cls.get_continuous_distribution(data[[name, data.columns.values[label_idx]]].values,
                                                                      label_idx=1) # the label_idx shall be 1 since we only pass two columns to the function
                else:
                    continuous_info = cls.get_continuous_distribution(data[[name]].values, label_idx=None)
                if len(continuous_info) == 0:
                    logger.warning("Failed get get_continuous_distribution information for column {} with label idx {} "
                                   "and return result:\ncontinuous_discrete_info: {}\nmissing_value_info: {}\n"
                                   "dominating_value_info: {}".format(name, label_idx, continuous_discrete_info,
                                                                      missing_value_info, dominating_value_info))
                    return {}
                general_table = {
                    "Name": [str(name)],
                    "Missing": [round(missing_value_info[idx], 4)],
                    "Dominant": [0 if not dominating_value_info[idx][0] else round(dominating_value_info[idx][1], 4)],
                    # since we only pass one column, there will be only 0
                    "Min": [round(continuous_info["0"]["general"][0], 4)],
                    "Max": [round(continuous_info["0"]["general"][1], 4)],
                    "Mean": [round(continuous_info["0"]["general"][2], 4)],
                    "Median": [round(continuous_info["0"]["general"][3], 4)],
                    "Var": [round(continuous_info["0"]["general"][4], 4)],
                    "OutlierRatio": [round(outlier_value_info[idx], 4)]
                }
                rst[str(name)]["general_table"] = pd.DataFrame.from_dict(general_table)

        return rst

    @classmethod
    def comprise_pearson_table(cls, data, label_idx, full_dict=None, top_n=3, pearson_threshold=None):
        """
        :param data: data frame
        :param label_idx: label index of data. If there is no label, it shall be None
        :param full_dict: previous rst dict
        :param pearson_threshold:
        :return:
        """
        from darwinutils.config import DARWIN_CONFIG
        if full_dict is not None:
            rst = full_dict
        else:
            rst = {}
        if data is None or not isinstance(data, pd.DataFrame):
            logger.error("Input data type is {}".format(type(data)))
            return rst
        # [(0, 1), (0, 2), (1,2)], [0.9, 0.8, 0.7]
        if pearson_threshold is None:
            pearson_threshold = DARWIN_CONFIG.get("pearson_threshold", 0.3)
        pearson_info = cls.get_top_pearson_coeff(data, top_n=-1, threshold=pearson_threshold)
        if len(pearson_info[0]) == 0:
            logger.warning("There is no column exceed pearson threshold {} with label_idx {}".format(pearson_threshold,
                                                                                                     label_idx))
            return rst

        pearson_column_pair = list(map(list, zip(*pearson_info[0])))
        pearson_values = pearson_info[1]
        if label_idx is not None:
            label_pearson_info = cls.get_top_pearson_coeff(data, top_n=-1, threshold=0, fixed_column_idx=label_idx)
            if len(label_pearson_info[0]) == 0:
                logger.error("Failed to get_top_pearson_coeff with label_idx {}".format(label_idx))
                return rst
            label_pearson_column_pair = list(map(list, zip(*label_pearson_info[0])))
            label_pearson_values = label_pearson_info[1]

        logger.debug("pearson_column_pair: {}\npearson_values: {}\nlabel_pearson_column_pair: {}".format(
            pearson_column_pair, pearson_values, label_pearson_column_pair))
        for idx, name in enumerate(data.columns.values):
            indices = np.where((np.array(pearson_column_pair[0])==idx) | (np.array(pearson_column_pair[1])==idx))[0]
            indices.sort()
            logger.debug("indices: {}".format(indices))
            name_cell = list(zip(np.array(pearson_column_pair[0])[indices], np.array(pearson_column_pair[1])[indices]))
            logger.debug("name_cell: {}".format(name_cell))
            label_indices = []
            if label_idx is not None and label_idx != idx:
                label_indices = np.where((np.array(label_pearson_column_pair[0])==idx) | (np.array(label_pearson_column_pair[1])==idx))[0]
                if len(label_indices) != 1:
                    if len(label_indices) > 0:
                        logger.error("Get unexpected label_indices {} for label_pearson_column_pair:{} "
                                     "label_idx {} and column name {}".format(label_indices,
                                                                              label_pearson_column_pair,
                                                                              label_idx, name))
                    label_indices = []
                else:
                    last_cell = list(zip(np.array(label_pearson_column_pair[0])[label_indices],
                                         np.array(label_pearson_column_pair[1])[label_indices]))[0]
                    logger.debug("label_indices: {}, last_cell: {}".format(label_indices, last_cell))
                    if last_cell in name_cell:
                        meet_idx = name_cell.index(last_cell)
                        indices = np.delete(indices, meet_idx)
                    elif (last_cell[1], last_cell[0]) in name_cell:
                        meet_idx = name_cell.index((last_cell[1], last_cell[0]))
                        indices = np.delete(indices, meet_idx)
            coefficient_table = {"Name": [], "Coefficient": []}
            for num, index in enumerate(indices):
                if top_n >= 0 and num >= top_n:
                    break
                coefficient_table["Name"].append("{} : {}".format(data.columns.values[pearson_column_pair[0][index]],
                                                                  data.columns.values[pearson_column_pair[1][index]]))
                coefficient_table["Coefficient"].append(pearson_values[index])
            for index in label_indices:
                coefficient_table["Name"].append("{} : {}".format(data.columns.values[label_pearson_column_pair[0][index]],
                                                                  data.columns.values[label_pearson_column_pair[1][index]]))
                coefficient_table["Coefficient"].append(label_pearson_values[index])
            rst[str(name)]["coefficient_table"] = pd.DataFrame.from_dict(coefficient_table)
        return rst
