#! /usr/bin/env python3
# -*- coding: utf-8 -*-

import pandas as pd
import numpy as np
import scipy as sc
import plotly
import plotly.graph_objs as go
import plotly.figure_factory as ff
from collections import namedtuple
from darwinutils.log import get_task_logger
logger = get_task_logger(__name__)
SubGraphDefine = namedtuple("SubGraphDefine", ["key", "name", "graph_type", "xaxis", "yaxis", "chart_seq"])


class PlotStats:
    default_plot_config = {
        "header_color": '#C2D4FF',
        "row_colors": ['#F5F8FF', '#F1F4Fc', '#F7FAFF'] ,
        "header_line_color": '#506784',
        "header_font_color": 'white',
        "header_font_size": 16,
        "header_align": ['left', 'center'],
        "cell_line_color": '#506784',
        "cell_font_color": '#506784',
        "cell_font_size": 14,
        "cell_color": '#F5F8FF',
        "cell_align": ['left', 'center'],
        "margin": dict(
            l=40,
            r=80,
            b=40,
            t=80,
        ),
        "bg_width": 1200,
        "bg_height": 980,
        "bg_color": 'rgb(243,243,243)', #'rgba(228, 222, 249, 0.65)',
        "show_legend": True,
        "box_color": 'rgba(93, 164, 214, 0.5)',
        "bar_marker_color": ['rgb(55, 83, 109)', 'rgb(26, 118, 255)'],
        "bar_axis_color": 'rgb(107, 107, 107)',
        "bar_border_color": "rgba(255, 255, 255, 0)"
        }

    def __init__(self, plot_config=None):
        if plot_config is not None and isinstance(plot_config, dict):
            self.default_plot_config.update(plot_config)

    @classmethod
    def plot_tables(cls, data_lst):
        """
        Get a trace list of plotly with layout
        :param data_lst: dataframe
        :return:
        """
        if isinstance(data_lst, pd.DataFrame):
            data_lst = [data_lst]
        total_tables = len(data_lst)
        high = (1 - cls.default_plot_config["margin"] * (total_tables - 1)) / total_tables
        start_y = 0

        trace_lst = []
        for idx, data in enumerate(data_lst):
            cell_color = []
            for c_idx in range(len(data.columns)):
                cell_color.append(cls.default_plot_config["row_colors"][c_idx % len(cls.default_plot_config["row_colors"])])
            yaxis = [start_y, start_y + high]
            start_y += high + cls.default_plot_config["margin"]
            trace_lst.append(go.Table(header=dict(values=list(data.columns),
                                                  fill=dict(color=cls.default_plot_config["header_color"]),
                                                  line=dict(color=cls.default_plot_config["header_line_color"]),
                                                  align=cls.default_plot_config["header_align"],
                                                  font=dict(color=cls.default_plot_config["header_font_color"],
                                                            size=cls.default_plot_config["header_font_size"])),
                                      cells=dict(values=np.transpose(data.values).tolist(),
                                                 line=dict(color=cls.default_plot_config["cell_line_color"]),
                                                 fill=dict(color=cls.default_plot_config["cell_color"]),
                                                 align=cls.default_plot_config["cell_align"],
                                                 font=dict(color=cls.default_plot_config["cell_font_color"],
                                                           size=cls.default_plot_config["cell_font_size"])),
                                      domain=dict(x=[0, 1], y=yaxis))
                             )
        layout = dict(
            width=cls.default_plot_config["bg_width"],
            height=cls.default_plot_config["bg_height"],
            autosize=False,
            margin=cls.default_plot_config["margin"],
            showlegend=cls.default_plot_config["show_legend"],
            plot_bgcolor=cls.default_plot_config["bg_color"],
            paper_bgcolor=cls.default_plot_config["bg_color"],
        )

        return trace_lst, layout

    @classmethod
    def plot(cls, trace_lst, layout, file_name, offline=True):
        fig = go.Figure(data=trace_lst, layout=layout)
        if offline:
            logger.info("Save to {}".format(file_name))
            plotly.offline.plot(fig, filename=file_name, auto_open=False)
        else:
            logger.error("Not support Yet")

    @classmethod
    def plot_tables_charts_for_data_dist(cls, data_stats, column_name, raw_data):
        """
        There are two types figure with subgraph:
        Discrete type:
            2 table 1 bar chart
        Continuous type:
            2 table 2 bar chart
        :param data_stats: stats dict from get_all_stats of DataDistribution. Only one column.
        :param column_name:
        :param raw_data: the column data for box chart
        :return: trace_lst and layout for the column
        """
        if data_stats is None or len(data_stats) == 0:
            logger.error("Empty data stats dict for column".format(column_name))
            return None, None, column_name
        if data_stats.get("type") == "discrete":
            sub_graph_location = [[0.9, 1], [0.74, 0.898], [0, 0.7]]
            sub_graph_sequence = [SubGraphDefine("general_table", "General Information", "table", "", "", 0),
                                  SubGraphDefine("coefficient_table", "Pearson Coefficient Table", "table", "", "", 0),
                                  SubGraphDefine("value_bar_chart", "Value Distribution Bar Chart", "bar", "x1", "y1", 1)]
        elif data_stats.get("type") == "continuous":
            if data_stats.get("chimerge_bar_chart") is not None:
                sub_graph_location = [[0.9, 1], [0.74, 0.898], [0.54, 0.72], [0.26, 0.44], [0, 0.16]]
                sub_graph_sequence = [SubGraphDefine("general_table", "General Information", "table", "", "", 0),
                                      SubGraphDefine("coefficient_table", "Pearson Coefficient Table", "table", "", "",
                                                     0),
                                      SubGraphDefine("box_chart", "Value Distribution Box Chart", "box", "x1", "y1", 1),
                                      SubGraphDefine("value_bar_chart", "Bin Distribution Bar Chart", "bar", "x2", "y2", 2),
                                      SubGraphDefine("chimerge_bar_chart", "Chi-Merge Bar Chart", "bar", "x3", "y3", 3)]
            else:
                sub_graph_location = [[0.9, 1], [0.74, 0.898], [0.54, 0.72], [0, 0.36]]
                sub_graph_sequence = [SubGraphDefine("general_table", "General Information", "table", "", "", 0),
                                      SubGraphDefine("coefficient_table", "Pearson Coefficient Table", "table", "", "", 0),
                                      SubGraphDefine("box_chart", "Value Distribution Box Chart", "box", "x1", "y1", 1),
                                      SubGraphDefine("value_bar_chart", "Bin Distribution Bar Chart", "bar", "x2", "y2", 2)]
        else:
            logger.error("Unsupported type {}".format(data_stats.get("type")))
            return None, None, column_name

        trace_lst = []
        layout = dict(
            width=cls.default_plot_config["bg_width"],
            height=cls.default_plot_config["bg_height"],
            autosize=False,
            margin=cls.default_plot_config["margin"],
            showlegend=cls.default_plot_config["show_legend"],
            plot_bgcolor=cls.default_plot_config["bg_color"],
            paper_bgcolor=cls.default_plot_config["bg_color"],
            legend=dict(
                x=1.0,
                y=0.54,
                bgcolor='rgba(255, 255, 255, 0)',
                bordercolor='rgba(255, 255, 255, 0)'
            ),
        )
        axis = dict(
            showline=True,
            zeroline=False,
            showgrid=True,
            mirror=True,
            ticklen=4,
            gridcolor='#ffffff',
            tickfont=dict(size=10)
        )
        if raw_data is not None:
            data_stats["box_chart"] = raw_data
        for idx, sub_grah in enumerate(sub_graph_sequence):
            data = data_stats.get(sub_grah.key)
            if data is not None:
                if sub_grah.graph_type == "table":
                    cell_color = []
                    for c_idx in range(len(data.columns)):
                        cell_color.append(
                            cls.default_plot_config["row_colors"][c_idx % len(cls.default_plot_config["row_colors"])])
                    yaxis = sub_graph_location[idx]
                    trace_lst.append(go.Table(header=dict(values=list(data.columns),
                                                          fill=dict(color=cls.default_plot_config["header_color"]),
                                                          line=dict(color=cls.default_plot_config["header_line_color"]),
                                                          align=cls.default_plot_config["header_align"],
                                                          font=dict(color=cls.default_plot_config["header_font_color"],
                                                                    size=cls.default_plot_config["header_font_size"])),
                                              cells=dict(values=np.transpose(data.values).tolist(),
                                                         line=dict(color=cls.default_plot_config["cell_line_color"]),
                                                         fill=dict(color=cls.default_plot_config["cell_color"]),
                                                         align=cls.default_plot_config["cell_align"],
                                                         font=dict(color=cls.default_plot_config["cell_font_color"],
                                                                   size=cls.default_plot_config["cell_font_size"])),
                                              domain=dict(x=[0, 1], y=yaxis),
                                              name=sub_grah.name)
                                     )
                elif sub_grah.graph_type == "box":
                    # , boxpoints='outliers' jitter=0.5, whiskerwidth=0.2,
                    trace_lst.append(go.Box(y=data, name="{} Box Chart".format(column_name), boxpoints = False,
                                            fillcolor=cls.default_plot_config["box_color"], whiskerwidth=0.2,
                                            marker=dict(size=2,), line=dict(width=1), xaxis=sub_grah.xaxis,
                                            yaxis=sub_grah.yaxis,))
                    sub_layout = {"xaxis{}".format(sub_grah.chart_seq): dict(axis,
                                                                             **dict(domain=[0, 1], anchor=sub_grah.yaxis,
                                                                                    showticklabels=True)),
                                  "yaxis{}".format(sub_grah.chart_seq): dict(axis, **dict(domain=sub_graph_location[idx],
                                                                                          anchor=sub_grah.xaxis,
                                                                                          hoverformat='.3f'))}
                    layout.update(sub_layout)
                elif sub_grah.graph_type == "bar":
                    columns = data.columns.values
                    # to avoid two grouped bar value difference more than 50 times, we do a scale in picture
                    #values = np.mean(data.iloc[:, 1:], axis=0)
                    #scale = np.where(max(values)/values >= 50, np.round(np.round(max(values)/values/5)), 1)
                    for bar_idx in range(1, len(columns)):
                        trace_lst.append(
                            go.Bar(
                                x=data[columns[0]].values,
                                y=data[columns[bar_idx]].values, #* scale[bar_idx-1],
                                name=columns[bar_idx],
                                marker=dict(
                                    color=cls.default_plot_config["bar_marker_color"][bar_idx%len(cls.default_plot_config["bar_marker_color"])]
                                ),
                                text=data[columns[bar_idx]].values,
                                textposition='auto',
                                xaxis=sub_grah.xaxis,
                                yaxis=sub_grah.yaxis,
                            ))
                    sub_layout = {
                        "xaxis{}".format(sub_grah.chart_seq): dict(
                            tickfont=dict(
                                size=cls.default_plot_config["header_font_size"],
                                color=cls.default_plot_config["bar_axis_color"]
                            ),
                            title="{}".format(sub_grah.name),
                            **dict(domain=[0, 1], anchor=sub_grah.yaxis,
                                   showticklabels=True)
                        ),
                        "yaxis{}".format(sub_grah.chart_seq):dict(
                            #title='Number',
                            titlefont=dict(
                                size=cls.default_plot_config["cell_font_size"],
                                color=cls.default_plot_config["bar_axis_color"]
                            ),
                            tickfont=dict(
                                size=cls.default_plot_config["cell_font_size"],
                                color=cls.default_plot_config["bar_axis_color"]
                            ),
                            **dict(domain=sub_graph_location[idx],
                                   anchor=sub_grah.xaxis,
                                   hoverformat='.3f')
                        ),
                        "barmode":'group',
                        "bargap":0.15,
                        "bargroupgap":0.1
                    }
                    layout.update(sub_layout)
                else:
                    logger.warning("Unsupported sub graph type {} for column {}".format(sub_grah.graph_type, column_name))
            else:
                logger.warning("There is no {} dataframe for column {}".format(sub_grah.name, column_name))
                #return None, None, column_name
        layout.update({"title": "{} Stats".format(column_name)})
        return trace_lst, layout, column_name





