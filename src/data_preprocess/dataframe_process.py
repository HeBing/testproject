import json
import numpy as np
import pandas as pd
from darwinutils.log import get_task_logger
from darwinutils.decorator import timeit
logger = get_task_logger(__name__)


@timeit
def add_one_hot_columns(df, encode_config=[], replace=False, save_path='./one_hot_record.json'):
    '''
        encode columns using `encode_config` and save processing record file to `save_path`
    :param df:
    :param encode_config: encode defined
                  eg:  [{"name": "AGNT_SVC_TP", "bool": True},
                        {"name": "MSTR_BRC_TP", "bool": False}]
    :param replace: True: df + new_one_hot_columns   False: only new_one_hot_columns
    :param save_path:  processing file saved path
    :return:
    '''
    df_all = None
    record = {}
    for conf in encode_config:
        columns_name = conf['name'].lower()
        logger.info('one_hot: {}'.format(columns_name))
        if columns_name in df.columns:
            tmp_df = pd.get_dummies(df[columns_name], prefix=columns_name)
            if not conf.get('bool'):   # 除了one-hot增加一个unknown
                tmp_df['{}_unknown'.format(columns_name)] = np.zeros((tmp_df.shape[0], 1))
            else:
                tmp_df = pd.get_dummies(df[columns_name], prefix=columns_name)
            record[columns_name] = (tmp_df.columns.tolist())
            if df_all is None:
                df_all = tmp_df
            else:
                df_all = pd.concat([df_all, tmp_df], axis=1)
        else:
            logger.warning('column {} not in Dataframe.columns!!!'.format(columns_name))
    if replace:
        df = df.drop(list(map(lambda x: x.get('name').lower(), encode_config)), axis=1)
        df_all = pd.concat([df, df_all], axis=1)
    logger.info('save one hot records at {}'.format(save_path))
    with open(save_path, 'w') as f:
        f.write(json.dumps(record))
    return df_all, record


@timeit
def add_one_hot_columns_with_record(df, encode_config=[], encode_record={}):
    '''
        using `encode_config` and `encode_record` to process one hot columns
    :param df:
    :param encode_config: encode defined
                  eg:  [{"name": "AGNT_SVC_TP", "bool": True},
                        {"name": "MSTR_BRC_TP", "bool": False}]
    :param encode_record: one hot columns records
                  eg: {'agnt_svc_tp': ['agnt_svc_tp_1.0', 'agnt_svc_tp_10.0'],
                      'mstr_brc_tp': ['mstr_brc_tp_0.0', 'mstr_brc_tp_1.0', 'mstr_brc_tp_2.0', 'mstr_brc_tp_3.0', 'mstr_brc_tp_unknown']}
    :return:
    '''
    df_all = None
    for conf in encode_config:
        columns_name = conf['name'].lower()
        logger.info(columns_name)
        if columns_name in df.columns and columns_name in encode_record.keys():
            if not conf.get('bool'):   # 除了one-hot增加一个unknown
                tmp_df = pd.get_dummies(df[columns_name], prefix=columns_name)
                tmp_df['{}_unknown'.format(columns_name)] = np.zeros((tmp_df.shape[0], 1))
                # if exists new column
                for new_columns_name in tmp_df.columns:
                    if new_columns_name not in encode_record[columns_name]:
                        tmp_df['{}_unknown'.format(columns_name)] = tmp_df['{}_unknown'.format(columns_name)] + tmp_df[new_columns_name]
                        tmp_df = tmp_df.drop(new_columns_name, axis=1)
                tmp_df['{}_unknown'.format(columns_name)] = tmp_df['{}_unknown'.format(columns_name)].apply(lambda x: 1 if x > 0 else 0)
            else:
                tmp_df = pd.get_dummies(df[columns_name], prefix=columns_name)
                if tmp_df.shape[1] > 2:
                    logger.info('{} is not an boolean type'.format(columns_name))
            for encode_column_name in encode_record[columns_name]:
                if encode_column_name not in tmp_df.columns:
                    tmp_df[encode_column_name] = np.zeros((tmp_df.shape[0], 1))
            if df_all is None:
                df_all = tmp_df
            else:
                df_all = pd.concat([df_all, tmp_df], axis=1)
        else:
            logger.error('column {} not in Dataframe.columns!!!'.format(columns_name))
    return df_all


