#! /usr/bin/env python3
# -*- coding: utf-8 -*-

import os
import time
import random
from darwinutils.ibis_utils import DBConnector
from darwinutils.config import DARWIN_CONFIG
from darwinutils.json_op import Json
from darwinutils.log import get_task_logger
from darwinutils.helper import loop_run_cnt
import pandas as pd
from darwinutils.decorator import timeit
logger = get_task_logger(__name__)

db_connector = None


@timeit
def load_db_connector(use_hive):
    global db_connector
    if db_connector is None:
        db_connector = DBConnector(impala_host=DARWIN_CONFIG["impala_config"]["host"],
                                   impala_port=DARWIN_CONFIG["impala_config"]["port"],
                                   hdfs_host=DARWIN_CONFIG["hdfs_config"]["host"],
                                   hdfs_port=DARWIN_CONFIG["hdfs_config"]["port"],
                                   hive_host=DARWIN_CONFIG["hive_config"]["host"],
                                   hive_port=DARWIN_CONFIG["hive_config"]["port"],
                                   user=DARWIN_CONFIG["impala_config"]["user"],
                                   passwd=DARWIN_CONFIG["impala_config"]["passwd"],
                                   database=DARWIN_CONFIG["impala_config"]["database"],
                                   use_hive=use_hive)
        if db_connector.connect():
            if db_connector.connect_database():
                return db_connector
        db_connector = None
        return None
    return db_connector


@timeit
def execute(dbc, sql_str, fetch_rst, use_hive, onebyone):
    loop_times = DARWIN_CONFIG.get('loop_times') if DARWIN_CONFIG.get('loop_times') is not None else 10

    def target_func():
        run_times = 0
        while run_times <= loop_times:
            if run_times >= 1:
                logger.info('Start {} times retry'.format(run_times))
            try:
                return dbc.raw_sql(sql_str, fetch_rst=fetch_rst, onebyone=onebyone)
            except Exception as e:
                logger.exception("Failed to execute {} with fetch_rst {} and use_hive {} [{}]".format(sql_str, fetch_rst,
                                                                                                      use_hive,
                                                                                                      str(e)))
                run_times += 1
                # sleep random seconds between 2 minutes and 5 minutes
                sleep_time = random.randint(120, 300)
                logger.info('Retry after sleep {}s'.format(sleep_time))
                time.sleep(sleep_time)
        logger.error("Failed to execute {} with fetch_rst {} and use_hive {} {} tiems".format(
            sql_str, fetch_rst, use_hive, loop_times))
        return None
    try:
        rst = loop_run_cnt(target=target_func, cnt_cond_func=1,
                           timeout=DARWIN_CONFIG.get("pipeline_task_default_retry_timeout"))
        if rst is None:
            raise ValueError("Failed to execute {} with fetch_rst {} and use_hive {} {} tiems".format(sql_str,
                                                                                                            fetch_rst,
                                                                                                            use_hive,
                                                                                                            loop_times))
    except TimeoutError as e:
        # Cannot complete the sql within configured timer. We iwll return None and next node shall be aware it
        raise TimeoutError("Timeout for sql {} with fetch_rst {} and use_hive {} and timer {} [{}]".format(
            sql_str, fetch_rst, use_hive, DARWIN_CONFIG.get("pipeline_task_default_retry_timeout"), str(e)))
    return rst


@timeit
def sql_execute(sql_str, fetch_rst=True, use_hive=False, onebyone=False):
    """
    The most sql run cmd shall go this function path
    :param sql_str:
    :param fetch_rst:
    :param use_hive:
    :param onebyone:
    :return:
    """
    dbc = load_db_connector(use_hive)
    if dbc is None:
        logger.error("Failed to get db connector")
        return None

    rst = execute(dbc, sql_str, fetch_rst, use_hive, onebyone)
    '''
    if rst is not None:
        # the return result normaly be [()]. I change it to [[]]. Hope there is no side effect
        if isinstance(rst[1], list) and len(rst[1]) > 0 and isinstance(rst[1][0], tuple):
            return list(map(lambda s: list(s), rst[1]))
        else:
            return rst[1]
    '''
    if rst is not None:
        return rst[1]
    else:
        return None


@timeit
def sql_fetch(sql_str, limit=None, use_hive=False):
    dbc = load_db_connector(use_hive)
    if dbc is None:
        logger.error("Failed to get db connector")
        return None
    loop_times = DARWIN_CONFIG.get('loop_times') if DARWIN_CONFIG.get('loop_times') is not None else 10

    def target_func():
        # retry 5 times if fail
        run_times = 0
        while run_times <= loop_times:
            if run_times >= 1:
                logger.info('Start {} times retry'.format(run_times))
            try:
                result = dbc.fetch(sql_str, limit=limit)
                return result
            except Exception as e:
                run_times += 1
                logger.exception("Failed with Exception: [{}]".format(str(e)))
                # sleep random seconds between 2 minutes and 5 minutes
                sleep_time = random.randint(120, 300)
                logger.info('Retry after sleep {}s'.format(sleep_time))
                time.sleep(sleep_time)
        logger.error("Failed to execute {} with use_hive {} ".format(sql_str, use_hive))
        return None

    # TODO: if we want parallel run some function, we can update this part later
    logger.info("sql {} timer {}".format(sql_str, DARWIN_CONFIG.get("pipeline_task_default_retry_timeout")))
    try:
        rst = loop_run_cnt(target=target_func, cnt_cond_func=1,
                           timeout=DARWIN_CONFIG.get("pipeline_task_default_retry_timeout"))
        if rst is None:
            raise ValueError("Failed to execute {} with use_hive {} ".format(sql_str, use_hive))
    except TimeoutError as e:
        # Cannot complete the sql within configured timer. We iwll return None and next node shall be aware it
        raise TimeoutError("Timeout for execute {} with use_hive {}  and timer {} [{}]".format(
            sql_str, use_hive, DARWIN_CONFIG.get("pipeline_task_default_retry_timeout"), str(e)))
    return rst


@timeit
def sql_execute_from_single_file(sql_cmd_file, use_hive=False):
    if not os.path.exists(sql_cmd_file):
        logger.error("File {} not existed".format(sql_cmd_file))
        return None

    with open(sql_cmd_file, 'r') as f:
        content = f.read()

    dbc = load_db_connector(use_hive)
    if dbc is None:
        logger.error("Failed to get db connector")
        return None
    return execute(dbc, content, fetch_rst=False, use_hive=use_hive, onebyone=False)


@timeit
def sql_fetch_from_file(sql_cmd_file, use_hive=False, file_type="excel", cmd_replace_dict_file=None, **kwargs):
    cmd_lst = load_file(sql_cmd_file, file_type)
    if cmd_lst is None:
        logger.error("Failed to load sql cmd file {} file type {}".format(sql_cmd_file, file_type))
        return None
    if len(cmd_lst) == 0:
        logger.error("There is no valid sql str in file {} file type {}".format(sql_cmd_file, file_type))
        return None
    if cmd_replace_dict_file is not None:
        replace_dict = Json.file_to_json_without_comments(cmd_replace_dict_file)
    else:
        replace_dict = None

    rst_lst = []
    for idx, cmd in enumerate(cmd_lst):
        logger.info('process sql {}/{}'.format(idx, len(cmd_lst)))
        if not pd.isna(cmd[0]):
            if replace_dict is not None:
                for k, v in replace_dict.items():
                    cmd[0] = cmd[0].replace(k, v)
            if file_type.lower() == 'plain':
                rst = sql_fetch(cmd, use_hive=use_hive)
            elif file_type.lower() == 'json':
                rst = sql_fetch(cmd.get("sql_str"), use_hive=use_hive)
            else: #excel
                rst = sql_fetch(cmd[0], use_hive=use_hive)
            if rst is not None:
                rst_lst.append(rst)
            else:
                rst_lst.append(None)
    return rst_lst


@timeit
def sql_execute_from_file(sql_cmd_file, use_hive=False, file_type="excel", cmd_replace_dict_file=None, **kwargs):
    """
    Since there are multiple sql cmd in one file, in the function, we hardly combine the result so just put them in
    one list to return. Exect two type file: plain or json or excel
    Plain format:
    select * from table1
    select * from table2
    ...

    Json format:
    {
        "sql_cmd_lst": [
            {
                "sql_str": "select * from table1",
                "fetch_rst": true,
                "onebyone": false
            },
            {
                "sql_str": "select * from table2",
                "onebyone": true
            },
            {
                "sql_str": "select * from table3"
            },
            ...
        ]
    }

    Excel format:
    sql_str,              fetch_rst
    select * from table1, True
    select * from table2, False
    ...

    :param sql_cmd_file:
    :param use_hive:
    :param file_type:
    :param cmd_replace_dict_file: replace sql str value by the file
    :return:
    """

    fetch_rst = True
    onebyone = False

    dbc = load_db_connector(use_hive)
    if dbc is None:
        logger.error("Failed to get db connector")
        return None
    cmd_lst = load_file(sql_cmd_file, file_type)
    if cmd_lst is None:
        logger.error("Failed to load sql cmd file {} file type {}".format(sql_cmd_file, file_type))
        return None
    if len(cmd_lst) == 0:
        logger.error("There is no valid sql str in file {} file type {}".format(sql_cmd_file, file_type))
        return None
    replace_dict = Json.file_to_json(cmd_replace_dict_file)

    rst_lst = []
    for idx, cmd in enumerate(cmd_lst):
        if file_type.lower() == 'plain':
            logger.info('sql_str = {}'.format(cmd))
            rst = execute(dbc, cmd, fetch_rst, use_hive, onebyone)
        elif file_type.lower() == 'json':
            logger.info('sql_str = {}'.format(cmd))
            rst = execute(dbc, cmd.get("sql_str"), fetch_rst=cmd.get("fetch_rst", fetch_rst),
                          use_hive=use_hive, onebyone=cmd.get("fetch_rst", onebyone))
        else: #excel
            if not pd.isna(cmd[0]):
                if replace_dict is not None:
                    for k, v in replace_dict.items():
                        cmd[0] = cmd[0].replace(k, v)
                logger.info('sql_str = {}'.format(cmd[0]))
                rst = execute(dbc, sql_str=cmd[0], fetch_rst=cmd[1], use_hive=use_hive, onebyone=onebyone)
            else:
                logger.warning("Get Nan value while excute sql from file {} line {}".format(sql_cmd_file, idx + 2))
                continue
        if rst is not None:
            rst_lst.append(rst[1])
        else:
            rst_lst.append(None)
    return rst_lst


@timeit
def load_file(file_path, file_type):
    if file_type.lower() not in ["plain", "json", "excel"]:
        logger.error("Unsupported file type {}".format(file_type))
        return None

    if not os.path.exists(file_path):
        logger.error("File {} not existed".format(file_path))
        return None

    if file_type.lower() == 'plain':
        with open(file_path, 'r') as f:
            cmd_lst = f.readlines()
        return list(filter(lambda c: len(c) > 0, map(lambda s: s.strip(), cmd_lst)))
    elif file_type.lower() == 'json':
        from darwinutils.json_op import Json
        try:
            file_content = Json.file_to_json_without_comments(file_path)
            cmd_lst = file_content.get("sql_cmd_lst")
            if isinstance(cmd_lst, list):
                return list(filter(lambda s: isinstance(s, dict) and s.get("sql_str") is not None, cmd_lst))
            else:
                logger.error("sql cmd file {} format is not right: \n{}".format(file_path, file_content))
                return None
        except Exception as e:
            logger.exception("Failed load json type file {} with reason {}".format(file_path, str(e)))
            return None
    else:
        try:
            file_content = pd.read_excel(file_path)
        except Exception as e:
            logger.exception("Read {} as excel failed".format(file_path))
            return None

        if "sql_str" not in file_content.columns.tolist() or "fetch_rst" not in file_content.columns.tolist():
            logger.error("Excel file {} format [{}] is not right".format(file_path, file_content.columns))
            return None
        file_content["fetch_rst"] = file_content["fetch_rst"].fillna(True)
        res = file_content[["sql_str", "fetch_rst"]]
        if 'var_name' in file_content.columns.tolist():
            res['var_name'] = file_content["var_name"]
        return res.values.tolist()


