#! /usr/bin/env python3
# -*- coding:utf8 -*-


from typing import (
    Any,
    Mapping,
)
import bson
import pymongo
import numpy as np
from bson.objectid import ObjectId
from darwinutils.log import get_task_logger
from darwinutils.config import DARWIN_CONFIG


logger = get_task_logger(__name__)


def get_db(uri=None, db=None):
    """
    Create and connect to the target database in mongodb server
    :param uri: server uri
    :type uri: str
    :param: db: target database name
    :type: db: str
    :return: target database pymongo object
    :rtype: pymongo.database.Database
    """
    if uri is None:
        uri = "mongodb://{}:{}/".format(
            DARWIN_CONFIG['mongodb_host'],
            DARWIN_CONFIG['mongodb_port'],
        )
    if db is None:
        db = DARWIN_CONFIG['mongodb_name']
    c = pymongo.MongoClient(uri)
    return c[db]


# TODO: deprecate this function?
def get_type_dict():
    """
    Generate type dictionary for each table in database
    :return: A dict for each table
    :rtype: dict
    """
    return DARWIN_CONFIG['mongodb_tables']


def value_type_check(key, value, table):
    """
    Check data type of a key-value pair
    :param key: key in key-value pair
    :type key: str
    :param value: value in key-value pair
    :type: value: any
    :param table: table name to which the key-value pair belongs
    :type: table: str
    :return: None
    """
    if value is None:
        """Not check it"""
        return False, ValueError("The value of field {1} in table {0} is None.".format(table, key))
    type_dict = DARWIN_CONFIG['mongodb_tables'][table]
    design_type = eval(type_dict[key])
    real_type = type(value)

    if not isinstance(value, design_type):
        return False, TypeError("{2} expected in field [{1}] of table [{0}], find {3} instead".format(
            table, key, design_type, real_type
        ))

    # rule based validation per specific table and columns
    if table == "dl_stat" and key == "flag" and value not in ["good", "bad", "candidate"]:
        return False, ValueError("The value of field [flag] in table [dl_stat] need be good/bad/candidate!")
    '''
    if table == "ea_job" and key == "data_size":
        logger.info("The value of field [data_size] in table [ea_job] is in MByte.")
    if table == "train_data_info" and key == "data_size":
        logger.info("The value of field [data_size] in table [train_data_info] is in MByte.")
    if table == "ea_stat" and key == "real_ea_duration":
        logger.info("The value of field [real_ea_duration] in table [ea_stat] is in Hours.")
    '''

    return True, None


def record_type_check(record, table):
    """
    Check data type of a complete record
    :param record: a complete record
    :type record: dict
    :param table: table name to which the record belongs
    :type: table: str
    :return: succ or not
    :rtype: bool
    """
    include_builtin_fields = set([
        # exclude "_id" in case caller wants to specify "_id" manually instead of
        # using the auto value from mongodb itself.
        # TODO: disable this "backdoor" in GA season, agree to search "Value Check Error" in log and trace
        # TODO: them with separate defect instead of adding extra cost for the upcoming GA.
        # "_id",
    ])
    removed_keys = []
    for x in record.keys()-include_builtin_fields:
        try:
            # do not abort if an exception raised
            value_type_check(x, record[x], table)
        except KeyError as e:
            logger.exception("Value Check Error: {} for table {}".format(str(e), table))
            removed_keys.append(x)
    for key in removed_keys:
        record.pop(key)
    '''
    removed_keys = []
    for x in record.keys():
        r, msg = value_type_check(x, record[x], table)
        if r:
            continue

        # abort if an exception should be raised
        #if isinstance(msg, Exception):
            #raise msg

        # or, log warning and ignore record from target table
        logger.warning("Value Check Error: {1} for table {0}".format(table, msg))
        removed_keys.append(x)

    for key in removed_keys:
        record.pop(key)

    # TODO: how to define the fail condition
    '''
    return True


def insert_one_record(
        record: Mapping[str, Any],
        table: str,
        enforce_validate: bool = True,
) -> bson.objectid.ObjectId:
    """
    Insert one record into table
    :param record: a complete record
    :param table: table name to which the record belongs
    :param enforce_validate: enforcing validate input record
    :return: object id of new insert id
    """
    rcd = record.copy()
    db = get_db()
    table_lst = DARWIN_CONFIG['mongodb_tables']
    if table in table_lst:
        post = db[table]
        if enforce_validate:
            record_type_check(rcd, table)
        try:
            obj_id = post.insert(rcd)
        except Exception:
            logger.exception("insert_one_record in table {} failed:".format(table))
            logger.info("##################### \n{}".format(rcd))
            return None
        logger.debug("Insert record in table {} done".format(table))
        logger.debug("{}".format(rcd))
    else:
        logger.error('Do not create a new table [{}]'.format(table))
        raise ValueError('Do not create a new table [{}]'.format(table))
    return obj_id


def insert_many_records(records, table):
    """
    Insert many records into table
    :param records: several complete records
    :type records: list
    :param table: table name to which the record belongs
    :type: table: str
    :return: object id of new insert ids
    :rtype: list
    """
    db = get_db()
    table_lst = DARWIN_CONFIG['mongodb_tables']
    if table in table_lst:
        post = db[table]
        for record in records:
            record_type_check(record, table)
        try:
            obj_id = post.insert(records)
        except Exception:
            logger.exception("insert_many_records in table {} failed:".format(table))
            return None
        logger.debug("Insert %d records in table {} successfully.".format(len(records), table))
    else:
        logger.error('Do not create a new table [{}]'.format(table))
        raise Exception('Do not create a new table [{}]'.format(table))
    return obj_id


def delete_one_by_id(id, table):
    """
    Delete one record by object id
    :param id: object id
    :type id: str
    :param table: table name which will be delete record
    :type: table: str
    :return: succ or not
    :rtype: bool
    """
    if id is None or table is None:
        logger.error("Input parameters are None")
        return False

    db = get_db()
    post = db[table]
    try:
        try:
            post.remove(ObjectId(str(id)))
        except Exception:
            logger.exception("delete_one_by_id in table {} failed:".format(table))
            return False
        logger.debug("Remove one record from table {} successfully.".format(table))
    except Exception as e:
        logger.error("Remove one record from table {} failed. {}".format(table, str(e)))
        return False

    return True


def delete_many_by_conds(condition=None, table=None):
    """
    Delete many records by object condition
    :param condition: delete condition
    :type condition: dict
    :param table: table name which will be delete records
    :type: table: str
    :return: succ or not
    :rtype: bool
    """
    if table is None and condition is None:
        logger.error("Invalid condition {} or table {}".format(condition, table))
        return False
    db = get_db()
    post = db[table]
    c0 = get_coll_count(table)
    try:
        post.remove(condition)
    except Exception:
        logger.exception("delete_many_by_conds in table {} failed:".format(table))
        return False
    c1 = get_coll_count(table)
    logger.debug("Remove {} record from table {} condition {} successfully.".format(c0-c1, table, condition))
    # TODO: how to confirm if the delete operation was really succ or not
    return True


def update_one_by_id(id, field, value, table):
    """
    Update one field of a record by object id
    :param id: target object id
    :type id: str
    :param field: field need to be updated
    :type: field: str
    :param value: update value of field
    :type: value: any
    :param table: table name which will be updated
    :type: table: str
    :return: success or not
    :rtype: bool
    """
    if id is None:
        logger.error("Wrong mongodb id")
        return False
    db = get_db()
    post = db[table]
    obj = get_one_by_id(id, table)
    if obj is not None:
        r, msg = value_type_check(field, value, table)
        if r:
            obj[field] = value
            try:
                post.save(obj)
            except Exception:
                logger.exception("update_one_by_id in table {} failed:".format(table))
                return False
            logger.debug("Update one record in table {} successfully.".format(table))
        elif isinstance(msg, Exception):
            logger.error(msg)
            raise msg
        else:
            logger.warning("Update record {1} in table {0} failed by value_type_check {3}".format(
                table, id, msg))
            return False
    else:
        logger.error("You are trying to update an non-exist record, {1}, in table {0}!".format(table, id))
        raise KeyError("You are trying to update an non-exist record, {1}, in table {0}!".format(table, id))
    return True


def update_record_by_id(id, record, table):
    """
    Update one record by object id
    :param id: target object id
    :type id: str
    :param field: field need to be updated
    :type: field: str
    :param value: update value of field
    :type: value: any
    :param table: table name which will be updated
    :type: table: str
    :return: succ or not
    :rtype: bool
    """
    db = get_db()
    post = db[table]
    obj = get_one_by_id(id, table)
    if record is None or len(record.items()) == 0:
        logger.error("Invalid record for id {}".format(id))
        return None
    if obj is not None:
        record_type_check(record, table)
        '''
        r = record_type_check(record, table)
        if r:
            logger.error('record type check failed in table {}, {}'.format(
                table, record
            ))
            return False
        '''
        record['_id'] = id
        try:
            post.save(record)
        except Exception:
            import sys, pickle
            logger.exception("update_record_by_id to table {} failed. Record size {}:".format(table, sys.getsizeof(pickle.dumps(record))))
            return None
        logger.debug("Update one record in table {} successfully.".format(table))
        # need return id to support check in data_info_interface

        return id
    else:
        logger.error("You are trying to update an empty record!")
        return None


def update_many_by_value(field, value_old, value_new, table):
    """
    Update many records by value
    :param field: field need to be updated
    :type: field: str
    :param value_old: value of field need to be replaced
    :type: value_old: any
    :param value_new: update value of field
    :type: value_new: any
    :param table: table name which will be updated
    :type: table: str
    :return: success or not
    :rtype: bool
    """
    db = get_db()
    post = db[table]
    obj = get_many_by_conds(condition={field: value_old}, table=table)
    if obj is not None:
        r, msg = value_type_check(field, value_new, table)
        if r:
            try:
                post.update_many({field: value_old}, {'$set': {field: value_new}})
            except Exception:
                logger.exception("update_many_by_value in table {} failed:".format(table))
                return False
            logger.debug("Update {} record successfully in table {}.".format(len(obj), table))

        elif isinstance(Exception, msg):
            logger.error(msg)
            raise msg
        else:
            logger.error("Update table {} failed {}".format(table, msg))
            return False
    else:
        logger.error("You are trying to update empty records!")
        return False
    return True

# TODO: useless???
'''
def get_all_colls():
    """
    Get all collection names in database
    :return: collections names
    :rtype: list
    """
    db = get_db()
    return db.collection_names()
'''

def get_coll_count(table):
    """
    Get records number of target table
    :param: table: table name need to be count
    :type: table: str
    :return: records number
    :rtype: int
    """
    db = get_db()
    post = db[table]
    try:
        count = post.count()
    except Exception:
        logger.exception("update_many_by_value in table {} failed:".format(table))
        return 0
    return count

def get_one_by_id(id, table):
    """
    Get one record by object id
    :param: table: target table name
    :type: table: str
    :return: record with target object id
    :rtype: dict
    """
    db = get_db()
    post = db[table]
    try:
        obj = post.find_one({"_id": ObjectId(str(id))})
    except Exception:
        logger.exception("update_many_by_value in table {} failed:".format(table))
        return None
    logger.debug("Find one record from table {}.".format(table))
    return obj

def get_many_by_conds(table, condition=None, sort_keys=None, descend=False, fields=None):
    """
    Get many record by condition
    :param condition: filter condition
    :type condition: dict
    :param: table: target table name
    :type: table: str
    :return: records with target object id
    :rtype: list
    """
    db = get_db()
    post = db[table]
    fields_dict = {}
    if fields is not None and len(fields) > 0:
        fields_dict = dict(map(lambda s: (s, 1), fields))
    try:
        if sort_keys is None or len(sort_keys) == 0:
            if len(fields_dict) == 0:
                cursor = post.find(condition)
            else:
                cursor = post.find(condition, fields_dict)
        else:
            sort_cond = []
            for key in sort_keys:
                if descend:
                    sort_cond.append((key, -1))
                else:
                    sort_cond.append((key, 1))
            if len(fields_dict) == 0:
                cursor = post.find(condition).sort(sort_cond)
            else:
                cursor = post.find(condition, fields_dict).sort(sort_cond)
        objs = [x for x in cursor]
    except Exception:
        logger.exception("get_many_by_conds in table {} failed:".format(table))
        return None
    logger.debug("Find {} records from table {}.".format(len(objs), table))
    return objs

def get_all_by_conds_fields(fields, table, condition=None, sort_keys=None, descend=False):
    '''
    Query records from a table by condition and return a specific list of fields
    :param fields:
    :param table:
    :param condition:
    :return: list or None
    '''
    db = get_db()
    post = db[table]
    objs = []
    if isinstance(fields, list):
        field_filter = dict(map(lambda s: (s, 1), fields))
        field_filter = {"_id": 0}
        try:
            if sort_keys is None or len(sort_keys) == 0:
                cursor = post.find(condition, field_filter)
            else:
                sort_cond = []
                for key in sort_keys:
                    if descend:
                        sort_cond.append((key, -1))
                    else:
                        sort_cond.append((key, 1))
                cursor = post.find(condition, field_filter).sort(sort_cond)

            objs = [x for x in cursor]
        except Exception:
            logger.exception("get_all_by_conds_fields in table {} failed:".format(table))
            return None
        logger.debug("Find {} records from table {}.".format(len(objs), table))
        return objs
    return None

