We need define the followed tables:

* # GenePool basic op performance table: 
| Key1             | Value1           | Value2                |
| ---------------- |:----------------:| ---------------------:|
| Base DL Op Name  | Estimated GFlops | Real Benchmark        |
|                  |                  | Result Real Time(s)   |


Input datasize is fixed as baseline such as 227*227*3

* #EA Job Table:
| Key1        | Key2              | Key3                          | Key4 | key5 | Key6 | key7 | Key8 | Key9 | key10 |Value |
| ----------- |:-----------------:|:-----------------------------:|:----:|:----:|:----:|:----:|:----:|:----:|:-----:|:----:|
| EA Job Name | Max Neural Number | Max Job Number per Generation | Target Object(classification, regression, object detection, segmentation and so on)|Initiated Policy Index|Last Policy Index|Data Size (MByte)|Start Time|End Time|EA Job Status| DL Job Name List|

* # EA Statistic Information:
| Key1        | Value             |
| ----------- |:-----------------:|
| EA Job Name | EA Statistic Information |
* ## EA Statistic Information include:
1. Best loss
2. Real Generation number
3. Real EA duration (Hours)
4. Last Policy ID
5. Initiated Policy ID
6. <"Keep Best" DL Job ID: (Graph Fingerprint, loss)>
7. Statistic EA op such as Mutation:100, Random:123 per Generation
8. Statistic DL basic op type such as Conv:100 per Generation
9. Statistic Good candidate Iteration Number * Batchsize / Total Number per Generation
10. Job number per Generation

* # DL Job Statistic Information:
| Key1        | Value             |
| ----------- |:-----------------:|
| DL Job Name | DL Job Statistic Information |
* ## DL Job Statistic Information include:
1. last loss
2. Graph embedding or fingerprint
3. Hyper-parameter collection
4. Basic DL op type statistic infor: such as conv2d:100 ops, activation: 10 ops and so on
5. Flag: Bad, Good, Candidate
6. Parent Network Job Name
7. Diff_loss(parent - Child)
8. EA Op Type
9. Start Time
10. End Time
11. Generation ID
12. Iteration number
13. every 10 iteration gradient Average of each basic op
14. every 10 iteration weight/parameter Average of each basic op
15. Network graph model link
16. Weight file link
17. Job Status

* # Policy Statistic Information:
| Key1        | Value             |
| ----------- |:-----------------:|
| Policy ID   | Policy Information |
* ## Policy Information Include:
1. EA op possibility
2. Transfer possibility between DL op 
3. Good Statistic Number
4. Candidate Statistic Number

* # Training Data Information:
| Key1        | Key2             | key3    | key4    | key5| value|
| ----------- |:-----------------:|:-----------|:-----|:----|:----|
| Dataset Name   | Owner | Create Time |status|data size|data set information|
* ## Dataset Information Include:
1. Data type (image, txt, csv,...)
2. DB Backend (raw, tfrecord, lmdb)
3. Data Dimensions (if it is image and resize it may be 256*256*3 or 256*256*1(gray), if it is csv, it may be 1000*800, if it is not resized, it may be any*any*3)
4. Data path (training:/x/y/, val:/x/z, test:/x/x)
5. Category number
6. Category Statistic: label_name: number
7. Dataset ratio: train:(800,0.8), test:(100,0.1), val:(100,0.1)
8. Special Information: It is a dict and can be different key for different type of data{image_mean:[123,123,345], miss_value_column:[col1, col2]}