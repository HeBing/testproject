#!/usr/bin/env bash

python generate_train_data.py --pipeline_file=../../risk_merchants_classification/tran_mrchnt_pipeline.json --input_param_file=../../risk_merchants_classification/tran_mrchnt_pipeline_risk_mrchant_params.json --base_path=../../risk_merchants_classification --stats_period=30 --stats_date_file=../../risk_merchants_classification/risk_mrchant_stats_date_file.txt --output_folder=30_risk_df
python generate_train_data.py --pipeline_file=../../risk_merchants_classification/tran_mrchnt_pipeline.json --input_param_file=../../risk_merchants_classification/tran_mrchnt_pipeline_good_mrchant_params.json --base_path=../../risk_merchants_classification --stats_period=30 --stats_date_file=../../risk_merchants_classification/good_mrchant_stats_date_file.txt --output_folder=30_good_df
