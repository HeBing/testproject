import argparse
import os
import json
import shutil
import numpy as np
import pandas as pd
from sklearn.utils import shuffle
from darwinutils.log import get_task_logger
from data_preprocess.dataframe_process import add_one_hot_columns
from data_preprocess.sql_executor import sql_fetch

logger = get_task_logger(__name__)


def assign_risk_merchnt_with_label(risk_records_file, output_folder):
    '''
        1. read risk merchant id from table `t_risk_merchant`
        2. assign code to different risk type
        3. save risk merchant with label to disk
    :param risk_records_file: all risk merchant sliding window sample
    :return: file name of risk merchant with label
    '''
    # get risk merchant key word from db
    db_risk_df = sql_fetch('select * from t_risk_merchant')
    db_risk_list = db_risk_df[db_risk_df['risk_type'] == '赌博']['mrchnt_key_wrd'].tolist()
    bm_risk_list = db_risk_df[db_risk_df['risk_type'] == '不明']['mrchnt_key_wrd'].tolist()
    tl_risk_list = db_risk_df[db_risk_df['risk_type'].isin(['内部专业套利', '部分套利', '套利'])]['mrchnt_key_wrd'].tolist()
    qz_risk_list = db_risk_df[db_risk_df['risk_type'] == '欺诈']['mrchnt_key_wrd'].tolist()
    tx_risk_list = db_risk_df[db_risk_df['risk_type'].isin(['内部专业套现', '部分套现', '套现'])]['mrchnt_key_wrd'].tolist()
    logger.info('(赌博){} + (不明){} + (套利){} + (欺诈){} + (套现){} = {}'.format(len(db_risk_list), len(bm_risk_list),
                                                                         len(tl_risk_list), len(qz_risk_list),
                                                                         len(tl_risk_list),
                                                                         len(db_risk_list) + len(bm_risk_list) + len(
                                                                             tl_risk_list) + len(qz_risk_list) + len(
                                                                             tx_risk_list)))

    # assign code to risk merchant
    # 不明：套利：套现：欺诈：赌博 = 1：2：3：4：5
    logger.info('reading {}'.format(risk_records_file))
    df_risk = pd.read_csv(risk_records_file)
    df_risk.loc[df_risk['mrchnt_key_wrd'].astype(str).isin(bm_risk_list), 'label'] = 1
    df_risk.loc[df_risk['mrchnt_key_wrd'].astype(str).isin(tl_risk_list), 'label'] = 2
    df_risk.loc[df_risk['mrchnt_key_wrd'].astype(str).isin(tx_risk_list), 'label'] = 3
    df_risk.loc[df_risk['mrchnt_key_wrd'].astype(str).isin(qz_risk_list), 'label'] = 4
    df_risk.loc[df_risk['mrchnt_key_wrd'].astype(str).isin(db_risk_list), 'label'] = 5
    logger.info('Successfully assign code to risk merchant')
    logger.info(df_risk['label'].value_counts())
    # df_risk_saved_path = risk_records_file
    prefix, file_name = os.path.split(risk_records_file)
    shotname, extension = os.path.splitext(file_name)
    if output_folder is not None:
        df_risk_with_label_saved_path = os.path.join(output_folder, '{}_with_label{}'.format(shotname, extension))
    else:
        df_risk_with_label_saved_path = os.path.join(prefix, '{}_with_label{}'.format(shotname, extension))
    logger.info('Saving risk merchant with label to {}   shape = {}'.format(df_risk_with_label_saved_path, df_risk.shape))
    df_risk.to_csv(df_risk_with_label_saved_path, index=False)
    return df_risk_with_label_saved_path


def preprocess_good_and_risk_merchant(risk_merchant_with_label_file, good_merchant_file, eva_only_risk_ids, eva_only_good_ids, cols_to_drop_list=None, encode_config=None, random_state=31, output_folder=None, pos_train_set_num=200000):
    '''
        1. concat risk_merchant_with_label and good_merchant
        2. split data set into three part: eva_only(merchant ids in eva_only_files), ea_all(15W random pos samples+ all neg samples except eva_only) and others(all pos samples - 15W random pos samples)
        3. save threee data sets
    :param risk_merchant_with_label_file: returned from assign_risk_merchnt_with_label function
    :param good_merchant_file: good merchant samples from pipeline
    :param eva_only_risk_ids: risk merchant ids for evaluation only
    :param eva_only_good_ids:  good merchant ids for evaluation only
    :param cols_to_drop_list:  columns to be dropped before running
    :param encode_config: config to encode data set with one hot
    :param random_state: random sampler random seed
    :return:
    '''
    output_folder = output_folder if output_folder is not None else os.path.split(risk_merchant_with_label_file)[0]
    path_prefix_merchant_ids = os.path.join(output_folder, 'data_with_merchant_ids')
    if not os.path.exists(path_prefix_merchant_ids):
        os.makedirs(path_prefix_merchant_ids)
        logger.info('create directory {}'.format(path_prefix_merchant_ids))
    path_prefix_ea_train = os.path.join(output_folder, 'data_ea_train')
    if not os.path.exists(path_prefix_ea_train):
        os.makedirs(path_prefix_ea_train)
        logger.info('create directory {}'.format(path_prefix_ea_train))
    path_prefix_eva_only = os.path.join(output_folder, 'data_evaluation_only')
    if not os.path.exists(path_prefix_eva_only):
        os.makedirs(path_prefix_eva_only)
        logger.info('create directory {}'.format(path_prefix_eva_only))

    logger.info('reading {}'.format(risk_merchant_with_label_file))
    df_risk=pd.read_csv(risk_merchant_with_label_file).fillna(0, axis=1)
    df_risk.replace(-np.inf, 0, inplace=True)
    logger.info('reading {}'.format(good_merchant_file))
    df_good=pd.read_csv(good_merchant_file).fillna(0, axis=1)
    df_good.replace(-np.inf, 0, inplace=True)
    df_good['label'] = [0] * df_good.shape[0]
    merged_df=pd.concat([df_risk, df_good], ignore_index=True)

    if cols_to_drop_list is not None and type(cols_to_drop_list) == list:
        to_be_drop_list = set(merged_df.columns).intersection(cols_to_drop_list)
        logger.info('cols drop: {} \n real cols drop : {}'.format(cols_to_drop_list, to_be_drop_list))
        merged_df.drop(to_be_drop_list, axis=1, inplace=True)
    else:
        logger.info('no columns to drop')

    merged_df, record_dict = add_one_hot_columns(merged_df, encode_config=encode_config, replace=True,
                                                 save_path=os.path.join(path_prefix_merchant_ids, 'one_hot_record.json'))
    merged_df["label"] = merged_df["label"].astype(int)
    eva_only_good_records = merged_df[merged_df['mrchnt_key_wrd'].isin(eva_only_good_ids)]
    eva_only_risk_records = merged_df[merged_df['mrchnt_key_wrd'].isin(eva_only_risk_ids)]

    # save evaluation only data with merchant ids
    eva_only_records = pd.concat([eva_only_good_records, eva_only_risk_records], axis=0, ignore_index=True)
    eva_only_all_records_file = os.path.join(path_prefix_merchant_ids, 'evaluation_only_all_records_with_ids.csv')
    logger.info('saving evaluation only all records with ids at {}   shape = {}'.format(eva_only_all_records_file, eva_only_records.shape))
    eva_only_records.to_csv(eva_only_all_records_file, index=False)

    # save evaluation only data without merchant ids
    eva_only_records = eva_only_records.drop(['mrchnt_key_wrd'], axis=1)
    if 'collection_date' in eva_only_records.columns:
        eva_only_records.drop(['collection_date'], axis=1, inplace=True)
    eva_only_all_records_without_ids = os.path.join(path_prefix_eva_only, 'evaluation_only_all_records_without_ids.csv')
    logger.info('saving evaluation only all records without ids at {}   shape = {}'.format(eva_only_all_records_without_ids, eva_only_records.shape))
    eva_only_records.to_csv(eva_only_all_records_without_ids, index=False)

    # merged_df = total_df - eva_only
    merged_df = merged_df[-merged_df['mrchnt_key_wrd'].isin(eva_only_good_ids)]
    merged_df = merged_df[-merged_df['mrchnt_key_wrd'].isin(eva_only_risk_ids)]

    # ea_all only need 15W good samples
    """
    merged_df_good_15w = merged_df[merged_df['label'] == 0].sample(n=pos_train_set_num, random_state=random_state)
    
    """
    good_merch_df = merged_df[merged_df['label'] == 0]
    if pos_train_set_num < len(good_merch_df):
        ratio = float(pos_train_set_num / len(good_merch_df))
        good_merch_gb = good_merch_df.groupby("mrchnt_key_wrd")
        merge_lst = []
        for key, df in good_merch_gb:
            merge_lst.append(df.sample(n=int(ratio * len(df)), random_state=31))
        merged_df_good_15w = pd.concat(merge_lst, ignore_index=True)
        merged_df_good_15w_exclude = merged_df[merged_df['label'] == 0][
            ~merged_df[merged_df['label'] == 0].index.isin(merged_df_good_15w.index)]
        other_good_merchant_file = os.path.join(path_prefix_merchant_ids, 'other_good_merchant_data.csv')
        logger.info('saving other good merchant data at {}'.format(other_good_merchant_file))
        merged_df_good_15w_exclude.to_csv(other_good_merchant_file,index=False)
    else:
        merged_df_good_15w = good_merch_df

    # save ea_all with ids
    logger.info('shuffle ea_all')
    ea_all = shuffle(pd.concat([merged_df[merged_df['label'] > 0], merged_df_good_15w], axis=0, ignore_index=True))
    ea_all_path_with_ids = os.path.join(path_prefix_merchant_ids, 'ea_all_with_ids.csv')
    logger.info('saving ea all with ids at {} shape = {}'.format(ea_all_path_with_ids, ea_all.shape))
    ea_all.to_csv(ea_all_path_with_ids, index=False)

    # save ea_all without ids
    ea_all.drop(['mrchnt_key_wrd'], axis=1, inplace=True)
    ea_all_path_without_ids = os.path.join(path_prefix_ea_train, 'ea_all.csv')
    logger.info('saving ea all data without ids at {}   shape = {}'.format(ea_all_path_without_ids, ea_all.shape))
    if 'collection_date' in ea_all.columns:
        ea_all.drop(['collection_date'], axis=1, inplace=True)
    ea_all.to_csv(ea_all_path_without_ids, index=False)
    logger.info('ea_all :\n {}'.format(ea_all['label'].value_counts()))


def main(args_flag):
    if not os.path.exists(args_flag.risk_df_file):
        logger.error("risk_df_file path {} is not valid".format(args_flag.risk_df_file))
        exit(1)
    if not os.path.exists(args_flag.good_df_file):
        logger.error("good_df_file path {} is not valid".format(args_flag.good_df_file))
        exit(1)
    if not os.path.exists(args_flag.good_eva_only_ids_file):
        logger.error("good_eva_only_ids_file path {} is not valid".format(args_flag.good_eva_only_ids_file))
        exit(1)
    if not os.path.exists(args_flag.risk_eva_only_ids_file):
        logger.error("risk_eva_only_ids_file path {} is not valid".format(args_flag.risk_eva_only_ids_file))
        exit(1)

    risk_records_file = args_flag.risk_df_file
    good_records_file = args_flag.good_df_file
    eva_only_good_merchant_file = args_flag.good_eva_only_ids_file
    eva_only_risk_merchant_file = args_flag.risk_eva_only_ids_file
    output_folder = args_flag.output_folder
    cols_to_drop_list = None
    encode_config = [
        {
            "name": "AGNT_SVC_TP",
            "description": "商户所属代理商类型",
            "bool": False
        },
        {
            "name": "MSTR_BRC_TP",
            "description": "商户所属总分店类型",
            "bool": False
        },
        {
            "name": "MRCHNT_CRPRTN_PRPTY",
            "description": "商户所属企业性质类型",
            "bool": False
        },
        {
            "name": "DSPNSBLE_PWD_FLG",
            "description": "商户是否开通小额双免",
            "bool": True
        },
        {
            "name": "STLMNT_BANK_NO",
            "description": "商户所属结算卡对应的银行编号",
            "bool": False
        },
        {
            "name": "reg_night",
            "description": "是否夜间注册",
            "bool": True
        }
    ]

    if not os.path.exists(output_folder):
        os.makedirs(output_folder)
        logger.info('create {}')

    df_risk_with_label_saved_path = assign_risk_merchnt_with_label(risk_records_file, output_folder)

    eva_only_good_df = pd.read_csv(eva_only_good_merchant_file)
    eva_only_good_ids = eva_only_good_df['mrchnt_key_wrd'].tolist()
    eva_only_risk_df = pd.read_csv(eva_only_risk_merchant_file)
    eva_only_risk_ids = eva_only_risk_df['mrchnt_key_wrd'].tolist()

    preprocess_good_and_risk_merchant(risk_merchant_with_label_file=df_risk_with_label_saved_path,
                                      good_merchant_file=good_records_file,
                                      eva_only_risk_ids=eva_only_risk_ids,
                                      eva_only_good_ids=eva_only_good_ids,
                                      cols_to_drop_list=cols_to_drop_list,
                                      encode_config=encode_config,
                                      random_state=31,
                                      output_folder=output_folder)

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '--good_df_file',
        type=str,
        help='To be processed good dataframe file full path',
        required=True
    )
    parser.add_argument(
        '--risk_df_file',
        type=str,
        help='To be processed risk dataframe file full path',
        required=True
    )
    parser.add_argument(
        '--good_eva_only_ids_file',
        type=str,
        help='To be evaluated good merchant ids file',
        required=True
    )
    parser.add_argument(
        '--risk_eva_only_ids_file',
        type=str,
        help='To be evaluated risk merchant ids file',
        required=True
    )
    parser.add_argument(
        '--output_folder',
        type=str,
        help='output folder',
        required=True
    )
    args_flag, unparsed = parser.parse_known_args()
    main(args_flag)