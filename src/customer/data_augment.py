import os
import random
import argparse
import numpy as np
import pandas as pd
from sklearn.utils import shuffle
from darwinutils.log import get_task_logger

logger = get_task_logger(__name__)
'''
example:  python data_augment.py  --train_file /data/workspace/yanghang/workspace/random_df.csv   --evaluation_file /data/workspace/yanghang/workspace/random_df_evaluation_only.csv   --sample_columns_file /data/workspace/yanghang/workspace/random_df_column_names.csv  --label_scope 1 2 3 4 5 --label_name label --augment_ratio 0.1 --std_n 1 --output ./augment_data
example:  python data_augment.py  
            --train_file /data/workspace/yanghang/workspace/random_df.csv   
            --evaluation_file /data/workspace/yanghang/workspace/random_df_evaluation_only.csv   
            --sample_columns_file /data/workspace/yanghang/workspace/random_df_column_names.csv  
            --label_scope 1 2 3 4 5 
            --label_name label 
            --augment_ratio 0.1 
            --std_n 1 
            --dropout
            --output ./augment_data
'''

def main(args_flag):
    logger.info(args_flag)
    train_file = args_flag.train_file
    evaluation_file = '' if args_flag.evaluation_file is None else args_flag.evaluation_file
    sample_columns_file = '' if args_flag.sample_columns_file is None else args_flag.sample_columns_file
    label_scope = args_flag.label_scope
    label_name = args_flag.label_name
    augment_ratio = args_flag.augment_ratio
    std_n = args_flag.std_n
    output_folder = args_flag.output_folder
    dropout = args_flag.dropout
    drop_sample_ratio = args_flag.drop_sample_ratio
    drop_loop_num = args_flag.drop_loop_num
    drop_column_ratio = args_flag.drop_column_ratio

    df_all = None
    train_df = None
    if not os.path.exists(train_file):
        logger.error('train_file ERROR: {}'.format(train_file))
        exit(1)
    else:
        logger.info('reading {}'.format(train_file))
        train_df = pd.read_csv(train_file)
        df_all = train_df
    if os.path.exists(evaluation_file):
        logger.info('reading {}'.format(evaluation_file))
        evaluation_df = pd.read_csv(evaluation_file)
        df_all = pd.concat([train_df, evaluation_df], axis=0, ignore_index=True)

    sample_columns = None
    if os.path.exists(sample_columns_file):
        sample_columns = list(pd.read_csv(sample_columns_file).columns)
    else:
        logger.error('sample_columns_file ERROR: {}'.format(sample_columns_file))

    # check if sample_columns valid
    tmp_sample_columns = []
    for column_name in sample_columns:
        if column_name in df_all.columns:
            tmp_sample_columns.append(column_name)
    sample_columns = tmp_sample_columns
    logger.info('sample_columns = {}'.format(sample_columns))
    std_df = df_all[sample_columns].std()
    mean_df = df_all[sample_columns].mean()

    for label_index in label_scope:
        tmp_df = train_df[train_df[label_name] == label_index]
        sample_num = min(int(tmp_df.shape[0] * augment_ratio), tmp_df.shape[0])
        logger.info("label = {} shape= {} sample_num= {} ".format(label_index, tmp_df.shape, sample_num))

        sample_df = tmp_df.sample(n=sample_num)
        #logger.info('\t before sample_df[sample_columns].shape = {}'.format(sample_df[sample_columns].shape))
        dtype_origin = sample_df.dtypes
        # prepare random sample_columns df within `std_n` standard deviations
        random_std_columns_df = pd.DataFrame()
        for column_name in sample_columns:
            logger.info('\t column = {}  std_n = {}   dtype = {}   std = {}'.format(column_name, std_n, tmp_df[column_name].dtype.kind, std_df.loc[column_name]))
            if tmp_df[column_name].dtype.kind == 'i':
                col_dist = df_all[column_name].value_counts(normalize=True)
                random_std_columns_df[column_name] = np.random.choice(col_dist.index.values, size=sample_df.shape[0], p=col_dist.values)
            else:
                random_std_columns_df[column_name] = sample_df[column_name] + np.random.normal(loc=0, scale=std_n * std_df.loc[column_name], size=(sample_df.shape[0], ))
        sample_df[sample_columns] = random_std_columns_df.values
        sample_df = sample_df.astype(dtype_origin)
        logger.info('before : \n{}'.format(train_df.label.value_counts().sort_index()))
        train_df = pd.concat([train_df, sample_df], axis=0, ignore_index=True)
        logger.info('after : \n{}'.format(train_df.label.value_counts().sort_index()))

    if output_folder is None:
        output_file = '{}_augmented.csv'.format(os.path.splitext(os.path.basename(train_file))[0])
    else:
        if not os.path.exists(output_folder):
            os.makedirs(output_folder)
        output_file = os.path.join(output_folder,
                                   '{}_augmented.csv'.format(os.path.splitext(os.path.basename(train_file))[0]))
    logger.info('writing data to {}'.format(output_file))
    train_df.to_csv(output_file, index=False)

    # dropout
    if dropout:
        logger.info('processing dropout...')
        drop_sample_block_num = int(drop_sample_ratio * train_df.shape[0])
        random_sample_idx = random.sample(range(0, train_df.shape[0]), drop_sample_block_num * drop_loop_num)
        drop_columns_num = int(train_df.shape[1] * drop_column_ratio)
        origin_columns_names = list(train_df.columns)
        origin_columns_names.remove('label')
        df_list = [train_df]
        for idx in range(drop_loop_num):
            logger.info('total {}   start {}   end {}'.format(len(random_sample_idx), idx*drop_sample_block_num, (idx + 1)*drop_sample_block_num))
            df_index = random_sample_idx[idx*drop_sample_block_num: (idx + 1)*drop_sample_block_num]
            drop_columns_names = random.sample(origin_columns_names, drop_columns_num)
            #logger.info('drop_columns_names = {}'.format(drop_columns_names))
            tmp_df = train_df.loc[train_df.index.isin(df_index)]
            for drop_column_name in drop_columns_names:
                tmp_df[drop_column_name] = 0
            df_list.append(tmp_df)
        train_df = pd.concat(df_list, axis=0, ignore_index=True)

    if output_folder is None:
        output_file = '{}_augmented_dropout.csv'.format(os.path.splitext(os.path.basename(train_file))[0])
    else:
        output_file = os.path.join(output_folder,
                                   '{}_augmented_dropout.csv'.format(os.path.splitext(os.path.basename(train_file))[0]))
    logger.info('writing data to {}'.format(output_file))
    train_df.to_csv(output_file, index=False)



if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '--train_file',
        type=str,
        help='Train data set file',
        required=True
    )
    parser.add_argument(
        '--evaluation_file',
        type=str,
        help='Evaluation only data set file',
        required=False
    )
    parser.add_argument(
        '--sample_columns_file',
        type=str,
        help='To be augment sample columns file',
        required=True
    )
    parser.add_argument(
        '--label_scope',
        type=int,
        help='To be augmented sample label list',
        required=True,
        nargs='+'
    )
    parser.add_argument(
        '--label_name',
        type=str,
        help='Label column name. default set "label"',
        required=False,
        default='label'
    ),
    parser.add_argument(
        '--augment_ratio',
        type=float,
        help='Data augment ratio. default set 0.3',
        required=False,
        default=0.3
    )
    parser.add_argument(
        '--std_n',
        type=int,
        help='Within "std_n" standard deviations. default set 1',
        required=False,
        default=1
    )
    parser.add_argument(
        '--dropout',
        help='whether dropout',
        action='store_true',
        default=False
    )
    parser.add_argument(
        '--drop_sample_ratio',
        type=float,
        help='To be dropped sample ratio every loop',
        required=False,
        default=0.05
    )
    parser.add_argument(
        '--drop_loop_num',
        type=int,
        help='loop sample num',
        required=False,
        default=10
    )
    parser.add_argument(
        '--drop_column_ratio',
        type=float,
        help='To be dropped random columns ratio every loop',
        required=False,
        default=0.2
    )
    parser.add_argument(
        '--output_folder',
        type=str,
        help='output folder name',
        required=False
    )
    args_flag, unparsed = parser.parse_known_args()
    main(args_flag)