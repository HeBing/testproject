import argparse

from darwinutils.json_op import Json
from pipeline.pipeline import PipeLine
from pipeline.pipetask import PipeTask
from darwinutils.log import get_task_logger

logger = get_task_logger(__name__)

def main(args_flag):
    pipeline_file = './data_process_pipeline.json'
    pipeline = Json.file_to_json_without_comments(pipeline_file)
    p = PipeLine(pipeline)
    task = PipeTask(p)
    params = {
        "input1": args_flag.df_glob,
        "input2": args_flag.output_folder
    }
    r = task(**params)
    logger.info('Run Complete!')


'''
example: python data_process.py  --df_glob '1_*/full*' --output_folder /data/share/yanghang/notebook/data_0507_1
'''
if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '--df_glob',
        type=str,
        help='good and risk df file glob define',
        required=True
    )
    parser.add_argument(
        '--output_folder',
        type=str,
        help='output folder',
        required=True
    )
    args_flag, unparsed = parser.parse_known_args()
    main(args_flag)