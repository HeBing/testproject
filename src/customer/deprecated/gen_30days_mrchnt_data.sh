#!/usr/bin/env bash

python data_extractor.py --pipeline_file pipeline/data_extractor/30days/mrchnt_pipeline_save_file.json --input_dict '{"input1": "pipeline/data_extractor/30days/tran_all_table.sql", "input2": "pipeline/data_extractor/30days/gen_real_mrchnt_table.sql",  "input3": "pipeline/data_extractor/30days/mrchnt_stat.xlsx", "input4": "pipeline/data_extractor/30days/replace_files/tran_all_repalce_dict_dec.json", "input5": "pipeline/data_extractor/30days/main_df.sql", "input6": "pipeline/data_extractor/30days/mrchnt_stat_formula.xlsx", "input7": "./output_mrchnt.csv"}'

