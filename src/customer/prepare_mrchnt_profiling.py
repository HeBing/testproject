#! /usr/bin/env python3
# -*- coding: utf-8 -*-
import os
import argparse
import pandas as pd
from darwinutils.log import get_task_logger
logger = get_task_logger(__name__)

excel_file = "../../data/merchant_profiling.xlsx"

xlsfile = pd.ExcelFile(excel_file)
sheet_names = xlsfile.sheet_names

merged_df = []
for sheet_idx in range(1, len(sheet_names)):
    df = pd.read_excel(excel_file, sheet_name=sheet_names[sheet_idx])
    merchant_ids = list(set(df["商户编号"].tolist()))
    new_df = pd.DataFrame.from_dict({"MNO": merchant_ids, "label": [sheet_idx]*len(merchant_ids)})
    merged_df.append(new_df)

merged_df = pd.concat(merged_df, ignore_index=True)
for key, tmp_df in merged_df.groupby("MNO"):
    if len(tmp_df) > 1:
        logger.error("Mrchants {} are marked as different type {}".format(key, tmp_df["label"].tolist()))
        exit(1)

merged_df.to_csv("tmp.csv", index=False)

#merged_df = pd.read_csv("tmp.csv")
masked_merchants = merged_df['MNO'].tolist()
full_merchants = pd.read_csv("../../data/1001_5814.csv")
full_merchants_mno = full_merchants['c_2'].tolist()
assert len(set(masked_merchants).difference(set(full_merchants_mno))) == 0

merged_df["mrchnt_key_wrd"] = 0
logger.info("Start to set key_wrd")
for k_id in masked_merchants:
    if len(full_merchants["c_0"][full_merchants["c_2"] == k_id]) == 0:
        logger.error("MNO: {} does not have IN_MNO".format(k_id))
        continue
    merged_df["mrchnt_key_wrd"][merged_df["MNO"] == k_id] = \
        full_merchants["c_0"][full_merchants["c_2"] == k_id].tolist()[0]
"""
merged_df["mrchnt_key_wrd"][merged_df["MNO"]==full_merchants["c_2"]] = \
    full_merchants["c_1"][merged_df["MNO"]==full_merchants["c_2"]]
"""
masked_merchants = merged_df['mrchnt_key_wrd'].tolist()

full_merchants = full_merchants['c_0'].tolist()
risk_merchants = pd.read_csv("../../data/risk_merchants.csv")
risk_merchants = risk_merchants['IN_MNO'].tolist()

assert len(set(masked_merchants).difference(set(full_merchants))) == 0
assert len(set(masked_merchants).intersection(set(risk_merchants))) == 0
assert len(set(full_merchants).intersection(set(risk_merchants))) == 0

merged_df.to_csv("../../data/merchant_profiling_lables.xlsx", index=False)



