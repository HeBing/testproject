from data_preprocess.sql_executor import sql_execute_from_file, sql_execute, sql_execute_from_single_file
from darwinutils.log import get_task_logger
logger = get_task_logger(__name__)

def generate_risk_rule_tables(sql_cmd_file):
    logger.info(sql_execute('show tables'))

    #sql_execute_from_single_file(sql_cmd_file='../../risk_merchants_classification/tran_all_table_create.sql')
    #sql_execute_from_single_file(sql_cmd_file='../../risk_merchants_classification/tran_all_table_data.sql')
    #sql_execute_from_single_file(sql_cmd_file='../../risk_merchants_classification/gen_real_mrchnt_table.sql')
    sql_execute_from_file(sql_cmd_file, use_hive=False, file_type="json", cmd_replace_dict_file=None)
    logger.info(sql_execute('show tables'))

generate_risk_rule_tables(sql_cmd_file='./generate_risk_rule_tables.json')