#! /usr/bin/env python3
# -*- coding: utf-8 -*-
import os
import argparse
from darwinutils.json_op import Json
import pandas as pd
import json
from pipeline.pipeline import PipeLine
from pipeline.pipetask import PipeTaskRetry, PipeTask
from darwinutils.log import get_task_logger
from datetime import datetime
from datetime import timedelta
import shutil
import random
logger = get_task_logger(__name__)


def main(args):
    if not os.path.exists(os.path.abspath(args.data_file)):
        logger.error("File {} is not available".format(args.data_file))
        exit(1)
    df = pd.read_csv(os.path.abspath(args.data_file))
    if args.merchant_key not in df.columns.tolist():
        logger.error("{} is not in data file columns {}".format(args.merchant_key, df.columns.tolist()))
        exit(1)
    merchants = df[args.merchant_key].tolist()
    merchants = list(set(merchants))

    if args.ratio is not None:
        if args.ratio > 1 or args.ratio <= 0:
            logger.warning("Input ratio {} is not reasonalbe. Force to 0.1".format(args.ratio))
            number = int(len(merchants)*0.1)
        else:
            number = int(len(merchants)*args.ratio)
    else:
        if args.number > len(df):
            logger.warning("Input number {} is not reasonalbe since it is larger than total record number {}. Force to {}"
                           "".format(args.number, len(df), int(len(merchants)*0.1)))
            number = int(len(merchants) * 0.1)
        else:
            number = args.number

    logger.info("Total {} merchants and pickup {} for evaluation".format(len(merchants), number))
    chosen = random.sample(merchants, number)
    new_df = pd.DataFrame(chosen, columns=[args.merchant_key])
    new_df.to_csv("{}_evaluation_only.csv".format(os.path.splitext(os.path.basename(args.data_file))[0]), index=False)
    logger.info("Chosen merchant saved in {}".format("{}_evaluation_only.csv"
                                                     "".format(os.path.splitext(os.path.basename(args.data_file))[0])))


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '--data_file',
        type=str,
        help='Dataframe file full path',
        required=True
    )
    parser.add_argument(
        '--merchant_key',
        type=str,
        help='Merchant key name of column',
        default="mrchnt_key_wrd"
    )
    group = parser.add_mutually_exclusive_group(required=True)
    group.add_argument(
        '--ratio',
        type=float,
        help='Scope is 0 - 1. Default is 0.1'
    )
    group.add_argument(
        '--number',
        type=int,
        help='How many merchants to be picked up. Default can be 100'
    )

    args_flag, unparsed = parser.parse_known_args()
    main(args_flag)

