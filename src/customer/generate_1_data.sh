#!/usr/bin/env bash

python generate_train_data.py --pipeline_file=../../risk_merchants_classification/tran_mrchnt_pipeline.json --input_param_file=../../risk_merchants_classification/tran_mrchnt_pipeline_risk_mrchant_params.json --base_path=../../risk_merchants_classification --stats_period=1 --stats_date_scope=2018/01/02-2018/12/31 --output_folder=1_risk_df --store_interval=5
python generate_train_data.py --pipeline_file=../../risk_merchants_classification/tran_mrchnt_pipeline.json --input_param_file=../../risk_merchants_classification/tran_mrchnt_pipeline_good_mrchant_params.json --base_path=../../risk_merchants_classification --stats_period=1 --stats_date_scope=2018/01/02-2018/12/31 --output_folder=1_good_df --store_interval=5
