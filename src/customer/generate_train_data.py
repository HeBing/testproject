#! /usr/bin/env python3
# -*- coding: utf-8 -*-
import os
import time
import argparse
from darwinutils.json_op import Json
import pandas as pd
import json
from pipeline.pipeline import PipeLine
from pipeline.pipetask import PipeTaskRetry, PipeTask
from darwinutils.log import get_task_logger
from datetime import datetime
from datetime import timedelta
import shutil
logger = get_task_logger(__name__)


def valid_pipeline_format(pipeline):
    if not isinstance(pipeline, list):
        return False
    return all(list(map(lambda s: isinstance(s, dict), pipeline)))


def valid_params_content(params, stats_period, base_path):
    replace_key = None
    for key, value in params.items():
        if value != "STATS_FILE": # it is a hard code magic value which be replaced by stats_date content
            if os.path.exists(os.path.abspath(os.path.join(base_path, str(stats_period), value))):
                params[key] = os.path.abspath(os.path.join(base_path, str(stats_period), value))
            elif os.path.exists(os.path.abspath(os.path.join(base_path, value))):
                params[key] = os.path.abspath(os.path.join(base_path, value))
            else:
                logger.error("In params {}:{} but {} cannot be found under {}".format(key, value, value, base_path))
                return None, None
        else:
            replace_key = key
    return params, replace_key


def get_datetime_scope(datetime_path):
    try:
        with open(os.path.abspath(datetime_path), 'r') as f:
            lines = f.readlines()
            datetime_scope = []
            for line in lines:
                content = line.strip().split(",")
                for datetime_str in content:
                    try:
                        datetime_str = datetime_str.strip()
                        if len(datetime_str) > 0 and datetime_str != ' ':
                            datetime_scope.append(datetime.strptime(datetime_str.strip(), "%Y/%m/%d"))
                    except:
                        logger.warning("{} format is not right you shall use YYYY/MM/DD".format(datetime_str))
            logger.info("Total {} datetime point to be sampled".format(len(datetime_scope)))
            if len(datetime_scope) == 0:
                datetime_scope = None
            return datetime_scope
    except:
        logger.error("Failed to read file {}".format(datetime_path))
        return None


def generate_stats_date_files(end_date, file_path, model_days):
    """
    tmp_repalce_dict = {
        "1天内": "'20180214' and '20180214'",
        "30天内": "'20180116' and '20180214'",
        "3天内": "'20180212' and '20180214'",
        "7天内": "'20180208' and '20180214'",
        "现在时间": "'2018-02-15'",
        "模型天数": "30"
    }
    """
    replace_dict = {
        "1天内": "'{}' and '{}'".format((end_date - timedelta(days=1)).strftime("%Y%m%d"),
                                      (end_date - timedelta(days=1)).strftime("%Y%m%d")),
        "30天内": "'{}' and '{}'".format((end_date - timedelta(days=30)).strftime("%Y%m%d"),
                                       (end_date - timedelta(days=1)).strftime("%Y%m%d")),
        "3天内": "'{}' and '{}'".format((end_date - timedelta(days=3)).strftime("%Y%m%d"),
                                      (end_date - timedelta(days=1)).strftime("%Y%m%d")),
        "7天内": "'{}' and '{}'".format((end_date - timedelta(days=7)).strftime("%Y%m%d"),
                                      (end_date - timedelta(days=1)).strftime("%Y%m%d")),
        "15天内": "'{}' and '{}'".format((end_date - timedelta(days=15)).strftime("%Y%m%d"),
                                       (end_date - timedelta(days=1)).strftime("%Y%m%d")),
        "前30天": "'{}' and '{}'".format((end_date - timedelta(days=31)).strftime("%Y%m%d"),
                                       (end_date - timedelta(days=2)).strftime("%Y%m%d")),
        "前15天": "'{}' and '{}'".format((end_date - timedelta(days=16)).strftime("%Y%m%d"),
                                       (end_date - timedelta(days=2)).strftime("%Y%m%d")),
        "前7天": "'{}' and '{}'".format((end_date - timedelta(days=8)).strftime("%Y%m%d"),
                                       (end_date - timedelta(days=2)).strftime("%Y%m%d")),
        "前3天": "'{}' and '{}'".format((end_date - timedelta(days=4)).strftime("%Y%m%d"),
                                       (end_date - timedelta(days=2)).strftime("%Y%m%d")),
        "现在时间": "'{}'".format(end_date.strftime("%Y-%m-%d")),
        "模型天数": "{}".format(model_days)
    }
    file_full_path = os.path.join(os.path.abspath(file_path),
                                  "{}_{}_day.json".format(end_date.strftime("%Y%m%d"), model_days))
    if not os.path.exists(file_full_path):
        try:
            with open(file_full_path, "w") as f:
                json.dump(replace_dict, f, indent=2)
            return file_full_path
        except:
            logger.error("Write file {} failed".format(file_full_path))
            return None
    else:
        logger.info("{} already existed. Use old file".format(file_full_path))
        return file_full_path


def main(args):
    if not os.path.exists(os.path.abspath(args.pipeline_file)):
        logger.error("Pipeline file path {} is not valid".format(args.pipeline_file))
        exit(1)
    try:
        pipeline = Json.file_to_json_without_comments(os.path.abspath(args.pipeline_file))
    except:
        logger.exception("Cannot load pipeline file {} as json format".format(args.pipeline_file))
        exit(1)
    if not valid_pipeline_format(pipeline):
        logger.error(
            "Pipeline file {} content is not right: \n{}".format(args.pipeline_file, repr(pipeline)))
        exit(1)

    if not os.path.exists(os.path.abspath(args.input_param_file)):
        logger.error("Parameter file {} to be used by Pipeline is not valid".format(args.input_param_file))
        exit(1)
    try:
        params = Json.file_to_json_without_comments(os.path.abspath(args.input_param_file))
    except:
        logger.exception("Cannot load pipeline file {} as json format".format(args.input_param_file))
        exit(1)
    if not isinstance(params, dict):
        logger.error(
            "Parameters file {} content is not right: \n{}".format(args.input_param_file, repr(params)))
        exit(1)

    if not os.path.exists(os.path.abspath(args.base_path)):
        logger.error("Base path {} is not valid".format(args.base_path))
        exit(1)

    params, replace_key = valid_params_content(params=params, stats_period=args.stats_period, base_path=args.base_path)
    datetime_scope = None
    if params is None:
        logger.error("Param content is not right")
        exit(1)
    params['input7'] = args.use_hive

    if args.stats_date_file is not None:
        if not os.path.exists(os.path.abspath(args.stats_date_file)):
            logger.error("Stats date scope file {} is not valid".format(args.stats_date_file))
            exit(1)
        else:
            datetime_scope = get_datetime_scope(os.path.abspath(args.stats_date_file))
    else:
        start_date, end_date = args.stats_date_scope.split("-")
        try:
            start_date = datetime.strptime(start_date, "%Y/%m/%d")
            end_date = datetime.strptime(end_date, "%Y/%m/%d")
            if end_date < start_date:
                logger.error("Input Stats date scope {} is not valid".format(args.stats_date_scope))
                exit(1)
            datetime_scope = [start_date + timedelta(days=i) for i in range((end_date -start_date).days + 1)]
        except:
            logger.error("Input Stats date scope {} is not valid".format(args.stats_date_scope))
            exit(1)
    if datetime_scope is None or len(datetime_scope) == 0:
        logger.error("Stats date scope content is not right".format(args.stats_date_file))
        exit(1)

    if not os.path.exists(os.path.abspath(os.path.join(args.tmp_folder, "generated_stats_date_files"))):
        os.makedirs(os.path.abspath(os.path.join(args.tmp_folder, "generated_stats_date_files")))

    if not os.path.exists(os.path.abspath(args.output_folder)):
        os.makedirs(os.path.abspath(args.output_folder))

    last_df = None
    store_interval = args.store_interval if args.store_interval > 0 else len(datetime_scope) + 1
    count = 0
    failed_date_lst = []
    error_msg_lst = []
    for idx, day in enumerate(datetime_scope):
        file_full_path = generate_stats_date_files(day,
                                                   os.path.abspath(os.path.join(args.tmp_folder,
                                                                                "generated_stats_date_files")),
                                                   args.stats_period)
        if file_full_path is None:
            logger.warning("Failed generate stats date files for data extractor")
            continue
        if replace_key is not None:
            params[replace_key] = file_full_path
        logger.info('params = {}'.format(params))
        try:
            p = PipeLine(pipeline)
            task = PipeTask(p)
            logger.info("==============Start execute pipeline for {}==============:\n".format(file_full_path))
            start_time = time.time()
            r = task(**params)
            logger.info("==============Complete pipeline. for {}==================\n".format(file_full_path))
            logger.info('run {}/{} pipeline costs {}s '.format(idx+1, len(datetime_scope), time.time() - start_time))
            if r.get("output") is not None and isinstance(r.get("output"), pd.DataFrame):
                sql_result = r.get("output")
                sql_result['collection_date'] = [str(day.strftime("%Y%m%d"))] * sql_result.shape[0]
                if last_df is not None:
                    last_df = pd.concat([last_df, sql_result], ignore_index=True)
                else:
                    last_df = sql_result
                if (idx+1) % store_interval == 0:
                    file_name = os.path.join(os.path.abspath(args.output_folder),
                                                "tmp_store_{}_{}_df.csv".format(datetime_scope[0].strftime("%Y%m%d"),
                                                                                datetime_scope[idx].strftime("%Y%m%d")))
                    if os.path.exists(file_name):
                        os.remove(file_name)
                    last_df.to_csv(file_name, index=False)
                count += 1
            else:
                logger.error("{} output type is {}".format(file_full_path, type(r.get("output"))))
        except Exception as e:
            error_str = "Failed to execute pipeline {}: {} with reason {}:\n{}".format(
                day, os.path.abspath(args.pipeline_file), str(e), pipeline)
            logger.exception(error_str)
            # we will continue and operator need aware this
            failed_date_lst.append(day)
            error_msg_lst.append(error_str)

    if last_df is not None:
        file_name = os.path.join(os.path.abspath(args.output_folder), "full_{}_df.csv".format(count))
        if os.path.exists(file_name):
            os.remove(file_name)
        last_df.to_csv(file_name, index=False)
    if len(failed_date_lst) > 0:
        logger.error("Please note !!!!! the followed sample date failed:")
        for idx in range(len(failed_date_lst)):
            logger.error("{}: {}".format(failed_date_lst[idx], error_msg_lst[idx]))


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '--pipeline_file',
        type=str,
        help='To be excuted piepline file full path',
        required=True
    )
    parser.add_argument(
        '--input_param_file',
        type=str,
        help='It is the full path file which include the input which align with pipeline define. ',
        required=True
    )
    parser.add_argument(
        '--base_path',
        type=str,
        help='The root path of all pipeline, to be executed files',
        required=True
    )
    parser.add_argument(
        '--stats_period',
        type=int,
        choices=[1,3, 7, 15, 30],
        help='The data extract period',
        required=True
    )
    group = parser.add_mutually_exclusive_group(required=True)
    group.add_argument(
        '--stats_date_scope',
        type=str,
        help='The format must be YYYY/MM/DD-YYYY/MM/DD'
    )
    group.add_argument(
        '--stats_date_file',
        type=str,
        help='The full path of the data scope and the content must be YYYY/MM/DD, YYYY/MM/DD, YYYY/MM/DD'
    )
    parser.add_argument(
        '--output_folder',
        type=str,
        default="output",
        help='The output file folder'
    )
    parser.add_argument(
        '--store_interval',
        type=int,
        help='Store interval for partial dataframe saving. Avoid failure in big pipeline',
        default=30
    )
    parser.add_argument(
        '--tmp_folder',
        type=str,
        help='Store replace file which default is /tmp',
        default='/tmp'
    )
    parser.add_argument(
        '--use_hive',
        action='store_true',
        default=False
    )
    args_flag, unparsed = parser.parse_known_args()
    main(args_flag)


