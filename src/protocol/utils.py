#! /usr/bin/env python3
# -*- coding:utf-8 -*-


import inspect
from enum import Enum
from functools import (
    partial,
    reduce,
)
from itertools import (
    chain,
    dropwhile,
    filterfalse,
    groupby,
    islice,
)
from operator import (
    attrgetter,
    concat,
    contains,
    eq,
    getitem,
    itemgetter,
    methodcaller,
    mul,
    not_,
    setitem,
    truth,
)
from darwinutils.fn import (
    f_call,
    f_compose_r,
    f_echo,
    f_flip_call,
    f_group,
    f_list_rize,
    f_si_call,
    f_star_call,
    f_star2_call,
    f_unstar_call,
    f_unstar2_call,
    f_zip,
    irevert,
)
import numpy as np
from google.protobuf.message import Message
from darwinutils.common import ExceptCatchRunResultErrorMsg
from pipeline.pipevariable import (
    CtrlMagic,
)
from protocol import basic_pb2 as pb_mod
from protocol.basic_pb2 import (
    ListFieldEntry_double,
    ListFieldEntry_exception,
    ListFieldEntry_float,
    ListFieldEntry_int32,
    ListFieldEntry_int64,
    ListFieldEntry_uint32,
    ListFieldEntry_uint64,
    ListFieldEntry_sint32,
    ListFieldEntry_sint64,
    ListFieldEntry_fixed32,
    ListFieldEntry_fixed64,
    ListFieldEntry_sfixed32,
    ListFieldEntry_sfixed64,
    ListFieldEntry_bool,
    ListFieldEntry_string,
    ListFieldEntry_bytes,
    ListFieldEntry_magic,
    ListFieldEntry_null,
    ListFieldEntry_varArray,
    ListFieldEntry_varList,
    ListFieldEntry_varMap,
    ListFieldIndexes,
    VarList,
    VarMap,
    VarArray,
)


# TODO: change to internal function
def pack_indexes(indexes):
    """
    Pack continuous index to be storage efficient.
    for example:
    [1,2,3,5,7,8,10] --> [[5,10], [[1,4], [7,9]]]
                          ^^^^^^\  ^^^^^^^^^^^^+-+
                                \->+ individuals  \
                                                  \->+ ranges
    :param indexes:
    :return:
    """
    it = iter(indexes)

    ranges = []
    individuals = []

    try:
        # try-catch StopIteration if input "it(erable)" was actually empty
        start = next(it)
        exp = start+1

        for cur in chain(it, (-1,)):
            if exp != cur:
                if start+1 == exp:
                    individuals.append(start)
                else:
                    ranges.append((start, exp))
                start = cur
            exp = cur + 1
    except StopIteration as _e:
        pass

    return tuple(individuals), tuple(ranges)


# TODO: change to internal function
def unpack_indexes(individuals, ranges):
    """
    Reverse operation of "pack_indexes"
    :param individuals:
    :param ranges:
    :return:
    """
    ranges = f_compose_r(
        partial(map, f_compose_r(partial(f_star_call, range), tuple)),
        lambda _: reduce(concat, _, ()),
    )(ranges)
    # enforce individuals to tuple because ranges had been converted to tuple for concat
    return concat(tuple(individuals), ranges)


class PBType(Enum):
    """
    Google protocol buffer scalar value types
    +--------+-------+-----------+---------------+---------+-----------------------------------------------------------+
    |.proto  |C++    |Java       | Python        | Go      | Notes                                                     |
    +--------+-------+-----------+---------------+---------+-----------------------------------------------------------+
    |double  |double |double     |float          |*float64 |                                                           |
    +--------+-------+-----------+---------------+---------+-----------------------------------------------------------+
    |float   |float  |float      |float          |*float32 |                                                           |
    +--------+-------+-----------+---------------+---------+-----------------------------------------------------------+
    |int32   |int32  |int        |int            |*int32   |Uses variable-length encoding. Inefficient for encoding    |
    |        |       |           |               |         |negative numbers – if your field is likely to have negative|
    |        |       |           |               |         |values, use sint32 instead.                                |
    +--------+-------+-----------+---------------+---------+-----------------------------------------------------------+
    |int64   |int64  |long       |int/long[3]    |*int64   |Uses variable-length encoding. Inefficient for encoding    |
    |        |       |           |               |         |negative numbers – if your field is likely to have negative|
    |        |       |           |               |         |values, use sint64 instead.                                |
    +--------+-------+-----------+---------------+---------+-----------------------------------------------------------+
    |uint32  |uint32 |int[1]     |int/long[3]    |*uint32  |Uses variable-length encoding.                             |
    +--------+-------+-----------+---------------+---------+-----------------------------------------------------------+
    |uint64  |uint64 |long[1]    |int/long[3]    |*uint64  |Uses variable-length encoding.                             |
    +--------+-------+-----------+---------------+---------+-----------------------------------------------------------+
    |sint32  |int32  |int        |int            |*int32   |Uses variable-length encoding. Signed int value. These more|
    |        |       |           |               |         |efficiently encode negative numbers than regular int32s.   |
    +--------+-------+-----------+---------------+---------+-----------------------------------------------------------+
    |sint64  |int64  |long       |int/long[3]    |*int64   |Uses variable-length encoding. Signed int value. These more|
    |        |       |           |               |         |efficiently encode negative numbers than regular int64s.   |
    +--------+-------+-----------+---------------+---------+-----------------------------------------------------------+
    |fixed32 |uint32 |int[1]     |int/long[3]    |*uint32  |Always four bytes. More efficient than uint32 if values are|
    |        |       |           |               |         |often greater than 228.                                    |
    +--------+-------+-----------+---------------+---------+-----------------------------------------------------------+
    |fixed64 |uint64 |long[1]    |int/long[3]    |*uint64  |Always eight bytes. More efficient than uint64 if values   |
    |        |       |           |               |         |are often greater than 256.                                |
    +--------+-------+-----------+---------------+---------+-----------------------------------------------------------+
    |sfixed32|int32  |int        |int            |*int32   |Always four bytes.                                         |
    +--------+-------+-----------+---------------+---------+-----------------------------------------------------------+
    |sfixed64|int64  |long       |int/long[3]    |*int64   |Always eight bytes.                                        |
    +--------+-------+-----------+---------------+---------+-----------------------------------------------------------+
    |bool    |bool   |boolean    |bool           |*bool    |                                                           |
    +--------+-------+-----------+---------------+---------+-----------------------------------------------------------+
    |string  |string |String     |str/unicode[4] |*string  |A string must always contain UTF-8 encoded or 7-bit ASCII  |
    |        |       |           |               |         |text.                                                      |
    +--------+-------+-----------+---------------+---------+-----------------------------------------------------------+
    |bytes   |string |ByteString |str            |[]byte   |May contain any arbitrary sequence of bytes.               |
    +--------+-------+-----------+---------------+---------+-----------------------------------------------------------+
    [1] In Java, unsigned 32-bit and 64-bit integers are represented using their signed counterparts, with the top bit
        simply being stored in the sign bit.
    [2] In all cases, setting values to a field will perform type checking to make sure it is valid.
    [3] 64-bit or unsigned 32-bit integers are always represented as long when decoded, but can be an int if an int is
        given when setting the field. In all cases, the value must fit in the type represented when set. See [2].
    [4] Python strings are represented as unicode on decode but can be str if an ASCII string is given
        (this is subject to change).
    # refer to "google protobuf scalar value types" in https://developers.google.com/protocol-buffers/docs/proto#scalar

    NOTE: add "_" suffix to avoid conflicting with reserved type
    """
    double_ = 1
    float_ = 2
    int32_ = 3
    int64_ = 4
    uint32_ = 5
    uint64_ = 6
    sint32_ = 7
    sint64_ = 8
    fixed32_ = 9
    fixed64_ = 10
    sfixed32_ = 11
    sfixed64_ = 12
    bool_ = 13
    string_ = 14
    bytes_ = 15

    # "magic" is a dummy type which mapping CtrlMagic enum to pb int32
    # so that this type of values could be handled specifically.
    magic_ = 16


"""
Map python data types to pb types
it contains: scalar types, array types and other supported mixture types
1) scala types
we uses types from numpy standard which is meaning ful to be translated to protobuf's scalar type
# numpy built-in scalar types: https://docs.scipy.org/doc/numpy/reference/arrays.scalars.html#arrays-scalars-built-in
# google protobuf scalar value types: https://developers.google.com/protocol-buffers/docs/proto#scalar

2) array types
we strictly stick to numpy.ndarray with scalar types which we supported.

3) mixture types
These types are refer to container types: list and dictionary/map, for now.
"""
_CODER_MAP_PYTHON_TYPE_TO_PB_TYPE = {
    # -------------- float ------------------
    # "np.double" is an alias of numpy builtin type "np.float64"
    np.float64: PBType.double_,
    # "np.float" is an alias of python builtin "float" instead of numpy array scalar type
    # TODO: do we needs to explicitly default "float" to "float32"?
    float: PBType.double_,
    # for now, np.dtype("float").type == np.float64
    np.dtype("float").type: PBType.double_,
    # TODO: do we need to define dumpy "float16" pb type and helps converting back same type in Python?
    np.float16: PBType.float_,
    np.float32: PBType.float_,

    # -------------- int ------------------
    # "np.int" is an alias of builtin "int" instead of numpy array scalar type
    # TODO: do we needs to explicitly default "int" to "int32"?
    # TODO: python builtin "int" is not fixed length.
    int: PBType.int64_,
    # for now, np.dtype("int").type == np.int64
    np.dtype("int").type: PBType.int64_,
    # TODO: do we need to define dumpy "int8" pb type and helps converting back same type in Python?
    np.int8: PBType.int32_,
    # TODO: do we need to define dumpy "int16" pb type and helps converting back same type in Python?
    np.int16: PBType.int32_,
    np.int32: PBType.int32_,
    np.int64: PBType.int64_,
    # TODO: do we need to define dumpy "uint8" pb type and helps converting back same type in Python?
    np.uint8: PBType.uint32_,
    # TODO: do we need to define dumpy "uint16" pb type and helps converting back same type in Python?
    np.uint16: PBType.uint32_,
    np.uint32: PBType.uint32_,
    np.uint64: PBType.uint64_,

    # -------------- other scalar ------------------
    bool: PBType.bool_,
    # TODO: should we encode python str to pb "bytes" for non-ascii?
    str: PBType.string_,
    bytes: PBType.bytes_,

    # their numpy inherits are also equivalent to python builtins.
    np.dtype("bool").type: PBType.bool_,
    np.dtype("str").type: PBType.string_,
    np.dtype("bytes").type: PBType.bytes_,
}
# duplidate non np.dtype items to its repr(np.dtype(type)).
_ = f_compose_r(
    f_group(
        # parameter
        f_compose_r(
            methodcaller("items"),
            partial(filterfalse, partial(f_flip_call, isinstance, np.dtype)),
            # transpose to <Ks, Vs> from [<K, V>{, <K, V>, ...}]
            partial(f_star_call, zip),
            list,
            # convert Ks to K's=np.dtype(K)s
            f_zip(
                f_compose_r(
                    partial(map, f_compose_r(
                        np.dtype,
                        # TODO: there seems a bug when using np.dtype object in dict, let's use its repr
                        repr,
                    )),
                    list,
                ),
                f_echo,
            ),
            # transpose back to [<K', V>{, <K', V>, ...}]
            partial(f_star_call, zip),
            dict,
            partial(methodcaller, "update")
        ),
        f_echo,
    ),
    list,
    partial(f_star_call, f_call),
)(_CODER_MAP_PYTHON_TYPE_TO_PB_TYPE)


"""
Map pb base data type to python type
NOTE: we map to numpy scalar type, by default.
"""
_CODER_MAP_PB_TYPE_TO_PYTHON_TYPE = {
    # -------------- float ------------------
    # TODO: use builtin "float" which is also 64bit?
    PBType.double_: np.float64,
    PBType.float_: np.float32,

    # -------------- int ------------------
    PBType.int32_: np.int32,
    # TODO: use builtin "int" which is also 64bit?
    PBType.int64_: np.int64,
    PBType.uint32_: np.uint32,
    PBType.uint64_: np.uint64,
    PBType.sint32_: np.int32,
    PBType.sint64_: np.int64,
    PBType.fixed32_: np.uint32,
    PBType.fixed64_: np.uint64,
    PBType.sfixed32_: np.int32,
    # TODO: use builtin "int" which is also 64bit?
    PBType.sfixed64_: np.int64,

    # -------------- other scalar ------------------
    PBType.bool_: np.dtype("bool").type,
    PBType.string_: np.dtype("str").type,
    PBType.bytes_: np.dtype("bytes").type,
}


def _ndarray_flatter(obj):
    """
    Flatten a numpy array to (<type>, <shape>, <data>) triple
    usually, the flattened triple will be persistent to a pb msg
    :param obj: numpy ndarray object
    :type obj: np.ndarray
    :return: a triple
    :rtype: type, tuple, np.ndarray(ndim=1)
    """
    return f_compose_r(
        f_group(
            attrgetter("dtype.type"),
            attrgetter("shape"),
            f_compose_r(
                # TypeError: only size-1 arrays can be converted to Python scalars
                # TODO: add mon
                methodcaller("reshape", (-1,)),
                # TODO: is this really not required for pb message repeat field accept an iterable?
                # TODO: it's practical work that pb repeated value accepts np.ndarray as input.
                #methodcaller("tolist"),
            ),
        ),
        list,
    )(obj)


def _ndarray_composer(dtype, shape, data):
    """
    Compose a numpy array from flattened triple
    usually, the flattened triple from a pb msg
    :param dtype: np.dtype
    :param shape:
    :param data:
    :return:
    :rtype: np.ndarray
    """
    # always convert base dtype to its default dtype
    if not isinstance(dtype, np.dtype):
        dtype = np.dtype(dtype)

    if not isinstance(data, np.ndarray):
        # TODO: add mon
        data = partial(np.array, dtype=dtype)(data).reshape(shape)
    else:
        if data.dtype != dtype:
            # TODO: add mon
            print("astype converting")
            data = data.astype(dtype)
        if data.shape != tuple(shape):
            # TODO: add mon
            print("reshape")
            data = data.reshape(shape)
    return data


def _pb_loader_from_ndarray(py_ndarray_obj):
    """
    Load a python ndarray object to items which could be fill into pb msg
    :param py_ndarray_obj: source python array of np.ndarray type
    :type py_ndarray_obj: np.ndarray
    :return: a triple of <pb_type>, <shape>, <pb_repeated_values>
    :rtype: tuple
    """
    dtype, shape, values = _ndarray_flatter(py_ndarray_obj)
    # TODO: if required, try to specify real np.dtype in the _CODER_MAP_PB_TYPE_TO_PYTHON_TYPE
    if not isinstance(dtype, np.dtype):
        dtype = np.dtype(dtype)
    return _CODER_MAP_PYTHON_TYPE_TO_PB_TYPE[dtype.type], shape, values


def _pb_dumper_to_ndarray(pb_type, shape, pb_repeated_values):
    """
    Dump a pb msg items to python ndarray object.
    :param pb_type: source pb scalar type
    :type pb_type: PBType
    :param shape:
    :type shape: tuple
    :param pb_repeated_values:
    :type pb_repeated_values: Union(tuple, list) (?)
    :return: a python numpy ndarray
    :rtype: np.ndarray
    """
    dtype = _CODER_MAP_PB_TYPE_TO_PYTHON_TYPE[pb_type]
    # TODO: if required, try to specify real np.dtype in the _CODER_MAP_PB_TYPE_TO_PYTHON_TYPE
    if not isinstance(dtype, np.dtype):
        dtype = np.dtype(dtype)
    return partial(np.array, dtype=dtype)(pb_repeated_values).reshape(shape)


"""
Map pb data type to loader and dumper function

Loader:
* Syntax:
  f_loader(python_iterable): pb_type, shape, pb_repeated_values(list?)

  The output of loader could be filled into target pb message, such as ListFieldEntry_XXX or VarArray entries.

Dumper:
* Syntax:
  f_dumper(pb_type, shape, pb_repeated_values(list?)): python_iterable

  The input of dumper would come from different pb msg with some adapter to fit for the trip format.
  For VarList, the adapter needs to fake a dummy shape.
  For VarArray, the adapter could extract all fields from msg.
"""
_MAP_PB_TYPE_LOADER_FROM_PYTHON, _MAP_PB_TYPE_DUMPER_TO_PYTHON = f_compose_r(
    partial(map, f_compose_r(
        lambda _: [
            _,
            _pb_loader_from_ndarray,
            partial(_pb_dumper_to_ndarray, _),
        ],
    )),
    # transpose from [<<type>, <loader>, <dumper>>, ...] to <types>, <loaders>, <dumpers>
    partial(f_star_call, zip),
    list,
    # split for loader and dumper
    f_group(
        # PB scalar type, loader
        itemgetter(0, 1),
        # PB scalar type, dumper
        itemgetter(0, 2),
    ),
    # convert to dict/map for convenient.
    partial(map, f_compose_r(
        partial(f_star_call, zip),
        dict,
    )),
    list,
)(_CODER_MAP_PB_TYPE_TO_PYTHON_TYPE.keys())


"""
Map between python base type and pb "list" type
NOTE:
* the value of each map entry is a callable to help converting source data into target data
  the syntax of callable for:
  python2pb converting is: f(indexes=, values=): ListFieldEntry_XXX
  pb2python converting is:
# this was the 1st part about scalar type of pb "list" attributes
# other "recursive" type of attributes will be appended to the map later this file.
# NOTE: Though the performance of map/dict generation might not as good as you would expect, it's impact to overall
#       performance is tiny since it only executes once.
"""
_CODER_MAP_PYTHON_TYPE_TO_PB_ListFieldEntry_TYPE, _CODER_MAP_PB_ListFieldEntry_TYPE_TO_PYTHON_TYPE = f_compose_r(
    # export dict as item list
    partial(map, f_compose_r(
        methodcaller("items"),
        partial(f_star_call, zip),
        list,
    )),
    list,
    # normalize item list as K=python_type, V=pb_type per mapping direction
    f_zip(
        f_echo,
        irevert,
    ),
    list,
    # convert
    partial(map, f_compose_r(
        f_zip(
            # python type
            f_echo,
            # pb type enum -> pb type's name --> pb ListFieldEntry's name --> ListFieldEntry_XXX
            f_compose_r(
                partial(map, f_compose_r(
                    # -> pb type's name
                    attrgetter("name"),
                    # -> pb ListFieldEntry name
                    methodcaller("split", "_"),
                    itemgetter(0),
                    partial(concat, "ListFieldEntry_"),
                    # --> pb ListFieldEntry_XXX class constructor
                    partial(getattr, pb_mod),
                )),
                list,
            ),
        ),
        list,
    )),
    list,
    # restore mapping direction
    f_zip(
        f_echo,
        irevert,
    ),
    list,
    # restore dict
    partial(map, f_compose_r(
        partial(f_star_call, zip),
        list,
        dict,
    )),
    list,
)([
    _CODER_MAP_PYTHON_TYPE_TO_PB_TYPE,
    _CODER_MAP_PB_TYPE_TO_PYTHON_TYPE,
])


"""
Static map/dictionary to map python pb class to VarList object's attribute name
# this was used to help filling converted field entry back to pb msg
"""
_ATTR_MAP_PYTHON_PB_CLS_TO_PB_ListFieldEntry_NAME = f_compose_r(
    # TODO: not sure if it's safe and professional to use "DESCRIPTOR" of pb msg
    attrgetter("DESCRIPTOR.fields"),
    partial(map, f_compose_r(
        attrgetter("name"),
        f_group(
            f_compose_r(
                methodcaller("split", "_"),
                itemgetter(0),
                partial(concat, "ListFieldEntry_"),
            ),
            f_echo,
        ),
        list,
    )),
    list,
    dict,
)(VarList)


"""
Static map/dictionary to map python pb class to VarArray object's attribute name
# this was used to help filling converted field entry back to pb msg
"""
_ATTR_MAP_PB_TYPE_TO_ArrayFieldEntry_NAME = f_compose_r(
    # TODO: not sure if it's safe and professional to use "DESCRIPTOR" of pb msg
    attrgetter("DESCRIPTOR.fields"),
    partial(map, attrgetter("name")),
    # exclude attributes which do not follow same patten to be processed.
    lambda _: set(_) - {
        "shape", # is a "ctrl" attributes for real value
        "magic_entries", # needs special handler.
    },
    partial(map, f_compose_r(
        methodcaller("split", "_"),
        itemgetter(0),
        f_group(
            # <pb_type>_entries in VarArray class
            "{}_entries".format,
            # PBType enum
            f_compose_r(
                "{}_".format,
                partial(getattr, PBType),
            ),
        ),
        list,
    )),
    list,
    dict,
)(VarArray)


_VarArray_attributes = _ATTR_MAP_PB_TYPE_TO_ArrayFieldEntry_NAME.keys()


def get_array_entry_value_pair(msg):
    for attr_name in _VarArray_attributes:
        val = getattr(msg, attr_name)
        if len(val) > 0:
            return attr_name, val
    return None


# static definition to speed up None type detection
_NONE_TYPES = (type(None), repr(type(None)))


# TODO: change to internal function
def convert_idx_val_pairs_to_pb_list_field_entry(cls, idx_val_pairs):
    """
    Convert python [<idx, val>{, <idx, val>, ...}] pairs to protobuf list's field entry
    :param cls: python object's type
    :param idx_val_pairs: pairs of python object and its index in the list
    :return: a protobuf VarList's field entry msg object
    """
    indexes, values = f_compose_r(
        # sort by index
        partial(sorted, key=itemgetter(0)),
        partial(f_star_call, zip)
    )(idx_val_pairs)

    # for continuous index, do not need it except NoneType data because None will not be saved in pb
    if max(indexes)+1 == len(values) and cls not in _NONE_TYPES:
        pb_indexes = None
    else:
        pb_indexes = f_compose_r(
            pack_indexes,
            f_zip(
                # individuals
                f_echo,
                # ranges
                f_compose_r(
                    partial(map, f_compose_r(
                        partial(zip, ("start", "stop")),
                        dict,
                        f_list_rize,
                        partial(concat, [[]]),
                        partial(f_star2_call, ListFieldIndexes.SubRange),
                    )),
                    list,
                ),
            ),
            partial(zip, ("individuals", "ranges")),
            dict,
            f_list_rize,
            partial(concat, [[]]),
            partial(f_star2_call, ListFieldIndexes),
        )(indexes)

    pb_cls = _CODER_MAP_PYTHON_TYPE_TO_PB_ListFieldEntry_TYPE.get(cls)
    if not pb_cls:
        print(["indexes", indexes])
        print(["values", values])
        # type error if there are unsupported data type in the list.
        raise TypeError("Unsupported data type in the python list when converting to VarList pb msg. {}".format(cls))

    return f_compose_r(
        partial(zip, ("indexes", "values")),
        dict,
        f_list_rize,
        partial(concat, [[]]),
        partial(f_star2_call, pb_cls),
    )([pb_indexes, values])


# TODO: change to internal function
def convert_pb_list_field_entry_to_idx_val_pairs(entry):
    """
    Convert protobuf list's field entry to python [<idx, val>{, <idx, val>, ...}] pairs
    :param entry: protobuf's field entry msg object
    :return: a list of pairs about each python object and its index in the list
    """
    pb_cls = type(entry)
    python_cls = _CODER_MAP_PB_ListFieldEntry_TYPE_TO_PYTHON_TYPE.get(pb_cls)

    indexes = f_compose_r(
        attrgetter("indexes"),
        attrgetter("individuals", "ranges"),
        f_zip(
            # individuals
            f_echo,
            # ranges
            f_compose_r(
                partial(map, attrgetter("start", "stop")),
                list,
            ),
        ),
        partial(f_star_call, unpack_indexes),
        sorted,
    )(entry)
    n_items = len(indexes)

    if hasattr(entry, "values"):
        values = entry.values
        # restore continuous index by rule
        if n_items == 0:
            n_items = len(values)
            indexes = range(n_items)
        else:
            assert n_items == len(values)
    else:
        values = [None]*n_items

    # TODO: is this fastpath really helpful for performance?
    if n_items > 0:
        """
        f_assert = f_compose_r(
            partial(map, lambda item: isinstance(item, python_cls)),
            all,
        )
        if inspect.isclass(python_cls):
            assert f_assert(values), "has oncompatible input data pb_cls={} py_cls={} val={} rtype={}".format(pb_cls, python_cls, values, type(values[0]))
        """
        values = f_compose_r(
            partial(map, python_cls),
            list,
        )(values)
        #"""
        r = zip(indexes, values)
    else:
        r = range(0)
    return r


class VarList_:
    @staticmethod
    def dumps(obj, to_bytes=True):
        """
        Dump/serialize a python list object to pb VarList msg obj or byte stream
        :param obj: a python list object to be serialized.
        :type obj: iterable
        :param to_bytes: if the target of dumps is bytes stream or protobuf VarList msg
        :type to_bytes: bool
        :return: a serialized object.
        :rtype: Union(bytes, VarList_)
        """
        f_key = f_compose_r(itemgetter(1), type, repr)

        r = f_compose_r(
            enumerate,
            # ----------------------------------------------------------------
            # inspect each items in the list to group them by item data type
            # THESE are hot operations!!!
            partial(sorted, key=f_key),
            partial(groupby, key=f_key),
            # TODO: needs to filter out unsupported types and log warning/error?
            partial(map, partial(f_star_call, convert_idx_val_pairs_to_pb_list_field_entry)),
            list,
            # ----------- now, the list field entries had been generated ---------------
            # ----------- it's time to generate list pb msg              ---------------
            partial(map, f_compose_r(
                f_group(
                    # cls name -> attr name
                    f_compose_r(
                        type,
                        attrgetter("__name__"),
                        partial(getitem, _ATTR_MAP_PYTHON_PB_CLS_TO_PB_ListFieldEntry_NAME),
                    ),
                    # attr value
                    f_echo,
                ),
            )),
            dict,
            f_list_rize,
            partial(concat, [[]]),
            partial(f_star2_call, VarList),
        )(obj)

        if to_bytes:
            r = r.SerializeToString()
        return r

    @staticmethod
    def loads(data):
        """
        Load/de-serialize a protobuf VarList msg to python list object
        :param data: source data or objec to be de-serialzied back to python list object
        :type data: union(bytes, VarList)
        :return: a python list object
        :rtype: list
        """
        if isinstance(data, bytes):
            obj = VarList()
            obj.ParseFromString(data)
            msg = obj
        elif isinstance(data, VarList):
            # TODO: assert VarList or VarList_ ?
            msg = data
        else:
            raise TypeError("Can only convert pb \"VarList\" type of msg to python list instead of {}".format(
                type(data),
            ))

        attr_names = _ATTR_MAP_PYTHON_PB_CLS_TO_PB_ListFieldEntry_NAME.values()
        return f_compose_r(
            attrgetter(*attr_names),
            partial(f_list_rize, _num_args=1),
            partial(map, convert_pb_list_field_entry_to_idx_val_pairs),
            partial(f_star_call, chain),
            partial(sorted, key=itemgetter(0)),
            # TODO: assume the list has no "hole" because list_to_msg should grantee it.
            partial(map, itemgetter(1)),
            list,
        )(msg)


class VarMap_:
    @staticmethod
    def dumps(obj, to_bytes=True):
        """
        Dump/serialize a python dict object to pb VarMap msg obj or byte stream
        :param obj: a python dict object to be serialized.
        :type obj: dict
        :param to_bytes: if the target of dumps is bytes stream or protobuf VarMap msg
        :type to_bytes: bool
        :return: a serialized object.
        :rtype: Union(bytes, VarMap_)
        """
        r = f_compose_r(
            f_group(
                methodcaller("keys"),
                methodcaller("values"),
            ),
            partial(map, partial(VarList_.dumps, to_bytes=False)),
            partial(zip, ["keys", "values"]),
            dict,
            f_list_rize,
            partial(concat, [[]]),
            partial(f_star2_call, VarMap),
        )(obj)

        if to_bytes:
            r = r.SerializeToString()
        return r

    @staticmethod
    def loads(data):
        """
        Load/de-serialize a protobuf VarMap msg to python dict object
        :param data: source data or objec to be de-serialzied back to python dict object
        :type data: union(bytes, VarMap)
        :return: a python dict object
        :rtype: dict
        """
        if isinstance(data, bytes):
            obj = VarMap()
            obj.ParseFromString(data)
            msg = obj
        elif isinstance(data, VarMap):
            # TODO: verify VarMap or VarMap_ ?
            msg = data
        else:
            raise TypeError("Can only convert pb \"VarMap\" type of msg to python dict instead of {}".format(
                type(data),
            ))

        return f_compose_r(
            attrgetter("keys", "values"),
            partial(map, VarList_.loads),
            partial(f_star_call, zip),
            dict,
        )(msg)


"""
function to generate pb VarList's field entry object constructing function which should accept
"indexes" and "values" kwargs generally.

:param f_item_converting_func: a callable to convert an item in "values" kwargs to compatible pb msg
:param f_pb_msg_constructor: a callable to generate target pb msg from updated kwargs
"""
_f_f_meta_python_obj_constructor = lambda f_item_converting_func, f_pb_msg_constructor: \
    partial(f_unstar2_call, f_compose_r(
        f_zip(
            # varargs
            f_echo,
            # varkwargs
            f_compose_r(
                f_group(
                    # convert and replace "values" items in the varkwargs online
                    f_compose_r(
                        f_group(
                            # convert list of list objs to list of VarList msgs
                            f_compose_r(
                                itemgetter("values"),
                                partial(map, f_compose_r(
                                    f_item_converting_func,
                                )),
                                list,
                                # prepare the 3rd parameter for setitem(...)
                                f_list_rize,
                            ),
                            # prepare the 1st and 2nd parameters of setitem(...)
                            f_compose_r(
                                f_list_rize,
                                partial(concat, ["values"]),
                            ),
                        ),
                        # merge 3 parameters for setitem in reversed order
                        partial(reduce, concat),
                        # call setitem(...) to update the "values" in varkwargs
                        partial(f_star_call, partial(f_flip_call, setitem)),
                    ),
                    # return the updated varkwargs
                    f_echo,
                    # it must use sync and serial "map" to execute this f_group
                    map_functor=map,
                ),
                list,
                itemgetter(1),
            ),
        ),
        list,
        partial(f_star2_call, f_pb_msg_constructor),
    ))


class VarArray_:
    @staticmethod
    def dumps(obj, to_bytes=True):
        """
        Dump/serialize a python list object to pb VarList msg obj or byte stream
        :param obj: a python list object to be serialized.
        :type obj: iterable
        :param to_bytes: if the target of dumps is bytes stream or protobuf VarList msg
        :type to_bytes: bool
        :return: a serialized object.
        :rtype: Union(bytes, VarList_)
        """
        pb_type = f_compose_r(
            partial(map, _CODER_MAP_PYTHON_TYPE_TO_PB_TYPE.get),
            partial(dropwhile, lambda _: _ is None),
            lambda _: islice(_, None, 1),
            list,
        )([
            # TODO: there seems a bug when using np.dtype object in dict, let's use its repr
            #obj.dtype,
            repr(obj.dtype),
            obj.dtype.type,
        ])
        if len(pb_type) < 1:
            _ = list(map(print, _CODER_MAP_PYTHON_TYPE_TO_PB_TYPE))
            raise TypeError("Does not support mapping python \"{}\" type to protobuf type".format(repr(obj.dtype)))
        else:
            pb_type = pb_type[0]

        loader = _MAP_PB_TYPE_LOADER_FROM_PYTHON.get(pb_type)
        if loader is None:
            raise TypeError("Does not support dump python \"{}\" type of array to VarArray".format(obj.dtype))

        r = f_compose_r(
            loader,
            f_group(
                # shape
                f_compose_r(
                    itemgetter(1),
                    f_list_rize,
                    partial(concat, ["shape"])
                ),
                f_compose_r(
                    f_group(
                        # pb type -> VarArray attribute name
                        f_compose_r(
                            itemgetter(0),
                            # -> pb type's name
                            attrgetter("name"),
                            # -> pb ListFieldEntry name
                            methodcaller("split", "_"),
                            itemgetter(0),
                            "{}_entries".format,
                        ),
                        # value
                        itemgetter(2),
                    ),
                    list,
                ),
            ),
            dict,
            f_list_rize,
            partial(concat, [[]]),
            partial(f_star2_call, VarArray)
        )(obj)

        if to_bytes:
            r = r.SerializeToString()
        return r

    @staticmethod
    def loads(data):
        """
        Load/de-serialize a protobuf VarArray msg to python np.ndarray object
        :param data: source data or objec to be de-serialzied back to python np.ndarray object
        :type data: union(bytes, VarArray)
        :return: a python np.ndarray object
        :rtype: np.ndarray
        """
        if isinstance(data, bytes):
            obj = VarArray()
            obj.ParseFromString(data)
            msg = obj
        elif isinstance(data, VarArray):
            # TODO: assert VarList or VarList_ ?
            msg = data
        else:
            raise TypeError("Can only convert pb \"VarArray\" type of msg to python numpy.array instead of {}".format(
                type(data),
            ))

        shape = msg.shape
        pair = get_array_entry_value_pair(msg)
        if pair is None:
            raise ValueError("Empty VarArray msg, loads error!")

        entry_name, entry_value = pair
        pb_type = _ATTR_MAP_PB_TYPE_TO_ArrayFieldEntry_NAME[entry_name]
        dumper = _MAP_PB_TYPE_DUMPER_TO_PYTHON[pb_type]
        return dumper(shape, entry_value)


def dumps(obj, *args, pb_msg_transparent=False, to_bytes=False, **kwargs):
    if isinstance(obj, dict):
        f_dumps = VarMap_.dumps
    elif isinstance(obj, (list, tuple)):
        f_dumps = VarList_.dumps
    elif isinstance(obj, np.ndarray):
        f_dumps = VarArray_.dumps
    elif isinstance(obj, Message) and pb_msg_transparent:
        if to_bytes:
            obj = obj.SerializeToString()
        return obj
    else:
        raise TypeError("Try to dump unsupported python obj \"{}\"".format(type(obj)))
    return f_dumps(obj, *args, to_bytes=False, **kwargs)


def loads(msg, *args, non_pb_msg_transparent=False, **kwargs):
    if isinstance(msg, VarMap):
        f_loads = VarMap_.loads
    elif isinstance(msg, VarList):
        f_loads = VarList_.loads
    elif isinstance(msg, VarArray):
        f_loads = VarArray_.loads
    elif not isinstance(msg, Message) and non_pb_msg_transparent:
        return msg
    else:
        raise TypeError("Try to loads unsupported pb msg \"{}\"".format(type(msg)))
    return f_loads(msg, *args, **kwargs)


# add other type of list field entry after VarXXX_ helper, had been defined.
_CODER_MAP_PYTHON_TYPE_TO_PB_ListFieldEntry_TYPE.update({
    type(None): partial(f_unstar2_call, f_compose_r(
        f_zip(
            # varargs
            f_echo,
            # varkwargs
            f_compose_r(
                f_group(
                    # use "pop" instead of "delitem" so that this function can also be used as
                    # ListFieldEntry_null constructor which does not needs "values"
                    methodcaller("pop", "values", None),
                    f_echo,
                ),
                list,
                itemgetter(1),
            ),
        ),
        list,
        partial(f_star2_call, ListFieldEntry_null),
    )),

    list:
        _f_f_meta_python_obj_constructor(
            partial(VarList_.dumps, to_bytes=False),
            ListFieldEntry_varList,
        ),
    dict:
        _f_f_meta_python_obj_constructor(
            partial(VarMap_.dumps, to_bytes=False),
            ListFieldEntry_varMap,
        ),
    np.ndarray:
        _f_f_meta_python_obj_constructor(
            partial(VarArray_.dumps, to_bytes=False),
            ListFieldEntry_varArray,
        ),
    CtrlMagic:
        _f_f_meta_python_obj_constructor(
            attrgetter("value"),
            ListFieldEntry_magic,
        ),
    ExceptCatchRunResultErrorMsg:
        _f_f_meta_python_obj_constructor(
            f_compose_r(
                f_zip(
                    # exception
                    repr,
                    # traceback
                    f_echo,
                ),
                list,
                partial(np.array, dtype=np.dtype(str)),
                partial(VarArray_.dumps, to_bytes=False),
            ),
            ListFieldEntry_exception,
        ),
})
# tuple has same handler as list
_CODER_MAP_PYTHON_TYPE_TO_PB_ListFieldEntry_TYPE[tuple] = _CODER_MAP_PYTHON_TYPE_TO_PB_ListFieldEntry_TYPE[list]
# duplidate hash(K) to same dict obj
_ = f_compose_r(
    f_group(
        # parameter
        f_compose_r(
            methodcaller("items"),
            partial(filter, f_compose_r(
                itemgetter(0),
                lambda _: isinstance(_, type),
            )),
            # transpose to <Ks, Vs> from [<K, V>{, <K, V>, ...}]
            partial(f_star_call, zip),
            list,
            # convert Ks to K's=hash(K)s
            f_zip(
                f_compose_r(
                    partial(map, f_compose_r(
                        repr,
                    )),
                    list,
                ),
                f_echo,
            ),
            # transpose back to [<K', V>{, <K', V>, ...}]
            partial(f_star_call, zip),
            dict,
            partial(methodcaller, "update")
        ),
        f_echo,
    ),
    list,
    partial(f_star_call, f_call),
)(_CODER_MAP_PYTHON_TYPE_TO_PB_ListFieldEntry_TYPE)


# add other type of list field entry after VarXXX_ helper, had been defined.
_CODER_MAP_PB_ListFieldEntry_TYPE_TO_PYTHON_TYPE.update({
    # NoneType constructor does not accept argument, wrap it to accept one
    # so that it would be consistent with others and convenient in the workflow.
    ListFieldEntry_null: partial(f_si_call, type(None)),

    ListFieldEntry_varList: VarList_.loads,
    ListFieldEntry_varMap: VarMap_.loads,
    ListFieldEntry_varArray: VarArray_.loads,
    ListFieldEntry_magic: CtrlMagic,
    ListFieldEntry_exception: f_compose_r(
        VarArray_.loads,
        f_zip(
            # exception: restore from repr
            # TODO: security hole, needs to replace with better solution.
            # TODO: its much more internal and will less possible to be expose to end user.
            eval,
            #
            f_echo,
        ),
        partial(f_star_call, ExceptCatchRunResultErrorMsg),
    ),
})
