#! /usr/bin/env python3
# -*- coding: utf-8 -*-


import nose
import sys
import time
import random
from operator import eq
from darwinutils.config import DARWIN_CONFIG
from darwinutils.app import get_celery_app
from darwinutils.log import get_task_logger
from darwinutils.decorator import logFuncHeaderFunctor
from darwinutils.fn import (
    f_timeit_call,
)
from pipeline.pipevariable import CtrlMagic
logger = get_task_logger(__name__)

logFunc = logFuncHeaderFunctor(logger.debug)


def add(x, y):
    if y == 9998:
        import time
        time.sleep(x)
    if y == 9999:
        raise AssertionError("Desired error in add when y equals to {}".format(y))
    if y == 5000:
        import time
        time.sleep(x)
        raise AssertionError("Desired error in add when y equals to {}".format(y))
    return x + y


def mul(x, y):
    if y == 9998:
        import time
        time.sleep(x)
    if y == 9999:
        raise AssertionError("Desired error in add when y equals to {}".format(y))
    return x * y


def periodic_task01(arg1, arg2):
    logger.debug("Calling periodic task01 with arg1={} arg2={}".format(arg1, arg2))


def periodic_task02(arg1, arg2):
    logger.debug("Calling periodic task02 with arg1={} arg2={}".format(arg1, arg2))

"""Fake trnet workflow"""
trnet_img_shape = (1089, 778)
trnet_adjusted_img_shape = (1000, 800)
trnet_aligned_img_shape = (2087, 1200)
trnet_text_img_shape = (1000, 10)
trnet_num_stamps_per_img = 2
trnet_qrcode_shape = (200, 200)
@logFunc
def trnet_image_loader(image_paths):
    assert image_paths

    #raise ValueError('000')
    import numpy as np
    invoice_images = list(map(lambda img: np.random.randint(0, 256, (*trnet_img_shape, 3), dtype=np.uint8), image_paths))
    return invoice_images


@logFunc
def trnet_stamp_detection(imgs):
    """
    #trnet_num_stamps per img
    :param imgs:
    :return:
    """
    nose.tools.assert_is_not_none(imgs)

    #raise ValueError('111')
    invoice_images = imgs
    nose.tools.assert_sequence_equal(invoice_images[0].shape[:2], trnet_img_shape)

    import numpy as np
    stamp_bboxes = list(map(lambda img: np.random.randint(0, 64, (trnet_num_stamps_per_img, 4)), invoice_images))
    return stamp_bboxes


@logFunc
def trnet_stamp_detection_warm(it):
    nose.tools.assert_is_not_none(it)

    logger.info("trnet_stamp_detection_warm entered...........")
    import numpy as np
    for kwargs in it:
        #raise ValueError('111')
        invoice_images = kwargs["imgs"]

        logger.info("trnet_stamp_detection_warm #invoice_images {}".format(len(invoice_images)))
        nose.tools.assert_sequence_equal(invoice_images[0].shape[:2], trnet_img_shape)

        stamp_bboxes = list(map(lambda img: np.random.randint(0, 64, (trnet_num_stamps_per_img, 4)), invoice_images))
        yield stamp_bboxes

@logFunc
def trnet_qrcode_detection(imgs):
    """
    One qrcode per img
    :param imgs:
    :return:
    """
    nose.tools.assert_is_not_none(imgs)

    #raise ValueError('222')
    invoice_images = imgs
    nose.tools.assert_sequence_equal(invoice_images[0].shape[:2], trnet_img_shape)

    import numpy as np
    qrcode_images = list(map(lambda img: np.random.randint(0, 256, (*trnet_qrcode_shape, 3)), invoice_images))
    qrcode_positions = list(map(lambda img: np.random.randint(0, 64, (4,)), invoice_images))
    return qrcode_images, qrcode_positions


@logFunc
def trnet_qrcode_recognition(imgs):
    nose.tools.assert_is_not_none(imgs)

    qrcode_images = imgs
    nose.tools.assert_sequence_equal(qrcode_images[0].shape[:2], trnet_qrcode_shape)

    qrcode_informations = list(map(lambda img: "qrcode_{}".format(random.randint(0, sys.maxsize)), qrcode_images))
    return qrcode_informations


@logFunc
def trnet_invoice_adjustment(imgs, bboxes):
    nose.tools.assert_is_not_none(imgs)
    nose.tools.assert_is_not_none(bboxes)

    invoice_images = imgs
    qrcode_positions = bboxes
    nose.tools.assert_sequence_equal(invoice_images[0].shape[:2], trnet_img_shape)

    import numpy as np
    adjusted_images = list(map(lambda img: np.random.randint(0, 256, (*trnet_adjusted_img_shape,3), dtype=np.uint8), invoice_images))
    return adjusted_images


@logFunc
def trnet_invoice_alignment(imgs):
    nose.tools.assert_is_not_none(imgs)

    adjusted_images = imgs
    nose.tools.assert_sequence_equal(adjusted_images[0].shape[:2], trnet_adjusted_img_shape)

    import numpy as np
    aligned_images = list(map(lambda img: np.random.randint(0, 256, (*trnet_aligned_img_shape,3), dtype=np.uint8), adjusted_images))
    return aligned_images


@logFunc
def trnet_textbox_detection(imgs):
    nose.tools.assert_is_not_none(imgs)

    aligned_images = imgs
    nose.tools.assert_sequence_equal(aligned_images[0].shape[:2], trnet_aligned_img_shape)

    import numpy as np
    # return 10~20 random bboxes per each aligned image
    text_bboxes = list(map(lambda img: np.random.randint(0, 64, (np.random.randint(10,20), 4,)), aligned_images))
    return text_bboxes


@logFunc
def trnet_textbox_crop(imgs, bboxes):
    nose.tools.assert_is_not_none(imgs)
    nose.tools.assert_is_not_none(bboxes)

    aligned_images = imgs
    nose.tools.assert_sequence_equal(aligned_images[0].shape[:2], trnet_aligned_img_shape)

    text_bboxes = bboxes
    nose.tools.assert_equal(len(text_bboxes), len(aligned_images))

    from darwinutils.helper import f_compose_r
    from functools import partial
    _ = f_compose_r(
        partial(map, f_compose_r(
            len,
            lambda _: 10 <= _ <= 20,
        )),
        all,
        partial(nose.tools.assert_equal, True)
    )(text_bboxes)

    import numpy as np
    # fake exact number of croped images per each bboxes in each aligned image.
    text_images = list(map(lambda bboxes: np.random.randint(0, 256, (len(bboxes), *trnet_text_img_shape, 3)), text_bboxes))
    return text_images


@logFunc
def trnet_textbox_recognition(imgs):
    nose.tools.assert_is_not_none(imgs)

    text_images = imgs
    nose.tools.assert_sequence_equal(text_images[0].shape[1:3], trnet_text_img_shape)

    text_contents = list(map(lambda img: "textbox_{}".format(random.randint(0, sys.maxsize)), text_images))
    return text_contents


@logFunc
def trnet_textbox_recognition_warm(it):
    nose.tools.assert_is_not_none(it)

    for kwargs in it:
        text_images = kwargs["imgs"]
        nose.tools.assert_sequence_equal(text_images[0].shape[1:3], trnet_text_img_shape)

        text_contents = list(map(lambda img: "textbox_{}".format(random.randint(0, sys.maxsize)), text_images))
        yield text_contents


@logFunc
def trnet_invoice_analysis(stamp_bboxes, qrcode_informations, text_contents, text_bboxes):
    nose.tools.assert_is_not_none(stamp_bboxes)
    nose.tools.assert_is_not_none(qrcode_informations)
    nose.tools.assert_is_not_none(text_contents)
    nose.tools.assert_is_not_none(text_bboxes)

    logger.info(len(stamp_bboxes))
    nose.tools.assert_equal(len(stamp_bboxes), len(qrcode_informations))
    nose.tools.assert_equal(len(stamp_bboxes[0]), trnet_num_stamps_per_img)
    invoice_informations = list(map(lambda img: "invoice_{}".format(random.randint(0, sys.maxsize)), stamp_bboxes))
    return invoice_informations


@logFunc
def test_return_lst(v):
    logger.info("V: {}".format(v))
    return [v]


def _cpu_loader(size):
    from functools import partial
    import numpy as np
    import pickle
    import zlib
    from darwinutils.fn import (f_compose_r, f_list_rize)

    _ = f_compose_r(
        f_list_rize,
        np.random.random,
        pickle.dumps,
        partial(zlib.compress, level=zlib.Z_BEST_COMPRESSION),
    )(size)


def warm_echo(gen, batch_load=100, sample_load=2000):
    from collections import Iterator
    import time
    from functools import (
        partial,
    )
    from darwinutils.helper import (
        f_compose_r,
    )
    assert isinstance(gen, Iterator), type(gen).__name__

    from operator import itemgetter
    logger.info("enter warm_echo main loop")
    for kwargs in gen:
        beg = time.time()
        if isinstance(kwargs, dict):
            # no batch, or batch_size==1
            is_batch = False
            batch_size = 1
            kwargs_lst = [kwargs]
        elif isinstance(kwargs, (tuple, list)):
            # batched input
            is_batch = True
            batch_size = len(kwargs)
            kwargs_lst = kwargs
            if batch_size > 1:
                logger.debug("got batched input {}".format(batch_size))

        result = []
        for i, kwargs in enumerate(kwargs_lst):
            pairs = sorted(kwargs.items(), key=itemgetter(0))

            # if there are intended exceptions object, raise it.
            exceptional_pairs = f_compose_r(
                partial(filter, lambda _: isinstance(_[1], Exception)),
                list,
            )(pairs)
            """
            _ = f_compose_r(
                partial(map, f_compose_r(
                    lambda _: "Output variable \"{}\" has exception \"{}\"".format(_[0], repr(_[1])),
                    logger.debug,
                )),
                list,
            )(exceptional_pairs)
            """
            if len(exceptional_pairs) > 0:
                raise exceptional_pairs[0][1]

            # if there are masked exceptions object from grpc channel, raise it.
            exceptional_pairs = f_compose_r(
                #partial(filter, partial(eq, CtrlMagic.ValueError_VALUE)),
                partial(filter, lambda _: isinstance(_[1], CtrlMagic) and _[1] == CtrlMagic.ValueError_VALUE),
                list,
            )(pairs)
            """
            _ = f_compose_r(
                partial(map, f_compose_r(
                    lambda _: "Output variable \"{}\" has magic exception \"{}\"".format(_[0], repr(_[1])),
                    logger.debug,
                )),
                list,
            )(exceptional_pairs)
            """
            if len(exceptional_pairs) > 0:
                raise ValueError("Got magic exception")

            #logger.debug("task handling kwargs[{}/{}]={}".format(i, batch_size, pairs))
            pairs = list(map(itemgetter(1), pairs))
            if len(pairs) == 1:
                pairs = pairs[0]
            result.append(pairs)

        # assume batched sample has constant overhead
        if sample_load > 0:
            _, sample_dur = f_timeit_call(_cpu_loader, sample_load)
        else:
            sample_dur = 0.0

        # batch overhead is linear scaling as batch_size
        if is_batch and batch_load > 0:
            _, batch_dur = f_timeit_call(_cpu_loader, batch_load*batch_size)
        else:
            batch_dur = 0.0

        # wait for debug retry task
        if False:
            interval_sleep = random.uniform(3, 5)
            time.sleep(interval_sleep)
            logger.info("sleep {}".format(interval_sleep))

        dur_all = time.time() - beg

        if DARWIN_CONFIG.debug_performance:
            logger.critical("warm echo cost all={:.6f}s sample={:.6f}s batch[{}]={:.6f}s diff={:.6f}s since {}".format(
                dur_all,
                sample_dur,
                batch_size, batch_dur,
                dur_all-sample_dur-batch_dur,
                beg,
            ))

        if not is_batch:
            yield result[0]
        else:
            yield result


@logFunc
def warm_incr(gen):
    from collections import Iterator
    assert isinstance(gen, Iterator), type(gen).__name__

    for kwargs in gen:
        logger.info(kwargs)
        a = kwargs['a']
        b = kwargs['b']
        logger.debug("task handling (a,b)={}".format((a, b)))
        yield a+1, b+1


def test_map(data):
    from darwinutils.mapreduce import parallel_map_p
    from darwinutils.fn import f_echo
    r = list(parallel_map_p(f_echo, data))
    return r
