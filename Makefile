
SHELL ?= /bin/bash


TOP := $(realpath $(patsubst %/,%,$(dir $(realpath $(firstword $(MAKEFILE_LIST))))))
PROFILE ?= dev
PROFILE_TOP ?= $(TOP)/profile
include $(PROFILE_TOP)/lib.mk
include $(PROFILE_TOP)/zip.mk
include $(PROFILE_TOP)/common.mk
include $(PROFILE_TOP)/$(PROFILE).mk


public_targets := usage clean maintainer-clean prereqs compile test build compress decompress
private_targets := all protocol
all_targets := $(public_targets) $(private_targets)


define f_usage
$(call f_usage_default)
endef


.PHONY: $(all_targets)
all: usage


# ------------------------------------------------------------------------------
# begin of alias targets
#
protocol:
	$(MAKE) -C $(SRC_TOP)/protocol $@

build:
	$(MAKE) -C $(BUILD_TOP) $@

analysis: pylint
#
# end of alias targets
# ------------------------------------------------------------------------------


# ------------------------------------------------------------------------------
# begin of meta targets
#
# compile deps in self repo
# for now: only "protocol" in self
compile: protocol decompress fix_links

# pre-reqs for all deps in order to fully run
#prereqs: compile
	# pre-reqs from core
	#$(MAKE) -C $(CORE_TOP) compile setup_platform
#
# end of meta targets
# ------------------------------------------------------------------------------


test:
	$(MAKE) -C $(TEST_TOP)

clean:
	$(MAKE) -C $(BUILD_TOP) $@
	$(MAKE) -C $(TEST_TOP) $@
	$(MAKE) -C $(SRC_TOP)/protocol $@


# ------------------------------------------------------------------------------
# begin of Makefile self unit-test
#
# NOTE:
# * only activate if calling to this Makefile directly
#
local_test_targets := $(call get_local_test_targets)

.PHONY: _test $(local_test_targets)
_test: _test_usage $(local_test_targets)
	$(MAKE) -C profile $@
	$(MAKE) -C $(SRC_TOP)/protocol $@
	$(MAKE) -C $(TEST_TOP) $@
	$(MAKE) -C $(BUILD_TOP) $@
	$(MAKE) -f $(firstword $(MAKEFILE_LIST)) _test_maintainer-clean
#
# end of Makefile self unit-test
# ------------------------------------------------------------------------------
