#Setup Conda ENV:<br />
<br />
1. cd src/conf <br />
2. conda create --name $CONDA_ENV_NAME python=3.6 <br />
3. conda activate $CONDA_ENV_NAME <br />
4. conda install --file package-list.txt <br />
5. pip install -r requirements.txt <br />
