#! /usr/bin/env bash

DEFAULT_project="riskAI"

project=${project:-${DEFAULT_project}}

function timestamp() {
    date "+%Y%m%d_%H%M"
}

# generate protocol
make -C ../../riskAI/src/protocol python && \
# start build
python build_cython.py build_ext --inplace && \
# record git revision history
git   -C ../../riskAI  log --oneline --graph -n10 | sed -e 's/^/riskAI >> /g'  | tee -a ./suixingfu-${project}/.versions && \
{
    ts=`timestamp`
    f_pkg=suixingfu-${project}-${ts}.tar.gz
    tar zcf $f_pkg suixingfu-${project}
}
