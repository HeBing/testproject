#! /usr/bin/env python3
# -*- coding:utf-8 -*-


"""
build command:
python build_cython.py build_ext --inplace
"""


import glob
from distutils.core import setup
from Cython.Build import cythonize


# firing the cpython building engine
setup(
    name='riskAI',
    version='1.0',
    description='SuiXingFu risk merchants detection project',
    author='iQubic',
    author_email='darwin.ai@iqubic.net',
    url='',
    license='Commercial',
    ext_modules=cythonize(
        glob.glob("*.py", recursive=False) + glob.glob("**/*.py", recursive=True),
        language_level=3,
    ),
)
