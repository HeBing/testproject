#! /usr/bin/env python3
# -*- coding:utf-8 -*-


"""
build command:
python build_cython.py build_ext --inplace
"""


import os
import glob
from shutil import copy2, rmtree
from distutils.core import setup
from Cython.Build import cythonize
from distutils.dir_util import copy_tree
#from pygit2 import Repository, GIT_SORT_TOPOLOGICAL, GIT_SORT_REVERSE
import datetime


def get_base_path():
    current_path = os.getcwd()
    while current_path.find('riskAI') > 0:
        current_path = os.path.dirname(current_path)
    return current_path


base_folder = get_base_path()
so_folder_map = {
    'riskAI': [
        # -------------------------------
        # keep the items in alphabet order.
        'darwinutils',
        'db',
        'data_preprocess',
        'license',
        'pipeline',
        'protocol',
        # keep the items in alphabet order.
        # -------------------------------
    ],
}
exclude_files = {
    'riskAI': [
        # -------------------------------
        # keep the items in alphabet order.
        'darwinutils/common.py',
        'pipeline/common.py',
        'pipeline/tasks.py',
        # keep the items in alphabet order.
        # -------------------------------
    ]
}
exclude_folders = {
    'riskAI': [
        # -------------------------------
        # keep the items in alphabet order.
        # -------------------------------
    ]
}
working_path = os.getcwd()
package_path = os.path.join(working_path, 'riskAI')
tmp_folder_path = os.path.join(working_path, ".build")

if os.path.exists(package_path):
    rmtree(package_path, ignore_errors=True)
if os.path.exists(tmp_folder_path):
    rmtree(tmp_folder_path, ignore_errors=True)
os.makedirs(package_path)
os.makedirs(tmp_folder_path)

for folder_name in list(so_folder_map.keys()):
    for sub_folder in so_folder_map[folder_name]:
        source_path = os.path.join(base_folder, folder_name, "src", sub_folder)
        target_path = os.path.join(tmp_folder_path, "src", sub_folder)
        if os.path.isdir(source_path):
            copy_tree(source_path, target_path)
        else:
            if not os.path.exists(os.path.dirname(target_path)):
                os.makedirs(os.path.dirname(target_path))
            copy2(source_path, target_path)

    if exclude_files.get(folder_name) is not None:
        full_lst = glob.glob(os.path.join(os.path.join(tmp_folder_path, "src"), "**/*.py"), recursive=True)
        full_lst += glob.glob(os.path.join(os.path.join(tmp_folder_path, "src"), "*.py"), recursive=False)
        rm_lst = []
        for exclude_f in exclude_files[folder_name]:
            for find_f in full_lst:
                if find_f.find(exclude_f) >= 0:
                    rm_lst.append(find_f)
        rst = list(map(lambda s: os.remove(s), rm_lst))

    if exclude_folders.get(folder_name) is not None:
        for exclude_folder in exclude_folders.get(folder_name):
            rm_lst = glob.glob(os.path.join(os.path.join(tmp_folder_path, "src"), "**/"+exclude_folder), recursive=True)
            rm_lst += glob.glob(os.path.join(os.path.join(tmp_folder_path, "src"), exclude_folder), recursive=False)
            rst = list(map(lambda s: rmtree(s, ignore_errors=True), rm_lst))

del_file_lst = glob.glob(os.path.join(tmp_folder_path, "**/*.md"), recursive=True)
del_file_lst += glob.glob(os.path.join(tmp_folder_path, "**/__pycache__"), recursive=True)
for file_name in del_file_lst:
    if os.path.isfile(file_name):
        os.remove(file_name)
    else:
        rmtree(file_name, ignore_errors=True)

target_file_lst = glob.glob(os.path.join(tmp_folder_path, "**/*.py"), recursive=True)
os.chdir(package_path)

setup(
    name='riskAI',
    version='1.0',
    description='SuiXingFu risk merchants detection project',
    author='iQubic',
    author_email='darwin.ai@iqubic.net',
    url='',
    license='Commercial',
    ext_modules=cythonize(
        target_file_lst,
        language_level=3,
    ),
)



"""
Any celery related function shall not be compiled
"""
non_so_folder_map = {
    'riskAI': [
        # -------------------------------
        # keep the items in alphabet order.
        'cmd',
        'conf',
        'customer',
        'darwinutils/common.py',
        'pipeline/common.py',
        'pipeline/tasks.py',
        'toolkit',
        # keep the items in alphabet order.
        # -------------------------------
    ],
}
for folder_name in non_so_folder_map.keys():
    for sub_folder in non_so_folder_map[folder_name]:
        source_path = os.path.join(base_folder, folder_name, "src", sub_folder)
        target_path = os.path.join(package_path, sub_folder)
        if os.path.isdir(source_path):
            copy_tree(source_path, target_path)
        else:
            if not os.path.exists(os.path.dirname(target_path)):
                os.makedirs(os.path.dirname(target_path))
            copy2(source_path, target_path)

if os.path.exists("build"):
    rmtree("build", ignore_errors=True)

"""
#
# deprecated: following code could be covered by the shell script in build.sh
# remove these deprecated code in future
#
repo = Repository(os.path.join(base_folder, "darwin-platform", ".git"))
if repo is not None:
    branch_name = repo.head.shorthand
    for commit in repo.walk(repo.head.target, GIT_SORT_TOPOLOGICAL):
        commit_id = commit.id
        break
    create_date = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    with open("version.txt", "w") as f:
        f.write("{}:{}:{}".format(branch_name, commit_id, create_date))
"""

copy2(os.path.join(base_folder, "riskAI", "LICENSE"), "LICENSE")
