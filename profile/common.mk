
SHELL := /bin/bash


TOP ?= $(realpath $(patsubst %/,%,$(dir $(realpath $(lastword $(MAKEFILE_LIST)))))/..)

PLATFORM_TOP = $(TOP)/../riskAI

PLATFORM_SRC = $(PLATFORM_TOP)/src

PLATFORM_TEST = $(PLATFORM_TOP)/tests


SRC_TOP = $(TOP)/src
BUILD_TOP = $(TOP)/build
TEST_TOP = $(TOP)/tests

PROFILE_TOP ?= $(PLATFORM_TOP)/profile

PYLINTHOME ?= $(TOP)/.pylint.d
CSCOPE_FILES := cscope.files cscope.out cscope.po.out cscope.in.out

TRACKED_LINK_FILES := $(call get_git_tracked_links,$(TOP))

.PHONY: usage maintainer-clean pylint cscope clean_cscope test_nose_all fix_links
# default usage target
# TODO: write its own usage
usage:
	$(call f_usage)

# clean everything which were not in the git repo.
maintainer-clean:
	-f_lst="$(filter-out .idea/,$(call get_git_untracked_files,./))"; if [ -n "$$f_lst" ]; then echo "$$f_lst" | xargs rm -rf; fi
	-find $(TOP) ! -regex "$(TOP)/\.git/.*" -type d -empty -delete

# ------------------------------------------------------------------------------
# pylint check
#
pylint:
	if [ "$(clean_first)" = "true" ]; then rm -rf $(PYLINTHOME); fi
	[ -d "$(PYLINTHOME)" ] || mkdir -p $(PYLINTHOME)
	cd $(TOP) && env PYLINTHOME=$(PYLINTHOME) pylint --rcfile=$(realpath $(PLATFORM_TOP))/src/conf/pylint.conf src

# ------------------------------------------------------------------------------
# cscope
#
# meta target to build all cscope related db
cscope: $(CSCOPE_FILES) tags
	#
	# generate cscope db for darwin-core and darwin-platform
	#
	@ls -ld $^

# clean cscope db
clean_cscope:
	-rm -f $(CSCOPE_FILES) tags

# cscope db generate
$(filter-out cscope.files,$(CSCOPE_FILES)): cscope.files tags
	cscope -b -q -v -i $<

cscope.files: $(call rwildcard, $(foreach suffix,/src /tests /toolkit, $(realpath $(addsuffix $(suffix),$(PLATFORM_TOP) $(CORE_TOP)))),*.py *.java)
	@echo "$^" | tr ' ' '\n' > $@

tags: cscope.files
	ctags -L $<

# ------------------------------------------------------------------------------
# nose test shortcut
#
test_nose_all:
	declare beg=`date +"%s"`; \
	declare TOPS="$(realpath $(TOP) $(PLATFORM_TOP))"; \
	TOPS=`echo "$$TOPS" | tr ' ' '\n' | nl -w1 -s' ' | sort -T' ' -k2 -u | sort -T' ' -k1 -n | cut -d' ' -f2-`; \
	declare DIRS=`echo "$$TOPS" | sed -e 's,$$,/tests/nose,g'`; \
	declare rc=0; \
	declare -a succ_cases=(); \
	declare -a fail_cases=(); \
	declare DIR=""; \
	for DIR in $$DIRS; \
	do \
	    if [ ! -d $$DIR/auto_run_logs ]; then mkdir -p $$DIR/auto_run_logs; else (exit 1); fi && \
	    if [ ! -f $$DIR/auto_run_logs/log.all ]; then touch $$DIR/auto_run_logs/log.all; else (exit 1); fi; \
	done; \
	for DIR in $$DIRS; \
	do \
	    repo_name=$$(basename `echo "$$DIR" | sed -e 's,/tests/nose.*$$,,'`); \
	    $(MAKE) -C $$DIR test; \
	    ((rc+=$$?)); \
	    f=$$DIR/auto_run_logs/pass.lst && [ -s $$f ] && \
	    succ_cases+=(`cat $$f 2>/dev/null | sed -e "s/^/[$$repo_name]:/g"`); \
	    f=$$DIR/auto_run_logs/fail.lst && [ -s $$f ] && \
	    fail_cases+=(`cat $$f 2>/dev/null | sed -e "s/^/[$$repo_name]:/g"`); \
	done; \
	sleep 2; \
	declare elapsed=`date +"%s"`; \
	((elapsed-=beg)); \
	elapsed=`date -u -d @$$elapsed +"%T"`; \
	if [ $${#fail_cases[@]} -gt 0 ]; then \
	    echo "$${fail_cases[@]}" | tr ' ' '\n' | nl -w4 -s' ' | sed -e 's/^/[E]: >> /g' >&2; \
	fi; \
	echo >&2; \
	echo "[I]: Run nose test finished: succ=$${#succ_cases[@]}, fail=$${#fail_cases[@]} in $$elapsed."; \
	echo >&2; \
	echo >&2; \
	echo >&2; \
	(exit $$rc)

# ------------------------------------------------------------------------------
# force fixing the soft-link incompatible problem from windows
#
# https://www.jianshu.com/p/05b777fa22bc
#
fix_links:
	@cd $(TOP) && \
	if [ "`git config --get core.symlinks`" != "true" ]; then \
		git config core.symlinks true; \
	fi && \
	declare rc=0; \
	for FILE in $(TRACKED_LINK_FILES); \
	do \
	    if ! test -L $$FILE; then \
	        echo "[I]: Fix symbol link `ls -l1d $(TOP)/$$FILE`" >&2; \
	        git checkout -f $$FILE && \
			test -L $$FILE; \
		fi; \
		((rc+=$$?)); \
	done; \
	(exit $$rc)


# ------------------------------------------------------------------------------
# alias target
#
.PHONY: cs clean_cs
cs: cscope

clean_cs: clean_cscope


# ------------------------------------------------------------------------------
# begin of Makefile self unit-test
#
# NOTE:
# * only activate if calling to this Makefile directly
#
ifeq ($(words $(MAKEFILE_LIST)),1)
include $(PROFILE_TOP)/lib.mk
local_test_targets := $(call get_local_test_targets)

.PHONY: _test $(local_test_targets)
_test: $(filter-out _test_usage _test_maintainer-clean,$(local_test_targets))
endif

# shared testing targets
_test_usage:
	$(call test_begin,$@)
	$(MAKE) -f $(firstword $(MAKEFILE_LIST)) usage 2>&1 | grep -sq "^Usage: "
	$(call test_succ,$@)

_test_maintainer-clean: maintainer-clean
	$(call test_begin,$@)
	test -z "$(filter-out .idea/,$(call get_git_untracked_files,./))"
	test -z "`find $(TOP) -type d -empty | grep -vE '\/\.git\/'`"
	$(call test_succ,$@)

_test_clean_cscope: clean_cscope
	$(call test_begin,$@)
	@test -z "`ls -1d $(CSCOPE_FILES) tags 2>/dev/null`"
	$(call test_succ,$@)
#
# end of Makefile self unit-test
# ------------------------------------------------------------------------------
