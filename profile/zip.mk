# ------------------------------------------------------------------------------
# begin of pre-defined constant
#
large_file_condition := +20M
preferred_compressed_file_extension := $(firstword $(COMPRESSED_FILE_EXTENSIONS))

# exclude large files which were not valueable to be compressed
# * .7z .gz .zip: files which were already compressed
compress_forbiden_file_patterns := .7z .gz .zip
# * .py .sh: source files should NOT be compressed to be convenient for read and compare
compress_forbiden_file_patterns += .py .sh .ipynb .json .yaml
# * .jpg .png .mrxs .darmrxs: image files
compress_forbiden_file_patterns += .jpg .png .mrxs .darmrxs
compress_forbiden_file_patterns := $(addprefix %,$(compress_forbiden_file_patterns))
# MUST exclude .git/*!!!
compress_forbiden_file_patterns += $(TOP)/.git/%
# exclude PyCharm files
compress_forbiden_file_patterns += $(TOP)/.idea/%
# exclude tmp directory, especially auto_run_logs directory which contain file with ":" in name.
compress_forbiden_file_patterns += $(TOP)/build/%
compress_forbiden_file_patterns += $(TOP)/tests/nose/auto_run_logs/%
#
# end of pre-defined constant
# ------------------------------------------------------------------------------


git_tracked_files := $(addprefix $(TOP)/,$(call get_git_tracked_files,$(TOP)))
compressed_files := $(call get_compressed_files,$(TOP))
large_files := $(call get_large_files,$(TOP),$(large_file_condition))
large_files := $(filter-out $(compress_forbiden_file_patterns),$(large_files))

git_tracked_compressed_files := $(filter $(git_tracked_files),$(compressed_files))
git_tracked_large_files := $(filter $(git_tracked_files),$(large_files))
git_untracked_large_files := $(filter-out $(git_tracked_files),$(large_files))

decompressed_git_tracked_compressed_files := $(call get_decompressed_file_names,$(git_tracked_compressed_files))
compressed_git_untracked_large_files := $(git_untracked_large_files:=.$(preferred_compressed_file_extension))


decompressed_git_tracked_compressed_files := $(subst :,\:,$(decompressed_git_tracked_compressed_files))
compressed_git_untracked_large_files := $(subst  :,\:,$(compressed_git_untracked_large_files))


.PHONY: usage decompress compress
# reattach "usage" target before .SECONDEXPANSION
# or, the decompress will become the default target
usage:
decompress: $(decompressed_git_tracked_compressed_files)
	@if [ -n "$^" ]; then ls -ld $(patsubst $(TOP)/%,%,$^); else echo "[I]: Nothing to decompress!"; fi

compress: $(compressed_git_untracked_large_files)
	@if [ -n "$^" ]; then ls -ld $(patsubst $(TOP)/%,%,$^); else echo "[I]: Nothing to compress!"; fi


# decompress git tracked compressed files
.SECONDEXPANSION:
$(decompressed_git_tracked_compressed_files): %: %.$(preferred_compressed_file_extension)
	$(call decompress_single_file,$<,$@)
	touch $@

# compress git untracked large files
.SECONDEXPANSION:
$(compressed_git_untracked_large_files): %.$(preferred_compressed_file_extension): %
	$(call compress_single_file,$@,$<)
