
SHELL ?= /bin/bash

# ------------------------------------------------------------------------------
# begin of library function definition
#

#
# common/default "usage" function
#
define f_usage_default
    @echo "Usage: $(MAKE) {`echo $(public_targets) | sed -e 's/ / | /g'`}"
    @echo
endef
ifndef f_usage
define f_usage
	$(call f_usage_default)
endef
endif
#
# recursive wildcard function
#
define rwildcard
$(filter-out ,\
    $(foreach f_dir,$(1),\
        $(foreach f_pat,$(2),\
            $(wildcard $(f_dir)/$(f_pat))\
        )\
    )\
    $(foreach f_pat,$(2),\
        $(foreach f_dir_sub,\
            $(filter-out $(1),$(foreach f_dir,$(1),$(shell find $(f_dir) -type d))),\
            $(wildcard $(f_dir_sub)/$(f_pat))\
        )\
    )\
)
endef
#
# filter directory items
#
define filter_dirs
$(sort \
    $(filter-out ,\
        $(patsubst %/,%,\
            $(filter %/,$(1)\
                $(patsubst %//,%/,\
                    $(shell if [ -n "$(1)" ]; then ls -1dF $(1) 2>/dev/null; fi)\
                )\
            )\
        )\
    )\
)
endef
#
# filter file items
#
define filter_files
$(filter-out $(call filter_dirs,$(1)),$(filter-out %/,$(1)))
endef
#
# cp a b while creating target's directory if not exist
#
define copy2
if [ -d "$(dir $(2))" ] || mkdir -p $(dir $(2)); then cp -u -p $(1) $(2); else false; fi
endef
#
# get all files tracked by given git repo
#
define get_git_tracked_files
$(shell cd $(1); git ls-tree --name-only -r HEAD)
endef
#
# get all changed files in specific git repo
#
define get_git_changed_files
$(shell git status -s -- $(1) | grep -Ev "^\?\?|^\!\!" | awk '{print $$2}')
endef
#
# show files which were not tracked by git, such as untracked files(??) and ignored files(!!)
#
define get_git_untracked_files
$(shell git status -s --ignored -- $(1) | grep -E "^\?\?|^\!\!" | awk '{print $$2}')
endef
#
# get all soft links which were tracked by given git repo
#
define get_git_tracked_links
$(shell cd $(1); git ls-tree -r HEAD | grep "^120000 " | awk '{print $$4}')
endef
#
# find all python module in $(TOP)
#
define get_all_python_module_dirs
$(dir $(shell find $(TOP)/src -type f -name "__init__.py"))
endef
#
# get test targets in current working Makefile
#
# https://unix.stackexchange.com/a/230050
# make -qp | awk -F':' '/^_test_[_\-a-zA-Z0-9][^$#\/\t=]*:([^=]|$)/ {split($1,A,/ /);for(i in A)print A[i]}'
define get_local_test_targets
$(shell awk -F':' '/^_test_[_\-a-zA-Z0-9][^$$#\/\t=]*:([^=]|$$)/ {split($$1,A,/ /);for(i in A)print A[i]}' $(realpath $(firstword $(MAKEFILE_LIST))))
endef
#
# show make test case begin banner
#
define test_begin
@echo "[D]: Run make test \"$(patsubst $(realpath $(TOP))/%,%,$(realpath $(firstword $(MAKEFILE_LIST)))):$(patsubst _test_%,%,$(1))\"..."
@echo
endef
#
# show make test case begin banner
#
define test_succ
@echo
@echo "[I]: Run make test \"$(patsubst $(realpath $(TOP))/%,%,$(realpath $(firstword $(MAKEFILE_LIST)))):$(patsubst _test_%,%,$(1))\"... succ"
endef
#
# Get the file path of current working Makefile
#
define get_makefile_path
$(realpath $(lastword $(MAKEFILE_LIST)))
endef
#
# Get the directory where the current working Makefile was located.
#
define get_makefile_dir
$(patsubst %/,%,$(dir $(call get_makefile_path)))
endef
#
# get compressed files
#
COMPRESSED_FILE_EXTENSIONS ?= 7z
define get_compressed_files
$(call rwildcard,$(1),$(addprefix *.,$(COMPRESSED_FILE_EXTENSIONS)))
endef
#
# get decompressed file names of target compressed files
#
define get_decompressed_file_names
$(basename $(filter $(addprefix %.,$(COMPRESSED_FILE_EXTENSIONS)),$(1)))
endef
#
# get large files by condition
#
define get_large_files
$(shell find $(1) -size $(2))
endef
#
# compress a file
#
# param 1: archive file
# param 2: source file
#
define compress_single_file
a_dir=`realpath $(dir $(1))` && \
if cd $(dir $(2)); then \
    7za a -y -mx=9 $$a_dir/$(notdir $(1)) $(notdir $(2)); \
    rc=$$?; \
    cd - >/dev/null; \
    (exit $$rc); \
fi
endef
#
# decompress a file
#
# param 1: archive file
# param 2: source file
#
define decompress_single_file
a_dir=`realpath $(dir $(1))` && \
if cd $(dir $(2)); then \
    7za x -y $$a_dir/$(notdir $(1)) $(notdir $(2)); \
    rc=$$?; \
    cd - >/dev/null; \
    (exit $$rc); \
fi
endef
#
# end of library function definition
# ------------------------------------------------------------------------------


# ------------------------------------------------------------------------------
# begin of Makefile self unit-test
#
# NOTE:
# * only activate if calling to this Makefile directly
#
ifeq ($(words $(MAKEFILE_LIST)),1)
local_test_targets := $(call get_local_test_targets)

# used by some test case.
TOP ?= $(call get_makefile_dir)/..
PLATFORM_TOP ?= $(TOP)

.PHONY: _test $(local_test_targets)
_test: $(local_test_targets)

_test_f_usage_default:
	$(call test_begin,$@)
	# TODO: "$@" NotImplemented, fake succ
	$(call test_succ,$@)

_test_rwildcard:
	$(call test_begin,$@)
	test -z `echo "$(call rwildcard,$(PLATFORM_TOP)/,tasks.py)" | tr ' ' '\n' | grep -v "tasks\.py$$"`
	$(call test_succ,$@)

_test_filter_dirs:
	$(call test_begin,$@)
	# postive for directory syntax definition
	test "$(call filter_dirs,avit7Atut7ai/)" = "avit7Atut7ai"
	# negative for file syntax definition
	test -z "$(call filter_dirs,avit7Atut7ai)"
	# null => null
	test -z "$(call filter_dirs,)"
	# multi-level abs directory
	test "$(call filter_dirs,/etc/profile.d/)" = "/etc/profile.d"
	test "$(call filter_dirs,/etc/profile.d)" = "/etc/profile.d"
	test "$(call filter_dirs,/etc)" = "/etc"
	# relative directory
	test "$(call filter_dirs,..)" = ".."
	test "$(call filter_dirs,../)" = ".."
	# filter_dirs only provides "directory"
	test -z "`ls -1dF $(call filter_dirs,$(shell ls -1d /etc/*)) | grep -v '\/$$'`"
	# filter_dirs only exclude "non-directory"
	test -z "`ls -1dF $(filter-out $(call filter_dirs,$(shell ls -1d /etc/*)),$(shell ls -1d /etc/*)) | grep '\/$$'`"
	$(call test_succ,$@)

_test_filter_files:
	$(call test_begin,$@)
	# postive for file syntax definition
	test "$(call filter_files,avit7Atut7ai)" = "avit7Atut7ai"
	# negative for directory syntax definition
	test -z "$(call filter_files,avit7Atut7ai/)"
	# null => null
	test -z "$(call filter_files,)"
	# multi-level abs file
	test "$(call filter_files,/usr/bin/env)" = "/usr/bin/env"
	# relative file
	test "$(call filter_files,./Makefile)" = "./Makefile"
	test "$(call filter_files,Makefile)" = "Makefile"
	# filter_files only exclude "non-file"
	test -z "`ls -1dF $(call filter_files,$(shell ls -1d /etc/*)) | grep '\/$$'`"
	# filter_files only provides "file"
	test -z "`ls -1dF $(filter-out $(call filter_files,$(shell ls -1d /etc/*)),$(shell ls -1d /etc/*)) | grep -v '\/$$'`"
	$(call test_succ,$@)

_test_copy2:
	$(call test_begin,$@)
	$(call copy2,/etc/hosts,/tmp/hosts)
	! test /etc/hosts -nt /tmp/hosts
	! test /etc/hosts -ot /tmp/hosts
	! $(call copy2,/etc/hosts,/root/hosts) 2>/dev/null
	! $(call copy2,hai7ouL9Piirooh9su,/tmp/hai7ouL9Piirooh9su) 2>/dev/null
	$(call test_succ,$@)

_fake_change:
	# fake changes in README.md for unit test
	echo "test from $@" >> $(TOP)/README.md

_test_get_git_changed_files: _fake_change
	$(call test_begin,$@)
	test -n "$(filter README.md,$(call get_git_changed_files,$(TOP)))"
	# undo the fake change
	cd $(TOP) && git checkout README.md
	$(call test_succ,$@)

_test_get_git_tracked_files:
	$(call test_begin,$@)
	test -n "$(filter Makefile,$(call get_git_tracked_files,$(TOP)))"
	$(call test_succ,$@)

_test_get_git_tracked_links:
	$(call test_begin,$@)
	test -n "$(filter package-list.txt,$(call get_git_tracked_links,$(PLATFORM_TOP)))"
	$(call test_succ,$@)

# garbage file to test get_git_untracked_files
$(TOP)/tmp.ke7CueC4:
	touch $(TOP)/tmp.ke7CueC4

_test_get_git_untracked_files: $(TOP)/tmp.ke7CueC4
	$(call test_begin,$@)
	test -n "$(filter $(notdir $<),$(call get_git_untracked_files,$(TOP)))"
	rm -f $<
	$(call test_succ,$@)

_test_get_all_python_module_dirs:
	$(call test_begin,$@)
	@ls $(addsuffix __init__.py,$(call get_all_python_module_dirs)) >/dev/null
	test `find $(TOP)/src -name __init__.py | wc -l` -eq `echo "$(call get_all_python_module_dirs)" | xargs | wc -w`
	$(call test_succ,$@)

_test_get_local_test_targets:
	$(call test_begin,$@)
	# TODO: "$@" NotImplemented, fake succ
	$(call test_succ,$@)

_test_test_begin:
	$(call test_begin,$@)
	# TODO: "$@" NotImplemented, fake succ
	$(call test_succ,$@)

_test_test_succ:
	$(call test_begin,$@)
	# TODO: "$@" NotImplemented, fake succ
	$(call test_succ,$@)

_test_get_makefile_path:
	$(call test_begin,$@)
	# TODO: "$@" NotImplemented, fake succ
	$(call test_succ,$@)

_test_get_makefile_dir:
	$(call test_begin,$@)
	# TODO: "$@" NotImplemented, fake succ
	$(call test_succ,$@)

_test_get_decompressed_file_names:
	$(call test_begin,$@)
	test "$(call get_decompressed_file_names,a.7z ./a.7z 9.c ../a.7z /etc/b.7z b.py)" = "a ./a ../a /etc/b"
	$(call test_succ,$@)

_test_get_large_files:
	$(call test_begin,$@)
	# TODO: "$@" NotImplemented, fake succ
	$(call test_succ,$@)

_test_get_compressed_files:
	$(call test_begin,$@)
	# TODO: "$@" NotImplemented, fake succ
	$(call test_succ,$@)

_test_compress_single_file:
	$(call test_begin,$@)
	# generate fake data
	dd if=/dev/zero of=/tmp/1.csv bs=1M count=10
	#
	# test $@[1]: call with absolute path
	#
	$(call compress_single_file,/tmp/1.csv.7z,/tmp/1.csv)
	ls -ld /tmp/1.csv.7z && rm /tmp/1.csv.7z
	#
	# test $@[2]: call with relative path
	#
	if mkdir -p /tmp/$$$$; then \
	    cd /tmp/$$$$ && pwd && $(call compress_single_file,./1.csv.7z,../1.csv) && ls -ld /tmp/$$$$/1.csv.7z; \
		rc=$$?; \
		rm -rf /tmp/$$$$; \
		(exit $$rc); \
	fi
	$(call test_succ,$@)

_test_decompress_single_file:
	$(call test_begin,$@)
	# generate fake data
	dd if=/dev/zero of=/tmp/1.csv bs=1M count=10
	$(call compress_single_file,/tmp/1.csv.7z,/tmp/1.csv)
	rm -f /tmp/1.csv
	#
	# test $@[1]: call with absolute path
	#
	$(call decompress_single_file,/tmp/1.csv.7z,/tmp/1.csv)
	ls -ld /tmp/1.csv && rm /tmp/1.csv
	#
	# test $@[2]: call with relative path
	#
	if mkdir -p /tmp/$$$$; then \
	    cd /tmp/$$$$ && pwd && $(call decompress_single_file,../1.csv.7z,./1.csv) && ls -ld /tmp/$$$$/1.csv; \
		rc=$$?; \
		rm -rf /tmp/$$$$; \
		(exit $$rc); \
	fi
	$(call test_succ,$@)
endif
#
# end of Makefile self unit-test
# ------------------------------------------------------------------------------
