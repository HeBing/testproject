WITH t_merchant_create_dt AS (
        SELECT mrchnt_key_wrd
            , to_date(concat(substr(MRCHNT_CRT_DT, 1, 4), '-', substr(MRCHNT_CRT_DT, 5, 2), '-', substr(MRCHNT_CRT_DT, 7, 2))) AS t_date
        FROM real_mrchnt
        WHERE mrchnt_key_wrd IN (
            SELECT mrchnt_key_wrd
            FROM t_good_merchant_test
        )
    ),
    t_qualified_merchant AS (
        SELECT mrchnt_key_wrd, COUNT(1) AS t_count, SUM(TX_AMT_FLOAT) AS t_amount
        FROM tran_all
        WHERE is_suc = 'Y'
            AND tx_dt BETWEEN 3天内 AND mrchnt_key_wrd IN (
                SELECT mrchnt_key_wrd
                FROM t_merchant_create_dt
                WHERE datediff(现在时间, t_date) >= 模型天数
            )
        GROUP BY mrchnt_key_wrd
        HAVING t_count > 0
    )
SELECT distinct(mrchnt_key_wrd)
FROM t_qualified_merchant
