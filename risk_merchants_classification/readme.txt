# 30天变量修改出7天变量方法：
Tran_agg_mrchnt.xlsx
Description:
	1. 过去30天 -> 过去7天
Variable_name
	1. d30 -> d7
	2. 删除before30_valid_tran_days， before30_fail_tran_num， before30_forward_tran_num，before30_forward_tran_amt
Sql_str
	1. d30 -> d7
	2. 30天内 -> 7天内


Tran_agg_mrchnt_formula.xlsx
Varname
	1. 过去30天->过去7天
	2. 过去30天内有效交易日天数/3 -> 过去3天内有效交易日天数/3
sql_str
	1. d30 ->  d7
	2. d7_valid_tran_days/7  分母为7
	3. 删除CX_RATIO03_TRAN_CNT，CX_RATIO03_TRAN_AMT，CX_RATIO03_FAILTRAN_CNT
Var_name
	1. CX30 -> CX7


！！！注意：
1天变量中，需要将涉及到有效交易天数的分母改成1

直接copy mrchnt相关的两个excel到相应目录
