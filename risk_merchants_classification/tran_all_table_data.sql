insert into TRAN_ALL partition(tx_dt)
with
-- 可用的交易流水表 （非测试交易 & 非基础交易）
-- TODO 删除非合理极大金额的失败交易
TMP_TRAN_ALL_AVALIABLE as (select UUID, MRCHNT_KEY_WRD, TRMNL_NO, CARD_NO_CFRTXT, TX_TM, TX_CD, OFF_LINE_PAY_TP,
CARD_CD_TP, TX_STS, TX_RVS_STS, TX_AMT, cast(TX_AMT as float) as TX_AMT_FLOAT, QRC_OPNID, TX_DT, CARD_MDUM_TP,
concat(substr(tx_dt,1,4),'-',substr(tx_dt,5,2),'-',substr(tx_dt,7,2)) as tx_dt_format,resp_cd
from prod_fct_tx_base_tx_lst_h
where (cast(TX_AMT as float) >= 1 and cast(TX_AMT as float) <= 5000000
and TX_CD in ('10110025','10111125','10120025','10130025','10110001','10110005','10110002','10110006','10110007','10110008','10510001','99999999'))
or tx_cd ='10110009'
),

-- 正向交易 & 30天内 & 按小时段划分交易区间     单条交易       ##仅作为交易时间段判断
TMP_TRAN_HOUR_SLICE as (select uuid,
(case when 0 <= cast(substr(tx_tm, 1, 2) as int) and cast(substr(tx_tm, 1, 2) as int) < 6 then 'hour_0_6'
when 6 <= cast(substr(tx_tm, 1, 2) as int) and cast(substr(tx_tm, 1, 2) as int) < 11 then 'hour_6_11'
when 11 <= cast(substr(tx_tm, 1, 2) as int) and cast(substr(tx_tm, 1, 2) as int) < 15 then 'hour_11_15'
when 15 <= cast(substr(tx_tm, 1, 2) as int) and cast(substr(tx_tm, 1, 2) as int) < 20 then 'hour_15_20'
when 20 <= cast(substr(tx_tm, 1, 2) as int) then 'hour_20' else '' end) as tx_tm_slice
from TMP_TRAN_ALL_AVALIABLE),

-- 正向交易 & 30天内 & 按交易金额范围划分       单条交易        ##仅作为交易时间段判断
TMP_TRAN_AMT_SLICE as (select uuid,
(case when 1 <= TX_AMT_FLOAT and TX_AMT_FLOAT < 20 then 'tx_amt_1_20'
when 20 <= TX_AMT_FLOAT and TX_AMT_FLOAT < 50 then 'tx_amt_20_50'
when 50 <= TX_AMT_FLOAT and TX_AMT_FLOAT < 100 then 'tx_amt_50_100'
when 100 <= TX_AMT_FLOAT and TX_AMT_FLOAT < 200 then 'tx_amt_100_200'
when 200 <= TX_AMT_FLOAT and TX_AMT_FLOAT < 500 then 'tx_amt_200_500'
when 500 <= TX_AMT_FLOAT and TX_AMT_FLOAT < 1000 then 'tx_amt_500_1000'
when 1000 <= TX_AMT_FLOAT and TX_AMT_FLOAT < 2000 then 'tx_amt_1000_2000'
when 2000 <= TX_AMT_FLOAT and TX_AMT_FLOAT < 5000 then 'tx_amt_2000_5000'
when 5000 <= TX_AMT_FLOAT then 'tx_amt_5000' else '' end) as tx_amt_slice
from TMP_TRAN_ALL_AVALIABLE),

-- 交易金额临近值 ceil， round, floor           单笔交易
TMP_TRAN_AMT_ROUND as (select UUID,
-- 四舍五入 个位，十位，百位
round(TX_AMT_FLOAT) as tx_amt_units_round, round(TX_AMT_FLOAT/10)*10 as tx_amt_tens_round, round(TX_AMT_FLOAT/100)*100 as tx_amt_hundreds_round,
-- 向上取整 个位，十位，百位
ceil(TX_AMT_FLOAT) as tx_amt_units_ceil, ceil(TX_AMT_FLOAT/10)*10 as tx_amt_tens_ceil, ceil(TX_AMT_FLOAT/100)*100 as tx_amt_hundreds_ceil,
-- 向下取整 个位，十位，百位
floor(TX_AMT_FLOAT) as tx_amt_units_floor, floor(TX_AMT_FLOAT/10)*10 as tx_amt_tens_floor, floor(TX_AMT_FLOAT/100)*100 as tx_amt_hundreds_floor,
(case when (cast(TX_AMT as float) - floor(cast(TX_AMT as float))) != 0 then 'Y' else 'N' end) as tx_amt_not_integer
from TMP_TRAN_ALL_AVALIABLE)

-- 查余 tx_cd = '10110009'
-- 正向交易 tx_cd in ('10110001','10110005','10110025','10111125','10510001','99999999')
-- 失败交易 TX_STS != '1002300001' OR TX_RVS_STS not in ('1002400001','1002400003','1002400009','1002400006','1002400000')
-- 成功交易 TX_STS = '1002300001' AND TX_RVS_STS in ('1002400001','1002400003','1002400009','1002400006')               # 银联小微都是成功交易，TX_STS='1002300001' AND TX_RVS_STS='1002400001'
-- IC卡 CARD_MDUM_TP = '1001700000'                                                                                     # 银联小微没有IC/磁条卡信息
-- 磁条卡 CARD_MDUM_TP = '1001700001'
-- 贷记卡 CARD_CD_TP = '1001600010'                                                                                     # 银联小微没有贷记卡/借记卡信息
-- 借记卡 CARD_CD_TP = '1001600000'
-- 二维码正向交易 tx_cd in ('10110025','10111125','99999999')                                                           # 银联小微: tx_cd = '99999999' 等同于 OFF_LINE_PAY_TP = '1002000201'
-- 二维码反向交易 tx_cd in ('10120025','10130025')
-- 刷卡正向  tx_cd in ('10110001','10110005','10510001')
-- 刷卡反向  tx_cd in ('10110002','10110006','10110007','10110008')
-- 冲正  tx_cd in ('10110007')
-- 主扫  OFF_LINE_PAY_TP in ('1002000101','1002000111','1002000131','1002000141','1002000151','1002000161','1002000171','1002000181','1002000191')
-- 被扫  OFF_LINE_PAY_TP in ('1002000102','1002000112','1002000132','1002000142','1002000152','1002000162','1002000172','1002000182','1002000192')
-- 微信  OFF_LINE_PAY_TP in ('1002000111','1002000112','1002000110')
-- 支付宝 OFF_LINE_PAY_TP in ('1002000101','1002000102')
-- QQ  OFF_LINE_PAY_TP in ('1002000131','1002000132')
-- 银联所有交易（银联（非小微）二维码 & 银联小微二维码） OFF_LINE_PAY_TP in ('1002000161','1002000162','1002000201')                # 银联所有交易 = 银联（非小微）二维码 + 银联小微二维码， 都是二维码交易，没有非二维码交易。
-- 二维码交易 tx_cd in ('10110025','10111125','10120025','10130025','99999999')                                         # 二维码交易 = 正向 + 反向 + 银联小微二维码， 其中正反向交易中包含了银联（非小微）二维码。
-- 银联(不包含银联小微)二维码交易 OFF_LINE_PAY_TP in ('1002000161','1002000162')                                        # 都包含在二维码的正反向交易中。
-- 银联小微二维码交易 OFF_LINE_PAY_TP = '1002000201'
-- 未支付 TX_STS = '1002300001' AND TX_RVS_STS = '1002400000'

-- 单个商户的信息
select t1.UUID, t1.MRCHNT_KEY_WRD, t1.TRMNL_NO, t1.TX_AMT,t1.TX_AMT_FLOAT, t1.tx_dt_format, t1.TX_TM, t1.CARD_NO_CFRTXT, t1.QRC_OPNID,
t2.tx_tm_slice, t3.tx_amt_slice,
t4.tx_amt_units_round, t4.tx_amt_tens_round, t4.tx_amt_hundreds_round,
t4.tx_amt_units_ceil, t4.tx_amt_tens_ceil, t4.tx_amt_hundreds_ceil,
t4.tx_amt_units_floor, t4.tx_amt_tens_floor, t4.tx_amt_hundreds_floor,t4.tx_amt_not_integer,
-- 正向交易（Y/N）
(case when tx_cd in ('10110009') then 'Y' else 'N' end) as query,
(case when tx_cd in ('10110001','10110005','10110025','10111125','10510001','99999999') then 'Y' else 'N' end) as forward,
-- 成功交易：失败交易 = 'Y' : 'N'
(case when TX_STS = '1002300001' AND TX_RVS_STS in ('1002400001','1002400003','1002400009','1002400006') then 'Y'
when TX_STS != '1002300001' OR TX_RVS_STS not in ('1002400001','1002400003','1002400009','1002400006','1002400000') then 'N'
else '' end) as is_suc,
(case when tx_cd in ('10110001','10110005','10510001','10110002','10110006','10110007','10110008')
       and resp_cd in('4','14','34','38','41','43','51','54','55','61','62','62','75') then 'card'
when tx_cd in ('10110025','10111125','10120025','10130025','99999999') and resp_cd='62' then 'qrcode'
else '' end) as risk_resp_type,
-- 支付卡方式： IC卡：磁条卡= 'IC':'MAG'
(case when CARD_MDUM_TP = '1001700000' then 'IC'
when CARD_MDUM_TP = '1001700001' then 'MAG'
else '' end) as card_mdum,
-- 支付卡 ：  贷记卡：借记卡 = 'CREDIT':'DEBIT'
(case when CARD_CD_TP = '1001600010' then 'CREDIT'
when CARD_CD_TP = '1001600000' then 'DEBIT'
else '' end) as card_type,
-- 二维码正向交易： 'Y'
(case when tx_cd in ('10110025','10111125','99999999') then 'Y' else 'N' end) as qrc_forward,
-- 二维码反向交易： 'Y'
(case when tx_cd in ('10120025','10130025') then 'Y' else 'N' end) as qrc_back,
-- 刷卡正向
(case when tx_cd in ('10110001','10110005','10510001') then 'Y' else 'N' end) as card_forward,
-- 刷卡反向
(case when tx_cd in ('10110002','10110006','10110007','10110008') then 'Y' else 'N' end) as card_back,
-- 冲正
(case when tx_cd = '10110007' then 'Y' else 'N' end) as correct,
-- 主扫：被扫 = 'ACTIVE': 'PASSIVE'
(case when OFF_LINE_PAY_TP in ('1002000101','1002000111','1002000131','1002000141','1002000151','1002000161','1002000171','1002000181','1002000191') then 'ACTIVE'
when OFF_LINE_PAY_TP in ('1002000102','1002000112','1002000132','1002000142','1002000152','1002000162','1002000172','1002000182','1002000192') then 'PASSIVE'
else '' end) as scan,
-- 支付方式    微信：支付宝：QQ：银联 = 'WECHAT':'ALIPAY':'QQ':'UNIONPAY'
(case when OFF_LINE_PAY_TP in ('1002000111','1002000112','1002000110') then 'WECHAT'
when OFF_LINE_PAY_TP in ('1002000101','1002000102') then 'ALIPAY'
when OFF_LINE_PAY_TP in ('1002000131','1002000132') then 'QQ'
when OFF_LINE_PAY_TP in ('1002000161','1002000162','1002000201') then 'UNIONPAY'
else '' end) as pay_type,
-- 用户 将加密卡号与openid融合为user_id
(case when (CARD_NO_CFRTXT <> '' and CARD_NO_CFRTXT is not NULL) then CARD_NO_CFRTXT
when (QRC_OPNID <> '' and QRC_OPNID is not NULL) then QRC_OPNID
else '' end) as user_id,
(case when TX_STS = '1002300001' AND TX_RVS_STS = '1002400000' then 'Y' else 'N' end) as no_pay,
-- 二维码交易 tx_cd in ('10110025','10111125','10120025','10130025','99999999')
(case when tx_cd in ('10110025','10111125','10120025','10130025','99999999') then 'Y' else 'N' end) as qrc,
-- 银联二维码交易 tx_cd in ('10110025','10111125','10120025','10130025') and substr(OFF_LINE_PAY_TP, 1, 2) = 'Q6'
(case when OFF_LINE_PAY_TP in ('1002000161','1002000162','1002000201') then 'Y' else 'N' end) as unionpay_qrc,
-- 银联小微二维码交易 tx_cd = '99999999' and OFF_LINE_PAY_TP = 'Q00'
(case when OFF_LINE_PAY_TP = '1002000201' then 'Y' else 'N' end) as unionpay_xw_qrc,
t1.tx_dt
from TMP_TRAN_ALL_AVALIABLE t1 left join TMP_TRAN_HOUR_SLICE t2 on t1.uuid = t2.uuid
left join TMP_TRAN_AMT_SLICE t3 on t1.uuid = t3.uuid
left join TMP_TRAN_AMT_ROUND t4 on t1.uuid = t4.uuid