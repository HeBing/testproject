#! /usr/bin/env python3
#! -*- coding:utf-8 -*-


import sys
import os
import copy
import nose
from operator import (
    setitem,
    getitem,
    attrgetter,
    itemgetter,
    add,
    eq,
    contains,
    not_,
    methodcaller,
)
from functools import (
    reduce,
    partial,
)
from darwinutils.config import DARWIN_CONFIG
from darwinutils.log import get_task_logger
from darwinutils.helper import (
    f_compose_r,
)
from pipeline.pipeline import PipeLine
from pipeline.pipenode import PipeNode


logger = get_task_logger(__name__)


test_pl_graph_1 = [
    {
        "name": "image_loader",
        "func": ["tasks", "trnet_image_loader", ""],
        "type": PipeNode.TYPE.cold.name,
        "inputs": ["image_paths"],
        "outputs": ["invoice_images"],
        "extra_args": [],
        "extra_kwargs": {},
    },
    {
        "name": "stamp_detection",
        "func": ["tasks", "trnet_stamp_detection", ""],
        "type": PipeNode.TYPE.cold.name,
        "inputs": ["invoice_images:imgs"],
        "outputs": ["stamp_bboxes"],
        "extra_args": [],
        "extra_kwargs": {},
        "flags": ["_max_instances=2"]
    },
    {
        "name": "",
        "func": ["tasks", "trnet_qrcode_detection", ""],
        "type": PipeNode.TYPE.cold.name,
        "inputs": ["invoice_images:imgs"],
        "outputs": ["qrcode_images", "qrcode_positions"],
        "extra_args": [],
        "extra_kwargs": {},
    },
    {
        "name": "",
        "func": ["tasks", "trnet_qrcode_recognition", ""],
        "type": PipeNode.TYPE.cold.name,
        "inputs": ["qrcode_images:imgs"],
        "outputs": ["qrcode_informations"],
        "extra_args": [],
        "extra_kwargs": {},
    },
    {
        "name": "",
        "func": ["tasks", "trnet_invoice_adjustment", ""],
        "type": PipeNode.TYPE.cold.name,
        "inputs": ["invoice_images:imgs", "qrcode_positions:bboxes"],
        "outputs": ["adjusted_images"],
        "extra_args": [],
        "extra_kwargs": {},
    },
    {
        "name": "",
        "func": ["tasks", "trnet_invoice_alignment", ""],
        "type": PipeNode.TYPE.cold.name,
        "inputs": ["adjusted_images:imgs"],
        "outputs": ["aligned_images"],
        "extra_args": [],
        "extra_kwargs": {},
    },
    {
        "name": "",
        "func": ["tasks", "trnet_textbox_detection", ""],
        "type": PipeNode.TYPE.cold.name,
        "inputs": ["aligned_images:imgs"],
        "outputs": ["text_bboxes"],
        "extra_args": [],
        "extra_kwargs": {},
    },
    {
        "name": "",
        "func": ["tasks", "trnet_textbox_crop", ""],
        "type": PipeNode.TYPE.cold.name,
        "inputs": ["aligned_images:imgs", "text_bboxes:bboxes"],
        "outputs": ["text_images"],
        "extra_args": [],
        "extra_kwargs": {},
    },
    {
        "name": "",
        "func": ["tasks", "trnet_textbox_recognition", ""],
        "type": PipeNode.TYPE.cold.name,
        "inputs": ["text_images:imgs"],
        "outputs": ["text_contents"],
        "extra_args": [],
        "extra_kwargs": {},
        "flags": ["gpu"],
    },
    {
        "name": "",
        "func": ["tasks", "trnet_invoice_analysis", ""],
        "type": PipeNode.TYPE.cold.name,
        "inputs": ["stamp_bboxes", "qrcode_informations", "text_contents", "text_bboxes"],
        "outputs": ["invoice_informations"],
        "extra_args": [],
        "extra_kwargs": {},
    },
]


test_pl_graph_2 = copy.deepcopy(test_pl_graph_1)

class Test_basic():
    def test_construct_from_spec(self):
        pl = PipeLine(test_pl_graph_1)

        roots = pl.graph.roots
        _ = list(map(lambda node: logger.info(node.obj.func_name), roots))
        logger.info('1--------------')

        leaves = pl.graph.leaves
        _ = list(map(lambda node: logger.info(node.obj.func_name), leaves))
        logger.info('2--------------')

        nose.tools.assert_equal(roots[0].obj.func_name, test_pl_graph_1[0]["func"][1])
        nose.tools.assert_equal(leaves[0].obj.func_name, test_pl_graph_1[9]["func"][1])

        var_names = sorted(set(map(
            lambda s: s.split(':')[0],
            reduce(add, reduce(add, map(itemgetter("inputs", "outputs"), test_pl_graph_1))))))
        nose.tools.assert_list_equal(var_names, pl.var_names)

    def test_pipeline_embedded(self):
        from pipeline.pipetask import PipeTaskRetry, PipeTask
        import json
        pipeline_1 = [
            {
                "name": "",
                "func": ["tasks", "test_return_lst", ""],
                "type": PipeNode.TYPE.cold.name,
                "inputs": ["input:v"],
                "outputs": ["list1"],
                "extra_args": [],
                "extra_kwargs": {},
            },
            {
                "name": "",
                "func": ["tasks", "test_return_lst", ""],
                "type": PipeNode.TYPE.cold.name,
                "inputs": ["list1:v"],
                "outputs": ["output"],
                "extra_args": [],
                "extra_kwargs": {},
            }
        ]

        pipeline_2 = [
            {
                "name": "",
                "func": ["darwinutils.helper", "start_pipeline", ""],
                "type": PipeNode.TYPE.cold.name,
                "inputs": ["input1:pipeline_str", "input2:input"],
                "outputs": ["list1"],
                "extra_args": [],
                "extra_kwargs": {},
            },
            {
                "name": "",
                "func": ["darwinutils.helper", "start_pipeline", ""],
                "type": PipeNode.TYPE.cold.name,
                "inputs": ["input1:pipeline_str", "input4:input"],
                "outputs": ["list2"],
                "extra_args": [],
                "extra_kwargs": {},
            },
            {
                "name": "",
                "func": ["darwinutils.fn", "f_echo", ""],
                "type": PipeNode.TYPE.cold.name,
                "inputs": ["list1", "list2"],
                "outputs": ["output"],
                "extra_args": [],
                "extra_kwargs": {},
            }
        ]

        p = PipeLine(pipeline_2)
        task = PipeTask(p)
        logger.info("Start execute pipeline:\n")
        r = task(input1=json.dumps(pipeline_1), input2=4, input4=5)
        logger.info("Complete pipeline. Result:\n{}".format(r.get("output")))


@nose.tools.nottest
def test_op_standalone_pl(pl_id=None, op_name=None):
    from functools import (
        partial,
        lru_cache,
    )
    import db.mongodb_api as api
    from pipeline.pipeline import PipeLine

    @lru_cache(maxsize=2000)
    def get_pipeline_info(pl_id):
        condition = {'pipeline_id': pl_id}
        pl_res = api.get_many_by_conds(condition=condition, table='pipeline_info')
        return pl_res

    if pl_id is None:
        pl_id = os.environ.get("pl_id", "f2fe63fb-105b-4db8-8ec6-8b0ef9e4e8f2")
    if op_name is None:
        op_name = os.environ.get("op_name", "start_warm_nodes")

    pl_res = get_pipeline_info(pl_id)
    nose.tools.assert_is_not_none(pl_res)
    nose.tools.assert_greater(len(pl_res), 0)

    sys.path.append(DARWIN_CONFIG["model_location"])

    """
    if pl_res[0].get('is_available') is None or not pl_res[0].get('is_available'):
        logger.error('Inference of pipeline {} failed by pipeline is_available is {}'.format(
            pl_id, pl_res[0].get('is_available'),
        ))
        return {
            "count": 0, 'status': 'Failed', 'result': [],
            'reason': 'Inference of pipeline {} failed by pipeline is_available is {}'.format(
                pl_id, pl_res[0].get('is_available'),
            ),
        }
    """
    pl_graph = pl_res[0].get('graph')
    pl = PipeLine(pl_graph)

    track_results = []
    kwargs = {}
    if op_name == "start_warm_nodes":
        kwargs.update({
            "container": "process",
            "track_results": track_results,
        })
    logger.info("kwargs = {}".format(kwargs))

    if op_name == "start_warm_nodes":
        _ = f_compose_r(
            partial(map, f_compose_r(
                itemgetter(1),
                methodcaller("join"),
            )),
            list,
        )(track_results)
