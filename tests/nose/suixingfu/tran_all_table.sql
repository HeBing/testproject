CREATE TABLE IF NOT EXISTS TRAN_ALL as with
-- 可用的交易流水表 （非测试交易 & 非基础交易）
TMP_TRAN_ALL_AVALIABLE as (select UUID, MRCHNT_KEY_WRD, DVC_KEY_WRD, CARD_NO_CFRTXT, TX_TM, TX_CD, OFF_LINE_PAY_TP, 
CARD_CD_TP, TX_STS, TX_RVS_STS, TX_AMT, cast(TX_AMT as float) as TX_AMT_FLOAT, QRC_OPNID, TX_DT, CARD_MDUM_TP,
concat(substr(tx_dt,1,4),'-',substr(tx_dt,5,2),'-',substr(tx_dt,7,2)) as tx_dt_format
from prod_fct_tx_base_tx_lst_h 
where TX_AMT >= '1' 
and TX_CD not in ('10110024','10110027','10210001','10210009','10210010','10110003','10110004')),

-- 正向交易 & 30天内 & 按小时段划分交易区间     单条交易       ##仅作为交易时间段判断
TMP_TRAN_HOUR_SLICE as (select uuid,
(case when 0 <= cast(substr(tx_tm, 1, 2) as int) and cast(substr(tx_tm, 1, 2) as int) < 6 then 'hour_0_6'
when 6 <= cast(substr(tx_tm, 1, 2) as int) and cast(substr(tx_tm, 1, 2) as int) < 11 then 'hour_6_11'
when 11 <= cast(substr(tx_tm, 1, 2) as int) and cast(substr(tx_tm, 1, 2) as int) < 15 then 'hour_11_15'
when 15 <= cast(substr(tx_tm, 1, 2) as int) and cast(substr(tx_tm, 1, 2) as int) < 20 then 'hour_15_20'
when 20 <= cast(substr(tx_tm, 1, 2) as int) then 'hour_20' else '' end) as tx_tm_slice
from TMP_TRAN_ALL_AVALIABLE),

-- 正向交易 & 30天内 & 按交易金额范围划分       单条交易        ##仅作为交易时间段判断
TMP_TRAN_AMT_SLICE as (select uuid,
(case when 1 <= TX_AMT_FLOAT and TX_AMT_FLOAT < 20 then 'tx_amt_1_20'
when 20 <= TX_AMT_FLOAT and TX_AMT_FLOAT < 50 then 'tx_amt_20_50'
when 50 <= TX_AMT_FLOAT and TX_AMT_FLOAT < 100 then 'tx_amt_50_100'
when 100 <= TX_AMT_FLOAT and TX_AMT_FLOAT < 200 then 'tx_amt_100_200'
when 200 <= TX_AMT_FLOAT and TX_AMT_FLOAT < 500 then 'tx_amt_200_500'
when 500 <= TX_AMT_FLOAT and TX_AMT_FLOAT < 1000 then 'tx_amt_500_1000'
when 1000 <= TX_AMT_FLOAT and TX_AMT_FLOAT < 2000 then 'tx_amt_1000_2000'
when 2000 <= TX_AMT_FLOAT and TX_AMT_FLOAT < 5000 then 'tx_amt_2000_5000'
when 5000 <= TX_AMT_FLOAT then 'tx_amt_5000' else '' end) as tx_amt_slice
from TMP_TRAN_ALL_AVALIABLE),

-- 交易金额临近值 ceil， round, floor           单笔交易
TMP_TRAN_AMT_ROUND as (select UUID,
-- 四舍五入 个位，十位，百位
round(TX_AMT_FLOAT) as tx_amt_units_round, round(TX_AMT_FLOAT/10)*10 as tx_amt_tens_round, round(TX_AMT_FLOAT/100)*100 as tx_amt_hundreds_round,
-- 向上取整 个位，十位，百位
ceil(TX_AMT_FLOAT) as tx_amt_units_ceil, ceil(TX_AMT_FLOAT/10)*10 as tx_amt_tens_ceil, ceil(TX_AMT_FLOAT/100)*100 as tx_amt_hundreds_ceil,
-- 向下取整 个位，十位，百位
floor(TX_AMT_FLOAT) as tx_amt_units_floor, floor(TX_AMT_FLOAT/10)*10 as tx_amt_tens_floor, floor(TX_AMT_FLOAT/100)*100 as tx_amt_hundreds_floor,
(case when (cast(TX_AMT as float) - floor(cast(TX_AMT as float))) >0 then 'Y' else 'N' end) as tx_amt_not_integer
from TMP_TRAN_ALL_AVALIABLE)

-- 正向交易 tx_cd in ('10110001','10110005','10110025','10111125','10510001')
-- 失败交易 TX_STS != "S" AND TX_RVS_STS not in ('N','E','D','R','L')
-- 成功交易 TX_STS = "S" OR TX_RVS_STS IN ('N','E','D','R')
-- IC卡 CARD_MDUM_TP = '00'
-- 磁条卡 CARD_MDUM_TP = '01'
-- 贷记卡 CARD_CD_TP = '01'
-- 借记卡 CARD_CD_TP = '00'
-- 二维码正向交易 tx_cd in ('10110025','10111125') 
-- 二维码反向交易 tx_cd in ('10120025','10130025')
-- 刷卡正向  tx_cd in ('10110001','10110005')
-- 刷卡反向  tx_cd in ('10110002','10110006','10110007','10110008')
-- 冲正  tx_cd in ('10110007')
-- 主扫  substr(OFF_LINE_PAY_TP, 3, 1) = '1'
-- 被扫  substr(OFF_LINE_PAY_TP, 3, 1) = '2'
-- 微信  substr(OFF_LINE_PAY_TP, 1, 2) = 'Q1'
-- 支付宝 substr(OFF_LINE_PAY_TP, 1, 2) = 'Q0'  => in('Q01','Q02','Q03')
-- QQ  substr(OFF_LINE_PAY_TP, 1, 2) = 'Q3'
-- 银联 substr(OFF_LINE_PAY_TP, 1, 2) = 'Q6'     => in('Q6','Q00')
-- 未支付 TX_STS = "S" AND TX_RVS_STS = 'L'

-- 单个商户的信息
select t1.UUID, t1.MRCHNT_KEY_WRD, t1.DVC_KEY_WRD, t1.TX_AMT,t1.TX_AMT_FLOAT, t1.tx_dt, t1.tx_dt_format, t1.TX_TM, t1.CARD_NO_CFRTXT, t1.QRC_OPNID,
t2.tx_tm_slice, t3.tx_amt_slice, 
t4.tx_amt_units_round, t4.tx_amt_tens_round, t4.tx_amt_hundreds_round,
t4.tx_amt_units_ceil, t4.tx_amt_tens_ceil, t4.tx_amt_hundreds_ceil,
t4.tx_amt_units_floor, t4.tx_amt_tens_floor, t4.tx_amt_hundreds_floor,t4.tx_amt_not_integer,
-- 正向交易（Y/N）
(case when tx_cd in ('10110001','10110005','10110025','10111125','10510001') then 'Y' else 'N' end) as forward,
-- 成功交易：失败交易 = 'Y' : 'N'
(case when TX_STS = "S" AND TX_RVS_STS IN ('N','E','D','R') then 'Y' 
when TX_STS != "S" or TX_RVS_STS not in ('N','E','D','R','L') then 'N'
else '' end) as is_suc,
-- 支付卡方式： IC卡：磁条卡：贷记卡：借记卡 = 'IC':'MAG': 'DEBIT': 'CREDIT'
(case when CARD_MDUM_TP = '00' then 'IC' 
when CARD_MDUM_TP = '01' then 'MAG'
when CARD_CD_TP = '01' then 'DEBIT'
when CARD_CD_TP = '00' then 'CREDIT' 
else '' end) as card,
-- 二维码正向交易： 'Y'
(case when tx_cd in ('10110025','10111125') then 'Y' else 'N' end) as qrc_forward,
-- 二维码反向交易： 'Y'
(case when tx_cd in ('10120025','10130025') then 'Y' else 'N' end) as qrc_back,
-- 刷卡正向
(case when tx_cd in ('10110001','10110005') then 'Y' else 'N' end) as card_forward,
-- 刷卡反向
(case when tx_cd in ('10110002','10110006','10110007','10110008') then 'Y' else 'N' end) as card_back,
-- 冲正
(case when tx_cd = '10110007' then 'Y' else 'N' end) as correct,
-- 主扫：被扫 = 'ACTIVE': 'PASSIVE'
(case when substr(OFF_LINE_PAY_TP, 3, 1) = '1' then 'ACTIVE'
when substr(OFF_LINE_PAY_TP, 3, 1) = '2' then 'PASSIVE'
else '' end) as scan,
-- 支付方式    微信：支付宝：QQ：银联 = 'WECHAT':'ALIPAY':'QQ':'UNIONPAY'
(case when substr(OFF_LINE_PAY_TP, 1, 2) = 'Q1' then 'WECHAT'
when substr(OFF_LINE_PAY_TP, 1, 2) in ('Q01','Q02','Q03') then 'ALIPAY'
when substr(OFF_LINE_PAY_TP, 1, 2) = 'Q3' then 'QQ'
when substr(OFF_LINE_PAY_TP, 1, 2) in ('Q6','Q00') then 'UNIONPAY'
else '' end) as pay_type,
-- 用户 将加密卡号与openid融合为user_id
(case when CARD_NO_CFRTXT <> '' then CARD_NO_CFRTXT
when QRC_OPNID <> '' then QRC_OPNID
else '' end) as user_id,
(case when TX_STS = "S" AND TX_RVS_STS = 'L' then 'Y' else 'N' end) as no_pay
from TMP_TRAN_ALL_AVALIABLE t1 left join TMP_TRAN_HOUR_SLICE t2 on t1.uuid = t2.uuid
left join TMP_TRAN_AMT_SLICE t3 on t1.uuid = t3.uuid
left join TMP_TRAN_AMT_ROUND t4 on t1.uuid = t4.uuid