WITH t_merchant_create_dt AS (
		SELECT mrchnt_key_wrd
			, to_date(concat(substr(MRCHNT_CRT_DT, 1, 4), '-', substr(MRCHNT_CRT_DT, 5, 2), '-', substr(MRCHNT_CRT_DT, 7, 2))) AS t_date
		FROM prod_dim_ip_mrchnt_real_h
		WHERE mrchnt_key_wrd IN (
			SELECT mrchnt_key_wrd
			FROM t_good_merchant_test
		)
	),
	t_qualified_merchant AS (
		SELECT mrchnt_key_wrd, COUNT(1) AS t_count, SUM(CAST(TX_AMT AS float)) AS t_amount
		FROM prod_fct_tx_base_tx_lst_h
		WHERE tx_cd NOT IN (
				'10110024',
				'10110027',
				'10210001',
				'10210009',
				'10210010',
				'10110003',
				'10110004'
			)
			AND TX_AMT >= '1'
			AND tx_sts = 'S'
			AND tx_dt BETWEEN 30天内 AND mrchnt_key_wrd IN (
				SELECT mrchnt_key_wrd
				FROM t_merchant_create_dt
				WHERE datediff(现在时间, t_date) >= 模型天数
			)
		GROUP BY mrchnt_key_wrd
		HAVING t_count > 0
	)
SELECT mrchnt_key_wrd
FROM t_qualified_merchant