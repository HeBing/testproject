#-*-coding: utf-8 -*-
from nose.tools import ok_, assert_equal, assert_in, assert_list_equal, assert_dict_equal
import pandas as pd
from time import time
import numpy as np
from data_preprocess.dataframe_process import add_one_hot_columns
from darwinutils.log import get_task_logger
logger = get_task_logger(__name__)


class TestDataDist():
    def setUp(self):
        pass

    def tearDown(self):
        pass

    def test_add_one_hot_columns(self):
        encode_config = [
            {
                "name": "AGNT_SVC_TP",
                "description": "商户所属代理商类型",
                "bool": False
            },
            {
                "name": "MSTR_BRC_TP",
                "description": "商户所属总分店类型",
                "bool": False
            },
            {
                "name": "MRCHNT_CRPRTN_PRPTY",
                "description": "商户所属企业性质类型",
                "bool": False
            },
            {
                "name": "DSPNSBLE_PWD_FLG",
                "description": "商户是否开通小额双免",
                "bool": True
            },
            {
                "name": "STLMNT_CARD_TP",
                "description": "商户所属结算卡类型",
                "bool": False
            },
            {
                "name": "STLMNT_ACCT_TP",
                "description": "商户所属结算卡对应账户类型",
                "bool": False
            },
            {
                "name": "STLMNT_BANK_NO",
                "description": "商户所属结算卡对应的银行编号",
                "bool": False
            }
        ]
        data = pd.read_csv('./suixingfu/total_64_df_5000lines.csv')
        data, process_record = add_one_hot_columns(df=data, encode_config=encode_config, replace=True)
        data.to_csv('./suixingfu/total_64_df_5000lines_encode.csv', index=False)

