#-*-coding: utf-8 -*-
from nose.tools import ok_, assert_equal, assert_in
import os
import pandas as pd
import numpy as np
from time import time, sleep
from darwinutils.ibis_utils import DBConnector, check_thrift_sasl_version
from darwinutils.config import DARWIN_CONFIG
from pipeline.pipeline import PipeLine
from pipeline.pipetask import PipeTaskRetry, PipeTask
from darwinutils.mapreduce import (
    parallel_map_p,
    parallel_map_t,
)
from darwinutils.log import get_task_logger
logger = get_task_logger(__name__)


class TestPipelineDataExtractor():
    db_connector_impala = None

    def __init__(self):
        self.pipeline1 = [
            {
                "name": "load_prod_dim_ip_mrchnt_h_first_100",
                "func": ["data_preprocess.sql_executor", "sql_execute", ""],
                "type": "cold",
                "inputs": ["input:sql_str"],
                "outputs": ["output:dataframe"],
                "extra_args": [],
                "extra_kwargs": {"fetch_rst": True, "use_hive": False}
            },
        ]
        self.pipeline2 = [
            {
                "name": "load_prod_dim_ip_mrchnt_h_first_100",
                "func": ["data_preprocess.sql_executor", "sql_fetch", ""],
                "type": "cold",
                "inputs": ["input:sql_str"],
                "outputs": ["output:dataframe"],
                "extra_args": [],
                "extra_kwargs": {"limit": 10, "use_hive": False}
            },
        ]
        self.pipeline3 = [
            {
                "name": "load_prod_dim_ip_mrchnt_h_first_100_1",
                "func": ["data_preprocess.sql_executor", "sql_execute", ""],
                "type": "cold",
                "inputs": ["input1:sql_str"],
                "outputs": ["dataframe_1"],
                "extra_args": [],
                "extra_kwargs": {"fetch_rst": True, "use_hive": False}
            },
            {
                "name": "load_prod_dim_ip_mrchnt_h_first_100_2",
                "func": ["data_preprocess.sql_executor", "sql_fetch", ""],
                "type": "cold",
                "inputs": ["input2:sql_str"],
                "outputs": ["dataframe_2"],
                "extra_args": [],
                "extra_kwargs": {"limit": 10, "use_hive": False}
            },
            {
                "name": "echo_dataframe_1",
                "func": ["data_preprocess.data_analysis", "get_count", ""],
                "type": "cold",
                "inputs": ["dataframe_1", "dataframe_2"],
                "outputs": ["dataframe_4"],
                "extra_args": [],
                "extra_kwargs": {}
            },
            {
                "name": "load_prod_dim_ip_mrchnt_h_first_100_3",
                "func": ["data_preprocess.sql_executor", "sql_execute", ""],
                "type": "cold",
                "inputs": ["input3:sql_str"],
                "outputs": ["dataframe_3"],
                "extra_args": [],
                "extra_kwargs": {"use_hive": False}
            },
            {
                "name": "echo_dataframe_2",
                "func": ["data_preprocess.data_analysis", "get_count", ""],
                "type": "cold",
                "inputs": ["dataframe_3", "dataframe_4"],
                "outputs": ["output"],
                "extra_args": [],
                "extra_kwargs": {}
            },
        ]

        """
        Pipeline 3 is a DAG as:
        load_prod_dim_ip_mrchnt_h_first_100_1 ----------> echo_dataframe_1 ---------> echo_dataframe_2
                                                -                             -
                                               -                             -
        load_prod_dim_ip_mrchnt_h_first_100_2 -                             -
                                                                           -
                                                                          -
        load_prod_dim_ip_mrchnt_h_first_100_3 ----------------------------
        """

        self.pipeline4 = [
            {
                "name": "load_prod_dim_ip_mrchnt_h_first_100_1",
                "func": ["data_preprocess.sql_executor", "sql_execute_from_file", ""],
                "type": "cold",
                "inputs": ["input:sql_cmd_file"],
                "outputs": ["dataframe_lst_1"],
                "extra_args": [],
                "extra_kwargs": {"file_type": "plain", "use_hive": False}
            },
            {
                "name": "echo_dataframe_1",
                "func": ["data_preprocess.data_analysis", "get_count", ""],
                "type": "cold",
                "inputs": ["dataframe_lst_1"],
                "outputs": ["output"],
                "extra_args": [],
                "extra_kwargs": {}
            }
        ]

        self.pipeline5 = [
            {
                "name": "dataframe_2",
                "func": ["data_preprocess.sql_executor", "sql_execute", ""],
                "type": "cold",
                "inputs": ["input1:sql_str"],
                "outputs": ["dataframe_2"],
                "extra_args": [],
                "extra_kwargs": {"fetch_rst": True, "use_hive": False}
            },
            {
                "name": "dataframe_1",
                "func": ["data_preprocess.sql_executor", "sql_fetch", ""],
                "type": "cold",
                "inputs": ["input2:sql_str"],
                "outputs": ["dataframe_1"],
                "extra_args": [],
                "extra_kwargs": {"limit": 10, "use_hive": False}
            },
            {
                "name": "dataframe_3",
                "func": ["data_preprocess.sql_executor", "sql_execute_from_file", ""],
                "type": "cold",
                "inputs": ["input3:sql_cmd_file"],
                "outputs": ["dataframe_3"],
                "extra_args": [],
                "extra_kwargs": {"file_type": "json", "use_hive": False}
            },
            {
                "name": "merge_dataframe_to_csv",
                "func": ["data_preprocess.data_analysis", "merge_dataframe_to_csv", ""],
                "type": "cold",
                "inputs": ["dataframe_1", "dataframe_3", "dataframe_2"],
                "outputs": ["output"],
                "extra_args": [],
                "extra_kwargs": {"output_file":"test_merged.csv"}
            }
        ]

    @classmethod
    def setUpClass(cls):
        if check_thrift_sasl_version().find("0.2.1") > 0:
            cls._test_impala = True
        else:
            cls._test_impala = False
        if cls._test_impala:
            cls.db_connector = DBConnector(impala_host=DARWIN_CONFIG["impala_config"]["host"],
                                            impala_port=DARWIN_CONFIG["impala_config"]["port"],
                                            hdfs_host=DARWIN_CONFIG["hdfs_config"]["host"],
                                            hdfs_port=DARWIN_CONFIG["hdfs_config"]["port"],
                                            hive_host=DARWIN_CONFIG["hive_config"]["host"],
                                            hive_port=DARWIN_CONFIG["hive_config"]["port"],
                                            user=DARWIN_CONFIG["impala_config"]["user"],
                                            passwd=DARWIN_CONFIG["impala_config"]["passwd"],
                                            database=DARWIN_CONFIG["impala_config"]["database"],
                                            use_hive=False)
            ok_(cls.db_connector is not None)
        else:
            cls.db_connector = DBConnector(impala_host=DARWIN_CONFIG["impala_config"]["host"],
                                                   impala_port=DARWIN_CONFIG["impala_config"]["port"],
                                                   hdfs_host=DARWIN_CONFIG["hdfs_config"]["host"],
                                                   hdfs_port=DARWIN_CONFIG["hdfs_config"]["port"],
                                                   hive_host=DARWIN_CONFIG["hive_config"]["host"],
                                                   hive_port=DARWIN_CONFIG["hive_config"]["port"],
                                                   user=DARWIN_CONFIG["impala_config"]["user"],
                                                   passwd=DARWIN_CONFIG["impala_config"]["passwd"],
                                                   database=DARWIN_CONFIG["impala_config"]["database"],
                                                   use_hive=True)
            ok_(cls.db_connector is not None)
        cls.db_connector.connect()
        cls.db_connector.connect_database()
        tables = cls.db_connector.list_tables()
        #logger.info(tables)
        if "test" not in tables:
            # cls.db_connector.drop_table("test")
            df = pd.DataFrame(data=[[1, 2, "3", "20190301", "123456"],
                                    [11, 12, "13", "20190401", "113456"],
                                    [21, 22, "23", "20190501", "213456"]],
                              columns=["n1", "n2", "s1", "d1", "t1"])
            ok_(cls.db_connector.create_table(table_name="test", table_obj=df))
            ok_(cls.db_connector.connect_table(table_name="test") is not None)
        pass

    def tearDown(self):
        pass

    def test_sql_fetch_sync(self):
        p = PipeLine(self.pipeline1)
        task = PipeTask(p)
        logger.info("============Test pipeline1============================")
        r = task(input="select * from test")
        logger.info(type(r.get("output")))
        assert_equal(3, len(r.get("output")))
        p = PipeLine(self.pipeline2)
        task = PipeTask(p)
        logger.info("=============Test pipeline2===========================")
        r = task(input="select * from test")
        logger.info(type(r.get("output")))
        assert_equal(3, len(r.get("output")))

    def test_sql_two_nodes_sync(self):
        p = PipeLine(self.pipeline3)
        task = PipeTask(p)
        logger.info("============Test pipeline3============================")
        r = task(input1="select * from test", input2="select * from test where n1=1", input3="show tables")
        logger.info(r.get("output"))
        assert_equal(len(self.db_connector.list_tables()) + 4, r.get("output"))

    def test_sql_timeout_sync(self):
        return
        # TODO: The test case is invalid, I need change it later
        DARWIN_CONFIG["pipeline_task_default_retry_timeout"] = 0.001
        p = PipeLine(self.pipeline3)
        task = PipeTask(p)
        logger.info("============Test pipeline3============================")
        r = task(input1="select * from test", input2="select * from test where n1=1", input3="show tables",
                 graph_traverse_executor=[parallel_map_p, map, map])
        logger.info(r.get("output"))
        #assert_equal(len(self.db_connector.list_tables()) + 4, r.get("output"))

    def test_sql_file_sync(self):
        test_content = {
            "sql_cmd_lst": [
                {
                    "sql_str": "select * from test",
                    "fetch_rst": True,
                    "onebyone": False
                },
                {
                    "sql_str": "select * from test where n1=1",
                    "onebyone": True
                }
            ]
        }
        # first test json format
        with open("test_file.json", 'w') as f:
            import json
            json.dump(test_content, f, indent=2)
        self.pipeline4[0]["extra_kwargs"]["file_type"] = "json"
        p = PipeLine(self.pipeline4)
        task = PipeTask(p)
        logger.info("============Test pipeline4============================")
        r = task(input=os.path.join(os.getcwd(), "test_file.json"))
        logger.info(r.get("output"))
        os.remove(os.path.join(os.getcwd(), "test_file.json"))
        assert_equal(2, r.get("output"), r)

        file_content = list(map(lambda s: s.get("sql_str")+"\n", test_content.get("sql_cmd_lst")))
        with open("test_file.txt", 'w') as f:
            f.writelines(file_content)
        self.pipeline4[0]["extra_kwargs"]["file_type"] = "plain"
        p = PipeLine(self.pipeline4)
        task = PipeTask(p)
        logger.info("============Test pipeline4============================")
        r = task(input=os.path.join(os.getcwd(), "test_file.txt"))
        logger.info(r.get("output"))
        os.remove(os.path.join(os.getcwd(), "test_file.txt"))
        assert_equal(2, r.get("output"), r)

        write_lst = []
        for item in test_content["sql_cmd_lst"]:
            write_lst.append([item.get("sql_str"), item.get("fetch_rst", True)])
        file_content = pd.DataFrame(write_lst, columns=["sql_str", "fetch_rst"])
        file_content.to_excel("test_file.xlsx", index=False)

        self.pipeline4[0]["extra_kwargs"]["file_type"] = "excel"
        p = PipeLine(self.pipeline4)
        task = PipeTask(p)
        logger.info("============Test pipeline4============================")
        r = task(input=os.path.join(os.getcwd(), "test_file.xlsx"))
        logger.info(r.get("output"))
        os.remove(os.path.join(os.getcwd(), "test_file.xlsx"))
        assert_equal(2, r.get("output"), r)

    def test_merge_dataframe_to_csv(self):
        test_content = {
            "sql_cmd_lst": [
                {
                    "sql_str": "select * from test where n1=1",
                    "fetch_rst": True,
                    "onebyone": False
                },
                {
                    "sql_str": "select * from test where n1=1",
                    "onebyone": True
                }
            ]
        }
        # first test json format
        with open("test_file.json", 'w') as f:
            import json
            json.dump(test_content, f, indent=2)
        p = PipeLine(self.pipeline5)
        task = PipeTask(p)
        logger.info("============Test pipeline3============================")
        r = task(input1="select * from test where n1=11", input2="select * from test where n1=21",
                 input3=os.path.join(os.getcwd(), "test_file.json"))
        ok_(r.get("output"))
        df = pd.read_csv("test_merged.csv")
        ok_(all(df["n1"].values == np.array([21, 11, 1, 1])))
        os.remove("test_merged.csv")

    def test_cnt(self):
        import random
        from darwinutils.helper import loop_run_cnt
        a = []
        n_cnt = 5
        timeout = 5

        def add_a():
            s = random.random()
            a.append(s)
            return s
        r = loop_run_cnt(add_a, lambda x: x>=n_cnt-1)
        assert_equal(len(a), n_cnt)
        assert_equal(r, a[-1])
        logger.info("loop_run_cnt rst: {} - {}".format(r, a))

        r = loop_run_cnt(add_a, n_cnt)
        assert_equal(len(a), n_cnt*2)
        assert_equal(r, a[-1])
        logger.info("loop_run_cnt rst: {} - {}".format(r, a))

        def sleep_a():
            sleep(timeout*2)

        a = []
        # expect timeout
        try:
            r = loop_run_cnt(sleep_a, n_cnt, timeout=timeout)
            ok_(False)
        except TimeoutError:
            logger.info(r)
            ok_(True)
        except Exception as e:
            logger.info(str(e))
            ok_(False)

    #'''
    def test_sql_fetch_async(self):
        return
        ###### Centos 6 cannot run it
        p = PipeLine(self.pipeline1)
        task = PipeTaskRetry(p)
        logger.info("============Test pipeline1============================")
        r = task(input="select * from test")
        logger.info(type(r.get("output")))

        p = PipeLine(self.pipeline2)
        task = PipeTaskRetry(p)
        logger.info("=============Test pipeline2===========================")
        r = task(input="select * from test")
        logger.info(type(r.get("output")))
    #'''
