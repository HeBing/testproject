#! /usr/bin/env python3
#! -*- coding:utf-8 -*-


import uuid
import nose
import time
from darwinutils.config import DARWIN_CONFIG
from darwinutils.log import get_task_logger
from pipeline.pipevariable import (
    PipeVariableBase,
    PipeVariableCelery,
    PipeVariableRedis,
)


logger = get_task_logger(__name__)


class Test_pipevariable_basic:
    def test_define(self):
        pl_var = PipeVariableBase("var1")
        nose.tools.assert_equal(pl_var.name, "var1")
        nose.tools.assert_equal(pl_var.func_name, "var1")

        pl_var = PipeVariableBase("var1:var2")
        nose.tools.assert_equal(pl_var.name, "var1")
        nose.tools.assert_equal(pl_var.func_name, "var2")

        pl_var = PipeVariableBase("var1:var2:var_garadfa")
        nose.tools.assert_equal(pl_var.name, "var1")
        nose.tools.assert_equal(pl_var.func_name, "var2")

    def test_get_set(self):
        pl_var = PipeVariableBase("var1")
        pl_var.value = 10
        nose.tools.assert_equal(pl_var.value, 10)


"""
def sleep_put(q):
    import random
    import time
    s = random.random(10,50) / 10
    logger.info("sleep {:.2f}".format(s))
    time.sleep(s)

    q.put(s)
    return s


class Test_mp:
    def test_basic(self):
        from multiprocessing import Queue, Pool
        from darwinutils.mapreduce import parallel_map_p
        p = Pool(5)

        q = Queue()
        results = list(p.map(sleep_put, [q]*2))
        logger.info(results)
"""
