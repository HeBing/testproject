#! /usr/bin/env python3
#! -*- coding:utf-8 -*-


import uuid
import nose
import math
import random
import time
import numpy as np
from collections import (Callable)
from operator import (
    methodcaller,
    attrgetter,
    itemgetter,
    not_,
    eq,
    concat,
    delitem,
    getitem,
)
from itertools import (
    compress,
    starmap,
)
from functools import (
    partial,
    reduce,
)
from inspect import (
    signature,
    Signature,
)
from darwinutils.config import DARWIN_CONFIG
from darwinutils.mapreduce import (
    parallel_map_p,
    parallel_map_t,
    parallel_starmap_p,
    parallel_starmap_t,
)
from darwinutils.log import get_task_logger
from darwinutils.fn import (
    f_compose_r,
    f_star_call,
    f_echo,
    f_si_call,
    f_timeit_call,
    f_list_rize,
    f_group,
    f_star2_call,
    f_compose_l,
    f_unstar_call,
    f_flip_call,
)
from pipeline.common import (
    PLNodeSessDataSpec,
    PLFuncModuleSpec,
    PLCacheSpec,
    puas,
)
from pipeline.pipenode import (
    f_get_pl_node_input_var_names,
    inspect_func,
    is_grpc_accepted_channel,
    is_redis_accepted_channel,
    is_rpc_accepted_channel,
    is_zrpc_accepted_channel,
    load_and_inspect_func_from_spec,
    PayloadEvent,
    PipeNode,
    PipeQueue,
    pl_load_func_from_spec,
)


logger = get_task_logger(__name__)

def fcall2(pl_node, inst_id, data, i, **kwargs):
    # TODO: dynamically generate pl_node call signature
    return pl_node(a=data[i][0], b=data[i][1], _target_instance=inst_id, **kwargs)


def func1(a, b, c=1, *args, kwa, kwb, kwc=3, **kwargs):
    pass
    """
    _kwargs = {
        "a": a,
        "b": b,
        "c": c,
        "args": args,
        "kwa": kwa,
        "kwb": kwb,
        "kwc": kwc,
        "kwargs": kwargs,
    }
    _ = list(map(f_compose_r(
        "direct_kwargs  >> {}".format,
        logger.info,
    ), _kwargs.items()))
    """

    import inspect
    from functools import partial
    frame = inspect.currentframe()
    ins_result = inspect.getargvalues(frame)

    ins_args, ins_varargs, ins_varkw, ins_values = ins_result
    ins_kwargs = f_compose_r(
        lambda _: [[
            f_compose_r(partial(map, f_echo), list),
            f_compose_r(partial(map, ins_values.get), list),
        ], [[_]]*2],
        partial(f_star_call, zip),
        partial(starmap, f_star_call),
        partial(f_star_call, zip),
        dict,
    )(ins_args)
    """
    f_name = inspect.getframeinfo(frame)[2]
    _ = list(map(f_compose_r(
        "inspect_kwargs >> {}".format,
        logger.info,
    ), ins_kwargs.items()))
    logger.info("inspect_varargs >> {}".format(ins_values.get(ins_varargs)))
    logger.info("inspect_varkw   >> {}".format(ins_values.get(ins_varkw)))
    """

    return ins_kwargs, args, kwargs


func2 = partial(func1, 12, kwb=22)

def test_signature():
    from inspect import Parameter

    sig1 = signature(func1)
    """
    _ = f_compose_r(
        attrgetter("parameters"),
        dict,
        methodcaller("values"),
        partial(map, f_compose_r(
            attrgetter("kind", "name", "default"),
            "sig1 >> {}".format,
            logger.info,
        )),
        list,
    )(sig1)
    """


    sig2 = signature(func2)
    #"""
    _ = f_compose_r(
        attrgetter("parameters"),
        dict,
        methodcaller("values"),
        partial(map, f_compose_r(
            attrgetter("kind", "name", "default"),
            "sig2 >> {}".format,
            logger.info,
        )),
        list,
    )(sig2)
    #logger.info("---------------------------")
    #"""

    matrix = [
        [sig1, [
            [Parameter.POSITIONAL_OR_KEYWORD, 'a', Parameter.empty],
            [Parameter.POSITIONAL_OR_KEYWORD, 'b', Parameter.empty],
            [Parameter.POSITIONAL_OR_KEYWORD, 'c', 1],
            [Parameter.VAR_POSITIONAL, 'args', Parameter.empty],
            [Parameter.KEYWORD_ONLY, 'kwa', Parameter.empty],
            [Parameter.KEYWORD_ONLY, 'kwb', Parameter.empty],
            [Parameter.KEYWORD_ONLY, 'kwc', 3],
            [Parameter.VAR_KEYWORD, 'kwargs', Parameter.empty],
        ]],
        [sig2, [
            #[Parameter.POSITIONAL_OR_KEYWORD, 'a', Parameter.empty],
            [Parameter.POSITIONAL_OR_KEYWORD, 'b', Parameter.empty],
            [Parameter.POSITIONAL_OR_KEYWORD, 'c', 1],
            [Parameter.VAR_POSITIONAL, 'args', Parameter.empty],
            [Parameter.KEYWORD_ONLY, 'kwa', Parameter.empty],
            [Parameter.KEYWORD_ONLY, 'kwb', 22],
            [Parameter.KEYWORD_ONLY, 'kwc', 3],
            [Parameter.VAR_KEYWORD, 'kwargs', Parameter.empty],
        ]]
    ]
    _ = f_compose_r(
        partial(map, f_compose_r(
            lambda _: (
                f_compose_r(
                    attrgetter("parameters"),
                    dict,
                    methodcaller("values"),
                    partial(map, f_compose_r(
                        attrgetter("kind", "name", "default"),
                    )),
                    list,
                )(_[0]),
                _[1],
            ),
            partial(f_star_call, zip),
            partial(map, f_compose_r(
                partial(f_star_call, zip),
                partial(starmap, nose.tools.assert_equal),
                list,
            )),
            list,
        )),
        list,
    )(matrix)



class Test_load_and_inspect:
    def test_load_from_spec_tuple(self):
        self.test_load(["pandas", "read_csv", ""])

    def test_load_from_spec(self):
        self.test_load(PLFuncModuleSpec(*["pandas", "read_csv", ""]))

    def test_inspect_func(self):
        spec = ["pandas", "read_csv", ""]
        nose.tools.assert_is_instance(f_compose_r(
            pl_load_func_from_spec,
            inspect_func,
        )(spec), Signature)

    def test_inspect_func_from_spec(self):
        spec = ["pandas", "read_csv", ""]
        nose.tools.assert_is_instance(f_compose_r(
            inspect_func,
        )(spec), Exception)

    def test_load_and_inspect_from_spec(self):
        spec = load_and_inspect_func_from_spec(["pandas", "read_csv", ""])
        nose.tools.assert_is_instance(spec, Signature)

    def test_inspect_parallel(self):
        r_types, func_specs = zip(*[
            [Signature, ["pandas", "read_csv", ""]],
            [Signature, ["tasks", "trnet_image_loader", ""]],
            [ValueError, ["numpy", "zeros", ""]],
        ])

        results = list(parallel_map_p(load_and_inspect_func_from_spec, func_specs))
        _ = list(map(logger.debug, zip(func_specs, results)))
        func_mask = list(map(lambda item: isinstance(item[0], item[1]), zip(results, r_types)))
        nose.tools.ok_(all(func_mask), '{}'.format(list(compress(zip(func_specs, results), map(not_, func_mask)))))

    @nose.tools.nottest
    def test_load(self, func_spec):
        func = pl_load_func_from_spec(func_spec)
        nose.tools.assert_is_instance(func, Callable)
        nose.tools.assert_equal(func.__name__, func_spec[1])

    @nose.tools.nottest
    def test_inspect(self, func):
        spec = inspect_func(func)
        nose.tools.assert_is_instance(spec, Signature)


class Test_basic():
    def __init__(self):
        self.pl_node_spec = {
            "name": "test_func",
            "func": ["test_pipenode", "func1", ""],
            "type": PipeNode.TYPE.cold.name,
            "inputs": ['b', 'c', "kwa"],
            "outputs": ['param_c', 'param_d'],
            "extra_args": ['a_value'],
            "extra_kwargs": {"kwb": 3.65},
            "flags": ["乖宝宝", "给\"没有bug\"功能开bug", "cpu", "_a=b"]
        }

    def test_get_flags(self):
        pl_node = PipeNode.create_from_node_spec(self.pl_node_spec)
        flags = pl_node.flags
        nose.tools.assert_sequence_equal(flags, ["cpu", "乖宝宝", "给\"没有bug\"功能开bug"])

    def test_get_kwflags(self):
        pl_node_spec = {
            "name": "test_func",
            "func": ["tasks", "add", ""],
            "type": PipeNode.TYPE.cold.name,
            "inputs": ['x', 'y'],
            "outputs": ['r'],
            "extra_args": [],
            "extra_kwargs": {},
            "flags": ["乖宝宝", "给\"没有bug\"功能开bug", "cpu", "a=b=c", 'b=']
        }

        pl_node = PipeNode.create_from_node_spec(pl_node_spec)
        nose.tools.assert_sequence_equal(pl_node.flags, ["cpu", "乖宝宝", "给\"没有bug\"功能开bug"])

        nose.tools.assert_dict_equal(pl_node.kwflags, {"a": "b=c", "b": ""})

        pl_node_spec['flags'] = ["cpu"]
        pl_node = PipeNode.create_from_node_spec(pl_node_spec)
        nose.tools.assert_dict_equal(pl_node.kwflags, {})

    def test_get_pl_node_input_output_var_names(self):
        pl_node = PipeNode.create_from_node_spec(self.pl_node_spec)

        result = list(f_get_pl_node_input_var_names(pl_node))
        nose.tools.assert_sequence_equal(result, ['b', 'c', "kwa"])
        nose.tools.assert_sequence_equal(result, pl_node.input_var_names)

        nose.tools.assert_sequence_equal(pl_node.output_var_names, ["param_c", "param_d"])

    def test_get_pl_node_input_output_arg_names(self):
        pl_node_spec = {
            "name": "test_func",
            "func": ["test_pipenode", "func1", ""],
            "type": PipeNode.TYPE.cold.name,
            "inputs": ['b:_b', 'c:_c', "kwa"],
            "outputs": ['param_c:_param_c', 'param_d'],
            "extra_args": ['a_value'],
            "extra_kwargs": {"kwb": 3.65},
            "flags": ["乖宝宝", "给\"没有bug\"功能开bug", "cpu", "_a=b"]
        }
        pl_node = PipeNode.create_from_node_spec(pl_node_spec)
        nose.tools.assert_sequence_equal(pl_node.input_arg_names, ["_b", "_c", "kwa"])
        nose.tools.assert_sequence_equal(pl_node.output_arg_names, ["_param_c", "param_d"])

    def test_property_define(self):
        name, type_ = itemgetter('name', 'type')(self.pl_node_spec)

        pl_node = PipeNode(name, type_)
        nose.tools.assert_equal(name, pl_node.name)
        nose.tools.assert_equal(type_, pl_node.type)

        pl_node = PipeNode.create_from_node_spec(self.pl_node_spec)
        nose.tools.assert_equal(name, pl_node.name)
        nose.tools.assert_equal(type_, pl_node.type)

    def test_get_uuid(self):
        pass
        """
        Test PipeNode "uuid" readonly property's getter
        """
        # make sure the seed has not been changed
        nose.tools.assert_equal(DARWIN_CONFIG["pipeline_uuid_namespace"], "978b9962-b28b-435d-8cbc-5a579c8b0d9b")

        # basic test
        pl_node = PipeNode.create_from_node_spec(self.pl_node_spec)
        uuid1 = pl_node.uuid

        # change one attributes
        self.pl_node_spec['inputs'][0] = "paiteifaegoo4ieg7bah7vey3pho4ohc"
        pl_node = PipeNode.create_from_node_spec(self.pl_node_spec)
        uuid2 = pl_node.uuid

        # change more other attributes
        self.pl_node_spec['extra_kwargs'].update({
            "hello": "rei4ahu7ohm3aina4niez4eepeigh4ro",
            "data": list(range(10)),
        })
        pl_node = PipeNode.create_from_node_spec(self.pl_node_spec)
        uuid3 = pl_node.uuid

        matrix = [
            (
                uuid1,
                uuid2,
                uuid3,
            ),
            #('c380821c-cd92-3f19-a167-6dd9dccd1cf3',
            # '3e684c67-3d06-3613-8f34-5df40186bc0b',
            # '05b91237-60e2-36e5-9f9b-7316ca05ce2d'),
            ('f784bfc0-c524-3d9e-80a0-7a1b621f403a',
             '0e156f44-601f-33c7-ae93-a22a7cf19837',
             '5afe6598-ba1e-3345-bd7e-dffd1bb93268',
             ),
        ]
        nose.tools.assert_sequence_equal(*matrix)

    def test_get_func(self):
        pass
        """
        Test PipeNode "func" property's getter
        """
        pl_node = PipeNode.create_from_node_spec(self.pl_node_spec)
        nose.tools.assert_is_instance(pl_node.func, Callable)
        nose.tools.assert_equal(id(pl_node.func), id(func1))
        logger.info(pl_node.func)

    def test_get_signature_direct(self):
        pass
        """
        test PipeNode "signature" property's getter
        """
        pl_node = PipeNode.create_from_node_spec(self.pl_node_spec)

        # should be None before attribute get
        nose.tools.assert_equal(pl_node._c_signature, None)
        # now, the attribute will be filled as a Signature object
        _signature = pl_node.signature
        nose.tools.assert_is_instance(_signature, Signature)
        #logger.info(pl_node.signature)

        # now, the spec was cached by pl node
        _signature2 = pl_node.signature
        nose.tools.assert_equal(id(_signature), id(_signature2))

    def test_get_signature_indirect(self):
        pass
        """
        test PipeNode "signature" property's getter which indirectly triggered by "func" property
        """
        pl_node = PipeNode.create_from_node_spec(self.pl_node_spec)

        nose.tools.assert_is_none(pl_node._c_func)
        nose.tools.assert_is_instance(pl_node.func, Callable)
        nose.tools.assert_is_not_none(pl_node._c_func)

        nose.tools.assert_is_none(pl_node._c_signature)
        nose.tools.assert_is_instance(pl_node.signature, Signature)
        nose.tools.assert_is_not_none(pl_node._c_signature)

        #logger.info(pl_node.signature)

    def test_get_formatted_flags(self):
        pl_node_spec = {
            "name": "test_func",
            "func": ["tasks", "add", ""],
            "type": PipeNode.TYPE.cold.name,
            "inputs": ['x', 'y'],
            "outputs": ['r'],
            "extra_args": [],
            "extra_kwargs": {},
            "flags": ["乖宝宝", "给\"没有bug\"功能开bug", "cpu", "a=b=c", 'b=']
        }
        pl_node = PipeNode.create_from_node_spec(pl_node_spec)
        nose.tools.assert_sequence_equal(
            pl_node.formatted_flags,
            [
                "a=b=c",
                "b=",
                "cpu",
                "乖宝宝",
                "给\"没有bug\"功能开bug",
            ],
        )

    def test_get_spec(self):
        pass
        """
        test PipeNode "spec" property's getter
        """
        pl_node_spec = {
            "name": "test_func",
            "func": ["tasks", "add", ""],
            "type": PipeNode.TYPE.cold.name,
            "inputs": ['x', 'y'],
            "outputs": ['r'],
            "extra_args": [],
            "extra_kwargs": {},
            "flags": ["乖宝宝", "给\"没有bug\"功能开bug", "cpu", "a=b=c", 'b=']
        }
        pl_node = PipeNode.create_from_node_spec(pl_node_spec)
        #_spec_in = pl_node_spec
        #_spec_out = pl_node.spec
        #_ = list(map(logger.info, _spec_in.items()))
        #logger.info("----------------")
        #_ = list(map(logger.info, _spec_out.items()))
        nose.tools.assert_dict_equal(
            pl_node.spec,
            {
                "name": "test_func",
                "func": ("tasks", "add", ""),
                "type": PipeNode.TYPE.cold.name,
                "inputs": ['x', 'y'],
                "outputs": ['r'],
                "extra_args": (),
                "extra_kwargs": {},
                "flags": sorted(("乖宝宝", "给\"没有bug\"功能开bug", "cpu", "a=b=c", 'b='), key=str),
            }
        )

    def test_repr_node(self):
        pl_node = PipeNode.create_from_node_spec(self.pl_node_spec)
        pl_node_clone1 = eval(repr(pl_node))
        pl_node_clone2 = eval(repr(pl_node_clone1))

        nose.tools.assert_equal(repr(pl_node), repr(pl_node_clone1))
        nose.tools.assert_dict_equal(pl_node.spec, pl_node_clone1.spec)

        nose.tools.assert_equal(repr(pl_node_clone1), repr(pl_node_clone2))
        nose.tools.assert_dict_equal(pl_node_clone1.spec, pl_node_clone2.spec)

        _ = list(map(logger.debug, pl_node_clone2.spec.items()))
        logger.debug(repr(pl_node_clone2))

class Test_cold:
    def __init__(self):
        #def func1(a, b, c=1, *args, kwa, kwb, kwc=3, **kwargs):
        pl_node_def = {
            "name": "",
            "type": PipeNode.TYPE.cold.name,
            "func": ["test_pipenode", "func1", ""],
            "inputs": ["imgs:b", "paths:kwa"],
            "outputs": ["imgs"],
            "extra_args": [22], # a=22
            #"extra_args": [],
            "extra_kwargs": {"kwb": 33},
            #"extra_kwargs": {},
        }
        self.pl_node = PipeNode.create_from_node_spec(pl_node_def)


    def test_call(self):
        pl_node = self.pl_node
        _ = f_compose_r(
            attrgetter("name", "type", "signature", "inputs", "outputs", "extra_args", "extra_kwargs"),
            partial(map, logger.debug),
            list,
        )(pl_node)

        f_verify_matrix = f_compose_r(
            partial(map, f_compose_r(
                partial(f_star_call, lambda arg, truth: (pl_node(*arg[0], **arg[1]), truth)),
                partial(f_star_call, zip),
                partial(zip, [
                    nose.tools.assert_equal,
                    nose.tools.assert_sequence_equal,
                    nose.tools.assert_equal,
                ]),
                partial(starmap, f_star_call),
                list,
            )),
            list,
        )

        matrix = [
            # case 1:
            [
                # input
                [
                    # input args
                    [111],
                    # input kwargs
                    {"kwa": 3, "kwx": 4},
                ],
                # expected output
                [
                    # expected named args
                    {
                        "c": 1, # keep default
                        "kwc": 3,

                        "a": 22,
                        "kwb": 33,

                        "b": 111,
                        "kwa": 3,
                    },
                    # expected variable args
                    (),
                    # expected variable kwargs
                    {
                        "kwx": 4
                    },
                ],
            ],
            # case 2:
            [
                # input
                [
                    # input args
                    [111, 2222, 3333],
                    # input kwargs
                    {"kwa": 3, "kwx": 4},
                ],
                # expected output
                [
                    # expected named args
                    {
                        "c": 2222, # override extra_args
                        "kwc": 3,

                        "a": 22,
                        "kwb": 33,

                        "b": 111,
                        "kwa": 3,
                    },
                    # expected variable args
                    (3333,),
                    # expected variable kwargs
                    {
                        "kwx": 4
                    },
                ],
            ], #  case 1 end
        ]
        _ = f_verify_matrix(matrix)


class Test_pipequeue:
    def test_iterator(self):
        import time
        from pipeline.common import PLCacheSpec
        in_data = list(map(
            lambda _: PLCacheSpec(time.time(), PLNodeSessDataSpec(*_)), [
            [str(uuid.uuid4()), np.random.randint(0, 10, (5, 5)).tolist()],
            [str(uuid.uuid4()), np.random.randint(0, 10, (1, 5)).tolist()],
            [str(uuid.uuid4()), np.random.randint(0, 10, (10, 5)).tolist()],
            [str(uuid.uuid4()), np.random.randint(0, 10, (1, 5)).tolist()],
            [str(uuid.uuid4()), np.random.randint(0, 10, (10, 5)).tolist()],
        ]))
        from darwinutils.mapreduce import parallel_map_t, parallel_map_p
        for executor in [
            map,
            parallel_map_t,
            # TODO: not support multiprocess yet
            #parallel_map_p,
        ]:
            q = PipeQueue()
            _ = f_compose_r(
                partial(executor, f_compose_r(
                    q.put,
                )),
                list,
            )(in_data)
            q.put(StopIteration())

            enq_times, in_data_lst = zip(*in_data)
            sess_lst, in_data_lst = zip(*in_data_lst)
            _ = list(map(logger.info, enumerate(in_data_lst)))

            # process first 2(0-1)
            out_data_lst = list(map(lambda _: next(q), range(2)))
            nose.tools.assert_sequence_equal(in_data_lst[:2], out_data_lst)

            working_cache = list(q.working_cache)
            if True:
                from pipeline.common import PLCacheSpec
                nose.tools.assert_is_instance(working_cache[0], PLCacheSpec)
                enqueue_times, working_sess_lst = zip(*working_cache)
            nose.tools.assert_sequence_equal(sess_lst[:2], working_sess_lst)


            q.working_cache.clear()
            _ = list(map(lambda _: q.task_done(), range(2)))

            # process next 2(2-3)
            out_data_lst = list(map(lambda _: next(q), range(2)))
            nose.tools.assert_sequence_equal(in_data_lst[2:4], out_data_lst)

            working_cache = list(q.working_cache)
            #if DARWIN_CONFIG.debug_performance:
            if True:
                from pipeline.common import PLCacheSpec
                nose.tools.assert_is_instance(working_cache[0], PLCacheSpec)
                enqueue_times, working_sess_lst = zip(*working_cache)
            else:
                working_sess_lst = working_cache
            nose.tools.assert_sequence_equal(sess_lst[2:4], working_sess_lst)

            q.working_cache.clear()
            _ = list(map(lambda _: q.task_done(), range(2)))

@nose.tools.nottest
def verify_pl_inst_started(pl_inst):
    _pl_inst = eval(repr(pl_inst))
    nose.tools.assert_equal(_pl_inst.state, type(_pl_inst).STATE.started)
    del _pl_inst


@nose.tools.nottest
def assert_true(a):
    assert a
