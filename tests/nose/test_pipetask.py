#! /usr/bin/env python3
#! -*- coding:utf-8 -*-


import os
import sys
import nose
import uuid
import json
import requests
import time
from itertools import (
    product,
)
from operator import (
    attrgetter,
    concat,
    itemgetter,
    methodcaller,
    truediv,
)
from functools import (
    lru_cache,
    partial,
)
import numpy as np
from darwinutils.config import DARWIN_CONFIG
from darwinutils.log import get_task_logger
from darwinutils.fn import (
    f_compose_r,
    f_echo,
    f_flip_call,
    f_group,
    f_list_rize,
    f_si_call,
    f_star_call,
    f_star2_call,
    f_timeit_call,
)
from darwinutils.helper import (
    convert_uuid_to_bson_object_id,
)
from darwinutils.mapreduce import (
    parallel_map_p,
    parallel_map_t,
)
import db.mongodb_api as api
from darwinutils.graph import TraverseEngineSpec
from pipeline.pipevariable import CtrlMagic
from pipeline.pipeline import PipeLine
from pipeline.pipenode import PipeNode
from pipeline.pipetask import (
    PipeTask,
    PipeTaskRetry,
    f_get_pl_graph_input_var_names,
)
from test_pipeline import test_pl_graph_1, test_pl_graph_2


logger = get_task_logger(__name__)


def wait_echo(a, t_wait=0.0):
    time.sleep(t_wait)
    return a


def func1(a, b, c=1, *args, kwa, kwb, kwc=3, **kwargs):
    import inspect
    from functools import partial
    frame = inspect.currentframe()
    ins_result = inspect.getargvalues(frame)

    ins_args, ins_varargs, ins_varkw, ins_values = ins_result
    ins_kwargs = f_compose_r(
        lambda _: [[
            f_compose_r(partial(map, f_echo), list),
            f_compose_r(partial(map, ins_values.get), list),
        ], [[_]]*2],
        partial(f_star_call, zip),
        partial(starmap, f_star_call),
        partial(f_star_call, zip),
        dict,
    )(ins_args)
    return ins_kwargs, args, kwargs


def post_run_pl_task_req(url, pl, data):
    return requests.post(
        url=url,
        headers={'Content-Type': 'application/json'},
        data=json.dumps(data),
        params=pl,
    )


@lru_cache(maxsize=100)
def get_pl_run_task_url(pl_uuid):
    return "{}pipelines/{}/run_task".format(
            restfulMessage.get_basic_url(),
            pl_uuid,
        )


def web_run_pl_task(pl, **kwargs):
    return requests.post(
        url=get_pl_run_task_url(pl.uuid),
        headers={'Content-Type': 'application/json'},
        data=json.dumps(kwargs),
    )


def register_pipeline(pl, description=None):
    _id = convert_uuid_to_bson_object_id(pl.uuid)
    succ = False
    for i in range(2):
        record = api.get_one_by_id(_id, table="pipeline_info")
        if record is not None:
            record["_id"] = str(record["_id"])
            _ = list(map(
                logger.info,
                json.dumps(record, indent=2).split('\n')
            ))
            succ = True
            break
        elif i == 0:
            api.insert_one_record(
                record={
                    "_id": _id,
                    "pipeline_id": pl.uuid,
                    "pipeline_name": "warm_echo_{}".format(pl.uuid),
                    # TODO: use normalized "pl_nodes" after problem had been fixed.
                    "graph": pl._pl_nodes,
                    "last_edited_time": time.time(),
                    "description": description,
                    "owner_id": os.getuid(),
                    "is_available": True,
                },
                table="pipeline_info",
                enforce_validate=False,
            )
            logger.info("Insert pipeline {}/{}".format(_id, pl.uuid))
    return succ


def deregister_pipeline(pl):
    _id = convert_uuid_to_bson_object_id(pl.uuid)
    succ = False
    for i in range(2):
        record = api.get_one_by_id(_id, table="pipeline_info")
        if record is None:
            succ = True
            break
        elif i == 0:
            api.delete_one_by_id(_id, table="pipeline_info")
            logger.info("delete pipeline {}/{}".format(_id, pl.uuid))
    return succ


class Test_basic:
    def __init__(self):
        self._pl_graph = test_pl_graph_1
        self._pl = PipeLine(self._pl_graph)

        self._pl_graph_warm = test_pl_graph_2
        self._pl_warm = PipeLine(self._pl_graph_warm)

    def test_get_pl_input_var_names(self):
        r = f_get_pl_graph_input_var_names(self._pl.graph.roots)
        nose.tools.assert_list_equal(r, ['image_paths'])
        _ = list(map(logger.debug, r))

    def test_get_graph(self):
        t = PipeTask(self._pl)
        graph = t.graph
        root, *_ = graph.roots
        nose.tools.assert_set_equal(
            f_compose_r(
                partial(map, attrgetter("name")),
                set,
            )(root.obj.input_vars),
            {"image_paths"},
        )

    def test_cold_call(self):
        pl = self._pl

        image_paths = [
            "1ooyah9Eoxies7ux7thoo9eiW4eteeYu",
            "2ooyah9Eoxies7ux7thoo9eiW4eteeYu",
            "3ooyah9Eoxies7ux7thoo9eiW4eteeYu",
            "4ooyah9Eoxies7ux7thoo9eiW4eteeYu",
        ]
        logger.info(image_paths)


        workload_executors = [
            map,
            parallel_map_t,
            parallel_map_p,
        ]
        prep_executors = [
            map,
            parallel_map_t,
            parallel_map_p,
        ]
        post_executors = [
            map,
            parallel_map_t,
            # TODO: post does not support multiprocess for now!
            # The output variable broadcast needs to be in same address space
            #parallel_map_p,
        ]
        for e1, e2, e3 in product(workload_executors, prep_executors, post_executors):
            _ = list(map(logger.info, TraverseEngineSpec(e1, e2, e3)))
            logger.info("--------------------------")
            task = PipeTask(pl)

            r = task(image_paths=image_paths, graph_traverse_executor=[e1, e2, e3])
            nose.tools.ok_("invoice_informations" in r)

            input_var = task._get_pl_var_by_name("image_paths")
            nose.tools.assert_equal(input_var.value, image_paths)
            logger.debug("============================================================================")
            logger.debug("============================================================================")
            logger.debug("============================================================================")
            logger.debug("============================================================================")
            logger.debug("============================================================================")
            logger.debug("============================================================================")
            logger.debug("============================================================================")
            logger.debug("============================================================================")

    @nose.tools.nottest
    def test_perf_e2e(self):
        pass
        """
        #
        # NOTE: this could be another nose case to verify if registered pl will have consistent pl_uuid
        #
        from rest.blueprints.pl_view import _get_spec_by_uuid_from_db
        _pl_spec = _get_spec_by_uuid_from_db(pl.uuid)
        nose.tools.assert_equal(_pl_spec, pl_nodes)
        _pl = PipeLine(_pl_spec)
        nose.tools.assert_equal(_pl.uuid, pl.uuid)
        """
        return self.test_perf(
            run_func=web_run_pl_task,
            # register/import pipeline
            pre_func=f_compose_r(
                partial(register_pipeline, description="test_pipetask:Test_basic.test_perf_e2e"),
                partial(f_flip_call, nose.tools.ok_, "Fail to register pipeline"),
            ),
            # deregister/clean auto created pipeline
            post_func=f_compose_r(
                partial(deregister_pipeline),
                partial(f_flip_call, nose.tools.ok_, "Fail to deregister pipeline"),
            ),
        )

    @nose.tools.nottest
    def test_perf(self, run_func=None, pre_func=None, post_func=None):
        max_instances = 4
        max_pool_workers = 100
        hb_interval = 10
        use_list = False

        gen_funcs = [
            np.random.random,
            methodcaller("astype", dtype=np.dtype("float64")),
            methodcaller("tolist"),
        ]
        if use_list:
            gen_funcs.append(methodcaller("tolist"))
        f_gen_input = f_compose_r(*gen_funcs)

        # number of data for input_a:
        # * smaller number will make pl overhead be obvious
        # * larger number will make communicate and framework overhead be obvious
        num_input_a = int(os.environ.get("num_input_a", 200))
        input_shape_a = (num_input_a,)
        f_gen_input_a = partial(f_gen_input, input_shape_a)
        logger.info("sizeof(input_a)={}".format(sys.getsizeof(f_gen_input_a())))

        num_input_b = int(os.environ.get("num_input_b", 1))
        input_shape_b = (num_input_b,)
        f_gen_input_b = partial(f_gen_input, input_shape_b)
        logger.info("sizeof(input_b)={}".format(sys.getsizeof(f_gen_input_b())))

        pl_node_spec = {
            "name": "test_func",
            "func": ["tasks", "warm_echo", ""],
            "type": PipeNode.TYPE.warm.name,
            "inputs": ["a", "b"],
            "outputs": ["c", "d"],
            "extra_args": [],
            "extra_kwargs": {
                "batch_load": 100,
                "sample_load": 4000,
            },
            "flags": [
                "cpu",
                "_max_batch_size=1000",
                "_max_instances={}".format(max_instances),
                "_hb_interval={}".format(hb_interval),
                "_max_pool_workers={}".format(max_pool_workers),
                # turn off GC to make sure the latency was not smooth
                "_gc_interval=-1",
            ],
        }
        pl_spec = [pl_node_spec]
        pl = PipeLine(pl_spec)
        pl.stop_warm_nodes()

        pl_task = PipeTaskRetry(pl)
        # this is perf test. let's always start warm node first.
        #_ = pl_task(__auto_start_warm_nodes=True)
        """ use process for offline debug w/o celery while only rely on redis """
        pl.start_warm_nodes(
            # container="process",
            container="celery",
        )
        logger.info("Started up pipe line warm instances...")

        if pre_func:
            pre_func(pl)

        def f_assert_direct(truth, pred, _map={}):
            nose.tools.assert_set_equal(set(map(_map.get, truth.keys())), set(pred.keys()))
            for key, val_truth in truth.items():
                val_pred = pred[_map.get(key, key)]
                nose.tools.ok_(np.all(np.equal(val_truth, val_pred)))

        def f_assert_rest(truth, resp, _map={}):
            nose.tools.assert_equal(resp.status_code, 200)
            pred = resp.json()
            return f_assert_direct(truth, pred, _map=_map)

        _map = {"a": "c", "b": "d"}

        default_channel = DARWIN_CONFIG["pipeline_warm_node_call_default_channel"]
        is_zrpc_default = default_channel == "zrpc"
        is_grpc_default = default_channel == "grpc"
        is_rpc_channel = is_zrpc_default or is_grpc_default

        if run_func is None:
            run_func = pl_task
            f_assert = partial(f_assert_direct, _map=_map)
        else:
            run_func = partial(run_func, pl)
            f_assert = partial(f_assert_rest, _map=_map)

        try:
            # TODO: do NOT run smoke pipetask test from "zrpc" channel
            # TODO: before the gevent and thread problem had been solved.
            # TODO: do NOT run smoke pipetask test from "grpc" channel due to unknown reason
            if not is_zrpc_default and not is_grpc_default:
                a = f_gen_input_a()
                b = f_gen_input_b()
                truth = {"a": a, "b": b}
                logger.info("run smoke test")
                pred = run_func(**truth, _timeout=[1,2,3,4,5,6,None])
                f_assert(truth, pred)
                logger.info("smoke test good")

            #raise ValueError()
            for is_include, executor, n_tsks, cnt in [
                # parallel_p first!
                [
                    # TODO: gevent/rpc w/ monkey_patch will fail if parallel_map_p is not in the non-greenlet poluted main thread.
                    # TODO: grpc uses async io also, it seems have same problem as gevent which polute main thread for multi-process.
                    True,
                    partial(
                        parallel_map_p,
                        # max_workers should larger enough to lauch all tasks in one time.
                        max_workers=max_instances << 4,
                    ),
                    [
                        max_instances >> 1,
                        round(max_instances * 0.75),
                        max_instances,
                        round(max_instances * 1.5),
                        max_instances << 1,
                        round(max_instances * 3),
                        max_instances << 2,
                        #round(max_instances * 6),
                        #max_instances << 3,
                        #round(max_instances * 12),
                        #max_instances << 4,
                    ],
                    1000,
                ],
                [
                    # TODO: gevent/rpc w/o monkey_patch will fail parallel_map_t
                    True if not is_zrpc_default or DARWIN_CONFIG["apply_gevent_monkey_patch"] is True else False,
                    partial(
                        parallel_map_t,
                        # max_workers should larger enough to lauch all tasks in one time.
                        max_workers=max_instances << 4,
                    ),
                    [
                        max_instances >> 1,
                        round(max_instances * 0.75),
                        max_instances,
                        round(max_instances * 1.5),
                        max_instances << 1,
                        round(max_instances * 3),
                        max_instances << 2,
                        #round(max_instances * 6),
                        #max_instances << 3,
                        #round(max_instances * 12),
                        #max_instances << 4,
                    ],
                    1000,
                ],
                [
                    True,
                    map,
                    [
                        1,
                    ],
                    10,
                ],
            ]:
                if not is_include: continue
                f_l1 = f_compose_r(
                    range,
                    partial(map, f_compose_r(
                        # generate input data
                        f_group(
                            # input generator for "a"
                            partial(f_si_call, f_gen_input_a),
                            # input generator for "b"
                            partial(f_si_call, f_gen_input_b),
                        ),
                        list,
                        # call pl
                        f_group(
                            # call pl
                            f_compose_r(
                                partial(zip, pl_node_spec["inputs"]),
                                list,
                                dict,
                                f_list_rize,
                                partial(concat, [[]]),
                                partial(f_timeit_call, f_star2_call, run_func),
                                # ------------------ DO NOT OPEN THIS DEBUG in parallel_p
                                #f_group(
                                #    logger.info,
                                #    f_echo,
                                #),
                                #list,
                                #itemgetter(1),
                                # ------------------
                                # pick up duration only and drop result
                                itemgetter(1),
                            ),
                            # pass through input in case we needs to verify the result
                            tuple,
                        ),
                        list,
                        # pick up duration
                        itemgetter(0),
                    )),
                    list,
                )
                f_l2 = f_compose_r(
                    range,
                    partial(executor, partial(f_si_call, partial(f_timeit_call, f_l1, cnt))),
                    list,
                    partial(f_star_call, zip),
                    list,
                )
                for n_tsk in n_tsks:
                    if n_tsk < 1:
                        continue
                    f = partial(f_timeit_call, f_l2, n_tsk)
                    logger.debug([
                        ["executor", executor],
                        ["parallel", [n_tsk, cnt]],
                        ["channel", DARWIN_CONFIG["pipeline_warm_node_call_default_channel"]],
                    ])
                    (durs_per_cal, durs_per_tsk), dur = f()
                    """
                    #
                    # turn on following debug if you wants to know more detailed latency information
                    #
                    logger.info("dur={}".format(dur))
                    for i, v in enumerate(durs_per_tsk):
                        logger.info("durs_per_tsk[{}]={}".format(i, v))
                    for i, v in enumerate(durs_per_cal):
                        logger.info("durs_per_cal[{}]={}".format(i, v))
                    """
                    logger.info("-------------------------------------")
                    _ = list(map(
                        lambda _: logger.info(">> perf rst: {}".format(_)),
                        [
                            ["executor", executor],
                            ["parallel", [n_tsk, cnt]],
                            ["channel", DARWIN_CONFIG["pipeline_warm_node_call_default_channel"]],
                            ["avg_per_all", dur/n_tsk/cnt],
                            ["avg_per_tsk", np.average(durs_per_tsk)/cnt],
                            ["avg_per_cal", np.average(durs_per_cal)],
                            ["top1_lat   ", list(zip(
                                np.argmax(durs_per_cal, axis=1).tolist(),
                                np.max(durs_per_cal, axis=1).tolist(),
                            ))],
                        ],
                    ))
                    logger.info("-------------------------------------")
        except ValueError:
            logger.exception("unknown fail")
        finally:
            _ = pl.stop_warm_nodes()
            if post_func:
                post_func(pl)


@nose.tools.nottest
class Test_perf:
    def test_internal_py_perf(self):
        test = Test_basic()
        return test.test_perf()

    def test_external_rest_perf(self):
        test = Test_basic()
        return test.test_perf_e2e()


class Test_retry:
    def test_cold(self):
        pl_graph_def = [
            {
                "func": ["test_pipetask", "wait_echo", ""],
                "type": PipeNode.TYPE.cold.name,
                "inputs": ['variable_a:a'],
                "outputs": ['variable_b:b'],
            },
        ]
        pl = PipeLine(pl_graph_def)
        task = PipeTask(pl)
        r = task(variable_a=1, _cold_node_t_wait=10)
        nose.tools.assert_dict_equal({"variable_b": 1}, r)
