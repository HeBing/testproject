#-*-coding: utf-8 -*-
from nose.tools import ok_, assert_equal, assert_in, assert_list_equal, assert_dict_equal
import pandas as pd
from time import time
import numpy as np
from data_preprocess.data_distribution import DataDistribution
from darwinutils.log import get_task_logger
logger = get_task_logger(__name__)


class TestDataDist():
    def setUp(self):
        pass

    def tearDown(self):
        pass

    def test_get_miss_value_info(self):
        test_data = [[1,2,3,4,np.nan], [1,np.nan,3,4,np.nan], [1,2,np.nan,4,4], [1,2,3,4,np.nan], [1,2,3,4,6]]
        rst = DataDistribution.get_miss_value_info(test_data)
        assert_list_equal([0.,  0.2, 0.2, 0.,  0.6], rst.round(decimals=2).tolist(), repr(rst))

        test_data = np.array(test_data)
        rst = DataDistribution.get_miss_value_info(test_data)
        assert_list_equal([0.,  0.2, 0.2, 0.,  0.6], rst.round(decimals=2).tolist(), repr(rst))

        test_data = pd.DataFrame(test_data)
        rst = DataDistribution.get_miss_value_info(test_data)
        assert_list_equal([0.,  0.2, 0.2, 0.,  0.6], rst.round(decimals=2).tolist(), repr(rst))

    def test_get_dominant_value_info(self):
        test_data = {"c1": [1,1,1,1,1,1,1, 1, 1, 1], "c2": [1,1,1,1,1,1,1, 1, 1, 2], "c3": [1,1,1,1,1,1,1, 1, 2, 2]}
        test_data = list(test_data.values())
        test_data = np.transpose(test_data).tolist()
        rst = DataDistribution.get_dominant_value_info(test_data)
        assert_list_equal([(True, 1.0), (True, 0.9), (False, 0.8)], rst, repr(rst))

        test_data = np.array(test_data)
        rst = DataDistribution.get_dominant_value_info(test_data)
        assert_list_equal([(True, 1.0), (True, 0.9), (False, 0.8)], rst, repr(rst))

        test_data = pd.DataFrame(test_data)
        rst = DataDistribution.get_dominant_value_info(test_data, threshold=0.8)
        assert_list_equal([(True, 1.0), (True, 0.9), (True, 0.8)], rst, repr(rst))

    def test_get_top_pearson_coeff(self):
        test_data = {"c1": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10], "c2": [1, 1, 1, 1, 1, 1, 1, 1, 1, 2],
                     "c3": ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j']}
        test_data = list(test_data.values())
        test_data = np.transpose(test_data).tolist()
        rst = DataDistribution.get_top_pearson_coeff(test_data, top_n=2)
        assert_list_equal([[0, 2], [1, 2]], rst[0].tolist())
        assert_list_equal([1, 0.522], rst[1].round(3).tolist())

        test_data = np.array(test_data)
        rst = DataDistribution.get_top_pearson_coeff(test_data, top_n=2)
        assert_list_equal([[0, 2], [1, 2]], rst[0].tolist())
        assert_list_equal([1, 0.522], rst[1].round(3).tolist())

        test_data = pd.DataFrame.from_dict({"c1": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10], "c2": [1, 1, 1, 1, 1, 1, 1, 1, 1, 2],
                     "c3": ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j']})
        rst = DataDistribution.get_top_pearson_coeff(test_data, top_n=2)
        assert_list_equal([[0, 2], [1, 2]], rst[0].tolist())
        assert_list_equal([1., 0.522], rst[1].round(3).tolist())

        rst = DataDistribution.get_top_pearson_coeff(test_data, top_n=2, fixed_column_idx=1)
        assert_list_equal([[0, 1], [1, 2]], rst[0].tolist())
        assert_list_equal([0.522, 0.522], rst[1].round(3).tolist())

    def test_get_discrete_distribution(self):
        test_data = {"c1": [1, 2, 3, 1, 2, 3, 1, 1, 1, 2], "c2": [1, 1, 1, 1, 1, 1, 1, 1, 1, 2],
                     "c3": ['a', 'b', 'c', 'a', 'b', 'c', 'a', 'b', 'c', 'a'],
                     "l": [0,0,0,0,0,1,1,1,1,1]}
        test_data = list(test_data.values())
        test_data = np.transpose(test_data).tolist()
        rst = DataDistribution.get_discrete_distribution(test_data, label_idx=3)
        assert_list_equal(rst['0'][0].tolist(), ['1', '2', '3'])
        assert_list_equal(rst['0'][1].tolist(), [5, 3, 2])
        assert_list_equal(rst['0'][2].tolist(), [3, 1, 1])
        assert_list_equal(rst['2'][0].tolist(), ['a', 'b', 'c'])
        assert_list_equal(rst['2'][1].tolist(), [4, 3, 3])
        assert_list_equal(rst['2'][2].tolist(), [2, 1, 2])

        test_data = np.array(test_data)
        rst = DataDistribution.get_discrete_distribution(test_data, label_idx=3)
        assert_list_equal(rst['0'][0].tolist(), ['1', '2', '3'])
        assert_list_equal(rst['0'][1].tolist(), [5, 3, 2])
        assert_list_equal(rst['0'][2].tolist(), [3, 1, 1])
        assert_list_equal(rst['2'][0].tolist(), ['a', 'b', 'c'])
        assert_list_equal(rst['2'][1].tolist(), [4, 3, 3])
        assert_list_equal(rst['2'][2].tolist(), [2, 1, 2])

        test_data = pd.DataFrame.from_dict({"c1": [1, 2, 3, 1, 2, 3, 1, 1, 1, 2], "c2": [1, 1, 1, 1, 1, 1, 1, 1, 1, 2],
                     "c3": ['a', 'b', 'c', 'a', 'b', 'c', 'a', 'b', 'c', 'a'],
                     "l": [0,0,0,0,0,1,1,1,1,1]})
        rst = DataDistribution.get_discrete_distribution(test_data, label_idx=3)
        #logger.info(rst)
        assert_list_equal(rst['c1'][0].tolist(), [1, 2, 3])
        assert_list_equal(rst['c1'][1].tolist(), [5, 3, 2])
        assert_list_equal(rst['c1'][2].tolist(), [3, 1, 1])
        assert_list_equal(rst['c3'][0].tolist(), ['a', 'b', 'c'])
        assert_list_equal(rst['c3'][1].tolist(), [4, 3, 3])
        assert_list_equal(rst['c3'][2].tolist(), [2, 1, 2])

    def test_get_continuous_distribution(self):
        test_data = {"c1": [1, 2, 3, 1, 2, 3, 1, 1, 1, 2], "c2": [1, 1, 1, 1, 1, 1, 1, 1, 1, 2],
                     "c3": [0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 0.1],
                     "l": [0, 0, 0, 0, 0, 1, 1, 1, 1, 1]}
        test_data = list(test_data.values())
        test_data = np.transpose(test_data).tolist()
        rst = DataDistribution.get_continuous_distribution(test_data, label_idx=3, bins=5)
        assert_list_equal(np.array(rst['0']["general"]).round(2).tolist(), [1.0, 3.0, 1.7, 1.5, 0.61])
        assert_list_equal(rst['0']['bins'][0].round(2).tolist(), [1. , 1.4, 1.8, 2.2, 2.6, 3. ])
        assert_list_equal(rst['0']['bins'][1].tolist(), [5, 0, 3, 0, 2])
        assert_list_equal(rst['0']['bins'][2].tolist(), [3, 0, 1, 0, 1])
        assert_list_equal(np.array(rst['2']["general"]).round(2).tolist(), [0.1, 0.9, 0.46, 0.45, 0.07])
        assert_list_equal(rst['2']['bins'][0].round(2).tolist(), [0.1 , 0.26, 0.42, 0.58, 0.74, 0.9 ])
        assert_list_equal(rst['2']['bins'][1].round(2).tolist(), [3, 2, 1, 2, 2])
        assert_list_equal(rst['2']['bins'][2].tolist(), [1, 0, 0, 2, 2])

        test_data = np.array(test_data)
        rst = DataDistribution.get_continuous_distribution(test_data, label_idx=3, bins=5)
        assert_list_equal(np.array(rst['0']["general"]).round(2).tolist(), [1.0, 3.0, 1.7, 1.5, 0.61])
        assert_list_equal(rst['0']['bins'][0].round(2).tolist(), [1., 1.4, 1.8, 2.2, 2.6, 3.])
        assert_list_equal(rst['0']['bins'][1].tolist(), [5, 0, 3, 0, 2])
        assert_list_equal(rst['0']['bins'][2].tolist(), [3, 0, 1, 0, 1])
        assert_list_equal(np.array(rst['2']["general"]).round(2).tolist(), [0.1, 0.9, 0.46, 0.45, 0.07])
        assert_list_equal(rst['2']['bins'][0].round(2).tolist(), [0.1, 0.26, 0.42, 0.58, 0.74, 0.9])
        assert_list_equal(rst['2']['bins'][1].round(2).tolist(), [3, 2, 1, 2, 2])
        assert_list_equal(rst['2']['bins'][2].tolist(), [1, 0, 0, 2, 2])

        test_data = pd.DataFrame.from_dict({"c1": [1, 2, 3, 1, 2, 3, 1, 1, 1, 2], "c2": [1, 1, 1, 1, 1, 1, 1, 1, 1, 2],
                     "c3": [0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 0.1],
                     "l": [0, 0, 0, 0, 0, 1, 1, 1, 1, 1]})
        rst = DataDistribution.get_continuous_distribution(test_data, label_idx=3, bins=5)
        #logger.info(rst)
        assert_list_equal(np.array(rst['c1']["general"]).round(2).tolist(), [1.0, 3.0, 1.7, 1.5, 0.61])
        assert_list_equal(rst['c1']['bins'][0].round(2).tolist(), [1., 1.4, 1.8, 2.2, 2.6, 3.])
        assert_list_equal(rst['c1']['bins'][1].tolist(), [5, 0, 3, 0, 2])
        assert_list_equal(rst['c1']['bins'][2].tolist(), [3, 0, 1, 0, 1])
        assert_list_equal(np.array(rst['c3']["general"]).round(2).tolist(), [0.1, 0.9, 0.46, 0.45, 0.07])
        assert_list_equal(rst['c3']['bins'][0].round(2).tolist(), [0.1, 0.26, 0.42, 0.58, 0.74, 0.9])
        assert_list_equal(rst['c3']['bins'][1].round(2).tolist(), [3, 2, 1, 2, 2])
        assert_list_equal(rst['c3']['bins'][2].tolist(), [1, 0, 0, 2, 2])

    def test_get_chimerge(self):
        expected_rst = {'sepal_l': [[4.3, 4.8, 0, 16], [4.9, 4.9, 2, 4], [5.0, 5.4, 5, 25], [5.5, 5.7, 17, 4],
                              [5.8, 7.0, 64, 1], [7.1, 7.9, 12, 0]],
                        'sepal_w': [[2.0, 2.2, 4, 0], [2.3, 2.4, 6, 1], [2.5, 2.8, 36, 0], [2.9, 2.9, 9, 1],
                              [3.0, 3.3, 39, 18], [3.4, 4.4, 6, 30]],
                        'petal_l': [[1.0, 1.9, 0, 50], [3.0, 4.4, 29, 0], [4.5, 4.7, 16, 0], [4.8, 4.9, 9, 0],
                              [5.0, 5.1, 12, 0], [5.2, 6.9, 34, 0]],
                        'petal_w': [[0.1, 0.6, 0, 50], [1.0, 1.3, 28, 0], [1.4, 1.6, 24, 0], [1.7, 1.7, 2, 0],
                              [1.8, 1.8, 12, 0], [1.9, 2.5, 34, 0]]}
        expected_rst_2 = {'0': [[4.3, 4.8, 0, 16], [4.9, 4.9, 2, 4], [5.0, 5.4, 5, 25], [5.5, 5.7, 17, 4],
                                    [5.8, 7.0, 64, 1], [7.1, 7.9, 12, 0]],
                        '1': [[2.0, 2.2, 4, 0], [2.3, 2.4, 6, 1], [2.5, 2.8, 36, 0], [2.9, 2.9, 9, 1],
                                    [3.0, 3.3, 39, 18], [3.4, 4.4, 6, 30]],
                        '2': [[1.0, 1.9, 0, 50], [3.0, 4.4, 29, 0], [4.5, 4.7, 16, 0], [4.8, 4.9, 9, 0],
                                    [5.0, 5.1, 12, 0], [5.2, 6.9, 34, 0]],
                        '3': [[0.1, 0.6, 0, 50], [1.0, 1.3, 28, 0], [1.4, 1.6, 24, 0], [1.7, 1.7, 2, 0],
                                    [1.8, 1.8, 12, 0], [1.9, 2.5, 34, 0]]}
        from sklearn.preprocessing import OrdinalEncoder
        enc = OrdinalEncoder()
        iris = pd.read_csv('data/iris.csv')
        enc.fit(iris.loc[:, ["type"]].values)
        iris.loc[:, "type"] = enc.transform(iris.loc[:, "type"].values.reshape(-1, 1)).reshape(-1, )
        logger.info(np.shape(iris))
        rst = DataDistribution.get_chimerge(iris, label_idx=4, bin=6)
        assert_dict_equal(rst, expected_rst)

        tmp_iris = iris.values
        rst = DataDistribution.get_chimerge(tmp_iris, label_idx=4, bin=6)
        assert_dict_equal(rst, expected_rst_2)

        tmp_iris = iris.values.tolist()
        rst = DataDistribution.get_chimerge(tmp_iris, label_idx=4, bin=6)
        assert_dict_equal(rst, expected_rst_2)

    def test_get_discrete_continuous_columns(self):
        test_data = {"c1": ['a', 'b', 'c', 'd', 'e', 'f', 'f', 'f', 'f', 'a'], "c2": [1, 1, 1, 1, 1, 1, 1, 1, 1, 2],
                     "c3": [1.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 0.1],
                     "l": [0, 0, 0, 0, 0, 1, 1, 1, 1, 1]}
        test_data = pd.DataFrame.from_dict(test_data)
        rst = DataDistribution.get_discrete_continuous_columns(test_data)
        assert_dict_equal({'discrete': [0, 1, 3], 'continuous': [2]}, rst)

        rst = DataDistribution.get_discrete_continuous_columns(test_data.values)
        assert_dict_equal({'discrete': [0, 1, 3], 'continuous': [2]}, rst)

        rst = DataDistribution.get_discrete_continuous_columns(test_data.values.tolist())
        assert_dict_equal({'discrete': [0, 1, 3], 'continuous': [2]}, rst)

    def test_get_all_stats(self):
        from sklearn.preprocessing import OrdinalEncoder
        enc = OrdinalEncoder()
        iris = pd.read_csv('data/iris.csv')
        enc.fit(iris.loc[:, ["type"]].values)
        iris.loc[:, "type"] = enc.transform(iris.loc[:, "type"].values.reshape(-1, 1)).reshape(-1, )
        logger.info(np.shape(iris))
        logger.info(iris.columns.values)

        rst = DataDistribution.get_all_stats(iris, label_idx=4, bins=6)
        assert_equal(rst["sepal_l"]["type"], 'continuous')
        assert_equal(round(rst["sepal_l"]["coefficient_table"].loc[0, "Coefficient"], 3), 0.872, rst["sepal_l"])
        assert_equal(rst["sepal_l"]["value_bar_chart"].loc[0, "total"], 16, rst["sepal_l"])
        assert_equal(rst["sepal_l"]["value_bar_chart"].loc[0, "positive"], 0, rst["sepal_l"])
        assert_equal(rst["sepal_l"]["chimerge_bar_chart"].loc[1, "positive"], 2, rst["sepal_l"])
        assert_equal(rst["sepal_l"]["chimerge_bar_chart"].loc[1, "negative"], 4, rst["sepal_l"])
        #logger.info(rst)
