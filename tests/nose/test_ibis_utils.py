#-*-coding: utf-8 -*-
from nose.tools import ok_, assert_equal, assert_in
import pandas as pd
from time import time
from darwinutils.ibis_utils import DBConnector, check_thrift_sasl_version
from darwinutils.config import DARWIN_CONFIG
from darwinutils.log import get_task_logger
logger = get_task_logger(__name__)


class TestIbisUtils():
    def __init__(self):
        if check_thrift_sasl_version().find("0.2.1") > 0:
            self._test_impala = True
        else:
            self._test_impala = False
        if self._test_impala:
            self.db_connector_impala = DBConnector(impala_host=DARWIN_CONFIG["impala_config"]["host"],
                                            impala_port=DARWIN_CONFIG["impala_config"]["port"],
                                            hdfs_host=DARWIN_CONFIG["hdfs_config"]["host"],
                                            hdfs_port=DARWIN_CONFIG["hdfs_config"]["port"],
                                            hive_host=DARWIN_CONFIG["hive_config"]["host"],
                                            hive_port=DARWIN_CONFIG["hive_config"]["port"],
                                            user=DARWIN_CONFIG["impala_config"]["user"],
                                            passwd=DARWIN_CONFIG["impala_config"]["passwd"],
                                            database=DARWIN_CONFIG["impala_config"]["database"],
                                            use_hive=False)
            ok_(self.db_connector_impala is not None)
        else:
            self.db_connector_hive = DBConnector(impala_host=DARWIN_CONFIG["impala_config"]["host"],
                                                   impala_port=DARWIN_CONFIG["impala_config"]["port"],
                                                   hdfs_host=DARWIN_CONFIG["hdfs_config"]["host"],
                                                   hdfs_port=DARWIN_CONFIG["hdfs_config"]["port"],
                                                   hive_host=DARWIN_CONFIG["hive_config"]["host"],
                                                   hive_port=DARWIN_CONFIG["hive_config"]["port"],
                                                   user=DARWIN_CONFIG["impala_config"]["user"],
                                                   passwd=DARWIN_CONFIG["impala_config"]["passwd"],
                                                   database=DARWIN_CONFIG["impala_config"]["database"],
                                                   use_hive=True)
            ok_(self.db_connector_hive is not None)

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def test_connect_impala(self):
        if self._test_impala:
            ok_(self.db_connector_impala.connect())
            ok_(self.db_connector_impala.connect_database())

    def test_connect_hive(self):
        if not self._test_impala:
            ok_(self.db_connector_hive.connect())
            ok_(self.db_connector_hive.connect_database())

    def test_table_operation_impala(self):
        if self._test_impala:
            self.db_connector_impala.connect()
            self.db_connector_impala.connect_database()
            tables = self.db_connector_impala.list_tables()
            if "test1" in tables:
                self.db_connector_impala.drop_table("test1")
            df = pd.DataFrame(data=[[1, 2, "3", "20190301", "123456"],
                                    [11, 12, "13", "20190401", "113456"],
                                    [21, 22, "23", "20190501", "213456"]],
                              columns=["n1", "n2", "s1", "d1", "t1"])
            ok_(self.db_connector_impala.create_table(table_name="test1", table_obj=df))
            ok_(self.db_connector_impala.connect_table(table_name="test1") is not None)
            ok_(self.db_connector_impala.exists_table(table_name="test1"))
            ok_(self.db_connector_impala.exists_table(table_name="test12") is False)
            ok_(self.db_connector_impala.exists_table(table_name="test1", database="d1") is False)
            db_schema = self.db_connector_impala.get_table_schema(table_name="test1")
            assert_equal(db_schema.names, ["n1", "n2", "s1", "d1", "t1"])
            assert_in(DARWIN_CONFIG["impala_config"]["database"], self.db_connector_impala.list_databases())
            assert_in("test1", self.db_connector_impala.list_tables())
            logger.info(self.db_connector_impala.fetch(sql_str="select n1 from test1"))
            assert_equal(21, self.db_connector_impala.fetch(sql_str="select n1 from test1 where t1='213456'")["n1"][0])
            assert_equal(21, self.db_connector_impala.fetch("select * from test1")["n1"][2])
            assert_equal(2, len(self.db_connector_impala.fetch("select * from test1", limit=2)))
            self.db_connector_impala.insert(df[1:])
            assert_equal(2, len(self.db_connector_impala.fetch(sql_str="select n1 from test1 where t1='213456'")))
            column_stats = self.db_connector_impala.column_stats()
            assert_equal(column_stats["Avg Size"][0], 8.0)
            self.db_connector_impala.truncate_table("test1")
            assert_equal(0, len(self.db_connector_impala.fetch("select * from test1")))
            ok_(self.db_connector_impala.exists_table(table_name="test1"))
            self.db_connector_impala.drop_table("test1")
            ok_(self.db_connector_impala.exists_table(table_name="test1") is False)

    def test_table_operation_hive(self):
        if not self._test_impala:
            from time import  sleep
            self.db_connector_hive.connect()
            self.db_connector_hive.connect_database()
            flag, tables = self.db_connector_hive.raw_sql("show tables",fetch_rst=True)
            logger.info(tables)
            if ("test3",) in tables:
                self.db_connector_hive.raw_sql("drop table test3", fetch_rst=False)
                sleep(3)
            ok_(self.db_connector_hive.raw_sql("create table test3 (MRCHNT_KEY_WRD string,register_date string, risk_date string, risk_type string)", fetch_rst=False)[1] is None)
            #logger.info(self.db_connector.get_table_schema("test3"))
            ok_(self.db_connector_hive.insert("insert into test3 values ('800491153110051','2019-03-12','2019-03-13', '不明'), ('800102359410062', '2019-03-08', '2019-03-12', '不明')"))
            tmp_df = self.db_connector_hive.fetch("select * from test3")
            ok_(len(tmp_df) == 2)

    def test_performance(self):
        if self._test_impala:
            self.db_connector_impala.connect()
            self.db_connector_impala.connect_database()
            start_time = time()
            rst = self.db_connector_impala.fetch(sql_str="select count(1) from prod_fct_tx_base_tx_lst_h")
            end_time = time()
            logger.info("Impala Total {} records spend {} seconds".format(rst, end_time - start_time))
        else:
            self.db_connector_hive.connect()
            self.db_connector_hive.connect_database()
            start_time = time()
            rst = self.db_connector_hive.raw_sql(sql_str="select count(1) from prod_fct_tx_base_tx_lst_h")[1]
            end_time = time()
            logger.info("Hive Total {} records spend {} seconds".format(rst, end_time - start_time))

    def test_db_op(self):
        if self._test_impala:
            self.db_connector_impala.connect()
            self.db_connector_impala.connect_database()
            start_time = time()
            ok_(len(self.db_connector_impala.raw_sql(sql_str="show tables")[1]) > 0)
            rst = self.db_connector_impala.raw_sql(sql_str="select * from prod_dim_ip_mrchnt_h where real_mrchnt_flg='Y' limit 100", onebyone=True)[1]
            logger.info("OnebyOne method Total: {}".format(len(rst)))
            rst = \
            self.db_connector_impala.raw_sql(sql_str="select * from prod_dim_ip_mrchnt_h where real_mrchnt_flg='Y' limit 100",
                                             onebyone=False)[1]
            logger.info("Fetch All method Total: {}".format(len(rst)))
            ok_(len(self.db_connector_impala.fetch("select * from prod_dim_ip_mrchnt_h where real_mrchnt_flg='Y'", limit=100)) > 0)
            end_time = time()
            logger.info("Total spend {} seconds".format(end_time - start_time))
        else:
            self.db_connector_hive.connect()
            self.db_connector_hive.connect_database()
            start_time = time()
            ok_(len(self.db_connector_hive.raw_sql(sql_str="show tables")[1]) > 0)
            rst = self.db_connector_hive.raw_sql(sql_str="select * from prod_fct_tx_frz_lst_h")
            logger.info(len(rst))
            '''
            ok_(len(self.db_connector_hive.raw_sql(sql_str="select * from prod_dim_ip_mrchnt_h where real_mrchnt_flg='Y' limit 10")[1]) > 0)
            rst = self.db_connector_hive.raw_sql(sql_str="select * from prod_dim_ip_mrchnt_h where real_mrchnt_flg='Y' limit 100", onebyone=True)[1]
            logger.info("One by one method Total: {}".format(len(rst)))
            rst = self.db_connector_hive.raw_sql(sql_str="select * from prod_dim_ip_mrchnt_h where real_mrchnt_flg='Y' limit 100000",
                                                 onebyone=False)[1]
            logger.info("One by one method Total: {}".format(len(rst)))
            '''
            end_time = time()
            logger.info("Total spend {} seconds".format(end_time - start_time))

