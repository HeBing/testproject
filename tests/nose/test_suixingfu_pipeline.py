#-*-coding: utf-8 -*-
from nose.tools import ok_, assert_equal, assert_in
import os
import pandas as pd
from time import time, sleep
from darwinutils.ibis_utils import DBConnector, check_thrift_sasl_version
from darwinutils.config import DARWIN_CONFIG
from pipeline.pipeline import PipeLine
from pipeline.pipetask import PipeTaskRetry, PipeTask
from darwinutils.log import get_task_logger
import numpy as np
logger = get_task_logger(__name__)


class TestPipelineDataExtractor():
    db_connector_impala = None

    def __init__(self):
        self.pipeline1 = [
              {
                "name": "generate_valid_tran_all_table",
                "func": ["data_preprocess.sql_executor", "sql_execute_from_single_file", ""],
                "type": "cold",
                "inputs": ["input1:sql_cmd_file"],
                "outputs": ["tran_all_table"],
                "extra_args": []
              },
              {
                "name": "generate_valid_mrchnt_table",
                "func": ["data_preprocess.sql_executor", "sql_execute_from_single_file", ""],
                "type": "cold",
                "inputs": ["input2:sql_cmd_file"],
                "outputs": ["real_mrchnt_table"],
                "extra_args": []
              },
              {
                  "name": "read_data_from_impala_tran_all",
                  "func": ["data_preprocess.sql_executor", "sql_fetch_from_file", ""],
                  "type": "cold",
                  "inputs": ["input3:sql_cmd_file", "input4:cmd_replace_dict_file", "tran_all_table", "real_mrchnt_table"],
                  "outputs": ["dataframe_lst"],
                  "extra_args": []
              },
              {
                  "name": "merge_tran_agg_to_mrchnt",
                  "func": ["data_preprocess.data_analysis", "merge_df_lst_to_main_df", ""],
                  "type": "cold",
                  "inputs": ["input6:main_df_sql_file", "dataframe_lst:df_lst", "input4:cmd_replace_dict_file"],
                  "outputs": ["tran_agg_mrchnt_df"],
                  "extra_args": []
              },
              {
                  "name": "process_tran_agg_dataframe",
                  "func": ["data_preprocess.data_analysis", "df_process_with_formula", ""],
                  "type": "cold",
                  "inputs": ["tran_agg_mrchnt_df:df", "input5:process_file"],
                  "outputs": ["tran_mrchnt_df"],
                  "extra_args": []
              },
              {
                  "name": "save_file",
                  "func": ["data_preprocess.data_analysis", "save_file", ""],
                  "type": "cold",
                  "inputs": ["tran_mrchnt_df:df"],
                  "outputs": ["output"],
                  "extra_args": []
              }
            ]

        self.pipeline2 = [
              {
                "name": "generate_valid_tran_all_table",
                "func": ["data_preprocess.sql_executor", "sql_execute_from_single_file", ""],
                "type": "cold",
                "inputs": ["input1:sql_cmd_file"],
                "outputs": ["tran_all_table"],
                "extra_args": []
              },
              {
                "name": "generate_valid_mrchnt_table",
                "func": ["data_preprocess.sql_executor", "sql_execute_from_single_file", ""],
                "type": "cold",
                "inputs": ["input2:sql_cmd_file"],
                "outputs": ["real_mrchnt_table"],
                "extra_args": []
              },
              {
                  "name": "read_data_from_impala_tran_all",
                  "func": ["data_preprocess.sql_executor", "sql_fetch_from_file", ""],
                  "type": "cold",
                  "inputs": ["input3:sql_cmd_file", "input4:cmd_replace_dict_file", "tran_all_table", "real_mrchnt_table"],
                  "outputs": ["dataframe_lst"],
                  "extra_args": []
              },
              {
                  "name": "merge_mrchnt_stat_to_mrchnt",
                  "func": ["data_preprocess.data_analysis", "merge_df_lst_to_main_df", ""],
                  "type": "cold",
                  "inputs": ["input5:main_df_sql_file", "dataframe_lst:df_lst", "input4:cmd_replace_dict_file"],
                  "outputs": ["mrchnt_stat_df"],
                  "extra_args": []
              },
              {
                  "name": "save_file",
                  "func": ["data_preprocess.data_analysis", "save_file", ""],
                  "type": "cold",
                  "inputs": ["mrchnt_stat_df:df", "input6:output"],
                  "outputs": ["output"],
                  "extra_args": []
              }
            ]

    @classmethod
    def setUpClass(cls):
        if check_thrift_sasl_version().find("0.2.1") > 0:
            cls._test_impala = True
        else:
            cls._test_impala = False
        if cls._test_impala:
            cls.db_connector_impala = DBConnector(impala_host=DARWIN_CONFIG["impala_config"]["host"],
                                            impala_port=DARWIN_CONFIG["impala_config"]["port"],
                                            hdfs_host=DARWIN_CONFIG["hdfs_config"]["host"],
                                            hdfs_port=DARWIN_CONFIG["hdfs_config"]["port"],
                                            hive_host=DARWIN_CONFIG["hive_config"]["host"],
                                            hive_port=DARWIN_CONFIG["hive_config"]["port"],
                                            user=DARWIN_CONFIG["impala_config"]["user"],
                                            passwd=DARWIN_CONFIG["impala_config"]["passwd"],
                                            database=DARWIN_CONFIG["impala_config"]["database"],
                                            use_hive=False)
            ok_(cls.db_connector_impala is not None)
        else:
            cls.db_connector_hive = DBConnector(impala_host=DARWIN_CONFIG["impala_config"]["host"],
                                                   impala_port=DARWIN_CONFIG["impala_config"]["port"],
                                                   hdfs_host=DARWIN_CONFIG["hdfs_config"]["host"],
                                                   hdfs_port=DARWIN_CONFIG["hdfs_config"]["port"],
                                                   hive_host=DARWIN_CONFIG["hive_config"]["host"],
                                                   hive_port=DARWIN_CONFIG["hive_config"]["port"],
                                                   user=DARWIN_CONFIG["impala_config"]["user"],
                                                   passwd=DARWIN_CONFIG["impala_config"]["passwd"],
                                                   database=DARWIN_CONFIG["impala_config"]["database"],
                                                   use_hive=True)
            ok_(cls.db_connector_hive is not None)
        cls.db_connector_impala.connect()
        cls.db_connector_impala.connect_database()

    def tearDown(self):
        pass

    def test_tran_list(self):
        p = PipeLine(self.pipeline1)
        task = PipeTask(p)
        logger.info("============Test suixingfu pipeline============================")
        r = task(input1='suixingfu/tran_all_table.sql',
                 input2='suixingfu/real_mrchnt_table.sql',
                 input3='suixingfu/tran_agg_mrchnt.xlsx',
                 input4='suixingfu/tran_all_repalce_dict.json',
                 input5='suixingfu/tran_agg_mrchnt_formula.xlsx',
                 input6="suixingfu/main_df.sql")
        logger.info('OK!!!')

    def test_mrchnt(self):
        p = PipeLine(self.pipeline2)
        task = PipeTask(p)
        logger.info("===========Test suixingfu pipeline=============================")
        r = task(input1='suixingfu/tran_all_table.sql',
                 input2='suixingfu/real_mrchnt_table.sql',
                 input3='suixingfu/mrchnt_stat.xlsx',
                 input4='suixingfu/tran_all_repalce_dict.json',
                 input5='suixingfu/main_df.sql',
                 input6='./output_mrchnt.csv')
        logger.info('OK!!!')